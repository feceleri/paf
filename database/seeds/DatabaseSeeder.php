<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

        $this->call('UsuariosSeeder');
        $this->call('SeccionaisSeeder');
        $this->call('EstadosSeeder');
        $this->call('CidadesSeeder');

		// Popular tabelas
        factory ( 'App\Ramo', 50 ) -> create ();
		factory ( 'App\Cargo', 50 ) -> create ();
        factory ( 'App\Tema', 5 ) -> create ();
        factory ( 'App\Categoria', 50 ) -> create ();
        factory ( 'App\Artigo', 20 ) -> create ();
		factory ( 'App\Empresa', 1500 ) -> create ();
		factory ( 'App\Parceiro', 50 ) -> create ();
		factory ( 'App\Desconto', 20 ) -> create ();
		factory ( 'App\Banner', 15 ) -> create ();
        factory ( 'App\Vaga', 2584 ) -> create ();
        factory ( 'App\Concurso', 758 ) -> create ();
	}
}
