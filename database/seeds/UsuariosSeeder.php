<?php

use App\Usuario;
use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Usuario::insert([
            'TT001CC000' => 9999, // ID
            'TT001CC001' => 'Administrador', // Nome
			'TT001CC002' => 'Admin', // Login
			'TT001CC004' => 'murillo.araujo@crfsp.org.br', // E-mail
            'TT001CC003' => bcrypt('admin@paf'), // Senha
        ]);
    }
}
