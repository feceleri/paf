<?php

use Illuminate\Database\Seeder;

class SeccionaisSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $seccionais[] = array(
            array( "TT006CC000" => "1", "TT006CC001" => "Adamantina"),
            array( "TT006CC000" => "2", "TT006CC001" => "Araçatuba"),
            array( "TT006CC000" => "3", "TT006CC001" => "Araraquara"),
            array( "TT006CC000" => "4", "TT006CC001" => "Avaré"),
            array( "TT006CC000" => "5", "TT006CC001" => "Barretos"),
            array( "TT006CC000" => "6", "TT006CC001" => "Bauru"),
            array( "TT006CC000" => "7", "TT006CC001" => "Bragança Paulista"),
            array( "TT006CC000" => "8", "TT006CC001" => "Campinas"),
            array( "TT006CC000" => "9", "TT006CC001" => "Caraguatatuba"),
            array( "TT006CC000" => "10", "TT006CC001" => "Fernadópolis"),
            array( "TT006CC000" => "11", "TT006CC001" => "Franca"),
            array( "TT006CC000" => "12", "TT006CC001" => "Guarulhos"),
            array( "TT006CC000" => "13", "TT006CC001" => "Jundiaí"),
            array( "TT006CC000" => "14", "TT006CC001" => "Marília"),
            array( "TT006CC000" => "15", "TT006CC001" => "Mogi das Cruzes"),
            array( "TT006CC000" => "16", "TT006CC001" => "Osasco"),
            array( "TT006CC000" => "17", "TT006CC001" => "Piracicaba"),
            array( "TT006CC000" => "18", "TT006CC001" => "Presidente Prudente"),
            array( "TT006CC000" => "19", "TT006CC001" => "Registro"),
            array( "TT006CC000" => "20", "TT006CC001" => "Ribeirão Preto"),
            array( "TT006CC000" => "21", "TT006CC001" => "São José do Rio Preto"),
            array( "TT006CC000" => "22", "TT006CC001" => "Santo André"),
            array( "TT006CC000" => "23", "TT006CC001" => "São João da Boa Vista"),
            array( "TT006CC000" => "24", "TT006CC001" => "Sorocaba"),
            array( "TT006CC000" => "25", "TT006CC001" => "Santos"),
            array( "TT006CC000" => "26", "TT006CC001" => "São José dos Campos"),
            array( "TT006CC000" => "27", "TT006CC001" => "Zona Sul")
        );

        foreach ( $seccionais as $seccional ) {
            \App\Seccional::insert( $seccional );
        }
    }
}
