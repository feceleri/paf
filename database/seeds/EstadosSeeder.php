<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadosSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $estados[] = array(

            array( "TT007CC000" => "AC", "TT007CC001" => "Acre"),
            array( "TT007CC000" => "AL", "TT007CC001" => "Alagoas"),
            array( "TT007CC000" => "AP", "TT007CC001" => "Amapá"),
            array( "TT007CC000" => "AM", "TT007CC001" => "Amazonas"),
            array( "TT007CC000" => "BA", "TT007CC001" => "Bahia"),
            array( "TT007CC000" => "CE", "TT007CC001" => "Ceará"),
            array( "TT007CC000" => "DF", "TT007CC001" => "Distrito Federal"),
            array( "TT007CC000" => "ES", "TT007CC001" => "Espirito Santo"),
            array( "TT007CC000" => "GO", "TT007CC001" => "Goiás"),
            array( "TT007CC000" => "MA", "TT007CC001" => "Maranhão"),
            array( "TT007CC000" => "MT", "TT007CC001" => "Mato Grosso"),
            array( "TT007CC000" => "MS", "TT007CC001" => "Mato Grosso do Sul"),
            array( "TT007CC000" => "MG", "TT007CC001" => "Minas Gerais"),
            array( "TT007CC000" => "PA", "TT007CC001" => "Pará"),
            array( "TT007CC000" => "PB", "TT007CC001" => "Paraíba"),
            array( "TT007CC000" => "PR", "TT007CC001" => "Paraná"),
            array( "TT007CC000" => "PE", "TT007CC001" => "Pernanbuco"),
            array( "TT007CC000" => "PI", "TT007CC001" => "Piauí"),
            array( "TT007CC000" => "RJ", "TT007CC001" => "Rio de Janeiro"),
            array( "TT007CC000" => "RN", "TT007CC001" => "Rio Grande do Norte"),
            array( "TT007CC000" => "RS", "TT007CC001" => "Rio Grande do Sul"),
            array( "TT007CC000" => "RO", "TT007CC001" => "Rondônia"),
            array( "TT007CC000" => "RR", "TT007CC001" => "Roraima"),
            array( "TT007CC000" => "SC", "TT007CC001" => "Santa Catarina"),
            array( "TT007CC000" => "SP", "TT007CC001" => "São Paulo"),
            array( "TT007CC000" => "SE", "TT007CC001" => "Sergipe"),
            array( "TT007CC000" => "TO", "TT007CC001" => "Tocantins")
        );

        foreach ( $estados as $estado ) {
            \App\Estado::insert( $estado );
        }
    }
}
