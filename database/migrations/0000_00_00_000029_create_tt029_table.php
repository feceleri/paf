<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt029Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT029 - Pivot Cidades | Cuncursos
     *
	 * @return void
	 */
    public function up() {

        Schema::create ('TT029', function (Blueprint $table) {

			// Indices
			$table -> integer('TT029CC001') -> unsigned(); // ID Cidade
            $table -> integer('TT029CC002' ) -> unsigned(); // ID Concurso

			// Relacionamentos
			$table -> foreign( 'TT029CC001' ) -> references( 'TT008CC000' ) -> on( 'TT008' );
            $table -> foreign( 'TT029CC002' ) -> references( 'TT015CC000' ) -> on( 'TT015' );
            $table -> timestamps();
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT029 - Pivot Cidades | Cuncursos
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT029');
    }
}
