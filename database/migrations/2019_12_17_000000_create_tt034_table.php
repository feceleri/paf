<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatebloqueiosTemporariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TT034', function (Blueprint $table) {


            // Indices
			$table -> increments ( 'TT034CC000' ) -> unsigned(); // ID
			$table -> string ( 'TT034CC001' , 50 ) ; // nome da função
            $table -> string ( 'TT034CC002' , 500 ) ; // Texto do aviso
            $table -> dateTime('TT034CC003');//data de expiração do banner
            $table -> dateTime('TT034CC004');//data de expiração do banner
            $table -> timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TT034');
    }
}
