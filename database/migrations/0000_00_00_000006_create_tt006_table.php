<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt006Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT006 - Seccionais
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT006', function (Blueprint $table) {

			// Indices
			$table -> increments ( 'TT006CC000' ) -> unsigned(); // ID

			// Campos
			$table -> string ( 'TT006CC001', 150 ); // Titulo
			$table -> timestamps ();

		} );
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT006 - Seccionais
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT006' );
	}
}
