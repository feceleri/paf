<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt030Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TT030', function (Blueprint $table) {


            // Indices
			$table -> increments ( 'TT030CC000' ) -> unsigned(); // ID
			$table -> string ( 'TT030CC001' ) ; // Nome
            $table -> integer ( 'TT030CC002' ) -> unsigned(); // ID Cidade

			// Campos
            $table -> string ( 'TT030CC003', 11 ) -> unique(); // CPF
            $table -> string ( 'TT030CC004', 11 ); // RG
            $table -> string ( 'TT030CC005', 50 ); // Nacionalidade
            $table -> string ( 'TT030CC006', 100 ); // Estado Civil
            $table -> string ( 'TT030CC007', 11 ); // N. CRF
            $table -> string ( 'TT030CC008', 250 ); // Endereço completo
            $table -> string ( 'TT030CC009', 150 ); // Bairro
			$table -> string ( 'TT030CC010', 9 ); // CEP
			$table -> string ( 'TT030CC011', 150 ); // E-mail
			$table -> string ( 'TT030CC012', 15 ) -> nullable(); // Telefone
            $table -> string ( 'TT030CC013', 15 ) -> nullable(); // Celular
            $table -> boolean ( 'TT030CC014' ) -> default ( true ); // Ativo

            $table -> string ( 'TT030CC015', 250 ); // Titulo especialização 1
            $table -> string ( 'TT030CC016', 250 ); // Titulo instituição 1
            $table -> string ( 'TT030CC017', 50 ); // carga especialização 1
            $table -> string ( 'TT030CC018', 4); // conclusão 1

            $table -> string ( 'TT030CC019', 250 ) -> nullable(); // Titulo especialização 2
            $table -> string ( 'TT030CC020', 250 ) -> nullable(); // Titulo instituição 2
            $table -> string ( 'TT030CC021', 50 ) -> nullable(); // carga especialização 2
            $table -> string ( 'TT030CC022', 4) -> nullable(); // conclusão 2

            $table -> string ( 'TT030CC023', 250 ) -> nullable(); // Titulo especialização 3
            $table -> string ( 'TT030CC024', 250 ) -> nullable(); // Titulo instituição 3
            $table -> string ( 'TT030CC025', 50 ) -> nullable(); // carga especialização 3
            $table -> string ( 'TT030CC026', 4 ) -> nullable(); // conclusão 3

            $table -> string ( 'TT030CC027', 500 ) -> nullable(); // Descrição de Atividades


            $table -> timestamps();


			// Relacionamentos
		    $table -> foreign ( 'TT030CC002' ) -> references ( 'TT008CC000' ) -> on ( 'TT008' );



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TT030');
    }
}
