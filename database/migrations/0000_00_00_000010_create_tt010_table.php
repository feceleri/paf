<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt010Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT010 - Empresa
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT010', function (Blueprint $table) {

			// Indices
			$table -> increments ( 'TT010CC000' ) -> unsigned(); // ID
			$table -> integer ( 'TT010CC001' ) -> unsigned(); // ID Ramo
            $table -> integer ( 'TT010CC002' ) -> unsigned(); // ID Cidade

			// Campos
            $table -> string ( 'TT010CC003', 14 ) -> unique(); // CNPJ
            $table -> string ( 'TT010CC004', 150 ); // Razão social
			$table -> string ( 'TT010CC005', 150 ); // Nome fantasia
			$table -> string ( 'TT010CC006', 8 ) -> nullable(); // CEP
			$table -> string ( 'TT010CC007', 150 ) -> nullable(); // Endereço
            $table -> string ( 'TT010CC008', 150 ) -> nullable(); // Bairro
			$table -> string ( 'TT010CC009', 11 ) -> nullable(); // Telefone
			$table -> string ( 'TT010CC010', 150 ) -> nullable(); // E-mail
			$table -> string ( 'TT010CC011', 150 ) -> nullable(); // Link
			$table -> string ( 'TT010CC012' ) -> nullable(); // Observação
            $table -> string ( 'TT010CC013', 50 ) -> nullable(); // Senha
			$table -> timestamps();

			// Relacionamentos
			$table -> foreign ( 'TT010CC001' ) -> references ( 'TT002CC000' ) -> on ( 'TT002' );
            $table -> foreign ( 'TT010CC002' ) -> references ( 'TT008CC000' ) -> on ( 'TT008' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT010 - Empresa
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT010' );
	}
}
