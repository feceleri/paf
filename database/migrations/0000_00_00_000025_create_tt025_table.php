<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt025Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT025 - Experiências
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT025', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT025CC000' ) -> unsigned(); // ID
			$table -> integer( 'TT025CC001' ) -> unsigned(); // ID Currículo
            $table -> integer( 'TT025CC002' ) -> unsigned(); // ID Ramo
            $table -> integer( 'TT025CC003' ) -> unsigned(); // ID Cargo

			// Campos
			$table -> string( 'TT025CC004', 150 ); // Empresa
            $table -> date( 'TT025CC005' ); // Data de Inicio
            $table -> date( 'TT025CC006' ) -> nullable(); // Data de Encerramento
            $table -> boolean ( 'TT025CC007' ) -> default ( false ); // False = Não trabalha mais no local/ True = trabalha
            $table -> string ( 'TT025CC008', 250 ) -> nullable (); // Descritivo
			$table -> timestamps ();

			// Relacionamentos
			$table -> foreign( 'TT025CC001' ) -> references( 'TT021CC000' ) -> on( 'TT021' );
            $table -> foreign( 'TT025CC002' ) -> references( 'TT012CC000' ) -> on( 'TT012' );
            $table -> foreign( 'TT025CC003' ) -> references( 'TT003CC000' ) -> on( 'TT003' );
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT025 - Experiências
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT025');
    }
}
