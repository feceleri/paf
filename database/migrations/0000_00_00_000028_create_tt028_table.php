<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt028Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT028 - Pivot Estados | Cuncursos
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT028', function (Blueprint $table) {

			// Indices
			$table -> string( 'TT028CC001', 2 ); // ID Estado
            $table -> integer( 'TT028CC002' ) -> unsigned(); // ID Concurso

			// Relacionamentos
			$table -> foreign( 'TT028CC001' ) -> references( 'TT007CC000' ) -> on( 'TT007' );
            $table -> foreign( 'TT028CC002' ) -> references( 'TT015CC000' ) -> on( 'TT015' );
            $table -> timestamps();
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT028 - Pivot Estados | Cuncursos
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT028');
    }
}
