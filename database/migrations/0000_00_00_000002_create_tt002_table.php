<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt002Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT002 - Ramos
     *
	 * @return void
	 */
	public function up() {
		Schema::create( 'TT002', function( Blueprint $table ) {

			// Indices
			$table -> increments( 'TT002CC000' ) -> unsigned(); // ID

			// Campos
			$table -> string( 'TT002CC001', 150 ); // Titulo
			$table -> timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT002 - Ramos
     *
	 * @return void
	 */
	public function down() {
		Schema::drop( 'TT002' );
	}
}
