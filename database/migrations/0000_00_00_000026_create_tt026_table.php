<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt026Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT026 - Idiomas
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT026', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT026CC000' ) -> unsigned(); // ID
			$table -> integer( 'TT026CC001' ) -> unsigned(); // ID Currículo

			// Campos
			$table -> string( 'TT026CC002', 150 ); // Idioma
            $table -> string( 'TT026CC003', 150 ); // Instituição
            $table -> date( 'TT026CC004' ); // Data de Inicio
            $table -> date( 'TT026CC005' ) -> nullable(); // Data de Encerramento
            $table -> enum( 'TT026CC006', [ 'BAS','INT', 'AVA', 'FLU' ]); // Nível ('Básico','Intermediário', 'Avançado', 'Fluente')
            $table -> string( 'TT026CC007', 150 ); // País

			$table -> timestamps();

			// Relacionamentos
			$table -> foreign( 'TT026CC001' ) -> references( 'TT021CC000' ) -> on( 'TT021' );
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT026 - Idiomas
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT026');
    }
}
