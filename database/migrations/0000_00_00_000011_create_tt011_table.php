<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt011Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT011 - Parceiros
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT011', function (Blueprint $table) {

			// Indices
			$table -> increments ( 'TT011CC000' ) -> unsigned(); // ID

			// Campos
            $table -> string ( 'TT011CC001', 150 ); // Nome
            $table -> enum ( 'TT011CC002', [ 'CB','BE' ]); // tipo
			$table -> string ( 'TT011CC003', 150 ); // Imagem
            $table -> string ( 'TT011CC004', 150 ); // Site
            $table -> boolean ( 'TT011CC005' ) -> default ( false ); // Ativo
			$table -> timestamps ();
		});
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT011 - Parceiros
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT011' );
	}
}
