<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt013Table extends Migration
{
    /**
	 * Run the migrations.
	 *
     * Create table TT013 - Banners
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT013', function (Blueprint $table) {

			// Indices
			$table -> increments ( 'TT013CC000' ) -> unsigned(); // ID
			$table -> integer ( 'TT013CC001' ) -> unsigned() -> nullable(); // ID Parceiro

			// Campos
            $table -> string ( 'TT013CC002', 150 ); // Título
            $table -> string ( 'TT013CC003', 150 ); // Link
            $table -> integer ( 'TT013CC004' ) -> nullable (); // Ordem
			$table -> string ( 'TT013CC005', 150 ); // Imagem
			$table -> string ( 'TT013CC006', 250 ) -> nullable (); // Descritivo
			$table -> boolean ( 'TT013CC007' ) -> default ( false ); // Ativo
            $table -> dateTime('TT013CC008')-> nullable();//data de expiração do banner
			$table -> timestamps ();

			// Relacionamentos
			$table -> foreign ( 'TT013CC001' ) -> references ( 'TT011CC000' ) -> on ( 'TT011' );
		});
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT013 - Banners
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT013' );
	}
}
