<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt024Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT024 - Deficiências
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT024', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT024CC000' ) -> unsigned(); // ID
			$table -> integer( 'TT024CC001' ) -> unsigned(); // ID Currículo

			// Campos
            $table -> enum( 'TT024CC002', [ 'MOT', 'INT', 'AUD', 'VIS' ]); // Deficiência ( 'Motora', 'Intelectual', 'Auditiva', 'Visual' )
			$table -> string( 'TT024CC003', 250 ); // Detalhes
			$table -> timestamps ();

			// Relacionamentos
			$table -> foreign( 'TT024CC001' ) -> references( 'TT021CC000' ) -> on( 'TT021' );
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT024 - Deficiências
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists ('TT024' );
    }
}
