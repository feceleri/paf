<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt005Table extends Migration {
    /**
	 * Run the migrations.
	 *
     * Create table TT005 - Categorias
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT005', function (Blueprint $table) {

			// Indices
			$table -> increments ( 'TT005CC000' ) -> unsigned(); // ID

			// Campos
			$table -> string ( 'TT005CC001', 150 ); // Titulo
			$table -> timestamps ();

		} );
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT005 - Categorias
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT005' );
	}
}
