<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt032Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT032 - Cursos e Participações
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT032', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT032CC000' ) -> unsigned(); // ID
			$table -> integer( 'TT032CC001' ) -> unsigned(); // ID Currículo

			// Campos
            $table -> string( 'TT032CC002', 150 ); // Curso
            $table -> enum( 'TT032CC003', [ 'CU','PA','CO' ]);  // Tipo
            $table -> string( 'TT032CC004', 150 ); // Instituição
            $table -> string( 'TT032CC005', 150 ); // País
            $table -> date( 'TT032CC006' ); // Data de Inicio
            $table -> date( 'TT032CC007' ) -> nullable(); // Data de Encerramento
			$table -> timestamps();

			// Relacionamentos
			$table -> foreign( 'TT032CC001' ) -> references( 'TT021CC000' ) -> on( 'TT021' );
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT032 - Formações
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT032');
    }
}
