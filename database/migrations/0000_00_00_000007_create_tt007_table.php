<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt007Table extends Migration
{
    /**
	 * Run the migrations.
	 *
     * Create table TT007 - Estados
     *
	 * @return void
	 */
    public function up() {
        Schema::create ( 'TT007', function (Blueprint $table) {

			// Indices
			$table -> string( 'TT007CC000', 2 ) -> primary(); // ID

			// Campos
			$table -> string( 'TT007CC001', 150 ); // Nome
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT007 - Estados
     *
	 * @return void
	 */
    public function down() {
        Schema::drop ( 'TT007' );
    }
}
