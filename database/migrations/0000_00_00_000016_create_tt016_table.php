<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt016Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT016 - Tipos de Documentos
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT016', function ( Blueprint $table ) {

			// Indices
			$table -> increments ( 'TT016CC000' ) -> unsigned(); // ID

			// Campos
			$table -> string ( 'TT016CC001', 150 ); // Título
			$table -> timestamps ();

		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT016 - Tipos de Documentos
     *
	 * @return void
	 */
    public function down() {
        Schema::drop ( 'TT016' );
    }
}
