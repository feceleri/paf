<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt015Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT015 - Concursos
     *
	 * @return void
	 */
	public function up() {
		Schema::create('TT015', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT015CC000' ) -> unsigned(); // ID

			// Campos
            $table -> string( 'TT015CC001', 150 ); // Título
            $table -> string( 'TT015CC002', 150 ) -> nullable(); // Link
            $table -> string( 'TT015CC003', 150 ); // Instituição
			$table -> string( 'TT015CC004', 150 ); // Organizadora
            $table -> text('TT015CC005'); // Descritivo
            $table -> text( 'TT015CC006'); // Requisitos
            $table -> text('TT015CC007') -> nullable(); // Informações Adicionais
            $table -> date( 'TT015CC008' ); // Data Inicial de Inscrição
            $table -> date( 'TT015CC009' ); // Data Final de Inscrição
            $table -> boolean( 'TT015CC010' ) -> default( false ); // Ativo
            $table -> boolean( 'TT015CC011' ) -> default( false ); // Abrangência Nacional

            $table->string('TT015CC012')->nullable();
            $table->string('TT015CC013')->nullable();

			$table -> timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT015 - Concursos
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT015' );
	}
}
