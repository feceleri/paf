<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt033Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT014 - Vagas
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT033', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT033CC000' ) -> unsigned(); // ID
			$table -> integer( 'TT033CC001' ) -> unsigned(); // ID Vaga
			$table -> enum( 'TT033CC002', [ 'SI','NO','AD','NP' ]) ; // sim, não, admin mudou, não preenchida
            $table -> dateTime('TT033CC003')-> nullable();//data de expiração do banner
            $table -> smallInteger( 'TT033CC004' ) -> unsigned(); // Número de Vagas
            $table -> smallInteger( 'TT033CC005' ) -> unsigned(); // Número de preenchidas pelo paf

            $table -> timestamps();

			// Relacionamentos
			$table -> foreign( 'TT033CC001' ) -> references( 'TT014CC000' ) -> on( 'TT014' );

		});
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT014 - Vagas
     *
	 * @return void
	 */
	public function down() {
		Schema::drop( 'TT033' );
	}
}
