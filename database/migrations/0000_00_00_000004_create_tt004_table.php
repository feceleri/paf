<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt004Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT004 - Temas
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT004', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT004CC000' ) -> unsigned(); // ID

			// Campos
			$table -> string ( 'TT004CC001', 7 ); // Cor
			$table -> string ( 'TT004CC002', 150 ); // Titulo
			$table -> timestamps ();

		} );
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT004 - Temas
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT004' );
	}
}
