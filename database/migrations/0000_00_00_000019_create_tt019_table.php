<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt019Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT019 - Pivot Estados | Descontos
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT019', function (Blueprint $table) {

			// Indices
			$table -> string( 'TT019CC001', 2 ); // ID Estado
            $table -> integer( 'TT019CC002' ) -> unsigned(); // ID Desconto

			// Relacionamentos
			$table -> foreign( 'TT019CC001' ) -> references( 'TT007CC000' ) -> on( 'TT007' );
            $table -> foreign( 'TT019CC002' ) -> references( 'TT012CC000' ) -> on( 'TT012' );
            $table -> timestamps();
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT019 - Pivot Estados | Descontos
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT019');
    }
}
