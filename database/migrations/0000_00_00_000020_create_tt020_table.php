<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt020Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT020 - Pivot Cidades | Descontos
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT020', function (Blueprint $table) {

			// Indices
			$table -> integer( 'TT020CC001' ) -> unsigned(); // ID Cidade
            $table -> integer( 'TT020CC002' ) -> unsigned(); // ID Desconto

			// Relacionamentos
			$table -> foreign( 'TT020CC001' ) -> references( 'TT008CC000' ) -> on( 'TT008' );
            $table -> foreign( 'TT020CC002' ) -> references( 'TT012CC000' ) -> on( 'TT012' );
            $table -> timestamps();
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT020 - Pivot Cidades | Descontos
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT020');
    }
}
