<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt031Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT031 - Linguas
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT031', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT031CC000' ) -> unsigned(); // ID

			// Campos
			$table -> string( 'TT031CC001', 150 ); // Titulo
			$table -> timestamps();

		} );
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT031 - Linguas
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT031' );
	}
}
