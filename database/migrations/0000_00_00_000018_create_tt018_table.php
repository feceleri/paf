<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt018Table extends Migration {

   /**
	 * Run the migrations.
	 *
     * Create table TT018 - Pivot Parceiros | Documentos
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT018', function (Blueprint $table) {

			// Indices
			$table -> integer ( 'TT018CC001' ) -> unsigned (); // ID Parceiro
            $table -> integer ( 'TT018CC002' ) -> unsigned (); // ID Documento

			// Relacionamentos
			$table -> foreign ( 'TT018CC001' ) -> references ( 'TT011CC000' ) -> on ( 'TT011' );
            $table -> foreign ( 'TT018CC002' ) -> references ( 'TT017CC000' ) -> on ( 'TT017' );
            $table -> timestamps();
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT018 - Pivot Parceiros | Documentos
     *
	 * @return void
	 */
    public function down() {
        Schema::drop ( 'TT018' );
    }
}
