<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt017Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT017 - Documentos
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT017', function (Blueprint $table) {

			// Indices
			$table -> increments ( 'TT017CC000' ) -> unsigned(); // ID
			$table -> integer ( 'TT017CC001' ) -> unsigned (); // ID Tipo de Documento

			// Campos
			$table -> string ( 'TT017CC002', 150 ); // nome
			$table -> timestamps ();

			// Relacionamentos
			$table -> foreign ( 'TT017CC001' ) -> references ( 'TT016CC000' ) -> on ( 'TT016' );
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT017 - Documentos
     *
	 * @return void
	 */
    public function down() {
        Schema::drop ( 'TT017' );
    }
}
