<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt001Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT001 - Usuarios
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT001', function (Blueprint $table) {

			// Indices
			$table -> increments ( 'TT001CC000' ) -> unsigned(); // ID

			// Campos
			$table -> string ( 'TT001CC001', 150 ); // Nome
			$table -> string ( 'TT001CC002', 50 ); // Login
			$table -> string ( 'TT001CC003', 250 ); // Senha
            $table -> string ( 'TT001CC004', 150 ) -> unique(); // Email
            $table -> boolean ( 'TT001CC005' ) -> default ( false ); // Ativo
            $table -> integer ('role_id');
			$table -> timestamps ();
            $table -> rememberToken();
            $table -> string ( 'TT001CC006', 15 ) -> unique(); // CNPJ ou CPF

		} );
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT001 - Usuarios
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT001' );
	}
}
