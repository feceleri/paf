<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt023Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT023 - Formações
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT023', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT023CC000' ) -> unsigned(); // ID
			$table -> integer( 'TT023CC001' ) -> unsigned(); // ID Currículo

			// Campos
            $table -> string( 'TT023CC002', 150 ); // Curso
            $table -> enum( 'TT023CC003', [ 'TC','BA','LI','TN','PL','ME','DO', 'PD' ]);  // Nível Escolar 18
            $table -> string( 'TT023CC004', 150 ); // Instituição
            $table -> string( 'TT023CC005', 150 ); // País
            $table -> date( 'TT023CC006' ); // Data de Inicio
            $table -> date( 'TT023CC007' ) -> nullable(); // Data de Encerramento
			$table -> timestamps();

			// Relacionamentos
			$table -> foreign( 'TT023CC001' ) -> references( 'TT021CC000' ) -> on( 'TT021' );
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT023 - Formações
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT023');
    }
}
