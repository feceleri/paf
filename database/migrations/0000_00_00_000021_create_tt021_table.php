<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt021Table extends Migration {

   /**
	 * Run the migrations.
	 *
     * Create table TT021 - Currículos
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT021', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT021CC000' ) -> unsigned(); // ID
            $table -> integer( 'TT021CC001' ) -> unsigned(); // ID Usuário
			$table -> integer( 'TT021CC002' ) -> unsigned(); // ID Cidade

			// Campos


            $table -> string( 'TT021CC003', 8 ) -> nullable(); // CEP 7
            $table -> string( 'TT021CC004', 150 ) -> nullable(); // Endereço 8
            $table -> string( 'TT021CC005', 11 ) -> nullable(); // Telefone 11
            $table -> string( 'TT021CC006', 150 ) -> nullable(); // Linkedin 13
            $table -> date( 'TT021CC007' ) -> nullable(); // Data de Nascimento 15
            $table -> string( 'TT021CC008', 150 ) -> nullable(); // Estado Civil 16
            $table -> string( 'TT021CC009', 150 ) -> nullable(); // Nacionalidade 17
            $table -> enum( 'TT021CC010', [ 'MI','MC','SI','SC','PO','ME','DO','PD' ]);  // Nível Escolar 18
            $table -> boolean( 'TT021CC011' ) -> default( true ); // Visível 19
            $table -> boolean( 'TT021CC012' ) -> default( true ); // Enviar Alertas 20
            $table -> enum( 'TT021CC013', [ 'M','F' ]); // Sexo ('Masculino','feminino') 21
            $table -> dateTime('TT021CC014')-> nullable();//publicado em
			$table -> timestamps ();

			// Relacionamentos
			$table -> foreign( 'TT021CC001' ) -> references( 'TT001CC000' ) -> on( 'TT001' ); //usuário
            $table -> foreign( 'TT021CC002' ) -> references( 'TT008CC000' ) -> on( 'TT008' ); //cidade
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT021 - Currículos
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT021');
    }
}
