<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt009Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT009 - Artigos
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT009', function( Blueprint $table ) {

			// Indices
			$table -> increments ( 'TT009CC000' ) -> unsigned(); // ID
			$table -> integer ( 'TT009CC001' ) -> unsigned(); // ID Tema

			// Campos
			$table -> string ( 'TT009CC002', 150 ); // Título
			$table -> string ( 'TT009CC003', 150 ) -> nullable(); // Imagem
			$table -> text ( 'TT009CC004'); // Descritivo
            $table -> boolean ( 'TT009CC005' ) -> default ( false ); // Ativo
			$table -> timestamps ();

			// Relacionamentos
			$table -> foreign( 'TT009CC001' ) -> references( 'TT004CC000' ) -> on( 'TT004' );
		} );
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT009 - Artigos
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT009' );
	}
}
