<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt012Table extends Migration {
    /**
	 * Run the migrations.
	 *
     * Create table TT012 - Descontos
     *
	 * @return void
	 */
	public function up() {
		Schema::create( 'TT012', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT012CC000' ) -> unsigned(); // ID
			$table -> integer( 'TT012CC001' ) -> unsigned(); // ID Parceiro
            $table -> integer( 'TT012CC002' ) -> unsigned(); // ID Categoria

			// Campos
			$table -> string( 'TT012CC003', 150 ); // Título
			$table -> string( 'TT012CC004', 150 ); // Imagem
			$table -> text( 'TT012CC005' ); // Descritivo
			$table -> boolean( 'TT012CC006' ) -> default( false ); // Ativo
            $table -> enum( 'TT012CC007', [ 'PF','PJ', 'GG' ]); // Benefíciarios ('Pessoa Fisica', 'Pessoa Jurídica', 'Geral')
            $table -> boolean( 'TT012CC008' ) -> default( false ); // Abrangência Nacional
			$table -> timestamps();

			// Relacionamentos
			$table -> foreign( 'TT012CC001' ) -> references( 'TT011CC000' ) -> on( 'TT011' );
            $table -> foreign( 'TT012CC002' ) -> references( 'TT005CC000' ) -> on( 'TT005' );
		});
	}

	/**
	 * Reverse the migrations.
	 *
     * Create table TT012 - Descontos
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT012' );
	}
}
