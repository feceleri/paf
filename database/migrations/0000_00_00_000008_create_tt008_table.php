<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt008Table extends Migration {

    /**
     * Run the migrations.
     *
     * Create table TT008 - Cidades
     *
     * @return void
     */
    public function up() {
        Schema::create ( 'TT008', function (Blueprint $table) {

            // Indices
            $table -> increments ( 'TT008CC000' ) -> unsigned(); // ID
            $table -> string ( 'TT008CC001', 2 ); // UF
            $table -> integer ( 'TT008CC002' ) -> unsigned() -> nullable(); // idSeccional

            // Campos
            $table -> string ( 'TT008CC003', 150 ); // Nome

            // Relacionamentos
            $table -> foreign ( 'TT008CC002' ) -> references ( 'TT006CC000' ) -> on ( 'TT006' );
            $table -> foreign ( 'TT008CC001' ) -> references ( 'TT007CC000' ) -> on ( 'TT007' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * Drop table TT008 - Cidades
     *
     * @return void
     */
    public function down() {
        Schema::drop ( 'TT008' );
    }
}
