<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt014Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT014 - Vagas
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT014', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT014CC000' ) -> unsigned(); // ID
			$table -> integer( 'TT014CC001' ) -> unsigned(); // ID Ramo
			$table -> integer( 'TT014CC002' ) -> unsigned(); // ID Cargo
            $table -> integer( 'TT014CC003' ) -> unsigned(); // ID Cidade
            $table -> integer( 'TT014CC004' ) -> unsigned(); // ID Empresa

			// Campos
            $table -> string( 'TT014CC005', 150 ) -> nullable(); // Título
            $table -> string( 'TT014CC006', 150 ) -> nullable(); // E-mail
            $table -> string( 'TT014CC007', 150 ) -> nullable(); // Contato
            $table -> string( 'TT014CC008', 11 ) -> nullable(); // Telefone
			$table -> text( 'TT014CC009' ) -> nullable(); // Descrição das Atividades
            $table -> text( 'TT014CC010' ) -> nullable(); // Requisitos
			$table -> text( 'TT014CC011' ) -> nullable(); // Benefícios
			$table -> text( 'TT014CC012' ) -> nullable(); // Instruções
			$table -> smallInteger( 'TT014CC013' ) -> unsigned(); // Número de Vagas
			$table -> decimal( 'TT014CC014', 8, 2 ); // Remuneração
			$table -> string( 'TT014CC015', 150 ) -> nullable(); // Carga Horária
            $table -> boolean( 'TT014CC016' ) -> default( false ); // Ativo
            $table -> boolean( 'TT014CC017' ) -> default( false ); // Mostrar Endereço
            $table -> enum( 'TT014CC018', [ 'EM','ES' ]) -> default( 'EM' ); // tipo ('Emprego','Estágio')
            $table -> dateTime('TT014CC019')-> nullable();//data de expiração do banner
            $table -> enum( 'TT014CC020', [ 'NO','DE', 'IN', 'AN' ]) -> default( 'NO' ); // tipo ("Novo", “Deferido”, “Indeferido”, “Em análise” )
            $table->string('TT014CC021')->nullable();
            $table->  enum( 'TT014CC022', [ 'HO','ME' ]) -> default( 'ME' ); // tipo de remuneração - por hora ou mensal
			$table -> timestamps();

			// Relacionamentos
			$table -> foreign( 'TT014CC001' ) -> references( 'TT002CC000' ) -> on( 'TT002' );
			$table -> foreign( 'TT014CC002' ) -> references( 'TT003CC000' ) -> on( 'TT003' );
            $table -> foreign( 'TT014CC003' ) -> references( 'TT008CC000' ) -> on( 'TT008' );
            $table -> foreign( 'TT014CC004' ) -> references( 'TT010CC000' ) -> on( 'TT010' );
		});
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT014 - Vagas
     *
	 * @return void
	 */
	public function down() {
		Schema::drop( 'TT014' );
	}
}
