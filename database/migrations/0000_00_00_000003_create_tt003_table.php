<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt003Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT003 - Cargos
     *
	 * @return void
	 */
	public function up() {
		Schema::create ( 'TT003', function (Blueprint $table) {

			// Indicesq
			$table -> increments( 'TT003CC000' ) -> unsigned(); // ID

			// Campos
			$table -> string( 'TT003CC001', 150 ); // Titulo
            $table -> boolean ( 'TT003CC002' ) -> default ( true ); // Ativo
            $table -> boolean ( 'TT003CC003' ) -> default ( true ); // Ativo
			$table -> timestamps();

		} );
	}

	/**
	 * Reverse the migrations.
	 *
     * Drop table TT003 - Cargos
     *
	 * @return void
	 */
	public function down() {
		Schema::drop ( 'TT003' );
	}
}
