<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt027Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT027 - Pivot Interesses | Currículos
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT027', function (Blueprint $table) {

			// Indices
			$table -> integer( 'TT027CC001' ) -> unsigned(); // ID Cargo
            $table -> integer( 'TT027CC002' ) -> unsigned(); // ID Currículo

			// Relacionamentos
			$table -> foreign( 'TT027CC001' ) -> references( 'TT003CC000' ) -> on( 'TT003' );
            $table -> foreign( 'TT027CC002' ) -> references( 'TT021CC000' ) -> on( 'TT021' );
            $table -> timestamps();
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT027 - Pivot Interesses | Currículos
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT027');
    }
}
