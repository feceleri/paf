<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTt022Table extends Migration {

    /**
	 * Run the migrations.
	 *
     * Create table TT022 - Atividades Extra Curriculares
     *
	 * @return void
	 */
    public function up() {

        Schema::create ( 'TT022', function (Blueprint $table) {

			// Indices
			$table -> increments( 'TT022CC000' ) -> unsigned(); // ID
			$table -> integer( 'TT022CC001' ) -> unsigned(); // ID Currículo

			// Campos
            $table -> string( 'TT022CC002', 150 ); // Título
			$table -> string( 'TT022CC003', 150 ); // Tipo
            $table -> string( 'TT022CC004', 150 ); // Instituição
            $table -> integer( 'TT022CC005' ); // Carga Horaria
            $table -> date( 'TT022CC006' ); // Data de Inicio
            $table -> date( 'TT022CC007' ) -> nullable(); // Data de Encerramento
			$table -> timestamps ();

			// Relacionamentos
			$table -> foreign( 'TT022CC001' ) -> references( 'TT021CC000' ) -> on( 'TT021' );
		});
    }

    /**
	 * Reverse the migrations.
	 *
     * Drop table TT022 - Atividades Extra Curriculares
     *
	 * @return void
	 */
    public function down() {
        Schema::dropIfExists('TT022');
    }
}
