<?php

/*
 * |--------------------------------------------------------------------------
 * | Model Factories
 * |--------------------------------------------------------------------------
 * |
 * | Here you may define all of your model factories. Model factories give
 * | you a convenient way to create models for testing and seeding your
 * | database. Just tell the factory how a default model should look.
 * |
 */

/**
 * Ramos
 */
$factory -> define ( App\Ramo::class, function (Faker\Generator $faker) {

	return [

            // Título
			'TT002CC001' => $faker -> word
	];
});

/**
 * Cargos
 */
$factory -> define ( App\Cargo::class, function (Faker\Generator $faker) {

	return [

            // Título
			'TT003CC001' => $faker -> jobTitle
	];
});

/**
 * Temas
 */
$factory -> define ( App\Tema::class, function (Faker\Generator $faker) {

	return [

            'TT004CC001' => $faker -> hexColor, // Cor
			'TT004CC002' => $faker -> word // Título
	];
});

/**
 * Categorias
 */
$factory -> define ( App\Categoria::class, function (Faker\Generator $faker) {

	return [

            // Título
			'TT005CC001' => $faker -> word
	];
});

/**
 * Artigos
 */
$factory -> define ( App\Artigo::class, function (Faker\Generator $faker) {

	return [

			'TT009CC001' => rand ( 1, 5 ), // ID Tema
			'TT009CC002' => $faker -> jobTitle, // Título
            'TT009CC003' => ('img/artigos/' . rand( 1, 10 ) . '.jpg'), // Imagem
			'TT009CC004' => $faker -> text ( 3000 ), // Descritivo
            'TT009CC005' => true // Ativo
	];
});

/**
 * Empresas
 */
$factory -> define ( App\Empresa::class, function (Faker\Generator $faker) {

	return [

			'TT010CC001' => rand ( 1, 50 ), // ID Ramo
            'TT010CC002' => rand ( 1, 10 ), // ID Cidade
            'TT010CC003' => rand ( 11111111111111, 99999999999999), // CNPJ
			'TT010CC004' => $faker -> company, // Razão Social
            'TT010CC005' => $faker -> companySuffix, // Nome Fantasia
            'TT010CC006' => '54125484', // CEP
            'TT010CC007' => $faker -> streetAddress, // Endereço
            'TT010CC008' => $faker -> streetName, // Bairro
            'TT010CC009' => '11991222582', // Telefone
            'TT010CC010' => $faker -> email, // E-mail
			'TT010CC011' => 'www.faker.com.br', // Link
			'TT010CC012' => $faker -> text // Observação
	];
});

/**
 * Parceiros
 */
$factory -> define ( App\Parceiro::class, function (Faker\Generator $faker) {

    $tipo = ['CB', 'BE'];

	return [

            'TT011CC001'  => $faker -> company,  // Nome
            'TT011CC002' => $tipo[ rand( 0, 1 )], // Tipo
			'TT011CC003' => ('img/parceiros/' . rand( 1, 50 ) . '.jpg'), // Imagem
            'TT011CC004' => 'www.faker.com.br', // Link
            'TT011CC005' => true // Ativo
	];
});

/**
 * Descontos
 */
$factory -> define ( App\Desconto::class, function (Faker\Generator $faker) {

	return [

			'TT012CC001' => rand ( 1, 50 ), // ID Parceiro
            'TT012CC002' => rand ( 1, 50 ), // ID Categoria
			'TT012CC003' => $faker -> text ( 50 ), // Título
            'TT012CC004' => ('img/descontos/' . rand( 1, 10 ) . '.png'), // Imagem
            'TT012CC005' => $faker -> text ( 2000 ), // Descritivo
            'TT012CC006' => true, // Ativo
            'TT012CC007' => 'PF', // Benefíciarios
            'TT012CC008' => false, // Abrangência
	];
});

/**
 * Banners
 */
$factory -> define ( App\Banner::class, function (Faker\Generator $faker) {

	return [

			'TT013CC001' => rand ( 1, 50 ), // ID Parceiro
			'TT013CC002' => $faker -> word, // Título
            'TT013CC003' => 'www.faker.com.br', // Link
            'TT013CC004' => 1, // Ordem
            'TT013CC005' => ('img/banners/' . rand( 1, 15 ) . '.jpg'), // Imagem
            'TT013CC006' => $faker -> text ( 250 ), // Descritivo
            'TT013CC007' => true // Ativo,
            'TT013CC008' => $faker -> dateTime ( $max = 'now' ), // Data Final de publicação
    ];
});

/**
 * Vagas
 */
$factory -> define ( App\Vaga::class, function (Faker\Generator $faker) {

	return [

            'TT014CC001' => rand ( 1, 50 ), // ID Ramo
            'TT014CC002' => rand ( 1, 50 ), // ID Cargo
            'TT014CC003' => rand ( 1, 10 ), // ID Cidade
			'TT014CC004' => rand ( 1, 1500 ), // ID Empresa
			'TT014CC005' => 'Farmacêutico Responsável', // Título
            'TT014CC006' => $faker -> email, // E-mail
			'TT014CC007' => $faker -> name, // Contato
			'TT014CC008' => '55846957', // Telefone
            'TT014CC009' => $faker -> text, // Descritivo
            'TT014CC010' => $faker -> text, // Requisitos
            'TT014CC011' => $faker -> text, // Descontos
			'TT014CC012' => $faker -> text, // Instruções
            'TT014CC013' => rand ( 1, 4 ), // Número de Vagas
			'TT014CC014' => rand ( 4000, 6000 ), // Remuneração
			'TT014CC015' => (rand ( 6, 8 ) . ' horas por dia de seg. a sex.'), // Horário de Serviço
			'TT014CC016' => true // Ativo
	];
});

/**
 * Concursos
 */
$factory -> define ( App\Concurso::class, function (Faker\Generator $faker) {

	return [

            'TT015CC001' => $faker -> word, // Título
            'TT015CC002' => 'www.faker.com.br', // Link
            'TT015CC003' => $faker -> company, // Instituição
            'TT015CC004' => $faker -> company, // Organizadora
            'TT015CC005' => $faker -> text, // Descritivo
            'TT015CC006' => $faker -> text, // Requisitos
            'TT015CC007' => $faker -> text, // Informações Adicionais
            'TT015CC008' => $faker -> dateTime ( $max = 'now' ), // Data Inicial de Inscrição
            'TT015CC009' => $faker -> dateTime ( $max = 'now' ), // Data Final de Inscrição
            'TT015CC010' => true // Ativo
	];
});
