var elixir = require('laravel-elixir');
/*
 * |-------------------------------------------------------------------------- |
 * Elixir Asset Management
 * |-------------------------------------------------------------------------- |
 * Elixir provides a clean, fluent API for defining some basic Gulp tasks for
 * your Laravel application. By default, we are compiling the Sass file for
 * our application, as well as publishing vendor resources.
 */
elixir(function (mix) {
    mix.sass(['app.scss'], 'resources/assets/css/app.css');
    mix.sass(['admin.scss'], 'resources/assets/css/admin.css');
    mix.styles(['app.css']);
    mix.styles(['admin.css'], 'public/css/admin.css', 'resources/assets/css');
    mix.scripts(['jquery-2.2.4.js', 'carousel.js', 'carousel-custom.js', 'mask.js', 'mask-custom.js', 'paginate-responsive.js', 'paginate-ajax.js', 'cidades.js','cidades-custom.js', 'home.js', 'script.js', 'tether.js', 'bootstrap.js']);
    mix.scripts(['jquery-2.2.4.js', 'mask.js', 'mask-custom.js', 'files.js', 'paginate-responsive.js', 'paginate-ajax.js', 'cidades.js',  'cidades-custom.js', 'admin.js', 'tether.js', 'bootstrap.js'], 'public/js/admin.js');
    mix.version(['css/all.css', 'css/admin.css', 'js/all.js', 'js/admin.js']);
    mix.copy('node_modules/font-awesome/fonts', 'public/build/fonts');
    mix.copy('vendor/ckeditor/ckeditor', 'public/vendors/ckeditor/');
});
