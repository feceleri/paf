<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tema extends Model {

    protected $table = 'TT004';
    protected $primaryKey = 'TT004CC000';

	protected $fillable = [

			'TT004CC001', 'TT004CC002'
	];

    public static $rules = [

            'TT004CC001' => 'required|max:7', // Cor
            'TT004CC002' => 'required|max:150' // Titulo
    ];

	// Relacionemento 1:Ramo - N:Empresas
	public function artigos() {
		return $this -> hasMany ( 'App\Artigo', 'TT009CC001', 'TT004CC000' );
	}
}
