<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idioma extends Model {

    protected $table = 'TT026';
    protected $primaryKey = 'TT026CC000';

	protected $fillable = [

            'TT026CC001', // ID Currículo
			'TT026CC002', // Idioma
            'TT026CC003', // Instituição
            'TT026CC004', // Data de Inicio
            'TT026CC005', // Data de Encerramento
            'TT026CC006', // Nível ( 'Básico','Intermediário', 'Avançado', 'Fluente' )
            'TT026CC007', // País
	];

    public static $rules = [

			'TT026CC001' => 'required', // ID Currículo
			'TT026CC002' => 'required|max:150', // Idioma
            'TT026CC003' => 'required|max:150', // Instituição
            'TT026CC004' => 'required|date', // Data de Inicio
            'TT026CC005' => 'date|after:TT026CC004', // Data de Encerramento
            'TT026CC006' => 'required', // Nível ( 'Básico','Intermediário', 'Avançado', 'Fluente' )
    ];

    // Relacionamento 1:Curriculo - N:Idioma
	public function curriculo() {
            return $this -> belongsTo ( 'App\Curriculo', 'TT026CC001', 'TT021CC000' );
	}
       // Relacionemento 1:Lingua - N:Idioma
	public function lingua() {
		return $this -> belongsTo ( 'App\Lingua', 'TT026CC002','TT031CC000' );
	}

}
