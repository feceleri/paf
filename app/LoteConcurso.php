<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

class loteConcurso extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exemploemail:send {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Desativar concursos que tenham expirado o prazo de inscrições';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
