<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model {

    protected $table = 'TT022';
    protected $primaryKey = 'TT022CC000';

	protected $fillable = [

            'TT022CC001', // ID Currículo
			'TT022CC002', // Título
			'TT022CC003', // Tipo
            'TT022CC004', // Instituição
            'TT022CC005', // Carga Horaria
            'TT022CC006', // Data de Inicio
            'TT022CC007' // Data de Encerramento
	];

    public static $rules = [

			'TT022CC001' => 'required', // ID Currículo
			'TT022CC002' => 'required|max:150', // Título
			'TT022CC003' => 'required|max:150', // Tipo
            'TT022CC004' => 'required|max:150', // Instituição
            'TT022CC005' => 'required|numeric|min:1|max:9999', // Carga Horaria
            'TT022CC006' => 'required|date', // Data de Inicio
            'TT022CC007' => 'required|date|after:TT022CC006' // Data de Encerramento
    ];

	// Relacionamento 1:Curriculo - N:Atividades
	public function curriculo() {
		return $this -> belongsTo ( 'App\Curriculo', 'TT022CC001', 'TT021CC000' );
	}
}
