<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artigo extends Model {

	protected $table = 'TT009';
    protected $primaryKey = 'TT009CC000';

	protected $fillable = [

            'TT009CC001', 'TT009CC002', 'TT009CC003', 'TT009CC004'
	];

    public static $rules = [

            'TT009CC001' => 'required', // ID Tema
            'TT009CC002' => 'required', // Título
            'TT009CC003' => 'required|max:100|mimes:jpeg,png,jpg|dimensions:width=380,height=280', // Imagem
            'TT009CC004' => 'required' // Descritivo
    ];


     public static $updateRules = [


            'TT009CC001' => 'required', // ID Tema
            'TT009CC002' => 'required', // Título

            'TT009CC004' => 'required' // Descritivo
    ];

     public static $imageRules = [

          'TT009CC003' => 'required|max:100|mimes:jpeg,png,jpg|dimensions:width=380,height=280', // Imagem
     ];

	// Relacionamento 1:Tema - N:Artigos
	public function tema() {
		return $this -> belongsTo ( 'App\Tema', 'TT009CC001', 'TT004CC000' );
	}
}
