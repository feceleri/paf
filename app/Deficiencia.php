<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deficiencia extends Model {

    protected $table = 'TT024';
    protected $primaryKey = 'TT024CC000';

	protected $fillable = [

            'TT024CC001', // ID Currículo
			'TT024CC002', // Deficiência ('Motora', 'Mental', 'Auditiva', 'Visual')
			'TT024CC003', // Tipo
	];

    public static $rules = [

			'TT024CC001' => 'required', // ID Currículo
			'TT024CC002' => 'required', // Deficiência ('Motora', 'Mental', 'Auditiva', 'Visual')
			'TT024CC003' => 'max:150', // Observações
    ];

	// Relacionamento 1:Curriculo - N:Deficiências
	public function curriculo() {
		return $this -> belongsTo ( 'App\Curriculo', 'TT024CC001', 'TT021CC000' );
	}
}
