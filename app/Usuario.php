<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Notifications\PasswordResetNotification;

class Usuario extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword, Notifiable;

	protected $table = 'TT001';
    protected $guarded = 'TT001CC000';
    protected $primaryKey = 'TT001CC000';
    protected $hidden = ['TT001CC003', 'remember_token'];
    
    protected $fillable = [

            'TT001CC001', 'TT001CC002', 'TT001CC003', 'TT001CC004', 'TT001CC005', 'TT001CC006', 'role_id'
	];

    public static $rules = [

        'TT001CC002' => 'required|max:50', // Login
        'TT001CC003' => 'required|max:50' // Senha
    ];
    
    public static $createrules = [
        
        'TT001CC001' => 'required|max:100', // Nome
        'TT001CC002'=> 'required|max:50|unique:TT001',// Login
        'TT001CC003' => 'required|min:6|max:50|confirmed', // Senha
        'TT001CC004' => 'required|email|max:150|unique:TT001',  // E-mail
        'TT001CC006'=> 'required', //cpf ou cnpj
        'role_id'=> 'required'// 1 2 ou 3 - tipo de usuário
        
      
    ];
    
    public static $editrules = [
        
        
        'TT001CC002' => 'required|max:50', // Login
      
    ];

    /**
     * Overriding the exiting getAuthPassword so that I can customize it
     *
     */
    public function getAuthPassword() {
        return $this -> TT001CC003;
    }

    public function role(){

        return $this->belongsto('App\Role');

    }

    public function isAdmin(){

     if($this->role->name == 'administrador'){

         return true;
     }
        return false;

    }


    public function isFarmaceutico (){

     if($this->role->name == 'farmaceutico'){

         return true;
     }
        return false;

    }

    public function isEmpresa (){

     if($this->role->name == 'empresa'){

         return true;
     }
        return false;

    }

     public function estaDesativado(){

     if($this->TT001CC005 == false){

         return true;
     }
        return false;

    }

    /**
     * Overriding the exiting sendPasswordResetNotification so that I can customize it
     *
     * @var array
     */
    public function sendPasswordResetNotification($token) {
        $this->notify(new PasswordResetNotification($token));
    }
}
