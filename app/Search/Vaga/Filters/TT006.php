<?php

namespace App\Search\Vaga\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// Seccionais
class TT006 implements Filter {

    /**
     * Aplicar o Filtro das Seccionais
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {
        return $builder -> wherein( 'TT014CC003', function( $query ) use ( $value ) {

                $query -> select( 'TT008CC000' )
                -> from( 'TT008' )
                -> whereIn( 'TT008CC002', $value );

        });
    }
}
