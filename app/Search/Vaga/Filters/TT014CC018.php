<?php

namespace App\Search\Vaga\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// tipo de Vaga
class TT014CC018 implements Filter {

    /**
     * Aplicar o Filtro de Tipo de Vaga
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {
        return $builder -> wherein('TT014CC018', $value);
    }
}
