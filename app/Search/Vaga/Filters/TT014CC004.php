<?php

namespace App\Search\Vaga\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

class TT014CC004 implements Filter {

    /**
     * Aplicar o Filtro de Empresa
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {
        return $builder -> wherein( 'TT014CC004', $value );
    }
}
