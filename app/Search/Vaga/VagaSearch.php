<?php

namespace App\Search\Vaga;

use App\Vaga;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;


class VagaSearch {

    public static function apply( Request $filters ) {

        $query = static::applyDecoratorsFromRequest( $filters, ( new Vaga ) -> newQuery() );
        return static::getResults( $query );
    }

    private static function applyDecoratorsFromRequest( Request $request, Builder $query ) {
        foreach ( $request->all() as $filter => $value ) {

            $decorator = static::createFilterDecorator( $filter );

            if ( static::isValidDecorator( $decorator )) {
                $query = $decorator::apply( $query, $value );
            }
        }
        return $query;
    }

    private static function createFilterDecorator( $name ) {
        return __NAMESPACE__ . '\\Filters\\' . str_replace(' ', '', ucwords(str_replace( '_', ' ', $name )));
    }

    private static function isValidDecorator( $decorator ) {
        return class_exists( $decorator );
    }

    private static function getResults( Builder $query ) {
         $encerra_vaga = Carbon::now()->subDays(30);
        return $query -> where('TT014CC016',true)-> where( 'TT014CC019', '>', $encerra_vaga ) -> orderBy ( 'TT014CC019', 'DESC' ) -> paginate ( 4 );
    }
}
