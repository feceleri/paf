<?php

namespace App\Search\Artigo\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

class TT009CC001 implements Filter {

    /**
     * Aplicar o Filtro de Temas
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {
        return $builder -> wherein( 'TT009CC001', $value );
    }
}
