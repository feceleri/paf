<?php

namespace App\Search\Artigo;

use App\Artigo;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class ArtigoSearch {

    public static function apply( Request $filters ) {

        $query = static::applyDecoratorsFromRequest( $filters, ( new Artigo ) -> newQuery() );
        return static::getResults( $query );
    }

    private static function applyDecoratorsFromRequest( Request $request, Builder $query ) {
        foreach ( $request->all() as $filter => $value ) {

            $decorator = static::createFilterDecorator( $filter );

            if ( static::isValidDecorator( $decorator )) {
                $query = $decorator::apply( $query, $value );
            }
        }
        return $query;
    }

    private static function createFilterDecorator( $name ) {
        return __NAMESPACE__ . '\\Filters\\' . str_replace(' ', '', ucwords(str_replace( '_', ' ', $name )));
    }

    private static function isValidDecorator( $decorator ) {
        return class_exists( $decorator );
    }

    private static function getResults( Builder $query ) {
        return $query -> where( 'TT009CC005', true ) -> orderBy ( 'created_at' , 'desc') -> paginate ( 5 );
    }
}
