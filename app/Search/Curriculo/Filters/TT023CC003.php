<?php

namespace App\Search\Curriculo\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

class TT023CC003 implements Filter {

    /**
     * Aplicar o Filtro de Cidades
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {
        return $builder->whereHas('rsvps.event', function ($q) use ($value) {
           $q->where('events.name', $value);
        });
    }




}
