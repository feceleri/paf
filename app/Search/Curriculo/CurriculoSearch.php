<?php

namespace App\Search\Curriculo;

use App\Curriculo;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class CurriculoSearch {

    public static function apply( Request $filters ) {

        $query = static::applyDecoratorsFromRequest( $filters, ( new Curriculo ) -> newQuery() );
        return static::getResults( $query );
    }

    private static function applyDecoratorsFromRequest( Request $request, Builder $query ) {
        foreach ( $request->all() as $filter => $value ) {

            $decorator = static::createFilterDecorator( $filter );

            if ( static::isValidDecorator( $decorator )) {
                $query = $decorator::apply( $query, $value );
            }
        }
        return $query;
    }

    private static function createFilterDecorator( $name ) {
        return __NAMESPACE__ . '\\Filters\\' . str_replace(' ', '', ucwords(str_replace( '_', ' ', $name )));
    }

    private static function isValidDecorator( $decorator ) {
        return class_exists( $decorator );
    }

    private static function getResults( Builder $query ) {
        return $query -> where( 'TT021CC011', true ) -> orderBy ( 'created_at' ) -> paginate ( 6 );
    }
}
