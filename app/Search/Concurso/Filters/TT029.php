<?php

namespace App\Search\Concurso\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// Cidades
class TT029 implements Filter {

    /**
     * Aplicar o Filtro de Cidades
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value) {
        return $builder -> wherein('TT015CC000', function($query) use ($value) {

                $query -> select('TT029CC002')
                -> from('TT029')
                -> whereIn('TT029CC001', $value)
                     -> where ('TT015CC010', true);
        }) -> orwhere('TT015CC011', true);
    }
}
