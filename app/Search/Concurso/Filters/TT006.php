<?php

namespace App\Search\Concurso\Filters;

use DB;
use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// Seccionais
class TT006 implements Filter {

    /**
     * Aplicar o Filtro de Cidades
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {
        $hoje = Carbon::today();
        // Cidades From Seccionais
        $cidades = DB::table('TT008') -> select('TT008CC000')
        -> whereIn('TT008CC002', $value)
        -> get();

        // Cidades selecionados
        foreach($cidades as $cidade) {
            $idCidades[] = $cidade -> TT008CC000;
        }

        return $builder -> wherein('TT015CC000', function($query) use ($idCidades) {

            $query -> select('TT029CC002')
            -> from('TT029')
            -> whereIn('TT029CC001', $idCidades)
             -> where ('TT015CC010', true);

        }) -> orwhere('TT015CC011', true) ;
    }
}
