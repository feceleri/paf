<?php

namespace App\Search\Concurso\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// Estados
class TT028 implements Filter {

    /**
     * Aplicar o Filtro de Estados
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {
        return $builder -> wherein( 'TT015CC000', function( $query ) use ( $value ) {

                $query -> select( 'TT028CC002' )
                -> from( 'TT028' )
                -> whereIn( 'TT028CC001', $value )
                 -> where ('TT015CC010', true);
        }) -> orwhere('TT015CC011', true);
    }
}
