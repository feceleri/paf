<?php

namespace App\Search\Concurso;

use App\Concurso;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

class ConcursoSearch {

    public static function apply( Request $filters ) {

        $query = static::applyDecoratorsFromRequest( $filters, ( new Concurso ) -> newQuery() );
        return static::getResults( $query );
    }

    private static function applyDecoratorsFromRequest( Request $request, Builder $query ) {
        foreach ( $request->all() as $filter => $value ) {

            $decorator = static::createFilterDecorator( $filter );

            if ( static::isValidDecorator( $decorator )) {
                $query = $decorator::apply( $query, $value );
            }
        }
        return $query;
    }

    private static function createFilterDecorator( $name ) {
        return __NAMESPACE__ . '\\Filters\\' . str_replace(' ', '', ucwords(str_replace( '_', ' ', $name )));
    }

    private static function isValidDecorator( $decorator ) {
        return class_exists( $decorator );
    }

    private static function getResults( Builder $query ) {
         $hoje = Carbon::today();
        return $query -> where( 'TT015CC010', true ) /*  ////aqui está comentado por solicitação do Tarsis, chamado 18628/////   -> where( 'TT015CC009', '>=', $hoje ) */ -> orderBy( 'TT015CC009', 'ASC' ) -> paginate ( 6 );
    }





}
