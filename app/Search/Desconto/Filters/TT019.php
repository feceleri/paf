<?php

namespace App\Search\Desconto\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// Estados
class TT019 implements Filter {

    /**
     * Aplicar o Filtro de Estados
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {

        return $builder -> where(function($query) use($value) {

            // Abrangência Nacional?
            $query -> where('TT012CC008', true)

                // Estados selecionados
                -> orWhereIn('TT012CC000', function($query) use($value) {
                    $query -> select('TT019CC002')
                        -> from('TT019')
                        -> whereIn('TT019CC001', $value);
                })

                // Cidades dos Estados selecionados
                -> orWhereIn('TT012CC000', function($query) use($value) {
                    $query -> select('TT020CC002')
                        -> from('TT008')
                        -> join('TT020', 'TT008CC000', '=', 'TT020CC001')
                        -> whereIn('TT008CC001', $value);
                });
        });
    }
}
