<?php

namespace App\Search\Desconto\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// Categorias
class TT012CC002 implements Filter {

    /**
     * Aplicar o Filtro das Categorias
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {
        return $builder -> whereIn( 'TT012CC002', $value );
    }
}
