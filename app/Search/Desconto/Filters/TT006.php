<?php

namespace App\Search\Desconto\Filters;

use DB;
use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// Seccionais
class TT006 implements Filter {

    /**
     * Aplicar o Filtro de Seccionais
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {

        // Cidades From Seccionais
        $cidades = DB::table('TT008') -> select('TT008CC000')
            -> whereIn( 'TT008CC002', $value)
            -> get();

        // Cargos selecionados
        foreach($cidades as $cidade) {
            $idCidades[] = $cidade -> TT008CC000;
        }

        return $builder -> where(function($query) use($idCidades) {

            // Abrangência nacional?
            $query -> where('TT012CC008', true)

            // Cidades das Seccionais selecionadas
            -> orWhereIn('TT012CC000', function($query) use($idCidades) {
                $query -> select('TT020CC002')
                       -> from('TT020')
                       -> whereIn('TT020CC001', $idCidades);
            })

            // Descontos do Estado de São Paulo
            -> orWhereIn('TT012CC000', function($query) {
                $query -> select('TT019CC002')
                       -> from('TT019')
                       -> where('TT019CC001', 'SP');
            });
        });
    }
}
