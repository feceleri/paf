<?php

namespace App\Search\Desconto\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// Parceiros
class TT012CC001 implements Filter {

    /**
     * Aplicar o Filtro dos Parceiros
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {
        return $builder -> whereIn( 'TT012CC001', $value );
    }
}
