<?php

namespace App\Search\Desconto\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// Cidades
class TT020 implements Filter {

    /**
     * Aplicar o Filtro de Cidades
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {

        return $builder -> where(function($query) use($value) {

            // Abrangência Nacional?
            $query -> where('TT012CC008', true)

                // Cidades selecionados
                -> orWhereIn('TT012CC000', function($query) use($value) {
                    $query -> select('TT020CC002')
                        -> from('TT020')
                        -> whereIn('TT020CC001', $value);
                })

                // Estados das Cidades selecionados
                -> orWhereIn('TT012CC000', function($query) use($value) {
                    $query -> select('TT019CC002')
                        -> from('TT008')
                        -> join('TT019', 'TT008CC001', '=', 'TT019CC001')
                        -> whereIn('TT008CC000', $value);
                });
        });
    }
}
