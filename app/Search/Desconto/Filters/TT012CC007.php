<?php

namespace App\Search\Desconto\Filters;

use App\Search\Filter;
use Illuminate\Database\Eloquent\Builder;

// Benefíciarios
class TT012CC007 implements Filter {

    /**
     * Aplicar o Filtro dos Benefíciarios
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply( Builder $builder, $value ) {
        $value[] = 'GG';
        return $builder -> whereIn( 'TT012CC007', $value );
    }
}
