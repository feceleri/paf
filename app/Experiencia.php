<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experiencia extends Model {

    protected $table = 'TT025';
    protected $primaryKey = 'TT025CC000';

	protected $fillable = [

            'TT025CC001', // ID Currículo
            'TT025CC002', // ID Ramo
            'TT025CC003', // ID Cargo
			'TT025CC004', // Empresa
            'TT025CC005', // Data de Inicio
            'TT025CC006', // Data de Encerramento
            'TT025CC007', // Autal ou não, defaul false
            'TT025CC008', // Descrição
	];

    public static $rules = [

            'TT025CC001' => 'required', // ID Currículo
            'TT025CC002' => 'required', // ID Ramo
            'TT025CC003' => 'required', // ID Cargo
			'TT025CC004' => 'required|max:150', // Empresa
            'TT025CC005' => 'required|date', // Data de Inicio
            'TT025CC006' => 'date|after:TT025CC005', // Data de Encerramento
    ];

	// Relacionamento 1:Curriculo - N:Experiências
	public function curriculo() {
		return $this -> belongsTo ( 'App\Curriculo', 'TT025CC001', 'TT021CC000' );
	}

    // Relacionamento 1:Ramo - N:Experiências
	public function ramo() {
		return $this -> belongsTo ( 'App\Ramo', 'TT025CC002', 'TT002CC000' );
	}

    // Relacionamento 1:Cargo - N:Experiências
	public function cargo() {
		return $this -> belongsTo ( 'App\Cargo', 'TT025CC003', 'TT003CC000' );
	}
}
