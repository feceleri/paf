<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model {

	protected $table = 'TT005';
    protected $primaryKey = 'TT005CC000';

	protected $fillable = [

			'TT005CC001'
	];

    public static $rules = [

            'TT005CC001' => 'required|max:150' // Titulo
    ];

	// Relacionemento 1:Categoria - N:Parceiros
	public function descontos() {
		return $this -> hasMany ( 'App\Desconto', 'TT012CC002', 'TT005CC000' );
	}
}
