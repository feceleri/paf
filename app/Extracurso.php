<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extracurso extends Model {

    protected $table = 'TT032';
    protected $primaryKey = 'TT032CC000';

	protected $fillable = [

            'TT032CC001', // ID Currículo
			'TT032CC002', // Curso
			'TT032CC003', // Tipo
            'TT032CC004', // Instituição
            'TT032CC005', // País
            'TT032CC006', // Data de Inicio
            'TT032CC007', // Data de Encerramento
	];

    public static $rules = [

			'TT032CC001' => 'required', // ID Currículo
			'TT032CC002' => 'required|max:150', // Curso
			'TT032CC003' => 'required|max:150', // Tipo
            'TT032CC004' => 'required|max:150', // Instituição
            'TT032CC005' => 'required|max:150', // País
            'TT032CC006' => 'required|date', // Data de Inicio
            'TT032CC007' => 'date|after:TT032CC006', // Data de Encerramento
    ];

	// Relacionamento 1:Curriculo - N:Formações
	public function curriculo() {
		return $this -> belongsTo ( 'App\Curriculo', 'TT032CC001', 'TT021CC000' );
	}
}
