<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seccional extends Model {

	protected $table = 'TT006';
    protected $primaryKey = 'TT006CC000';

	protected $fillable = [

			'TT006CC001'
	];

    // Relacionemento 1:Seccional - N:Cidades
	public function cidades() {
		return $this -> hasMany ( 'App\Cidade', 'TT008CC002', 'TT006CC000' );
	}
}
