<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculo extends Model {

    protected $table = 'TT021';
    protected $primaryKey = 'TT021CC000';

	protected $fillable = [
        'TT021CC001', // ID Usuário
        'TT021CC002', // ID Cidade
        'TT021CC003', // CEP
        'TT021CC004', // Endereço
        'TT021CC005', // Telefone
        'TT021CC006', // Linkedin
        'TT021CC007', // Data de Nascimento
        'TT021CC008', // Estado Civil
        'TT021CC009', // Nacionalidade
        'TT021CC010', // Nível Escolar
        'TT021CC011', // Visível
        'TT021CC012', // Enviar Alertas
        'TT021CC013', // Sexo ('Masculino','feminino')
        'TT021CC014', // data publicação
	];

    public static $rules = [
//        'TT021CC002' => 'required', // ID Cidade
//        'TT021CC013' => 'required', // Sexo ('Masculino','feminino')
    ];

     public static $createrules = [
        'TT021CC002' => 'required', // ID Cidade
        'TT021CC013' => 'required', // Sexo ('Masculino','feminino')
    ];

    // Relacionemento 1:Usuário - N:Curriculos - teste
	public function usuario() {
		return $this -> belongsTo( 'App\Usuario', 'TT021CC001', 'TT001CC000' );
	}

    // Relacionemento 1:Cidade - N:Curriculos
	public function cidade() {
		return $this -> belongsTo( 'App\Cidade', 'TT021CC002', 'TT008CC000' );
	}

    // Relacionemento 1:Curriculo - N:Atividades
	public function atividades() {
		return $this -> hasMany( 'App\Atividade', 'TT022CC001', 'TT021CC000' );
	}

    // Relacionemento 1:Curriculo - N:Formações
	public function formacoes() {
		return $this -> hasMany( 'App\Formacao', 'TT023CC001', 'TT021CC000' );
	}

    // Relacionemento 1:Curriculo - N:Deficiências
	public function deficiencias() {
		return $this -> hasMany( 'App\Deficiencia', 'TT024CC001', 'TT021CC000' );
	}

    // Relacionemento 1:Curriculo - N:Experiências
	public function experiencias() {
		return $this -> hasMany( 'App\Experiencia', 'TT025CC001', 'TT021CC000' );
	}

    // Relacionemento 1:Curriculo - N:Idiomas
	public function idiomas() {
		return $this -> hasMany( 'App\Idioma', 'TT026CC001', 'TT021CC000' );
	}

     // Relacionemento 1:Curriculo - N:Interesses
	public function interesses() {
		return $this -> hasMany( 'App\Interesse', 'TT027CC002', 'TT021CC000' );
	}

     // Relacionemento 1:Curriculo - N:Extracursos
	public function extracursos() {
		return $this -> hasMany( 'App\Extracurso', 'TT032CC001', 'TT021CC000' );
	}

    // Relacionemento N:Cargos - N:Curriculos
    public function cargos() {
        return $this -> belongsToMany( 'App\Cargo', 'TT027', 'TT027CC002', 'TT027CC001' ) -> withTimestamps();
    }
}
