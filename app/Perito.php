<?php

namespace App;

use Illuminate\Database\Eloquent\Model; //estou importando as funcionalidades do model

class Perito extends Model {

	protected $table = 'TT030'; //Por defaul ele coloca no plural da classe, que seria 'peritos', mas aqui definimos com o nome usado no banco de dados.
    protected $primaryKey = 'TT030CC000'; // aqui estou definindo qual coluna é o primarykey, já que neste caso não se chama id (default)

	protected $fillable = [

            'TT030CC001', 'TT030CC002', 'TT030CC003', 'TT030CC004', 'TT030CC005', 'TT030CC006', 'TT030CC007', 'TT030CC008', 'TT030CC009', 'TT030CC010', 'TT030CC011', 'TT030CC012', 'TT030CC013',  'TT030CC015','TT030CC016','TT030CC017','TT030CC018','TT030CC019','TT030CC020','TT030CC021','TT030CC022','TT030CC023','TT030CC024','TT030CC025','TT030CC026','TT030CC027'
	];

    public static $rules = [

            'TT030CC001' => 'required', // Nome
            'TT030CC002' => 'required', // ID Cidade
            'TT030CC003' => 'required|cpf', // CPF
            'TT030CC004' => 'required|numeric', // RG
            'TT030CC005' => 'required', // Nacionalidade
            'TT030CC006' => 'required|max:150', // Estado Civil
            'TT030CC007' => 'required|max:7', //  N. CRF
            'TT030CC008' => 'required', // Endereço completo
            'TT030CC009' => 'required', // Bairro
            'TT030CC010' => 'required|cep', // CEP
            'TT030CC011' => 'required|email|max:150', // E-mail
            'TT030CC013' => 'required|tel', // Celular
            'TT030CC015' => 'required', // Titulo especialização 1
            'TT030CC016' => 'required', // Titulo instituição 1
            'TT030CC017' => 'required', // carga especialização 1
            'TT030CC018' => 'required', // conclusão 1
            'TT030CC027' => 'max:500' // descrição de atividades

    ];


    // Relacionemento 1:Cidade - N:Vagas
	public function cidade() {
		return $this -> belongsTo ( 'App\Cidade', 'TT030CC002','TT008CC000' );
	}


}
