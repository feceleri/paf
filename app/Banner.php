<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model {

	protected $table = 'TT013';
    protected $primaryKey = 'TT013CC000';

	protected $fillable = [

            'TT013CC001', 'TT013CC002', 'TT013CC003', 'TT013CC004', 'TT013CC005', 'TT013CC006',  'TT013CC008',
    ];

    public static $rules = [

            'TT013CC002' => 'required|max:150', // Título
            'TT013CC003' => 'required|url', // Link
			'TT013CC005' => 'required|max:150|mimes:jpeg,png,jpg|dimensions:width=736,height=420', // Imagem
			'TT013CC006' => 'max:250', // Descritivo
             'TT013CC008' => 'required|date', // Data Final de Inscrição
    ];


    public static $updateRules = [

            'TT013CC002' => 'required|max:150', // Título
            'TT013CC003' => 'required|url', // Link

			'TT013CC006' => 'max:250', // Descritivo
             'TT013CC008' => 'required|date', // Data Final de Inscrição
    ];

     public static $imageRules = [

         'TT013CC005' => 'required|max:150|mimes:jpeg,png,jpg|dimensions:width=736,height=420', // Imagem
     ];


    // Relacionemento 1:Parceiro - N:Banners
	public function parceiro() {
		return $this -> belongsTo ( 'App\Parceiro', 'TT013CC001', 'TT011CC000' );
	}
}
