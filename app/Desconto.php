<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desconto extends Model {

	protected $table = 'TT012';
    protected $primaryKey = 'TT012CC000';

	protected $fillable = [

			'TT012CC001', 'TT012CC002', 'TT012CC003', 'TT012CC004', 'TT012CC005', 'TT012CC007', 'TT012CC008'
	];

    public static $rules = [

            'TT012CC001' => 'required', // ID Parceiro
            'TT012CC002' => 'required', // ID Categoria
            'TT012CC003' => 'required|max:150', // Título
			'TT012CC004' => 'required|image|max:150|mimes:jpeg,png,jpg|dimensions:width=380,height=280', // Imagem
			'TT012CC005' => 'required', // Descritivo
            'TT012CC007' => 'required', // Tipo de Beneficiarios
    ];

    public static $updateRules = [
             'TT012CC001' => 'required', // ID Parceiro
            'TT012CC002' => 'required', // ID Categoria
            'TT012CC003' => 'required|max:150', // Título

			'TT012CC005' => 'required', // Descritivo
            'TT012CC007' => 'required', // Tipo de Beneficiarios
    ];

    public static $imageRules = [
        	'TT012CC004' => 'required|image|max:150|mimes:jpeg,png,jpg|dimensions:width=380,height=280', // Imagem
    ];


	// Relacionamento 1:Parceiro - N:Descontos
	public function parceiro() {
		return $this -> belongsTo ( 'App\Parceiro', 'TT012CC001', 'TT011CC000' );
	}

    // Relacionamento 1:Categoria - N:Descontos
	public function categoria() {
		return $this -> belongsTo ( 'App\Categoria', 'TT012CC002', 'TT005CC000' );
	}

    // Relacionemento N:Estados - N:Descontos
    public function estados() {
        return $this -> belongsToMany( 'App\Estado', 'TT019', 'TT019CC002', 'TT019CC001' ) -> withTimestamps();
    }

    // Relacionemento N:Cidades - N:Descontos
    public function cidades() {
        return $this -> belongsToMany( 'App\Cidade', 'TT020', 'TT020CC002', 'TT020CC001' ) -> withTimestamps();
    }
}
