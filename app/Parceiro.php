<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parceiro extends Model {

	protected $table = 'TT011';
    protected $primaryKey = 'TT011CC000';
	protected $fillable = [
			'TT011CC001', 'TT011CC002', 'TT011CC003', 'TT011CC004'
	];

    public static $rules = [
            'TT011CC001' => 'required|max:150', // Nome
            'TT011CC002' => 'required', // tipo
            'TT011CC003' => 'required|max:150|mimes:jpeg, png, jpg|dimensions:width=400, height=150', // Imagem
            'TT011CC004' => 'max:150|url' // Link
    ];

    public static $updateRules = [

            'TT011CC001' => 'required|max:150', // Nome
            'TT011CC002' => 'required', // tipo
            'TT011CC004' => 'max:150|url' // Link
    ];

     public static $imageRules = [
            'TT011CC003' => 'required|max:150|mimes:jpeg, png, jpg|dimensions:width=400, height=150', // Imagem
     ];


	// Relacionemento 1:Parceiro - N:Descontos
	public function descontos() {
		return $this -> hasMany ( 'App\Desconto', 'TT012CC001', 'TT011CC000' );
	}

	// Relacionemento 1:Parceiro - N:Banners
	public function banners() {
		return $this -> hasMany ( 'App\Banner', 'TT013CC001', 'TT011CC000' );
	}

    // Relacionemento N:Documento - N:Parceiro
	public function documentos() {
		return $this -> belongsToMany ( 'App\Documentos' );
	}
}
