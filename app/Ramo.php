<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ramo extends Model {

	protected $table = 'TT002';
    protected $primaryKey = 'TT002CC000';

	protected $fillable = [

			'TT002CC001'
	];

    public static $rules = [

            'TT002CC001' => 'required|max:150' // Título
    ];

	// Relacionemento 1:Ramo - N:Empresas
	public function empresas() {
		return $this -> hasMany ( 'App\Empresa', 'TT010CC001', 'TT002CC000' );
	}

	// Relacionemento 1:Ramo - N:Vagas
	public function vagas() {
		return $this -> hasMany ( 'App\Vaga', 'TT014CC001', 'TT002CC000' );
	}
}
