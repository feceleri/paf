<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model {

    protected $fillable = [

            'nome',
            'email',
            'assunto',
            'telefone',
            'cnpj',
            'titulocargo',
            'razao',
			'mensagem',
            'enviarCopia'
    ];

    public static $rules = [

            'nome' => 'required',
            'email' => 'required|email',
            'assunto' => 'required',
            'telefone' => 'required|tel',
            'mensagem' => 'required',
            //'g-recaptcha-response' => 'required',

    ];
}
