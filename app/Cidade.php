<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model {

    protected $table = 'TT008';
    public $timestamps = false;
    protected $primaryKey = 'TT008CC000';

	protected $fillable = [

            'TT008CC001', // UF
            'TT008CC002', // idSeccional
            'TT008CC003', // Nome
	];

    // Relacionemento 1:Cidade - N:Vagas
	public function vagas() {
		return $this -> hasMany( 'App\Vaga', 'TT014CC003', 'TT008CC000' );
	}

    // Relacionemento 1:Cidade - N:Empresas
	public function empresas() {
		return $this -> hasMany( 'App\Empresa', 'TT010CC002','TT008CC000' );
	}

    // Relacionemento 1:Seccional - N:Cidades
	public function seccional() {
		return $this -> belongsTo( 'App\Seccional', 'TT008CC002','TT006CC000' );
	}

    // Relacionemento 1:Estado - N:Cidades
	public function estado() {
		return $this -> belongsTo( 'App\Estado', 'TT008CC001','TT007CC000' );
	}
}
