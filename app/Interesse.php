<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interesse extends Model {

    protected $table = 'TT027';
    protected $primaryKey = 'TT027CC000';

	protected $fillable = [

            'TT027CC001', // ID Cargo
			'TT027CC002', // ID Currículo

	];

    public static $rules = [

			'TT027CC001' => 'required', // ID Currículo
			'TT027CC002' => 'required', // ID Cargo

    ];

	// Relacionamento 1:Curriculo - N:Deficiências
	public function curriculo() {
		return $this -> belongsTo ( 'App\Curriculo', 'TT027CC002', 'TT021CC000' );
	}


    // Relacionamento 1:Cargo - N:Experiências
	public function cargo() {
		return $this -> belongsTo ( 'App\Cargo', 'TT027CC001', 'TT003CC000' );
	}
}
