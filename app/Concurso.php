<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concurso extends Model {

	protected $table = 'TT015';
    protected $primaryKey = 'TT015CC000';

	protected $fillable = [

            'TT015CC001', // Título
            'TT015CC002', // Link
            'TT015CC003', // Instituição
			'TT015CC004', // Organizadora
            'TT015CC005', // Descritivo
            'TT015CC006', // Requisitos
            'TT015CC007', // Informações Adicionais
            'TT015CC008', // Data Inicial de Inscrição
            'TT015CC009', // Data Final de Inscrição
        'TT015CC012', // Estado instituição
        'TT015CC013', // Cidade instituição
	];

    public static $rules = [

            'TT015CC001' => 'required|max:150', // Título
            'TT015CC002' => 'max:150|url', // Link
            'TT015CC003' => 'required|max:150', // Instituição
			'TT015CC004' => 'max:150', // Organizadora
            'TT015CC005' => 'required', // Descritivo
            'TT015CC006' => 'required', // Requisitos
            'TT015CC008' => 'required|date', // Data Inicial de Inscrição
            'TT015CC009' => 'required|date', // Data Final de Inscrição

    ];
    // Relacionemento 1:Cidade - N:Vagas
	public function cidade() {
		return $this -> belongsTo ( 'App\Cidade', 'TT015CC012','TT008CC000' );
	}


    // Relacionemento N:Estados - N:Descontos
    public function estados() {
        return $this -> belongsToMany( 'App\Estado', 'TT028', 'TT028CC002', 'TT028CC001' ) -> withTimestamps();
    }


    // Relacionemento N:Cidades - N:Descontos
    public function cidades() {
        return $this -> belongsToMany( 'App\Cidade', 'TT029', 'TT029CC002', 'TT029CC001' ) -> withTimestamps();
    }
}
