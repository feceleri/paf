<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lingua extends Model {

	protected $table = 'TT031';
    protected $primaryKey = 'TT031CC000';

	protected $fillable = [

			'TT031CC001'
	];

    public static $rules = [

            'TT031CC001' => 'required|max:150' // Titulo
    ];

//	 Relacionamento 1:Lingua - N:Idiomas
	public function idiomas() {
		return $this -> hasMany ( 'App\Idioma', 'TT026CC002', 'TT031CC000' );
	}
}
