<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class IsEmpresa
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) { //se não estiver logado, vai para o login
        return redirect()->intended('/login');
        }

        $user = Auth::user();


        if($user->estaDesativado()){

            return redirect()->intended('../usuarioinativo');
        }


         if($user->isFarmaceutico()){

            return redirect()->intended('/curriculos/farmaceuticos/curriculo');
        }

        if($user->isAdmin()){

            return redirect()->intended('/admin');
        }




        return $next($request);
    }
}
