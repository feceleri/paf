<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class IsFarmaceutico
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!Auth::check()) { //se não estiver logado, vai para o login
        return redirect()->intended('/login');
        }

        $user = Auth::user();

        if($user->estaDesativado()){

            return redirect()->intended('../usuarioinativo');
        }

         if($user->isAdmin()){

            return redirect()->intended('/admin');
        }

        if($user->isEmpresa()){

            return redirect()->intended('/curriculos/empresas');
        }



        return $next($request);
    }
}
