<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();

        if($user->estaDesativado()){

            return redirect()->intended('../usuarioinativo');
        }

        if($user->isFarmaceutico()){

            return redirect()->intended('../curriculos/farmaceuticos/curriculo');
        }

        if($user->isEmpresa()){

            return redirect()->intended('../curriculos/empresas');
        }


        return $next($request);
    }
}
