<?php

namespace App\Http\Controllers\Curriculos;

use DB;
use App\Usuario;
use App\Ramo;
use App\Cargo;
use App\Formacao;
use App\Curriculo;
use App\Deficiencia;
use App\Experiencia;
use App\Extracurso;
use App\Idioma;
use App\Interesse;
use App\Lingua;
use App\Cidade;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use Carbon\Carbon;
use PDF;


class CurriculoController extends Controller {

    /**
    *
    * Página Inicial
    *
    **/
	public function index() {
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $id = $idusuario ;
        return view( 'site::Curriculos.farmaceuticos.index', compact('usuario', 'id', 'curriculo') );
	}

     /**
    *
    * Dados do usuário
    *
    **/
	public function perfilfarmaceutico(  ) {
        return view( 'site::Curriculos.farmaceuticos.perfilfarmaceutico') ;
	}

    /**
    *
    * Editar Currículo
    *
    **/

    public function edit( $id ) {
        $curriculo = Curriculo::find($id);
        $idusuario = Auth::id();
        if ( $idusuario  == $curriculo->TT021CC001){  // Aqui impede que o usuário edite dados de outro usuário
        return View ('site::Curriculos.farmaceuticos.edit', compact('curriculo', 'id'));
        } else { return back();}
    }

     /**
    *
    * Mostrar Currículo
    *
    **/
	public function show( $id ) {
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $curriculoid = $curriculo-> TT021CC000;
        $deficiencias = Deficiencia::where('TT024CC001' , '=', $curriculoid)->get();

        $temdeficiencia = Deficiencia::groupBy('TT024CC001')
            ->selectRaw('count(*) as total, TT024CC001')
            ->count();

        //
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
        $experiencias = Experiencia::where('TT025CC001' , '=', $curriculoid)->get();
        //
        $extracursos = Extracurso::where('TT032CC001' , '=', $curriculoid)->get();
        //
        $formacoes = Formacao::where('TT023CC001' , '=', $curriculoid)->get();
        //
        $linguas = Lingua::orderBy('TT031CC001') -> pluck( 'TT031CC001', 'TT031CC000' );
        $idiomas = Idioma::where('TT026CC001' , '=', $curriculoid)->get();
        //
        $interesses = Interesse::where('TT027CC002' , '=', $curriculoid)->get();

        //
        if ( $curriculo = Curriculo::find( $id )) {
            return View ( 'site::Curriculos.farmaceuticos.show', compact([ 'curriculo' , 'id', 'deficiencias', 'temdeficiencia', 'experiencias', 'extracursos', 'formacoes', 'idiomas', 'linguas', 'interesses', 'cargos', 'ramos']));
        }
        return redirect() -> back() -> with( 'alert', 'Currículo não encontrado' );
	}


    public function createformacao( ) {
        return View ('site::Curriculos.farmaceuticos.createformacao');
    }

    /**
    *
    * Atualizar Currículo
    *
    **/
    public function update(Request $request, $id) {

        $this -> validate($request, Curriculo::$rules);

        if ($curriculo = Curriculo::find($id)) {

            // Dados editados
            $curriculo -> fill($request -> all());
            $curriculo -> TT021CC003 = preg_replace('/\D/', '', $request -> get('TT021CC003'));
            $curriculo -> TT021CC005 = preg_replace('/[^0-9]/', '', $request -> get('TT021CC005')); //apenas numeros

            // Currículo visível?
            if ($request -> get('TT021CC011') == 'on') {
                $curriculo -> TT021CC011 = true;
            } else {
                $curriculo -> TT021CC011 = false;
            }

            // Receber Alertas?
            if ($request -> get('TT021CC012') == 'on') {
                $curriculo -> TT021CC012 = true;
            } else {
                $curriculo -> TT021CC012 = false;
            }

            $curriculo -> save();
            return redirect() -> back() -> with('success', 'Curriculo atualizado com sucesso');
        }

        return redirect() -> back() -> with('alert', 'Curriculo não encontrada');
    }

        /**
    *
    * PDF de currículo
    *
    **/
    public function generatePDF()
    {
//        $data = ['title' => 'Welcome to HDTuto.com'];
//        $pdf = PDF::loadView('site::Curriculos.farmaceuticos.show', $data);
//
//        return $pdf->download('itsolutionstuff.pdf');
//
//
//
//
//         $idusuario = Auth::id();
//        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
//        $curriculoid = $curriculo-> TT021CC000;
//        $deficiencias = Deficiencia::where('TT024CC001' , '=', $curriculoid)->get();
//
//        $temdeficiencia = Deficiencia::groupBy('TT024CC001')
//            ->selectRaw('count(*) as total, TT024CC001')
//            ->count();
//
//        //
//        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
//        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
//        $experiencias = Experiencia::where('TT025CC001' , '=', $curriculoid)->get();
//        //
//        $extracursos = Extracurso::where('TT032CC001' , '=', $curriculoid)->get();
//        //
//        $formacoes = Formacao::where('TT023CC001' , '=', $curriculoid)->get();
//        //
//        $linguas = Lingua::orderBy('TT031CC001') -> pluck( 'TT031CC001', 'TT031CC000' );
//        $idiomas = Idioma::where('TT026CC001' , '=', $curriculoid)->get();
//        //
//        $interesses = Interesse::where('TT027CC002' , '=', $curriculoid)->get();

        //
//        if ( $curriculo = Curriculo::find( $id )) {
//            return View ( 'site::Curriculos.farmaceuticos.show', compact([ 'curriculo' , 'id', 'deficiencias', 'temdeficiencia', 'experiencias', 'extracursos', 'formacoes', 'idiomas', 'linguas', 'interesses', 'cargos', 'ramos']));
//        }
//        return redirect() -> back() -> with( 'alert', 'Currículo não encontrado' );






    }


//public function active( $id ) {
//
//
//        // Dados do Vaga
//        if ( $curriculo = Curriculo::find( $id )) {
//            // Vaga Habilitada?
//            if ( $curriculo -> TT021CC011 ) {
//                $msg = 'Curriculo desabilitado com sucesso';
//                $curriculo -> TT021CC011 = false;
//
//            } else {
//                $msg = 'Curriculo habilitado com sucesso';
//                $curriculo -> TT021CC011 = true;
//                $curriculo -> TT021CC014 = carbon::now();
//            }
//
//            $curriculo -> save();
//
//            return redirect() -> back()-> with( 'success', $msg );
//
//        }
//        return redirect() -> back() -> with( 'alert', 'Curriculo não encontrado' );
//	}




    public function store(Request $request) {

//        $this -> validate( $request, Formacao::$rules );
//
//
//        $formacao = new Formacao();
//        $formacao -> fill( $request -> all() );
//        $formacao -> save();
//
//        return redirect() -> back() -> with( 'success', 'Formação cadastrada com sucesso' );
    }

}
