<?php

namespace App\Http\Controllers\Curriculos\Farmaceuticos;

use DB;
use App\Usuario;
use App\Experiencia;
use App\Curriculo;
use App\Ramo;
use App\Cargo;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;



class ExperienciaController extends Controller {



    /**
    *
    * Criar experiência
    *
    **/
   public function create( ) {
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001')->where('TT003CC003', true ) -> pluck( 'TT003CC001', 'TT003CC000' );
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $experiencias = Experiencia::where('TT025CC001' , '=', $curriculo-> TT021CC000)->get();

        return View ('site::Curriculos.farmaceuticos.experienciacreate', compact( 'curriculo', 'experiencias', 'ramos', 'cargos'));
    }
      /**
    * Editar experiência
    *
    **/
   public function edit( $id ) {
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
        $experiencia = Experiencia::find(  $id );
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $experiencias = Experiencia::where('TT025CC001' , '=', $curriculo-> TT021CC000)->get();
        $idexperiencia = Experiencia::find($id) ;
        if ( $experiencia->TT025CC001 == $curriculo->TT021CC000){ // Aqui impede que o usuário edite dados de outro usuário
             return View ('site::Curriculos.farmaceuticos.experienciaedit', compact( 'experiencia', 'curriculo', 'experiencias',  'id', 'ramos', 'cargos'));
        } else { return back();}
    }
    /**
    *
    * Atualizar experiência
    *
    **/
    public function update(Request $request, $id) {

        $this -> validate( $request, Experiencia::$rules );
        if ($experiencia = Experiencia::find($id)) {
            // Dados editados
            $experiencia -> fill($request -> all());
            $experiencia -> save();
            return redirect() -> back() -> with('success', 'Experiência atualizado com sucesso');
        }
        return redirect() -> back() -> with('alert', 'Experiência não encontrada');
    }

     /**
    *
    * gravar experiência
    *
    **/
    public function store(Request $request) {
        $this -> validate( $request, Experiencia::$rules );
        // Dados da formação
        $experiencia = new Experiencia();
        $experiencia -> fill( $request -> all() );
        $experiencia -> save();
        return redirect() -> back() -> with( 'success', 'Experiência cadastrada com sucesso' );
    }

       /**
    *
    * deletar experiência
    *
    **/

    public function destroy( $id ) {
        if ( $experiencia = Experiencia::find( $id )) {
            $experiencia -> delete();
            return redirect() -> back() -> with( 'success', 'Experiência deletada com sucesso' );
        }
        return redirect() -> back() -> with( 'alert', 'Experiência não encontrada' );
    }
}
