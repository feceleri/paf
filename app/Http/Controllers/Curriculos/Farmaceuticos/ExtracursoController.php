<?php

namespace App\Http\Controllers\Curriculos\Farmaceuticos;

use DB;
use App\Usuario;
use App\Extracurso;
use App\Curriculo;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;



class ExtracursoController extends Controller {



    /**
    *
    * Criar atividade
    *
    **/
   public function create( ) {
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $extracursos = Extracurso::where('TT032CC001' , '=', $curriculo-> TT021CC000)->get();
//        var_dump($extracurso); die();
        return View ('site::Curriculos.farmaceuticos.extracursocreate', compact( 'curriculo', 'extracursos'));
    }
      /**
    * Editar atividade
    *
    **/
   public function edit( $id ) {
        $extracurso = Extracurso::find( $id );
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $extracursos = Extracurso::where('TT032CC001' , '=', $curriculo-> TT021CC000)->get();
        $idextracurso = Extracurso::find($id) ;
        if ( $extracurso->TT032CC001 == $curriculo->TT021CC000){ // Aqui impede que o usuário edite dados de outro usuário
                          return View ('site::Curriculos.farmaceuticos.extracursoedit', compact( 'extracurso', 'curriculo', 'extracursos',  'id'));
          } else { return back();}
    }
    /**
    *
    * Atualizar atividade
    *
    **/
    public function update(Request $request, $id) {

        $this -> validate( $request, Extracurso::$rules );
        if ($extracurso = Extracurso::find($id)) {
            // Dados editados
            $extracurso -> fill($request -> all());
            $extracurso -> save();
            return redirect() -> back() -> with('success', 'Atividade atualizada com sucesso');
        }

        return redirect() -> back() -> with('alert', 'Atividade não encontrada');
    }

    /**
    *
    * gravar atividade
    *
    **/
    public function store(Request $request) {
        $this -> validate( $request, Extracurso::$rules );
        // Dados da formação
        $extracurso = new Extracurso();
        $extracurso -> fill( $request -> all() );
        $extracurso -> save();
        return redirect() -> back() -> with( 'success', 'Atividade cadastrada com sucesso' );
    }


    /**
    *
    * deletar atividade
    *
    **/

    public function destroy( $id ) {
        if ( $extracurso = Extracurso::find( $id )) {
            $extracurso -> delete();
            return redirect() -> back() -> with( 'success', 'Atividade deletada com sucesso' );
        }
        return redirect() -> back() -> with( 'alert', 'Atividade não encontrada' );
    }
}
