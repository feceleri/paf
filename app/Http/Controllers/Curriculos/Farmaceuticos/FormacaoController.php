<?php

namespace App\Http\Controllers\Curriculos\Farmaceuticos;

use DB;
use App\Usuario;
use App\Formacao;
use App\Curriculo;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;



class FormacaoController extends Controller {



    /**
    *
    * Criar formação
    *
    **/
   public function create( ) {
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $formacoes = Formacao::where('TT023CC001' , '=', $curriculo-> TT021CC000)->get();
//        var_dump($formacao); die();
        return View ('site::Curriculos.farmaceuticos.formacaocreate', compact( 'curriculo', 'formacoes'));
    }
      /**
    * Editar formação
    *
    **/
   public function edit( $id ) {
        $formacao = Formacao::find(  $id );
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $formacoes = Formacao::where('TT023CC001' , '=', $curriculo-> TT021CC000)->get();
        $idformacao = Formacao::find($id) ;
        if ( $formacao->TT023CC001 == $curriculo->TT021CC000){ // Aqui impede que o usuário edite dados de outro usuário
                          return View ('site::Curriculos.farmaceuticos.formacaoedit', compact( 'formacao', 'curriculo', 'formacoes',  'id'));
          } else { return back();}
    }
    /**
    *
    * Atualizar formação
    *
    **/
    public function update(Request $request, $id) {

        $this -> validate( $request, Formacao::$rules );
        if ($formacao = Formacao::find($id)) {
            // Dados editados
            $formacao -> fill($request -> all());
            $formacao -> save();
            return redirect() -> back() -> with('success', 'Formação atualizado com sucesso');
        }

        return redirect() -> back() -> with('alert', 'Formação não encontrada');
    }

     /**
    *
    * gravar formação
    *
    **/
    public function store(Request $request) {
        $this -> validate( $request, Formacao::$rules );
        // Dados da formação
        $formacao = new Formacao();
        $formacao -> fill( $request -> all() );
        $formacao -> save();
        return redirect() -> back() -> with( 'success', 'Formação cadastrada com sucesso' );
    }


       /**
    *
    * deletar formação
    *
    **/

    public function destroy( $id ) {
        if ( $formacao = Formacao::find( $id )) {
            $formacao -> delete();
            return redirect() -> back() -> with( 'success', 'Formação deletada com sucesso' );
        }
        return redirect() -> back() -> with( 'alert', 'Formação não encontrada' );
    }
}
