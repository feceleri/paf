<?php

namespace App\Http\Controllers\Curriculos\Farmaceuticos;

use DB;
use App\Usuario;
use App\Curriculo;
use App\Interesse;
use App\Cargo;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;



class InteresseController extends Controller {


    /**
    *
    * Criar interesse
    *
    **/
   public function create( ) {
        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $interesses = Interesse::where('TT027CC002' , '=', $curriculo-> TT021CC000)->get();
        return View ('site::Curriculos.farmaceuticos.interessecreate', compact( 'curriculo', 'interesses', 'cargos' ));
    }

    /**
    *
    * gravar interesse
    *
    **/
    public function store(Request $request) {
        $this -> validate( $request, Interesse::$rules );
        // Dados da formação
        $interesse = new Interesse();
        $interesse -> fill( $request -> all() );
        $interesse -> save();
        return redirect() -> back() -> with( 'success', 'Interesse cadastrado com sucesso' );
    }

       /**
    *
    * deletar interesse
    *
    **/

    public function destroy( $id ) {
        if ( $interesse = Interesse::find( $id )) {
            $interesse -> delete();
            return redirect() -> back() -> with( 'success', 'Interesse deletado com sucesso' );
        }
        return redirect() -> back() -> with( 'alert', 'Interesse não encontrado' );
    }
}
