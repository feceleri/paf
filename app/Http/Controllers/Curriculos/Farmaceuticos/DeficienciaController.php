<?php

namespace App\Http\Controllers\Curriculos\Farmaceuticos;

use DB;
use App\Usuario;
use App\Curriculo;
use App\Deficiencia;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;



class DeficienciaController extends Controller {



    /**
    *
    * Criar deficiência
    *
    **/
   public function create( ) {
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $deficiencias = Deficiencia::where('TT024CC001' , '=', $curriculo-> TT021CC000)->get();
        return View ('site::Curriculos.farmaceuticos.deficienciacreate', compact( 'curriculo', 'deficiencias' ));
    }


     /**
    *
    * gravar deficiência
    *
    **/
    public function store(Request $request) {
        $this -> validate( $request, Deficiencia::$rules );
        // Dados da formação
        $deficiencia = new Deficiencia();
        $deficiencia -> fill( $request -> all() );
        $deficiencia -> save();
        return redirect() -> back() -> with( 'success', 'Deficiência cadastrada com sucesso' );
    }

       /**
    *
    * deletar deficiência
    *
    **/

    public function destroy( $id ) {
        if ( $deficiencia = Deficiencia::find( $id )) {
            $deficiencia -> delete();
            return redirect() -> back() -> with( 'success', 'Deficiência deletada com sucesso' );
        }
        return redirect() -> back() -> with( 'alert', 'Deficiência não encontrada' );
    }
}
