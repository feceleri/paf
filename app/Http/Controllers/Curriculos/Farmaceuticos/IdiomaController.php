<?php

namespace App\Http\Controllers\Curriculos\Farmaceuticos;

use DB;
use App\Usuario;
use App\Curriculo;
use App\Idioma;
use App\Lingua;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;



class IdiomaController extends Controller {



    /**
    *
    * Criar idioma
    *
    **/
   public function create( ) {
        $linguas = Lingua::orderBy('TT031CC001') -> pluck( 'TT031CC001', 'TT031CC000' );
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $idiomas = Idioma::where('TT026CC001' , '=', $curriculo-> TT021CC000)->get();
        return View ('site::Curriculos.farmaceuticos.idiomacreate', compact( 'curriculo', 'idiomas', 'linguas'));
    }
      /**
    * Editar idioma
    *
    **/
   public function edit( $id ) {
        $linguas = Lingua::orderBy('TT031CC001') -> pluck( 'TT031CC001', 'TT031CC000' );
        $idioma = Idioma::find(  $id );
        $usuario = Auth::user();
        $idusuario = Auth::id();
        $curriculo = Curriculo::where('TT021CC001','=', $idusuario ) -> first ( );
        $idiomas = Idioma::where('TT026CC001' , '=', $curriculo-> TT021CC000)->get();
        $ididioma = Idioma::find($id) ;
        if ( $idioma->TT026CC001 == $curriculo->TT021CC000){ // Aqui impede que o usuário edite dados de outro usuário
             return View ('site::Curriculos.farmaceuticos.idiomaedit', compact( 'idioma', 'curriculo', 'idiomas',  'id', 'linguas'));
        } else { return back();}
    }
    /**
    *
    * Atualizar idioma
    *
    **/
    public function update(Request $request, $id) {

        $this -> validate( $request, Idioma::$rules );
        if ($idioma = Idioma::find($id)) {
            // Dados editados
            $idioma -> fill($request -> all());
            $idioma -> save();
            return redirect() -> back() -> with('success', 'Idioma atualizado com sucesso');
        }
        return redirect() -> back() -> with('alert', 'Idioma não encontrado');
    }

     /**
    *
    * gravar idioma
    *
    **/
    public function store(Request $request) {
        $this -> validate( $request, Idioma::$rules );
        $idioma = new Idioma();
        $idioma -> fill( $request -> all() );
        $idioma -> save();
        return redirect() -> back() -> with( 'success', 'Idioma cadastrado com sucesso' );
    }

       /**
    *
    * deletar idioma
    *
    **/

    public function destroy( $id ) {
        if ( $idioma = Idioma::find( $id )) {
            $idioma -> delete();
            return redirect() -> back() -> with( 'success', 'Idioma deletado com sucesso' );
        }
        return redirect() -> back() -> with( 'alert', 'Idioma não encontrado' );
    }
}
