<?php

namespace App\Http\Controllers\Curriculos\Empresas;

use DB;
use App\Usuario;
use App\Ramo;
use App\Cargo;
use App\Formacao;
use App\Curriculo;
use App\Deficiencia;
use App\Experiencia;
use App\Extracurso;
use App\Idioma;
use App\Interesse;
use App\Lingua;
use App\Cidade;
use App\Empresa;
use App\Vaga;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class VagaController extends Controller {



    /**
    *
    * Criar Vaga
    *
    **/
    public function create() {
         $tipoVaga = 'EM';
        $usuario = Auth::user();
//        var_dump( $usuario); die();
        $cnpj = $usuario->TT001CC002;
        $empresa = Empresa::where('TT010CC003','=', $cnpj ) -> first ( );
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001')->where('TT003CC002', true ) -> pluck( 'TT003CC001', 'TT003CC000' );
         return View ('site::Curriculos.empresas.vagas.create', compact('ramos', 'cargos', 'empresa', 'tipoVaga'));
    }

     /**
    *
    * Editar Vagas
    *
    **/
    public function edit( $id ) {
           $tipoVaga = 'EM';
        $vaga = Vaga::find( $id );
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
        $usuario = Auth::user();
        $cnpj = $usuario->TT001CC002;
        $empresa = Empresa::where('TT010CC003','=', $cnpj ) -> first ( );
        return View ( 'admin::Curriculos.empresas.vagas.edit', compact([ 'vaga', 'ramos', 'cargos', 'empresa', 'tipoVaga' ]));
    }



      /**
    *
    * Gravar Vaga
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Vaga::$rules );

        // Dados do Vaga
        $vaga = new Vaga();
        $vaga -> fill( $request -> all() );
        $vaga -> TT014CC005 = $vaga -> cargo -> TT003CC001;
        $vaga -> TT014CC008 = preg_replace( '/\D/', '', $request -> get( 'TT014CC008' ));
        $vaga -> TT014CC014 = floatval( str_replace(',', '.', str_replace( '.', '', $request -> get( 'TT014CC014' ))));



        $vaga -> save();

        return redirect() -> back() -> with( 'success', 'Vaga cadastrada com sucesso' );
    }

     /**
    *
    * Atualizar Vaga
    *
    **/
    public function update( Request $request, $id ) {

        // Dados do Vaga
        if ( $vaga = Vaga::find( $id )) {

            $this -> validate( $request, Vaga::$rules );

            // Dados novos
            $vaga -> fill( $request -> all() );
            $vaga -> TT014CC005 = $vaga -> cargo -> TT003CC001;
            $vaga -> TT014CC008 = preg_replace( '/\D/', '', $request -> get( 'TT014CC008' ));
            $vaga -> TT014CC014 = floatval( str_replace(',', '.', str_replace( '.', '', $request -> get( 'TT014CC014' ))));
            $vaga -> TT014CC005 = $vaga -> cargo -> TT003CC001;

            // Mostrar endereço?
            if ($request -> get('TT014CC017') == 'on') {
                $vaga -> TT014CC017 = true;
            } else {
                $vaga -> TT014CC017 = false;
            }

            $vaga -> save();

            return redirect() -> back() -> with( 'success', 'Vaga atualizada com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
    }




}
