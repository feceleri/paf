<?php

namespace App\Http\Controllers\Curriculos\Empresas;

use DB;
use App\Usuario;
use App\Ramo;
use App\Cargo;
use App\Formacao;
use App\Curriculo;
use App\Deficiencia;
use App\Experiencia;
use App\Extracurso;
use App\Idioma;
use App\Interesse;
use App\Lingua;
use App\Cidade;
use App\Empresa;
use App\Vaga;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;


class EmpresaController extends Controller {

    /**
    *
    * Página Inicial
    *
    **/
	public function index() {
        $usuario = Auth::user();
        $cnpj = $usuario->TT001CC002;
        $empresa = Empresa::where('TT010CC003','=', $cnpj ) -> first ( );
        $idempresa = $empresa -> TT010CC000;
        $vagas = Vaga::where('TT014CC004','=', $idempresa ) -> get ( );
                var_dump( $vagas); die();
        $acessobloqueado = $usuario -> TT001CC005;
        $today = Carbon::today();
        return view( 'site::Curriculos.empresas.index', compact('usuario', 'cnpj', 'empresa', 'idempresa', 'vagas',  'today', 'acessobloqueado'  ) );
	}


    /**
    *
    * Editar Empresa
    *
    **/

    public function edit( $id ) {
        $curriculo = Curriculo::find($id);
        $idusuario = Auth::id();
         $acessobloqueado = $usuario -> TT001CC005;
        if ( $idusuario  == $curriculo->TT021CC001){  // Aqui impede que o usuário edite dados de outro usuário
        return View ('site::Curriculos.empresas.index', compact('curriculo', 'id'));
        } else { return back();}
    }

     /**
    *
    * Mostrar Empresa
    *
    **/
	public function show( $id ) {
    $usuario = Auth::user();
        $cnpj = $usuario->TT001CC002;
        $empresa = Empresa::where('TT010CC003','=', $cnpj ) -> first ( );
        $idempresa = $empresa -> TT010CC000;
        $vaga = Vaga::where('TT014CC000','=', $id ) -> first ( );
        $numerovagas = $vaga->TT014CC013;


// verificar se esta função está correta *****
        if ( $curriculo = Vaga::find( $id )) {
            return View ( 'site::Curriculos.empresas.show', compact([ 'vaga', 'id', 'numerovagas']));
        }
        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
	}

    /**
    *
    * Criar Vaga
    *
    **/
    public function create() {

        $usuario = Auth::user();
        var_dump($usuario); die();
        $cnpj = $usuario->TT001CC002;
        $empresa = Empresa::where('TT010CC003','=', $cnpj ) -> first ( );
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001')->where('TT003CC002', true ) -> pluck( 'TT003CC001', 'TT003CC000' );
         return View ('site::Curriculos.empresas.create', compact('ramos', 'cargos', 'empresa'));
    }





}
