<?php

namespace App\Http\Controllers\Curriculos\Empresas;

use DB;
use App\Usuario;
use App\Ramo;
use App\Cargo;
use App\Formacao;
use App\Curriculo;
use App\Deficiencia;
use App\Experiencia;
use App\Extracurso;
use App\Idioma;
use App\Interesse;
use App\Lingua;
use App\Cidade;
use App\Empresa;
use App\Vaga;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\Search\Curriculo\CurriculoSearch;

class PesquisacvController extends Controller {



    public function __construct() {
        $deficienciaCV = \Request::get('deficienciaCV');
    }

    /**
    *
    * Página Inicial
    *
    **/
	public function index(Request $filters) {

        $usuario = Auth::user();
        $cnpj = $usuario->TT001CC002;
        $empresa = Empresa::where('TT010CC003','=', $cnpj ) -> first ( );
        $idempresa = $empresa -> TT010CC000;
        $qtasvagas = Vaga::where('TT014CC004','=', $idempresa ) ->where('TT014CC016', true ) -> count ( );
//                  var_dump($qtasvagas ); die();




        $cidades = Curriculo::select( DB::raw('TT008CC000, TT008CC001, TT008CC003'))
                -> join('TT008', 'TT021.TT021CC002', '=', 'TT008.TT008CC000')
                -> groupBy('TT008CC000', 'TT008CC001', 'TT008CC003')
                -> orderBy('TT008CC003')
                -> get();
        $deficienciaCV = \Request::get('deficienciaCV');
        $ramoCV = \Request::get('ramoCV');
        $cargoCV = \Request::get('cargoCV');
        $formacaoCV  = \Request::get('formacaoCV');
        $idiomaCV  = \Request::get('idiomaCV');
        $cidadeCV  = \Request::get('cidadeCV');

//        $cidadesCV = $user->roles->pluck('id')->toArray();

//        https://tutsforweb.com/15-laravel-collection-methods/
         $ramos = Ramo::orderBy('TT002CC001') -> get();
         $linguas = Lingua::orderBy('TT031CC001')->get();
         $cargos = Cargo::orderBy('TT003CC001') ->get();

        $curriculos = Curriculo::where('TT021CC011','=', 1 )
            // BUSCA POR DEFICIENCIAS
            -> where(function($query)
                            {
                                $deficienciaCV  = \Request::get('deficienciaCV');
                                if (  $deficienciaCV == "ANY" ){
                                    $query->  with('Deficiencias')->whereHas('Deficiencias', function($q){ $q->where('TT024CC002', "MOT")->orWhere('TT024CC002', "AUD")->orWhere('TT024CC002', "VIS")->orWhere('TT024CC002', "INT");});
                                }

                                else if (  $deficienciaCV != "" ){
                                    $query->  with('Deficiencias')->whereHas('Deficiencias', function($q){$deficienciaCV  = \Request::get('deficienciaCV'); $q->where('TT024CC002', $deficienciaCV);});
                                }
                            })
            /// BUSCA POR DEFICIENCIAS
             // BUSCA POR CIDADES
            -> where(function($query)
                            {
                                $cidadeCV  = \Request::get('cidadeCV');
                                if (  $cidadeCV != "" ){
                                    $query->   where('TT021CC002','=', $cidadeCV  );
                                }
                            })
            /// BUSCA POR CIDADES
            //      BUSCA POR NIVEL DE FORMACAO
            -> where(function($query)
                            {
                                $formacaoCV  = \Request::get('formacaoCV');
                               if (  $formacaoCV != "" ){
                                   $query->  with('Formacoes')->whereHas('Formacoes', function($q){$formacaoCV = \Request::get('formacaoCV'); $q->where('TT023CC003', $formacaoCV);});
                                }
                            })
            ///      BUSCA POR NIVEL DE FORMACAO
            // BUSCA POR RAMO
            -> where(function($query)
                            {
                                $ramoCV  = \Request::get('ramoCV');
                                if (  $ramoCV != "" ){
                                    $query->  with('Experiencias')->whereHas('Experiencias', function($q){$ramoCV  = \Request::get('ramoCV'); $q->where('TT025CC002', $ramoCV);});
                                }
                            })
            /// BUSCA POR RAMO
            // BUSCA POR CARGO
            -> where(function($query)
                            {
                                $cargoCV  = \Request::get('cargoCV');
                                if (  $cargoCV != "" ){
                                    $query->  with('Experiencias')->whereHas('Experiencias', function($q){$cargoCV  = \Request::get('cargoCV'); $q->where('TT025CC003', $cargoCV);});
                                }
                            })
            /// BUSCA POR CARGO
            // BUSCA POR IDIOMAS
            -> where(function($query)
                            {
                                $idiomaCV  = \Request::get('idiomaCV');
                                if (  $idiomaCV != "" ){
                                    $query->  with('Idiomas')->whereHas('Idiomas', function($q){$idiomaCV  = \Request::get('idiomaCV'); $q->where('TT026CC002', $idiomaCV);});
                                }
                            })
            /// BUSCA POR IDIOMAS
            ->inRandomOrder()-> paginate (20) ; //pegar em ordem aleatória para não privilegiar ninguém


//

         if ( $qtasvagas > 0){  // Aqui impede que a empresa sem vaga pesquisa currículos
       return view('site::Curriculos.empresas.pesquisacv', compact('curriculos', 'cidades', 'linguas','cargos', 'ramos',  'formacaoCV', 'deficienciaCV', 'idiomaCV', 'ramoCV', 'cargoCV', 'cidadeCV'));
        } else { return back();}





	}


    /**
    *
    * Editar Currículo
    *
    **/

    public function edit( $id ) {

    }

     /**
    *
    * Mostrar Vaga
    *
    **/
	public function show( $id ) {

        $curriculo = Curriculo::where('TT021CC000','=', $id ) -> first ( );


        $deficiencias = Deficiencia::where('TT024CC001' , '=', $id)->get();
        $temdeficiencia = Deficiencia::groupBy('TT024CC001')->where('TT024CC001' , '=', $id)
            ->selectRaw('count(*) as total, TT024CC001')
            ->count();
        $teminteresse = Interesse::where('TT027CC002' , '=', $id) -> groupBy('TT027CC002')->where('TT027CC002' , '=', $id)
            ->selectRaw('count(*) as total, TT027CC002')
            ->count();
        $temexperiencia = Experiencia::where('TT025CC001' , '=', $id) -> groupBy('TT025CC001')->where('TT025CC001' , '=', $id)
            ->selectRaw('count(*) as total, TT025CC001')
            ->count();
        $temextracurso = Extracurso::where('TT032CC001' , '=', $id) -> groupBy('TT032CC001')->where('TT032CC001' , '=', $id)
            ->selectRaw('count(*) as total, TT032CC001')
            ->count();
        $temidioma = Idioma::where('TT026CC001' , '=', $id) -> groupBy('TT026CC001')->where('TT026CC001' , '=', $id)
            ->selectRaw('count(*) as total, TT026CC001')
            ->count();
        $temformacao = Formacao::where('TT023CC001' , '=', $id) -> groupBy('TT023CC001')->where('TT023CC001' , '=', $id)
            ->selectRaw('count(*) as total, TT023CC001')
            ->count();
//
//                                   var_dump($teminteresse ); die();
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
        $experiencias = Experiencia::where('TT025CC001' , '=', $id)->get();
        $extracursos = Extracurso::where('TT032CC001' , '=', $id)->get();
        $formacoes = Formacao::where('TT023CC001' , '=', $id)->get();
        $linguas = Lingua::orderBy('TT031CC001') -> pluck( 'TT031CC001', 'TT031CC000' );
        $idiomas = Idioma::where('TT026CC001' , '=', $id)->get();
        $interesses = Interesse::where('TT027CC002' , '=', $id)->get();



         return View ('site::Curriculos.empresas.showcv', compact( 'curriculo' , 'id', 'deficiencias', 'temexperiencia', 'temextracurso', 'temidioma', 'temformacao','temdeficiencia', 'teminteresse', 'experiencias', 'extracursos', 'formacoes', 'idiomas', 'linguas', 'interesses', 'cargos', 'ramos'));

	}


    public function createformacao( ) {
        return View ('site::Curriculos.empresas.pesquisacv');
    }

    /**
    *
    * Atualizar Currículo
    *
    **/
    public function update() {

    }


     /**
    *
    * Criar formação
    *
    **/
    public function store(Request $request) {


    }

}
