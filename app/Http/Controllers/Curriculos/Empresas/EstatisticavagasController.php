<?php

namespace App\Http\Controllers\Curriculos\Empresas;

use DB;
use App\Usuario;
use App\Ramo;
use App\Cargo;
use App\Formacao;
use App\Curriculo;
use App\Deficiencia;
use App\Experiencia;
use App\Extracurso;
use App\Idioma;
use App\Interesse;
use App\Lingua;
use App\Cidade;
use App\Empresa;
use App\Vaga;
use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\EstatisticaVagas;


class EstatisticavagasController extends Controller {



    public function __construct() {

    }

    /**

    *
    * Página Inicial
    *
    **/
	public function index(Request $filters) {



	}




     /**
    *
    * Desativar vaga e criar estatistica
    *
    **/


    public function disable ( Request $request,$id ) {

        $this -> validate( $request, EstatisticaVagas::$rules );

        // Aqui vou definir se as vagas foram preenchidas ou não pelo PAF, se maior que 0, foram.
        $maisquezero =   Input::get('TT033CC005');

        if ($maisquezero == 0){
            $preencheu = 'NO';

        } else {
            $preencheu = 'SI';
        }
        //


        // Dados do Vaga
        if ( $vaga = Vaga::find( $id )) {
            // Vaga Habilitada?
                $estatisticavagas = new Estatisticavagas();
                $estatisticavagas -> TT033CC001 = $id;
                $estatisticavagas -> TT033CC002 = $preencheu;
                $estatisticavagas -> TT033CC003 = $vaga->TT014CC019;
                $estatisticavagas -> TT033CC004 = $vaga->TT014CC013;
                $estatisticavagas -> TT033CC005 = $request -> get( 'TT033CC005' );
                $estatisticavagas -> save();
                $msg = 'Vaga desabilitada com sucesso';
                $vaga -> TT014CC016 = false;
                $vaga -> save();
                return redirect() -> back() -> with( 'success', $msg );
        }
        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
	}

}
