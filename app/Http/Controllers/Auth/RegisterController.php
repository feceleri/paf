<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {

        return Validator::make($data, [

            'TT001CC001' => 'required|max:150', // Nome
			'TT001CC002' => 'required|max:50', // Login
			'TT001CC003' => 'required|min:6|max:50|confirmed', // Senha
            'TT001CC004' => 'required|email|max:150|unique:TT001',  // E-mail
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {

        return Usuario::create([
            'TT001CC001' => $data['TT001CC001'],
            'TT001CC002' => $data['TT001CC002'],
            'TT001CC004' => $data['TT001CC004'],
            'TT001CC003' => bcrypt($data['TT001CC003']),
        ]);
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->create($request->all());

        return redirect() -> back() -> with( 'success', 'Usuário cadastrado com sucesso' );
    }
}
