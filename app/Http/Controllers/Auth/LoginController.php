<?php

namespace App\Http\Controllers\Auth;

use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
        $this -> middleware('guest', ['except' => 'logout']);
    }

    public function login(Request $request) {

        $this -> validate( $request, Usuario::$rules );

        if ( $this -> auth -> attempt (

            [
                'TT001CC002' => $request -> get('TT001CC002'),
                'password' => $request -> get('TT001CC003'),
            ],

            $request->get('remember') == 1 ? true : false

        )){

            return redirect() -> route('admin.home');

        }
        else {

            return redirect() -> back()
                -> with( 'erro', 'Autenticação não autorizada' )
                -> withInput();
        }

    }

    public function logout() {
        Auth::logout(); // logout user
        return redirect() -> route('login'); //redirect back to login
    }
}
