<?php

namespace App\Http\Controllers;

use App\Vaga;
use App\Banner;
use App\Artigo;
use App\Parceiro;
use App\Concurso;
use App\Desconto;

use Carbon\Carbon;

class HomeController extends Controller {

	/**
    *
    * Tela Inicial
    *
    **/
	public function index() {
        $encerra_vaga = Carbon::now()->subDays(30);
        $hoje = Carbon::today();
        // Contagem dos Destaques
        $qtdArtigos = Artigo::where( 'TT009CC005', true ) -> count();
		$qtdConcursos = Concurso::where( 'TT015CC010', true ) /* ////// aqui foi comentado para que contabilize todos os concursos ativos e não somente os válidos - chamado 18628 ////// -> where( 'TT015CC009', '>=', $hoje )*/   -> count();
        $qtdDescontos = Desconto::where( 'TT012CC006', true ) -> count();
        $qtdVagas = Vaga::where( 'TT014CC016', true )-> where( 'TT014CC019', '>', $encerra_vaga ) -> sum( 'TT014CC013' );

        
        // Dados da Home
        $artigos = Artigo::where( 'TT009CC005', true )  -> orderBy( 'created_at', 'DESC' ) -> get() -> take( 3 ); //exibe os ultimos 3 artigos cadastrados


        //encerra banner informa as datas de 30 dias anteriores ao atual
        $encerra_banner = Carbon::now()->subDays(30);
        $hoje = Carbon::today();

        $qtdBanners = Banner::where( 'TT013CC008', '>=', $hoje )  -> count();
        //chama efetivamente os banners a serem exibidos considerando a data de 30 dias de sua criação
        $banners = Banner::where( 'TT013CC008', '>=', $hoje ) -> get() -> take( $qtdBanners );



        $concurso = Concurso::where( 'TT015CC010', true )
           // aqui foi adicionado este where para que se não houver consurso válido, apareça algum concurso que esteja ativo - chamado 18628
            -> where(function($query)
                   {
                     $hoje = Carbon::today();
                     $concursosValidos = Concurso::where( 'TT015CC010', true )-> where( 'TT015CC009', '>=', $hoje )   -> count();
                    if (  $concursosValidos != 0 ){
                                    $query->   where('TT015CC009', '>=', $hoje );
                                }
                            })

            -> orderBy( 'TT015CC009' ) -> get() -> first();



        $desconto = Desconto::where( 'TT012CC006', true ) -> get() -> random( 1 );
        $parEmpregos = Parceiro::where( 'TT011CC002', '=', 'BE' ) -> where( 'TT011CC005', true ) -> get() -> random( 3 );
        $parDescontos = Parceiro::where( 'TT011CC002', '=', 'CB' ) -> where( 'TT011CC005', true ) -> get() -> random( 9 );
        $vaga = Vaga::where( 'TT014CC019', '>', $encerra_vaga ) ->where( 'TT014CC016', true ) -> orderBy( 'TT014CC019', 'DESC' ) -> get() -> first();
        
		return view('site::home', compact(['vaga', 'banners', 'artigos', 'qtdVagas', 'parDescontos', 'parEmpregos', 'qtdConcursos', 'qtdDescontos', 'qtdArtigos', 'concurso', 'desconto','encerra_vaga', 'qtdBanners']));
	}
}
