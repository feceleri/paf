<?php

namespace App\Http\Controllers;

use App\Contato;
use App\Http\Requests;
use App\Mail\ContatoMail;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use App\BloqueiosTemporarios;

class ContatoController extends Controller {

    /**
    *
    * Mostrar form de envio do contato
    *
    **/
    public function create() {
        $tipoForm = 'CO';
        return view( 'site::contato', compact('tipoForm') );

    }

     public function serparceiro() {
        $tipoForm = 'SP';
        return view( 'site::contato', compact('tipoForm') );
    }

     public function indicarparceiro() {
        $tipoForm = 'IP';
        return view( 'site::contato', compact('tipoForm') );
    }

        public function retiraranuncio() {
        // no model está a condição
         $bloqueios = BloqueiosTemporarios::first(); // se estiver bloqueado, enviar para página de aviso
         if($bloqueios->estaBloqueado()){

           return view ( 'site::bloqueiotemporario');
        }

        else {  $tipoForm = 'RA';
        return view( 'site::contato', compact('tipoForm') );}



    }


    /**
    *
    * Enviar contato por E-mail
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Contato::$rules );

        // Dados do Contato
        $contato = new Contato();
        $contato -> fill( $request -> all() );

        Mail::to( 'paf@crfsp.org.br' )
             -> send( new ContatoMail( $contato ));

        // Enviar cópia de E-mail?
        if ( $contato -> enviarCopia == 'on' ) {
            Mail::to( $contato -> email )
                -> send( new ContatoMail( $contato ));
        }

        return redirect() -> route( 'contato' )
            -> with( 'success', 'Mensagem enviada com sucesso' );
    }
}
