<?php
namespace App\Http\Controllers\Soap; 
use DB;
use Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Mail\CadastrocvMail;
use App\Usuario;
use App\Curriculo;

class SoapController extends BaseSoapController
{
    private $service;
    
    public function index(){
         return view('auth/cpf-digitar');
        // aqui vai para tela para digitar o CPF do farmacêutico
        
    }

    
    public function buscasoap(Request $request){
        // inserir se vazio nao vai pra cadastro - se existe true, nao precisa retornar os dados ainda
        $cpf = preg_replace( '/\D/', '', $request->input('cpf'));
        
        try {
            self::setWsdl('http://arquivos.crfsp.org.br/ws/WebService.asmx?WSDL');
            $this->service = InstanceSoapClient::init();
            $nrcpf = $cpf;
            $params = [
                'nrcpf' => request()->input('nrcpf') ? request()->input('nrcpf') :  $nrcpf 
            ];
            $response = $this->service->GetDadosPf($params);
            $resultado = (array) $response;
            $resultado = json_decode($resultado['GetDadosPfResult'], true);//json decodificador 
            $crf = ($resultado[0]['crf']); //na array 0 pega o dado crf
            $nome = ($resultado[0]['nome']);
            $crf = substr($crf, 1, -1); //remove o primeiro "1" e o último "-1" caractere
            $sexo = ($resultado[0]['sexo']);
            $endereco = ($resultado[0]['endereco']);
            $cidade = ($resultado[0]['cidade']);
            $cep = ($resultado[0]['cep']);
            $email = ($resultado[0]['email']);

            $seccional = ($resultado[0]['seccional']);
            $idade = ($resultado[0]['idade']);
            $nascimento = ($resultado[0]['dt_nascimento']);
            $nacionalidade = ($resultado[0]['nascionalidade']);
            $celular = ($resultado[0]['celular']);
            $telefone = ($resultado[0]['telefone']);

            return view ('auth/crf-digitar', compact( 'crf', 'cpf', 'nome','sexo', 'endereco', 'cidade', 'cep'
                                                     , 'email', 'seccional', 'idade', 'nascimento', 'nacionalidade', 'celular', 'telefone'
                                                    ));


        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function validacrf(Request $request){
        // inserir se vazio vai pra cadastro
        $nome = $request->input('nome');
        $cpf = $request->input('cpf');
        $crf = $request->input('crf');
        $sexo = $request->input('sexo');
        $endereco = $request->input('endereco');
        $cidade = $request->input('cidade');
        $cep = $request->input('cep');
        $email = $request->input('email');
        $seccional = $request->input('seccional');
        $idade = $request->input('idade');
        $nascimento = $request->input('nascimento');
        $nacionalidade = $request->input('nacionalidade');
        $celular = $request->input('celular');
        $telefone = $request->input('telefone');
        $crfdigitado =  $request->input('crfdigitado');


        return view ('auth/mostradados', compact('cpf', 'crf', 'crfdigitado', 'nome','sexo', 'endereco', 'cidade', 'cep' , 'email', 'seccional', 'idade', 'nascimento', 'nacionalidade', 'celular', 'telefone'
                                                  ));
        

        
    }
    
      public function enviaemail(Request $request){
        $nome = $request->input('nome');
        $cpf = $request->input('cpf');
        $crf = $request->input('crf');
        $email = $request->input('email');
        $sexo = $request->input('sexo');
        $endereco = $request->input('endereco');
        $cidade = $request->input('cidade');
        $cep = $request->input('cep');
        $email = $request->input('email');
        $seccional = $request->input('seccional');
        $idade = $request->input('idade');
        $nascimento = $request->input('nascimento');
        $nacionalidade = $request->input('nacionalidade');
        $celular = $request->input('celular');
        $telefone = $request->input('telefone');

        $senha = '@Mudar#'.$crf;
           var_dump($telefone); die();

//                 var_dump($nacionalidade." ".$celular." ".$nascimento." ".$telefone." ".$email." ".$senha); die();
          
        $usuario = new Usuario();  
        $usuario -> fill( $request -> all() );
        // Limpar Formatação
        
        $usuario -> TT001CC001 = $nome;
        $usuario -> TT001CC002 = $crf; 
        $usuario -> TT001CC003 = bcrypt($senha);
        $usuario -> TT001CC004 = $email;   
        $usuario -> TT001CC006 = preg_replace( '/\D/', '', $request->input('cpf'));  
        $usuario-> role_id = 2;  
        $usuario -> save();  
        $usuarioid = $usuario->TT001CC000;



        $curriculo = new Curriculo();

        $curriculo -> TT021CC001 = $usuarioid;
        $curriculo -> TT021CC002 = "10208";
        $curriculo -> TT021CC003 = str_replace("-", "",$request->input('cep'));
        $curriculo -> TT021CC004 = $endereco;
        $curriculo -> TT021CC005 = $telefone;
        $curriculo -> TT021CC007 = $nascimento;
        $curriculo -> TT021CC009 = $nacionalidade;
        $curriculo -> TT021CC013 = $sexo;

//         celular


//        $cidade = $request->input('cidade');
//




        $curriculo -> save();
//
//         Mail::to( $email )
//        -> send( new CadastrocvMail(  $cpf,  $crf, $email, $nome));


          
        return view ('login');
        
    }
    
    
//    public function pfs(){
//        try {
//            self::setWsdl('http://arquivos.crfsp.org.br/ws/WebService.asmx?WSDL');
//            $this->service = InstanceSoapClient::init();
//            $nrcpf = 10029086809;
//            $params = [
//                'nrcpf' => request()->input('nrcpf') ? request()->input('nrcpf') :  $nrcpf 
//            ];
//            $response = $this->service->GetDadosPf($params);
//            $resultado = (array) $response;
//            $resultado = json_decode($resultado['GetDadosPfResult'], true);//json decodificador
//            $nome = ($resultado[0]['nome']); 
//            $crf = ($resultado[0]['crf']); //na array 0 peda o dado crf
//            $crf = substr($crf, 1, -1); //remove o primeiro 1 e o último -1 caractere
//            return view ('auth/pfs-soap', compact('nome', 'crf'));
//        }
//        catch(\Exception $e) {
//            return $e->getMessage();
//        }
//    }
    
    //        var_dump($crfdigitado." ".$crf." ".$cpf); die();

    
}
