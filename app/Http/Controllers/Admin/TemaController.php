<?php

namespace App\Http\Controllers\Admin;

use App\Tema;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TemaController extends Controller {

    /**
    *
    * Mostrar view de Temas
    *
    **/
	public function index() {
        $temas = Tema::orderBy ( 'TT004CC000', 'DESC' ) -> paginate ( 5 );
        return View ( 'admin::temas.index', compact([ 'temas' ]));
	}

    /**
    *
    * View de Cadastro de Temas
    *
    **/
    public function create() {
        return View ( 'admin::temas.create' );
    }

    /**
    *
    * Gravar temas do Artigo
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Tema::$rules );

        // Dados do Tema
        $tema = new Tema();
        $tema -> fill( $request -> all() );
        $tema -> save();

        return redirect() -> back() -> with( 'success', 'Tema cadastrado com sucesso' );
    }

    /**
    *
    * Editar Temas
    *
    **/
    public function edit( $id ) {
        $tema = Tema::find( $id );
        return View ( 'admin::temas.edit', compact( 'tema' ));
    }

    /**
    *
    * Atualizar Tema
    *
    **/
    public function update( Request $request, $id ) {

        // Dados do Tema
        if ( $tema = Tema::find( $id )) {

            $this -> validate( $request, Tema::$rules );

            $tema = Tema::find( $id );
            $tema -> fill( $request -> all() );
            $tema -> save();

            return redirect() -> back() -> with( 'success', 'Tema atualizado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert','Tema não encontrado' );
    }

    /**
    *
    * Mostrar view de Temas
    *
    **/
    public function destroy( $id ) {

       if ( $tema = Tema::find( $id )) {

            if ( count( $tema -> artigos )) {
                return redirect() -> back() -> with( 'alert', 'Artigos relacionados com o tema' );
            }

            $tema -> delete();

            return redirect() -> back() -> with( 'success', 'Tema deletado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Tema não encontrado' );
    }
}
