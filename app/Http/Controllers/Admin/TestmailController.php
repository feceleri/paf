<?php

namespace App\Http\Controllers\Admin;


use App\Contato;
use App\Http\Requests;
use App\Mail\TestMail;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;


class ContatoController extends Controller {

    /**
    *
    * Mostrar form de envio do contato
    *
    **/
    public function create() {

        return view( 'admin::testmail' );

    }



    }


    /**
    *
    * Enviar contato por E-mail
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Contato::$rules );

        // Dados do Contato
        $contato = new Contato();
        $contato -> fill( $request -> all() );

        Mail::to( 'paf@crfsp.org.br' )
             -> send( new ContatoMail( $contato ));

        // Enviar cópia de E-mail?
        if ( $contato -> enviarCopia == 'on' ) {
            Mail::to( $contato -> email )
                -> send( new ContatoMail( $contato ));
        }

        return redirect() -> route( 'contato' )
            -> with( 'success', 'Mensagem enviada com sucesso' );
    }
}
