<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Vaga;
use App\Ramo;
use App\Cargo;
use App\Empresa;
use App\Cidade;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Mail\PublicadaVagaMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\EstatisticaVagas;



//use Illuminate\Pagination;

class VagaController extends Controller {

    /**
    *
    * Mostrar Vagas
    *
    **/
	public function index(Request $filters) {

         $cidades = Vaga::select( DB::raw('TT008CC000, TT008CC001, TT008CC003, count(TT014CC013) as qtde'))
                -> join('TT008', 'TT014.TT014CC003', '=', 'TT008.TT008CC000')
                -> groupBy('TT008CC000', 'TT008CC001', 'TT008CC003')
                -> orderBy('TT008CC003')
                -> get();
          $empresas = Vaga::select( DB::raw('TT010CC000, TT010CC001, TT010CC003,TT010CC004, count(TT014CC013) as qtde'))
                -> join('TT010', 'TT014.TT014CC004', '=', 'TT010.TT010CC000')
                -> groupBy('TT010CC000', 'TT010CC001', 'TT010CC003', 'TT010CC004')
                -> orderBy('TT010CC004')
                -> get();
          $tituloVaga = \Request::get('tituloVaga');
          $cidadeVaga = \Request::get('cidadeVaga');
          $cnpjVaga = \Request::get('cnpjVaga');
          $emailVaga = \Request::get('emailVaga');
          $ativaVaga = \Request::get('ativaVaga');
          $dataVaga = \Request::get('dataVaga');
          $publicacaoVaga = \Request::get('publicacaoVaga');
          $empresaVaga = \Request::get('empresaVaga');
          $statusVaga = \Request::get('statusVaga');
          $idVaga = \Request::get('idVaga');

          $vagas = Vaga:: where('TT014CC005','like','%'.$tituloVaga.'%')
                -> where('TT014CC003','like','%'.$cidadeVaga.'%')
                -> where('TT014CC006','like','%'.$emailVaga.'%')
                -> whereDate('TT014.created_at','like','%'.$dataVaga.'%')
                -> whereDate('TT014CC019','like','%'.$publicacaoVaga.'%')
                -> where('TT014CC016','like','%'.$ativaVaga.'%')

             // BUSCA POR EMPRESA
            -> where(function($query)
                            {
                                $empresaVaga  = \Request::get('empresaVaga');
                                if (  $empresaVaga != "" ){
                                    $query->  with('Empresa')->whereHas('Empresa', function($q){$empresaVaga  = \Request::get('empresaVaga'); $q->where('TT010CC004','like','%'.$empresaVaga.'%');});
                                }


                            })
            /// BUSCA POR EMPRESA
            // BUSCA POR CNPJ
            -> where(function($query)
                            {
                                $cnpjVaga  = \Request::get('cnpjVaga');
                                if (  $cnpjVaga != "" ){
                                    $query->  with('Empresa')->whereHas('Empresa', function($q){$cnpjVaga  = \Request::get('cnpjVaga'); $q->where('TT010CC003','like','%'.$cnpjVaga.'%');});
                                }


                            })
            /// BUSCA POR CNPJ
//                -> where('TT014CC004','like','%'.$empresaVaga.'%')
//                -> where('TT014CC004','like','%'.$cnpjVaga.'%')
                -> where(function($query)
                            {
                                $statusVaga  = \Request::get('statusVaga');
                                if (  $statusVaga != "" ){
                                    $query->   where('TT014CC020','=', $statusVaga  );
                                }
                            })
               -> where(function($query)
                            {
                                $idVaga  = \Request::get('idVaga');
                                if (  $idVaga != "" ){
                                    $query->   where('TT014CC000','=', $idVaga  );
                                }
                            })
                -> orderBy('TT014.created_at', 'DESC')
                -> paginate ( 30 );


       //  $vagas->setPath('custom/url');
        // Request via Ajax?
        if ($filters -> ajax()) {
            return view('admin::vagas.load', compact('vagas', 'cidades', 'empresas', 'tituloVaga','cidadeVaga','cnpjVaga','emailVaga','ativaVaga','dataVaga','publicacaoVaga','empresaVaga', 'statusVaga', 'idVaga'));
        } else {
            return View('admin::vagas.index', compact(['vagas', 'cidades', 'empresas', 'tituloVaga','cidadeVaga','cnpjVaga','emailVaga','ativaVaga','dataVaga','publicacaoVaga','empresaVaga', 'statusVaga', 'idVaga']));
        }
	}
//


    /**
    *
    * Mostrar Vaga
    *
    **/
	public function show( $id ) {

        if ( $vaga = Vaga::find( $id )) {
            return View ( 'admin::vagas.show', compact([ 'vaga' ]));
        }
        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
	}

    /**
    *
    * Ativar Vaga
    *
    **/

    //função criada por liliana - usar para desabilitar vaga
	public function disable( $id ) {

         // Dados do Vaga
        if ( $vaga = Vaga::find( $id )) {


                $vaga -> TT014CC016 = false;

            $vaga -> save();
            return redirect() -> back() -> with( 'success', $msg );
        }

        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
	}
     //função criada por liliana - usar para desabilitar vaga



	public function active( $id ) {


        // Dados do Vaga
        if ( $vaga = Vaga::find( $id )) {
            // Vaga Habilitada?
            if ( $vaga -> TT014CC016 ) {
                $msg = 'Vaga desabilitada com sucesso';
                $vaga -> TT014CC016 = false;

            } else {
                $msg = 'Vaga habilitada com sucesso';
                $vaga -> TT014CC016 = true;
                $vaga -> TT014CC020 = 'DE';
                $vaga -> TT014CC019 = carbon::now();
                // ENVIAR EMAIL PARA O ANUNCIANTE

// manter ativo - inativo para testes # atenção ***********************************************************************************
                Mail::to( $vaga  -> TT014CC006, $vaga -> empresa ->  TT010CC005 ) -> send ( new PublicadaVagaMail( $vaga ));

            }


            $vaga -> save();
            $urlFiltroAtual = \Request::get('urlFiltroAtual');
            $paginacaoAtual = \Request::get('paginacaoAtual');
            return redirect('admin/vagas?page=' .  $paginacaoAtual . $urlFiltroAtual )-> with( 'success', $msg );

        }
        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
	}


    /**
    *
    * Cadastrar Vagas
    *
    **/
    public function create() {
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001')->where('TT003CC002', true ) -> pluck( 'TT003CC001', 'TT003CC000' );
        $empresas = Empresa::select(
            DB::raw("concat(TT010CC004,' | ',
            concat( left(TT010CC003, 2), '.',
            mid(TT010CC003, 3, 3), '.',
            mid(TT010CC003, 6, 3), '/',
            mid(TT010CC003, 9, 4), '-',
            right(TT010CC003, 2))) as RazaoSocial"), 'TT010CC000')
            -> orderBy('TT010CC004')
            -> pluck('RazaoSocial', 'TT010CC000');

        return View ( 'admin::vagas.create', compact([ 'ramos', 'cargos', 'empresas' ]));
    }

    /**
    *
    * Gravar Vaga
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Vaga::$rules );

        // Dados do Vaga
        $vaga = new Vaga();
        $vaga -> fill( $request -> all() );
        $vaga -> TT014CC005 = $vaga -> cargo -> TT003CC001;
        $vaga -> TT014CC008 = preg_replace( '/\D/', '', $request -> get( 'TT014CC008' ));
        $vaga -> TT014CC014 = floatval( str_replace(',', '.', str_replace( '.', '', $request -> get( 'TT014CC014' ))));

        // Mostrar endereço?
        if ($request -> get('TT014CC017') == 'on') {
            $vaga -> TT014CC017 = true;
        } else {
            $vaga -> TT014CC017 = false;
        }

        $vaga -> save();

        return redirect() -> back() -> with( 'success', 'Vaga cadastrada com sucesso' );
    }

    /**
    *
    * Editar Vagas
    *
    **/
    public function edit( $id ) {
        $vaga = Vaga::find( $id );
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
        $empresas = Empresa::select(
            DB::raw("concat(TT010CC004,' | ',
            concat( left(TT010CC003, 2), '.',
            mid(TT010CC003, 3, 3), '.',
            mid(TT010CC003, 6, 3), '/',
            mid(TT010CC003, 9, 4), '-',
            right(TT010CC003, 2))) as RazaoSocial"), 'TT010CC000')
            -> orderBy('TT010CC004')
            -> pluck('RazaoSocial', 'TT010CC000');
        return View ( 'admin::vagas.edit', compact([ 'vaga', 'ramos', 'cargos', 'empresas' ]));
    }

    /**
    *
    * Atualizar Vaga
    *
    **/
    public function update( Request $request, $id ) {

        // Dados do Vaga
        if ( $vaga = Vaga::find( $id )) {

            $this -> validate( $request, Vaga::$rules );

            // Dados novos
            $vaga -> fill( $request -> all() );
            $vaga -> TT014CC005 = $vaga -> cargo -> TT003CC001;
            $vaga -> TT014CC008 = preg_replace( '/\D/', '', $request -> get( 'TT014CC008' ));
            $vaga -> TT014CC014 = floatval( str_replace(',', '.', str_replace( '.', '', $request -> get( 'TT014CC014' ))));
            $vaga -> TT014CC005 = $vaga -> cargo -> TT003CC001;

            // Mostrar endereço?
            if ($request -> get('TT014CC017') == 'on') {
                $vaga -> TT014CC017 = true;
            } else {
                $vaga -> TT014CC017 = false;
            }

            $vaga -> save();

            return redirect() -> back() -> with( 'success', 'Vaga atualizada com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
    }

    /**
    *
    * Deletar Vaga
    *
    **/
    public function destroy( $id ) {

        if ( $vaga = Vaga::find( $id )) {

            if ( count( $vaga -> concurso )) {
                return redirect() -> back() -> with( 'alert','Concurso relacionado com a Vaga' );
            }

            $vaga -> delete();

            return redirect() -> back() -> with( 'success', 'Vaga deletada com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
    }





}
