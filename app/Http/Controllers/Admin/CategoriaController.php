<?php

namespace App\Http\Controllers\Admin;

use App\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriaController extends Controller {

    /**
    *
    * Mostrar Categorias
    *
    **/
	public function index(Request $filters) {
        $nomeCategoria = \Request::get('nomeCategoria');
        $categorias = Categoria::where('TT005CC001','like','%'.$nomeCategoria.'%')->orderBy ( 'TT005CC001', 'ASC' ) -> paginate ( 30 );

         // Request via Ajax?
        if ( $filters -> ajax() ) {
            return view( 'admin::categorias.load', compact( 'categorias','nomeCategoria' ));
        } else {
            return View ( 'admin::categorias.index', compact([ 'categorias','nomeCategoria' ]));
        }
	}

    /**
    *
    * Cadastrar Categorias
    *
    **/
    public function create() {
        return View ( 'admin::categorias.create' );
    }

    /**
    *
    * Gravar Categoria
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Categoria::$rules );

        // Dados do categoria
        $categoria = new Categoria();
        $categoria -> fill( $request -> all() );
        $categoria -> save();

        return redirect() -> back() -> with( 'success', 'Categoria cadastrada com sucesso' );
    }

    /**
    *
    * Editar Categorias
    *
    **/
    public function edit( $id ) {
        $categoria = Categoria::find( $id );
        return View ( 'admin::categorias.edit', compact( 'categoria' ));
    }

    /**
    *
    * Atualizar Categoria
    *
    **/
    public function update( Request $request, $id ) {

        if ( $categoria = Categoria::find( $id )) {

            $this -> validate( $request, Categoria::$rules );

            // Dados do categoria
            $categoria = Categoria::find( $id );
            $categoria -> fill( $request -> all() );
            $categoria -> save();

            return redirect() -> back() -> with( 'success', 'Categoria atualizado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Categoria não encontrada' );
    }

    /**
    *
    * Deletar Categoria
    *
    **/
    public function destroy( $id ) {

       if ( $categoria = Categoria::find( $id )) {

            if ( count( $categoria -> descontos )) {
                return redirect() -> back() -> with( 'alert', 'Há descontos ultilizando essa Categoria !' );
            }

            $categoria -> delete();

            return redirect() -> route( 'admin.categorias.index' ) -> with( 'success', 'Categoria deletada com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Categoria não encontrada' );
    }
}
