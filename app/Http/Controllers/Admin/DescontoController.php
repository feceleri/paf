<?php

namespace App\Http\Controllers\Admin;

use DB;
use File;
use App\Estado;
use App\Cidade;
use App\Parceiro;
use App\Desconto;
use App\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;


class DescontoController extends Controller {

    /**
    *
    * Mostrar Descontos
    *
    **/



    public function index(Request $filters) {
      $categorias = Desconto::select( DB::raw('TT005CC000, TT005CC001'))
                -> join('TT005', 'TT012CC002', '=', 'TT005CC000')
                -> groupBy('TT005CC000', 'TT005CC001')
                -> orderBy('TT005CC001')
                -> get();

           $estados = Estado::select( DB::raw('TT007CC000, TT007CC001, count(distinct TT019CC002) as qtde'))
                -> join('TT019', 'TT007CC000', '=', 'TT019CC001')
                -> groupBy('TT007CC000', 'TT007CC001')
                -> orderBy('TT007CC001')
                -> orderBy('qtde', 'DESC')
                -> get();

            $cidades = Cidade::select( DB::raw('TT008CC000, TT008CC001, TT008CC003, count(distinct TT020CC002) as qtde'))
                -> join('TT020', 'TT008CC000', '=', 'TT020CC001')
                -> groupBy('TT008CC000', 'TT008CC001', 'TT008CC003')
                -> orderBy('TT008CC003')
                -> orderBy('qtde', 'DESC')
                -> get();

        $parceiros = Parceiro::select( DB::raw('TT011CC000, TT011CC001'))
                -> orderBy('TT011CC001', 'ASC')
                -> get();



        $tituloDesconto = \Request::get('tituloDesconto');
        $parceiroDesconto = \Request::get('parceiroDesconto');
        $modalidadeDesconto = \Request::get('modalidadeDesconto');
        $ativoDesconto = \Request::get('ativoDesconto');
        $categoriaDesconto = \Request::get('categoriaDesconto');
        $dataDesconto = \Request::get('dataDesconto');
        $statusDesconto = \Request::get('statusDesconto');
        $cidadeDesconto = \Request::get('cidadeDesconto');
        $estadoDesconto = \Request::get('estadoDesconto');
        $descontos = Desconto:: where('TT012CC003','like','%'.$tituloDesconto.'%')
           -> where('TT012CC007','like','%'.$modalidadeDesconto.'%')
           -> where(function($query)
                            {
                                $categoriaDesconto  = \Request::get('categoriaDesconto');
                                if (  $categoriaDesconto != "" ){
                                    $query->   where('TT012CC002','=', $categoriaDesconto  );
                                }
                            })
            -> where('TT012CC006','like','%'.$statusDesconto.'%')
            -> where('TT012.created_at','like','%'.$dataDesconto.'%')
            -> where(function($query)
                            {
                                $parceiroDesconto = \Request::get('parceiroDesconto');
                                if (  $parceiroDesconto != "" ){
                                    $query->   where('TT012CC001','=', $parceiroDesconto );
                                }
                            })
            -> where(function($query)
                            {
                                $ativoDesconto = \Request::get('ativoDesconto');
                                if (  $ativoDesconto != "" ){
                                    $query->   where('TT012CC006','=', $ativoDesconto );
                                }
                            })
            -> orderBy('TT012CC001', 'ASC')
            -> paginate (12);


       //  $vagas->setPath('custom/url');
        // Request via Ajax?
        if ($filters -> ajax()) {
            return view('admin::descontos.load', compact('descontos', 'categorias', 'parceiros', 'estados', 'cidades', 'tituloDesconto', 'parceiroDesconto', 'dataDesconto', 'cidadeDesconto', 'estadoDesconto', 'modalidadeDesconto', 'categoriaDesconto', 'statusDesconto', 'ativoDesconto'));
        } else {
            return View('admin::descontos.index', compact(['descontos',  'categorias', 'parceiros', 'estados', 'cidades', 'tituloDesconto', 'parceiroDesconto', 'dataDesconto', 'cidadeDesconto', 'estadoDesconto', 'modalidadeDesconto', 'categoriaDesconto', 'statusDesconto', 'ativoDesconto']));
        }
	}
//





    /**
    *
    * Mostrar Desconto
    *
    **/
	public function show( $id ) {
        $desconto = Desconto::find( $id );
        return View ( 'admin::descontos.show', compact([ 'desconto' ]));
	}

    /**
    *
    * Ativar Desconto
    *
    **/
	public function active( $id ) {

        // Dados do Desconto
        if ( $desconto = Desconto::find( $id )) {

            // Desconto Habilitado?
            if ( $desconto -> TT012CC006 ) {
                $msg = 'Desconto desabilitado com sucesso';
                $desconto -> TT012CC006 = false;
            } else {
                $msg = 'Desconto habilitado com sucesso';
                $desconto -> TT012CC006 = true;
            }

            $desconto -> save();
//            return redirect() -> back() -> with( 'success', $msg );
            $urlFiltroAtual = \Request::get('urlFiltroAtual');
            $paginacaoAtual = \Request::get('paginacaoAtual');
            return redirect('admin/descontos?page=' .  $paginacaoAtual . $urlFiltroAtual )-> with( 'success', $msg );
        }

        return redirect() -> back() -> with( 'alert', 'Desconto não encontrada' );
	}

    /**
    *
    * Cadastrar Descontos
    *
    **/
    public function create() {
        $estados = Estado::pluck('TT007CC001', 'TT007CC000');
        $categorias = Categoria::orderBy('TT005CC001') -> pluck('TT005CC001', 'TT005CC000');
        $parceiros = Parceiro::where( 'TT011CC002', 'CB' ) -> orderBy('TT011CC001') -> pluck('TT011CC001', 'TT011CC000');
        $cidades = Cidade::select(
            DB::raw("concat(TT008CC003,' - ', TT008CC001) as Name"),'TT008CC000')
            -> orderBy('TT008CC001')
            -> orderBy('TT008CC003')
            -> pluck('Name', 'TT008CC000');

        return View ( 'admin::descontos.create', compact([ 'parceiros', 'categorias', 'estados', 'cidades' ]));
    }

    /**
    *
    * Gravar Desconto
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Desconto::$rules, ['dimensions' => 'Dimensões 380 x 280'] );

        // Dados do Desconto
        $desconto = new Desconto();
        $desconto -> fill( $request -> all() );

        // Abrangência Nacional?
        if ($request -> get('TT012CC008') == 'on') {
            $desconto -> TT012CC008 = true;
            $desconto -> save();
        } else {
            $desconto -> TT012CC008 = false;
            $desconto -> save();
            $desconto -> estados() -> attach($request -> get( 'estados' ));
            $desconto -> cidades() -> attach($request -> get( 'cidades' ));
        }

        //$desconto -> TT012CC004 = 'img/descontos/des-' . rand ( 111111, 999999 ) . '-' . $desconto -> TT012CC000 . '.' . $request -> file( 'TT012CC004' ) -> getClientOriginalExtension();
        $desconto -> TT012CC004 = 'img/descontos/' . $request -> file( 'TT012CC004' ) -> getClientOriginalName();

        $request -> file( 'TT012CC004' ) -> move (
            base_path() . '/public/img/descontos/', $desconto -> TT012CC004
        );

        $desconto -> save();

        return redirect() -> back() -> with( 'success', 'Desconto cadastrado com sucesso' );
    }

    /**
    *
    * Editar Descontos
    *
    **/
    public function edit( $id ) {
        $selectedEstados = [];
        $selectedCidades = [];
        $desconto = Desconto::find( $id );
        $estados = Estado::pluck('TT007CC001', 'TT007CC000');
        $categorias = Categoria::orderBy('TT005CC001') -> pluck('TT005CC001', 'TT005CC000');
        $parceiros = Parceiro::where( 'TT011CC002', 'CB' ) -> orderBy('TT011CC001') -> pluck('TT011CC001', 'TT011CC000');
        $cidades = Cidade::select(
            DB::raw("concat(TT008CC003,' - ', TT008CC001) as Name"),'TT008CC000')
            -> orderBy('TT008CC001')
            -> orderBy('TT008CC003')
            -> pluck('Name', 'TT008CC000');

        foreach($desconto -> estados as $estado) {
            $selectedEstados[] = $estado -> TT007CC000;
        }

        foreach($desconto -> cidades as $cidade) {
            $selectedCidades[] = $cidade -> TT008CC000;
        }

        return View ( 'admin::descontos.edit', compact([ 'desconto', 'parceiros', 'categorias', 'estados', 'cidades', 'selectedEstados', 'selectedCidades' ]));
    }

    /**
    *
    * Atualizar Desconto
    *
    **/
    public function update( Request $request, $id ) {

          $this -> validate($request, Desconto::$updateRules, ['dimensions' => 'Dimensões 380 x 280'] );

        // Dados do Desconto
        if ( $desconto = Desconto::find( $id )) {

            $validator = Validator::make(Input::all(), Desconto::$imageRules);
            if ($validator->passes()){
            // Deletar imagem antiga
                  if ( File::exists( $desconto -> TT012CC004 )) {
                File::delete( $desconto -> TT012CC004 );

            }}
        // Dados novos
            $desconto -> fill( $request -> all() );
            //$desconto -> TT012CC004 = 'img/descontos/des-' . rand ( 111111, 999999 ) . '-' . $desconto -> TT012CC000 . '.' . $request -> file( 'TT012CC004' ) -> getClientOriginalExtension();


             if ($validator->passes()){
            $desconto -> TT012CC004 = 'img/descontos/' . $request -> file( 'TT012CC004' ) -> getClientOriginalName();
            $request -> file( 'TT012CC004' ) -> move ( base_path() . '/public/img/descontos/', $desconto -> TT012CC004 );
            }

            // Zerar abrangência
            $desconto -> estados() -> detach();
            $desconto -> cidades() -> detach();

            // Abrangencia Nacional?
            if ($request -> get( 'TT012CC008' ) == 'on' ){
                $desconto -> TT012CC008 = true;
            } else {
                $desconto -> TT012CC008 = false;
                $desconto -> estados() -> attach($request -> get( 'estados' ));
                $desconto -> cidades() -> attach($request -> get( 'cidades' ));
            }

            $desconto -> save();

            return redirect() -> back() -> with( 'success', 'Desconto atualizado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Desconto não encontrado' );
    }

    /**
    *
    * Deletar Desconto
    *
    **/
    public function destroy( $id ) {

        if ( $desconto = Desconto::find( $id )) {

            // Zerar abrangência
            $desconto -> estados() -> detach();
            $desconto -> cidades() -> detach();

            $desconto -> delete();

            if ( File::exists( $desconto -> TT012CC004 )) {
                File::delete( $desconto -> TT012CC004 );
            }

            return redirect() -> route( 'admin.descontos.index' ) -> with( 'success', 'Desconto deletado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Desconto não encontrado' );
    }
}
