<?php

namespace App\Http\Controllers\Admin;

use DB;
use Excel;
use App\Perito;
use App\Cidade;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Mail\PublicadaperitoMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Rap2hpoutre\FastExcel\FastExcel;
//use Illuminate\Pagination;

class PeritoController extends Controller {

    /**
    *
    * Mostrar peritos
    *
    **/
	public function index(Request $filters) {
        $cidades = Cidade::select( DB::raw('TT008CC000, TT008CC001, TT008CC003'))
                -> join('TT030', 'TT008CC000', '=', 'TT030CC002')
                -> groupBy('TT008CC000', 'TT008CC001', 'TT008CC003')
                -> orderBy('TT008CC003')
                -> get();

        $nomePerito = \Request::get('nomePerito');
        $cidadePerito = \Request::get('cidadePerito');
        $crfPerito = \Request::get('crfPerito');
        $peritos = Perito::where('TT030CC001','like','%'.$nomePerito.'%')
//            ->where('TT030CC007','like','%'.$crfPerito.'%')
            -> where(function($query)
                            {
                                $cidadePerito  = \Request::get('cidadePerito');
                                if (  $cidadePerito != "" ){
                                    $query->   where('TT030CC002','=', $cidadePerito  );
                                }
                            })
             ->where('TT030CC002','like','%'.$cidadePerito.'%')
            -> orderBy('TT030CC001', 'ASC') -> paginate (50);

        // Request via Ajax?
        if ($filters -> ajax()) {
            return view('admin::peritos.load', compact('peritos', 'nomePerito', 'crfPerito','cidadePerito', 'cidades'));
        } else {
            return View('admin::peritos.index', compact(['peritos', 'nomePerito', 'crfPerito', 'cidadePerito', 'cidades']));
        }
	}

    /**
    *
    * Mostrar perito
    *
    **/
	public function show( $id ) {

        if ( $perito = Perito::find( $id )) {
            return View ( 'admin::peritos.show', compact([ 'perito' ]));
        }
        return redirect() -> back() -> with( 'alert', 'Perito não encontrado' );
	}

    /**
    *
    * Download xls perito
    *
    **/

 public function excel($type)	{
     $perito = Perito::orderBy('created_at', 'DESC')->get();
//     var_dump($perito); die();

//     FastExcel::data($perito)->export('file.xlsx');

     // Export all users
    return (new FastExcel($perito))->export('file.csv');
     }


    public function downloadExcel($type)
	{

		$data = Perito::get()->toArray();
		return Excel::create('Peritos_Judiciais', function ($excel) {

        $excel->sheet('Peritos_Judiciais', function ($sheet) {

        // first row styling and writing content
        $sheet->mergeCells('A1:AA1');
        $sheet->row(1, function ($row) {
            $row->setFontFamily('Arial');
            $row->setFontSize(20);
            $row->setFontWeight('bold');
        });

        $sheet->row(1, array('Peritos Judiciais'));

        // second row styling and writing content
        $sheet->row(2, function ($row) {

            // call cell manipulation methods
            $row->setFontFamily('Arial');
            $row->setFontSize(15);
            $row->setFontWeight('bold');

        });
//
        $sheet->row(2, array('ID','Nome', 'ID Cidade', 'CPF', 'RG', 'Nacionalidade', 'Estado Civil', 'CRF', 'Endereço Completo','Bairro', 'CEP', 'E-mail', 'Telefone', 'Celular','', 'Especialização 1',  'Instituição 1', 'Carga Horária 1', 'Ano de Conclusão 1', 'Especialização 2', 'Instituição 2', 'Carga Horária 2', 'Ano de Conclusão 2', 'Especialização 3', 'Instituição 3', 'Carga Horária 3', 'Ano de Conclusão 3'));

        // getting data to display - in my case only one record
        $peritos = Perito::get()->toArray();

        // setting column names for data - you can of course set it manually
        $sheet->appendRow(array_keys($peritos[0])); // column names

        // getting last row number (the one we already filled and setting it to bold
        $sheet->row($sheet->getHighestRow(), function ($row) {
            $row->setFontWeight('bold');
        });

        // putting users data as next rows
        foreach ($peritos as $perito) {
            $sheet->appendRow($perito);
        }
    });

        })->export($type);
	}





    /**
    *
    * Cadastrar peritos
    *
    **/
    public function create() {

        return View ( 'admin::peritos.create');
    }

    /**
    *
    * Gravar perito
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Perito::$rules );

      // Dados da Vaga
        $perito = new Perito();
        $perito -> fill( $request -> all() );

        // Limpar Formatação





         $perito -> TT030CC003 = preg_replace( '/\D/', '', $request -> get( 'TT030CC003' ));


        $perito -> save();

        return redirect() -> back() -> with( 'success', 'Perito cadastrado com sucesso' );
    }

    /**
    *
    * Editar peritos
    *
    **/
    public function edit( $id ) {
        $perito = Perito::find( $id );



        return View ( 'admin::peritos.edit', compact([ 'perito' ]));
    }

    /**
    *
    * Atualizar perito
    *
    **/
    public function update( Request $request, $id ) {

        // Dados do perito
        if ( $perito = Perito::find( $id )) {

            $this -> validate( $request, Perito::$rules );

               // Dados da Vaga

        $perito -> fill( $request -> all() );

        // Limpar Formatação


        $perito -> TT030CC003 = preg_replace( '/\D/', '', $request -> get( 'TT030CC003' ));

         $perito -> save();

            return redirect() -> back() -> with( 'success', 'Perito atualizado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Perito não encontrado' );
    }

    /**
    *
    * Deletar Perito
    *
    **/
    public function destroy( $id ) {

        if ( $perito = Perito::find( $id )) {

            if ( count( $perito -> concurso )) {
                return redirect() -> back() -> with( 'alert','Concurso relacionado com a perito' );
            }

            $perito -> delete();

            return redirect() -> back() -> with( 'success', 'Perito deletado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Perito não encontrado' );
    }
}
