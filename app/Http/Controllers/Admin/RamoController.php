<?php

namespace App\Http\Controllers\Admin;

use App\Ramo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RamoController extends Controller {

    /**
    *
    * Mostrar Ramos
    *
    **/
	public function index(Request $filters) {
        $nomeRamo = \Request::get('nomeRamo');

        $ramos = Ramo::where('TT002CC001','like','%'.$nomeRamo.'%')->orderBy('TT002CC001') -> paginate(30);


        // Request via Ajax?
        if ( $filters -> ajax() ) {
            return view( 'admin::ramos.load', compact( 'ramos','nomeRamo' ));
        } else {
            return View ( 'admin::ramos.index', compact([ 'ramos','nomeRamo' ]));
        }
	}

    /**
    *
    * Cadastrar Ramos
    *
    **/
    public function create() {
        return View ( 'admin::ramos.create' );
    }

    /**
    *
    * Gravar Ramo
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Ramo::$rules );

        // Dados do Ramo
        $ramo = new Ramo();
        $ramo -> fill( $request -> all() );
        $ramo -> save();

        return redirect() -> back() -> with( 'success', 'Ramo cadastrado com sucesso' );
    }

    /**
    *
    * Editar Artigos
    *
    **/
    public function edit( $id ) {
        $ramo = Ramo::find( $id );
        return View ( 'admin::ramos.edit', compact( 'ramo' ));
    }

    /**
    *
    * Atualizar Artigo
    *
    **/
    public function update( Request $request, $id ) {

        // Dados do Ramo
        if ( $ramo = Ramo::find( $id )) {

            $this -> validate( $request, Ramo::$rules );

            $ramo -> fill( $request -> all() );
            $ramo -> save();

            return redirect() -> back() -> with( 'success', 'Ramo atualizado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Ramo de Atividade não encontrado' );
    }

    /**
    *
    * Deletar Ramo
    *
    **/
    public function destroy( $id ) {

       if ( $ramo = Ramo::find( $id )) {

            if ( count( $ramo -> vagas )) {
                return redirect() -> back() -> with( 'alert', 'Vagas relacionadas com o Ramo de Atividade' );
            } elseif ( count( $ramo -> empresas )) {
                return redirect() -> back() -> with( 'alert', 'Empresas relacionadas com o Ramo de Atividade' );
            }

            $ramo -> delete();

            return redirect() -> back() -> with( 'success', 'Ramo deletado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Ramo de Atividade não encontrado' );
    }
}
