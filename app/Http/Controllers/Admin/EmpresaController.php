<?php

namespace App\Http\Controllers\Admin;
use DB;
use Route;
use App\Ramo;
use App\Empresa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\Usuario;


class EmpresaController extends Controller {

    /**
    *
    * Mostrar Empresas
    *
    **/
	public function index(Request $filters) {
        $buscaEmpresas = Empresa::orderBy('TT010CC004') -> get();
        $razaoEmpresa = \Request::get('razaoEmpresa');
        $cnpjEmpresa = \Request::get('cnpjEmpresa');
        $empresaSelected = \Request::get('empresaSelected');
        $dataEmpresa = \Request::get('dataEmpresa');
        $empresas = Empresa::where(function($query)
                // Aqui coloquei pelo ID do tema, que deve ser identico e por isso utilizo o "=". Mas apenas executa a função se $temaArtigo não estiver vazia.
                            {
                                 $razaoEmpresa = \Request::get('razaoEmpresa');
                                $cnpjEmpresa = \Request::get('cnpjEmpresa');
                                 if (  $razaoEmpresa != "" ){   $query ->   where('TT010CC000','=', $razaoEmpresa );     }
                                 if (  $cnpjEmpresa != "" ){   $query ->   where('TT010CC000','=', $cnpjEmpresa );     }

                            })
        -> where('created_at','like','%'.$dataEmpresa.'%')
        -> orderBy('TT010CC004') -> paginate (100);

        // Request via Ajax?
        if ( $filters -> ajax() ) {
            return view( 'admin::empresas.load', compact( 'empresas', 'empresaSelected', 'dataEmpresa', 'buscaEmpresas','razaoEmpresa','cnpjEmpresa' ));
        } else {
            return View ( 'admin::empresas.index', compact([ 'empresas' , 'empresaSelected', 'dataEmpresa', 'buscaEmpresas','razaoEmpresa','cnpjEmpresa']));
        }
	}
    /**
    *
    * Mostrar Empresa
    *
    **/

    public function show( $id ) {
        $empresa = Empresa::find( $id );
        return View ( 'admin::empresas.show', compact([ 'empresa' ]));
	}

    /**
    *
    * Cadastrar Empresas
    *
    **/
    public function create() {
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        return View ( 'admin::empresas.create', compact( 'ramos' ));
    }

    /**
    *
    * Gravar Empresa
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Empresa::$rules );

        $empresa = Empresa::firstOrNew([ 'TT010CC003' => preg_replace( '/\D/', '', $request -> get( 'TT010CC003' ))]);

        if ( $empresa -> TT010CC000 > 0 ) {
            return redirect() -> back() -> with( 'alert', 'CNPJ já cadastrado' );
        }

        $empresa -> fill( $request -> all() );
        $empresa -> TT010CC003 = preg_replace( '/\D/', '', $request -> get( 'TT010CC003' ));
        $empresa -> TT010CC006 = preg_replace( '/\D/', '', $request -> get( 'TT010CC006' ));
        $empresa -> TT010CC009 = preg_replace( '/\D/', '', $request -> get( 'TT010CC009' ));
        $empresa -> save();


        $usuario = new Usuario();

        // Limpar Formatação

        $usuario -> TT001CC001 = $empresa -> TT010CC004;
        $usuario -> TT001CC002 = $empresa -> TT010CC003;
        $usuario -> TT001CC003 = bcrypt('123456');
        $usuario -> TT001CC004 = $empresa -> TT010CC010;
        $usuario -> TT001CC006 = $empresa -> TT010CC003;
        $usuario-> role_id = 3;
        $usuario -> save();



        return redirect() -> back() -> with( 'success', 'Empresa cadastrado com sucesso' );
    }

    /**
    *
    * Editar Empresas
    *
    **/
    public function edit( $id ) {
        $empresa = Empresa::find( $id );
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        return View ( 'admin::empresas.edit', compact([ 'empresa', 'ramos' ]));
    }

    /**
    *
    * Atualizar Empresa
    *
    **/
    public function update( Request $request, $id ) {

        // Dados do Empresa
        if ( $empresa = Empresa::find( $id )) {

            $this -> validate( $request, Empresa::$rules );

            if ( $empresa -> TT010CC003 != preg_replace( '/\D/', '', $request -> get( 'TT010CC003' ))  ) {

                $empresaAux = Empresa::firstOrNew( ['TT010CC003' => preg_replace( '/\D/', '', $request -> get( 'TT010CC003' ))]);

                if ( $empresaAux -> TT010CC000 > 0 ) {
                    return redirect() -> back() -> with( 'alert', 'CNPJ já cadastrado' );
                }
            }

            $empresa -> fill( $request -> all() );
            $empresa -> TT010CC003 = preg_replace( '/\D/', '', $request -> get( 'TT010CC003' ));
            $empresa -> TT010CC006 = preg_replace( '/\D/', '', $request -> get( 'TT010CC006' ));
            $empresa -> TT010CC009 = preg_replace( '/\D/', '', $request -> get( 'TT010CC009' ));
            $empresa -> save();

            return redirect() -> back() -> with( 'success', 'Empresa atualizada com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Empresa não encontrada' );
    }

    /**
    *
    * Deletar Empresa
    *
    **/
    public function destroy( $id ) {

        if ( $empresa = Empresa::find( $id )) {

            if ( count( $empresa -> vagas )) {
                return redirect() -> back() -> with( 'alert', 'Vagas relacionadas com a Empresa' );
            }

            $empresa -> delete();

            return redirect() -> route( 'admin.empresas.index' ) -> with( 'success', 'Empresa deletada com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Empresa não encontrada' );
    }
}
