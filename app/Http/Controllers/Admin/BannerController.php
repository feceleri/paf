<?php

namespace App\Http\Controllers\Admin;

use DB;
use File;
use App\Banner;
use App\Parceiro;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;


class BannerController extends Controller {

    /**
    *
    * Mostrar Banners
    *
    **/
	public function index(Request $filters) {

        $parceiros = Banner::select( DB::raw('TT011CC000, TT011CC001'))
                -> join('TT011', 'TT013.TT013CC001', '=', 'TT011.TT011CC000')
                -> groupBy('TT011CC000', 'TT011CC001')
                -> orderBy('TT011CC001')
                -> get();

        $parceiroBanner = \Request::get('parceiroBanner');
        $hoje = Carbon::today();
        $banners = Banner::where('TT013CC001','like','%'.$parceiroBanner.'%')
            -> orderBy ( 'created_at', 'DESC' ) -> paginate ( 8 );

        // Request via Ajax?
        if ( $filters -> ajax() ) {
            return view( 'admin::banners.load', compact( 'banners', 'hoje', 'parceiros', 'parceiroBanner' ));
        } else {
            return View ( 'admin::banners.index', compact([ 'banners', 'hoje', 'parceiros', 'parceiroBanner' ]));
        }
	}

    /**
    *
    * Cadastrar Banners
    *
    **/
    public function create() {
        $parceiros = Parceiro::orderBy('TT011CC001')->pluck('TT011CC001', 'TT011CC000');
        return View ( 'admin::banners.create', compact( 'parceiros' ));
    }
    
    /**
    *
    * Gravar Banner
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Banner::$updateRules, ['dimensions' => 'Dimensões 736 x 420'] );

        // Dados do Banner
        $banner = new Banner();
        $banner -> fill( $request -> all() );

        // Parceiro indicado?
        if ( $banner -> TT013CC001 == "" ) {
            $banner -> TT013CC001 = null;
        }

        $banner -> save();

        //$banner -> TT013CC005 = 'img/banners/ban-' . rand ( 111111, 999999 ) . '-' . $banner -> TT013CC000 . '.' . $request -> file( 'TT013CC005' ) -> getClientOriginalExtension();
        $banner -> TT013CC005 = 'img/banners/' . $request -> file( 'TT013CC005' ) -> getClientOriginalName();

        $request -> file( 'TT013CC005' ) -> move ( base_path() . '/public/img/banners/', $banner -> TT013CC005 );

        $banner -> save();

        return redirect() -> back() -> with( 'success', 'Banner cadastrado com sucesso' );
    }

    /**
    *
    * Editar Banners
    *
    **/
    public function edit( $id ) {
        $banner = Banner::find( $id );
        $parceiros = Parceiro::pluck( 'TT011CC001', 'TT011CC000' );
        return View ( 'admin::banners.edit', compact([ 'banner', 'parceiros' ]));
    }

    /**
    *
    * Atualizar Banner
    *
    **/
    public function update( Request $request, $id ) {


        // Dados do Banner
        if ( $banner = Banner::find( $id )) {

            $this -> validate( $request, Banner::$updateRules, ['dimensions' => 'Dimensões 736 x 420'] );
            $validator = Validator::make(Input::all(), Banner::$imageRules);
            if ($validator->passes()){
                // Deletar imagem antiga
                if ( File::exists( $banner -> TT013CC005 )) {
                    File::delete( $banner -> TT013CC005 );
                }
                // Dados novos
                $banner -> fill( $request -> all() );

                $banner -> TT013CC005 = 'img/banners/' . $request -> file( 'TT013CC005' ) -> getClientOriginalName();

               $request -> file( 'TT013CC005' ) -> move ( base_path() . '/public/img/banners/', $banner -> TT013CC005 );

            }
            else{
                $banner -> fill( $request -> all() );
                }
            // Parceiro indicado?
            if ( $banner -> TT013CC001 == "" ) {
                $banner -> TT013CC001 = null;
            }
            $banner -> save();
            return redirect() -> back() -> with( 'success', 'Banner atualizado com sucesso' );
        }
        return redirect() -> back() -> with( 'alert', 'Banner não encontrado' );
    }

    /**
    *
    * Deletar Banner
    *
    **/
    public function destroy( $id ) {

        if ( $banner = Banner::find( $id )) {

            $banner -> delete();

            if ( File::exists( $banner -> TT013CC005 )) {
                File::delete( $banner -> TT013CC005 );
            }

            return redirect() -> route( 'admin.banners.index' ) -> with( 'success', 'Banner deletado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Banner não encontrado' );
    }
}
