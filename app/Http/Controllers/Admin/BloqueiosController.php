<?php

namespace App\Http\Controllers\Admin;

use App\BloqueiosTemporarios;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BloqueiosController extends Controller {

    /**
    *
    * Mostrar Linguas
    *
    **/
	public function index(Request $filters) {

        $bloqueios = BloqueiosTemporarios::orderBy('TT034CC004') -> paginate (30);


            return view( 'admin::bloqueios.index', compact('bloqueios'));


	}

    /**
    *
    * Cadastrar Lingua
    *
    **/
    public function create() {
        return View ( 'admin::bloqueios.create' );
    }

    /**
    *
    * Gravar Lingua
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, BloqueiosTemporarios::$rules );

        // Dados do lingua
//        $lingua = new Lingua();
//        $lingua -> fill( $request -> all() );
//        $lingua -> save();
//
//        return redirect() -> back() -> with('success', 'Lingua cadastrado com sucesso');
    }

    /**
    *
    * Editar Lingua
    *
    **/
    public function edit( $id ) {
//        $lingua = Lingua::find( $id );
//        return View ('admin::linguas.edit', compact('lingua'));
    }

    /**
    *
    * Atualizar Lingua
    *
    **/
    public function update( Request $request, $id ) {

//        if ($lingua = Lingua::find($id)) {
//
//            $this -> validate( $request, Lingua::$rules );
//
//            // Dados do lingua
//            $lingua = Lingua::find($id);
//            $lingua -> fill($request -> all());
//            $lingua -> save();
//
//            return redirect() -> back() -> with('success', 'Lingua atualizado com sucesso');
//        }
//
//        return redirect() -> route('admin.linguas.index') -> with('alert', 'Lingua não encontrado');
    }

    /**
    *
    * Deletar Lingua
    *
    **/
    public function destroy($id){

//       if ($lingua = Lingua::find($id)) {
//
//            if (count($lingua -> idiomas)) {
//                return redirect() -> back() -> with('alert', 'Registro de idiomas relacionadas com o língua');
//            }
//
//            $lingua -> delete();
//
//            return redirect() -> route('admin.linguas.index') -> with('success', 'Língua deletada com sucesso');
//        }
//
//        return redirect() -> back() -> with('alert', 'Língua não encontrada');
    }
}
