<?php

namespace App\Http\Controllers\Admin;

use DB;
use File;
use App\Tema;
use App\Artigo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Search\Artigo\ArtigoSearch;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class ArtigoController extends Controller {

    /**
    *
    * Mostrar Artigos
    *
    **/
	public function index(Request $filters) {


        $temas = Artigo::select( DB::raw('TT004CC000, TT004CC002'))
                -> join('TT004', 'TT009.TT009CC001', '=', 'TT004.TT004CC000')
                -> groupBy('TT004CC000', 'TT004CC002')
                -> orderBy('TT004CC002')
                -> get();


        $tituloArtigo = \Request::get('tituloArtigo');
        $dataArtigo = \Request::get('dataArtigo');
        $temaArtigo = \Request::get('temaArtigo');
        $artigos = Artigo::where(function($query)
                // Aqui coloquei pelo ID do tema, que deve ser identico e por isso utilizo o "=". Mas apenas executa a função se $temaArtigo não estiver vazia.
                            {
                                $temaArtigo = \Request::get('temaArtigo');
                                if (  $temaArtigo != "" ){
                                    $query->  where('TT009CC001','=', $temaArtigo );

                                }
                            })
            -> where('TT009CC002','like','%'.$tituloArtigo.'%')
              -> where('created_at','like','%'.$dataArtigo.'%')

            ->orderBy ( 'created_at', 'DESC' ) -> paginate ( 6 );

        // Request via Ajax?
        if ( $filters -> ajax() ) {
            return view( 'admin::artigos.load', compact( 'artigos', 'temas', 'tituloArtigo', 'dataArtigo', 'temaArtigo' ));
        } else {
            return View ( 'admin::artigos.index', compact([ 'artigos', 'temas', 'tituloArtigo', 'dataArtigo', 'temaArtigo' ]));
        }
	}

    /**
    *
    * Mostrar Artigo
    *
    **/
	public function show( $id ) {
        $artigo = Artigo::find( $id );
        return View ( 'admin::artigos.show', compact([ 'artigo' ]));
	}

    /**
    *
    * Ativar Artigo
    *
    **/
	public function active( $id ) {

        // Dados do Artigo
        if ( $artigo = Artigo::find( $id )) {

            // Artigo Habilitado?
            if ( $artigo -> TT009CC005 ) {
                $msg = 'Artigo desabilitado com sucesso';
                $artigo -> TT009CC005 = false;
            } else {
                $msg = 'Artigo habilitado com sucesso';
                $artigo -> TT009CC005 = true;
            }

            $artigo -> save();
//            return redirect() -> back() -> with( 'success', $msg );
            $urlFiltroAtual = \Request::get('urlFiltroAtual');
            $paginacaoAtual = \Request::get('paginacaoAtual');
            return redirect('admin/artigos?page=' .  $paginacaoAtual . $urlFiltroAtual )-> with( 'success', $msg );
        }

        return redirect() -> back() -> with( 'alert', 'Artigo não encontrada' );
	}

    /**
    *
    * Cadastrar Artigos
    *
    **/
    public function create() {
        $temas = Tema::pluck('TT004CC002', 'TT004CC000');
        return View ( 'admin::artigos.create', compact( 'temas' ));
    }

    /**
    *
    * Gravar Artigo
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Artigo::$rules, ['dimensions' => 'Dimensões 380 x 280'] );

        // Dados do Artigo
        $artigo = new Artigo();
        $artigo -> fill( $request -> all() );
        $artigo -> save();

        //$artigo -> TT009CC003 = 'img/artigos/art-' . rand ( 111111, 999999 ) . '-' . $artigo -> TT009CC000 . '.' . $request -> file( 'TT009CC003' ) -> getClientOriginalExtension();
        $artigo -> TT009CC003 = 'img/artigos/' . $request -> file( 'TT009CC003' ) -> getClientOriginalName();

        $request -> file( 'TT009CC003' ) -> move (

            base_path() . '/public/img/artigos/', $artigo -> TT009CC003

        );

        $artigo -> save();

        return redirect() -> back() -> with( 'success', 'Artigo cadastrado com sucesso' );
    }

    /**
    *
    * Editar Artigos
    *
    **/
    public function edit( $id ) {
        $artigo = Artigo::find( $id );
        $temas = Tema::pluck('TT004CC002', 'TT004CC000');
        return View ( 'admin::artigos.edit', compact([ 'artigo', 'temas' ]));
    }

    /**
    *
    * Atualizar Artigo
    *
    **/
    public function update( Request $request, $id ) {
        $this -> validate( $request, Artigo::$updateRules, ['dimensions' => 'Dimensões 380 x 280'] );
        // Dados do Artigo
        if ( $artigo = Artigo::find( $id )) {
            // Dados novos

            $validator = Validator::make(Input::all(), Artigo::$imageRules);
            if ($validator->passes()){
                // Deletar imagem antiga
                if ( File::exists( $artigo -> TT009CC003 )) {
                File::delete( $artigo -> TT009CC003 );
                }

            }
             $artigo -> fill( $request -> all() );
            //$artigo -> TT009CC003 = 'img/artigos/art-' . rand ( 111111, 999999 ) . '-' . $artigo -> TT009CC000 . '.' . $request -> file( 'TT009CC003' ) -> getClientOriginalExtension();

            if ($validator->passes()){
                $artigo -> TT009CC003 = 'img/artigos/' . $request -> file( 'TT009CC003' ) -> getClientOriginalName();
            $request -> file( 'TT009CC003' ) -> move ( base_path() . '/public/img/artigos/', $artigo -> TT009CC003 );
            }

            $artigo -> save();
            return redirect() -> back() -> with( 'success', 'Artigo atualizado com sucesso' );
        }
        return redirect() -> back() -> with( 'alert', 'Artigo não encontrado' );
    }

    /**
    *
    * Deletar Artigo
    *
    **/
    public function destroy( $id ) {

        if ( $artigo = Artigo::find( $id )) {

            $artigo -> delete();

            if ( File::exists( $artigo -> TT009CC003 )) {
                File::delete( $artigo -> TT009CC003 );
            }

            return redirect() -> route( 'admin.artigos.index' ) -> with( 'success', 'Artigo deletado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Artigo não encontrado' );
    }
}
