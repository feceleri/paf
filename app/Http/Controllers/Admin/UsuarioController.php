<?php

namespace App\Http\Controllers\Admin;

use DB;
use Excel;
use App\Usuario;
use App\Curriculo;
use App\Empresa;
use App\Cidade;
use Illuminate\Http\Request;
use Carbon\Carbon;
//use App\Mail\PublicadaperitoMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Vaga;
//use Illuminate\Pagination;

class UsuarioController extends Controller {

    /**
    *
    * Mostrar Usuarios
    *
    **/
	public function index(Request $filters) {
        $nomeUsuario = \Request::get('nomeUsuario');
        $loginUsuario = \Request::get('loginUsuario');
        $docUsuario = \Request::get('docUsuario');
        $emailUsuario = \Request::get('emailUsuario');
        $tipoUsuario = \Request::get('tipoUsuario');
        $statusUsuario = \Request::get('statusUsuario');
        $usuarios = Usuario::where('TT001CC001','like','%'.$nomeUsuario.'%')
            ->where('TT001CC002','like','%'.$loginUsuario.'%')
            ->where('TT001CC004','like','%'.$emailUsuario.'%')
            ->where('TT001CC006','like','%'.$docUsuario.'%')
            ->where(function($query)
                            {
                                $tipoUsuario  = \Request::get('tipoUsuario');
                                if (  $tipoUsuario != "" ){
                                    $query->   where('role_id','=', $tipoUsuario  );
                                }
                            })
            -> where(function($query)
                            {
                               $statusUsuario = \Request::get('statusUsuario');
                                if (  $statusUsuario != "" ){
                                    $query->   where('TT001CC005','=', $statusUsuario  );
                                }
                            })
            -> orderBy('TT001CC001', 'ASC') -> paginate (50);


        // Request via Ajax?
        if ($filters -> ajax()) {
            return view('admin::usuarios.load', compact('usuarios', 'tipoUsuario', 'statusUsuario', 'nomeUsuario', 'loginUsuario', 'docUsuario', 'emailUsuario', 'emailUsuario'));
        } else {
            return View('admin::usuarios.index', compact(['usuarios', 'tipoUsuario', 'statusUsuario', 'nomeUsuario', 'loginUsuario', 'docUsuario', 'emailUsuario', 'emailUsuario']));
        }
	}

    public function active( $id ) {


        // Dados do Usuário
        if ( $usuario = Usuario::find( $id )) {
            // Usuário Habilitada?
            if ( $usuario -> TT001CC005 ) {
                $msg = 'Usuário desabilitado com sucesso';
                $usuario -> TT001CC005 = false;

            } else {
                $msg = 'Usuário habilitado com sucesso. Para Empresas, checar condição de vaga ativa para pesquisa de CV.';
                $usuario -> TT001CC005 = true;

            }

            $usuario -> save();
            $urlFiltroAtual = \Request::get('urlFiltroAtual');
            $paginacaoAtual = \Request::get('paginacaoAtual');
            return redirect('admin/usuarios?page=' .  $paginacaoAtual . $urlFiltroAtual )-> with( 'success', $msg );

        }
        return redirect() -> back() -> with( 'alert', 'Usuário não encontrado' );
	}
    /**
    *
    * Mostrar Usuario
    *
    **/
	public function show( $id ) {

        if ( $usuario = Usuario::find( $id )) {

          if($usuario->isFarmaceutico()){
              /* Aqui selecionando os curriculos com o usuario */
              $curriculo = Curriculo::where('TT021CC001','=', $id ) -> first ( );
              /* Aqui selecionando os curriculos com o usuario */
              $tipoUsuario = 'farmaceutico';
              return View ( 'admin::usuarios.show', compact([ 'usuario', 'id', 'curriculo', 'tipoUsuario']));
          }

         if ($usuario->isEmpresa()){
                /* Aqui selecionando as vagas relacionadas com a empresa */
                $cnpj = $usuario->TT001CC006;
                $empresa = Empresa::where('TT010CC003','=', $cnpj ) -> first ( );
                $idempresa = $empresa -> TT010CC000;
                $vagas = Vaga::where('TT014CC004','=', $idempresa ) -> get ( );
                /* Aqui selecionando as vagas relacionadas com a empresa */
                $tipoUsuario = 'empresa';
                return View ( 'admin::usuarios.show', compact([ 'usuario', 'id', 'vagas', 'tipoUsuario' ]));

            }


                $tipoUsuario = 'admin';
                return View ( 'admin::usuarios.show', compact([ 'usuario', 'id',  'tipoUsuario' ]));


        }


        return redirect() -> back() -> with( 'alert', 'Usuário não encontrado' );
	}

    /**
    *
    * Ativar Usuario
    *
    **/


    public function downloadExcel($type)
	{
		$data = Usuario::get()->toArray();
		return Excel::create('Usuarios_tabela', function ($excel) {

        $excel->sheet('Usuarios_tabela', function ($sheet) {

        // first row styling and writing content
        $sheet->mergeCells('A1:AA1');
        $sheet->row(1, function ($row) {
            $row->setFontFamily('Arial');
            $row->setFontSize(20);
            $row->setFontWeight('bold');
        });

        $sheet->row(1, array('Usuarios_tabela'));

        // second row styling and writing content
        $sheet->row(2, function ($row) {

            // call cell manipulation methods
            $row->setFontFamily('Arial');
            $row->setFontSize(15);
            $row->setFontWeight('bold');

        });

        $sheet->row(2, array('ID','Nome', 'Login', 'Senha', 'e-mail',  ' ', ' ', ' ', 'tipo', 'cpf/cnpj'));

        // getting data to display - in my case only one record
        $usuarios = Usuario::get()->toArray();

        // setting column names for data - you can of course set it manually
        $sheet->appendRow(array_keys($usuarios[0])); // column names

        // getting last row number (the one we already filled and setting it to bold
        $sheet->row($sheet->getHighestRow(), function ($row) {
            $row->setFontWeight('bold');
        });

        // putting users data as next rows
        foreach ($usuarios as $usuario) {
            $sheet->appendRow($usuario);
        }
    });

        })->export($type);
	}





    /**
    *
    * Cadastrar Usuario
    *
    **/
    public function create() {

        return View ( 'admin::usuarios.create');
    }

    /**
    *
    * Gravar Usuario
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Usuario::$createrules );
        // Dados da Vaga
        $usuario = new Usuario();
        $usuario -> fill( $request -> all() );
        // Limpar Formatação
        $usuario -> TT001CC006 = preg_replace( '/\D/', '', $request -> get( 'TT001CC006' ));
        $usuario -> TT001CC003 =  bcrypt($request -> get( 'TT001CC003' ));
        $usuario -> TT001CC005 =  true;
        $usuario -> save();
        return redirect() -> back() -> with( 'success', 'Usuário cadastrado com sucesso' );
    }

    /**
    *
    * Editar Usuario
    *
    **/
    public function edit( $id ) {
        $usuario = Usuario::find( $id );
        return View ( 'admin::usuarios.edit', compact([ 'usuario' ]));
    }

    /**
    *
    * Atualizar Usuario
    *
    **/
    public function update( Request $request, $id ) {

        // Dados do perito
        if ( $usuario = Usuario::find( $id )) {

            $this -> validate( $request, Usuario::$editrules );

               // Dados da Vaga

        $usuario -> fill( $request -> all() );

        // Limpar Formatação


        $usuario -> TT001CC006 = preg_replace( '/\D/', '', $request -> get( 'TT001CC006' ));

         $usuario -> save();

            return redirect() -> back() -> with( 'success', 'Usuário atualizado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Usuário não encontrado' );
    }

    /**
    *
    * Deletar Usuario
    *
    **/
    public function destroy( $id ) { // não deleta se tem cv relacionado ou empresa relacionada

            $usuario = Usuario::find( $id );

            $curriculo = Curriculo::where('TT021CC001','=', $usuario ->TT001CC000 ) -> first ( );

            $empresa = Empresa::where('TT010CC003','=', $usuario ->TT001CC002 ) -> first ( );

//                        var_dump($empresa); die();

            if ( $usuario ) {
                if ( count($curriculo) ) {
                    return redirect() -> back() -> with( 'alert','Usuário com currículo relacionado' );

            }

             elseif ( count($empresa) ) {
                    return redirect() -> back() -> with( 'alert','Empresa relacionada com este registro' );
            }

            $usuario -> delete();

            return redirect() -> back() -> with( 'success', 'Usuário deletado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Usuário não encontrado' );
    }

}
