<?php

namespace App\Http\Controllers\Admin;

use App\Cargo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CargoController extends Controller {

    /**
    *
    * Mostrar Cargos
    *
    **/
	public function index(Request $filters) {
        $nomeCargo = \Request::get('nomeCargo');
        $statusNVaga = \Request::get('statusNVaga');
        $statusCurriculo = \Request::get('statusCurriculo');
        $cargos = Cargo::where('TT003CC001','like','%'.$nomeCargo.'%')
             -> where(function($query)
                            {
                               $statusNVaga = \Request::get('statusNVaga');
                                if (  $statusNVaga != "" ){
                                    $query->   where('TT003CC002','=', $statusNVaga  );
                                }
                            })
            -> where(function($query)
                            {
                               $statusCurriculo = \Request::get('statusCurriculo');
                                if (  $statusCurriculo != "" ){
                                    $query->   where('TT003CC003','=', $statusCurriculo  );
                                }
                            })
        ->orderBy('TT003CC001') -> paginate (30);

        // Request via Ajax?
        if ( $filters -> ajax() ) {
            return view( 'admin::cargos.load', compact('cargos','nomeCargo','statusNVaga','statusCurriculo'));
        } else {
            return View ( 'admin::cargos.index', compact(['cargos','nomeCargo','statusNVaga','statusCurriculo']));
        }
	}


    /**
    *
    * Ativar Cargo
    *
    **/

	public function active( $id ) {


        // Dados do Cargo
        if ( $cargo = Cargo::find( $id )) {
            // Cargo Habilitada?
            if ( $cargo -> TT003CC002 ) {
                $msg = 'Cargo desabilitado com sucesso para criação de novas vagas';
                $cargo -> TT003CC002 = false;

            } else {
                $msg = 'Cargo habilitado com sucesso para criação de novas vagas';
                $cargo -> TT003CC002 = true;

            }

            $cargo -> save();
            $urlFiltroAtual = \Request::get('urlFiltroAtual');
            $paginacaoAtual = \Request::get('paginacaoAtual');
            return redirect('admin/cargos?page=' .  $paginacaoAtual . $urlFiltroAtual )-> with( 'success', $msg );

        }
        return redirect() -> back() -> with( 'alert', 'Cargo não encontrado' );
	}


    public function activecv( $id ) {


        // Dados do Cargo
        if ( $cargo = Cargo::find( $id )) {
            // Cargo Habilitada?
            if ( $cargo -> TT003CC003 ) {
                $msg = 'Cargo desabilitado com sucesso para cadastro de experiências no currículo do farmacêutico';
                $cargo -> TT003CC003 = false;

            } else {
                $msg = 'Cargo habilitado com sucesso para cadastro de experiências no currículo do farmacêutico';
                $cargo -> TT003CC003 = true;

            }

            $cargo -> save();
            $urlFiltroAtual = \Request::get('urlFiltroAtual');
            $paginacaoAtual = \Request::get('paginacaoAtual');
            return redirect('admin/cargos?page=' .  $paginacaoAtual . $urlFiltroAtual )-> with( 'success', $msg );

        }
        return redirect() -> back() -> with( 'alert', 'Cargo não encontrado' );
	}



    /**
    *
    * Cadastrar Cargos
    *
    **/
    public function create() {
        return View ( 'admin::cargos.create' );
    }

    /**
    *
    * Gravar Cargo
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Cargo::$rules );

        // Dados do cargo
        $cargo = new Cargo();
        $cargo -> fill( $request -> all() );
        $cargo -> save();

        return redirect() -> back() -> with('success', 'Cargo cadastrado com sucesso');
    }

    /**
    *
    * Editar Cargos
    *
    **/
    public function edit( $id ) {
        $cargo = Cargo::find( $id );
        return View ('admin::cargos.edit', compact('cargo'));
    }

    /**
    *
    * Atualizar Cargo
    *
    **/
    public function update( Request $request, $id ) {

        if ($cargo = Cargo::find($id)) {

            $this -> validate( $request, Cargo::$rules );

            // Dados do cargo
            $cargo = Cargo::find($id);
            $cargo -> fill($request -> all());
            $cargo -> save();

            return redirect() -> back() -> with('success', 'Cargo atualizado com sucesso');
        }

        return redirect() -> route('admin.cargos.index') -> with('alert', 'Cargo não encontrado');
    }

    /**
    *
    * Deletar Cargo
    *
    **/
    public function destroy($id){

       if ($cargo = Cargo::find($id)) {

            if (count($cargo -> vagas)) {
                return redirect() -> back() -> with('alert', 'Vagas relacionadas com o cargo');
            }

            $cargo -> delete();

            return redirect() -> route('admin.cargos.index') -> with('success', 'Cargo deletado com sucesso');
        }

        return redirect() -> back() -> with('alert', 'Cargo não encontrado');
    }
}
