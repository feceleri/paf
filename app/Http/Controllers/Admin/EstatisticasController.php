<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Vaga;
use App\Ramo;
use App\Cargo;
use App\Empresa;
use App\Cidade;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Mail\PublicadaVagaMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\EstatisticaVagas;
use Khill\Lavacharts\Lavacharts;


//use Illuminate\Pagination;

class EstatisticasController extends Controller {

    /**
    *
    * Mostrar Vagas
    *
    **/
	public function index( ) {
//
//         $cidades = Vaga::select( DB::raw('TT008CC000, TT008CC001, TT008CC003, count(TT014CC013) as qtde'))
//                -> join('TT008', 'TT014.TT014CC003', '=', 'TT008.TT008CC000')
//                -> groupBy('TT008CC000', 'TT008CC001', 'TT008CC003')
//                -> orderBy('TT008CC003')
//                -> get();
//          $empresas = Vaga::select( DB::raw('TT010CC000, TT010CC001, TT010CC003,TT010CC004, count(TT014CC013) as qtde'))
//                -> join('TT010', 'TT014.TT014CC004', '=', 'TT010.TT010CC000')
//                -> groupBy('TT010CC000', 'TT010CC001', 'TT010CC003', 'TT010CC004')
//                -> orderBy('TT010CC004')
//                -> get();
//
//
//
//        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
//        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
//
//
//          $vagas = Vaga::get();
//          $estatisticas = EstatisticaVagas::get();
//          $simpaf = EstatisticaVagas::where('TT033CC002', '=','SI')->get();
//          $naopaf = EstatisticaVagas::where('TT033CC002', '=','NO')->get();
//          $seminfo = EstatisticaVagas::where('TT033CC002', '=','AD')->get();
//



$reasons =  Lava::DataTable();

$reasons->addStringColumn('Reasons')
        ->addNumberColumn('Percent')
        ->addRow(['Check Reviews', 5])
        ->addRow(['Watch Trailers', 2])
        ->addRow(['See Actors Other Work', 4])
        ->addRow(['Settle Argument', 89]);

Lava::DonutChart('IMDB', $reasons, [
    'title' => 'Reasons I visit IMDB'
]);



        return View ( 'admin::estatisticas.index', compact([ 'reasons']));





//          return View ( 'admin::estatisticas.index', compact([ 'estatisticas', 'simpaf', 'naopaf', 'seminfo']));

	}
//


    /**
    *
    * Mostrar Vaga
    *
    **/
	public function show( $id ) {

        if ( $vaga = Vaga::find( $id )) {
            return View ( 'admin::vagas.show', compact([ 'vaga' ]));
        }
        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
	}



       /**
    *
    * Estatísticas Vagas
    *
    **/

    public function estatisticas(  ) {
         $estatisticas = EstatisticaVagas::get();
        $simpaf = EstatisticaVagas::where('TT033CC002', '=','SI')->get();
        $naopaf = EstatisticaVagas::where('TT033CC002', '=','NO')->get();
         return View ( 'admin::vagas.estatisticas', compact([ 'estatisticas', 'simpaf', 'naopaf']));


    }

}
