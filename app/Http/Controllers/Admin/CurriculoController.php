<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Vaga;
use App\Ramo;
use App\Cargo;
use App\Empresa;
use App\Cidade;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Mail\PublicadaVagaMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\EstatisticaVagas;

use App\Usuario;
use App\Formacao;
use App\Curriculo;
use App\Deficiencia;
use App\Experiencia;
use App\Extracurso;
use App\Idioma;
use App\Interesse;
use App\Lingua;




//use Illuminate\Pagination;

class CurriculoController extends Controller {

        /**
    *
    * Página Inicial
    *
    **/
	public function index(Request $filters) {


        $cidades = Curriculo::select( DB::raw('TT008CC000, TT008CC001, TT008CC003'))
                -> join('TT008', 'TT021.TT021CC002', '=', 'TT008.TT008CC000')
                -> groupBy('TT008CC000', 'TT008CC001', 'TT008CC003')
                -> orderBy('TT008CC003')
                -> get();
        $deficienciaCV = \Request::get('deficienciaCV');
        $ramoCV = \Request::get('ramoCV');
        $cargoCV = \Request::get('cargoCV');
        $formacaoCV  = \Request::get('formacaoCV');
        $idiomaCV  = \Request::get('idiomaCV');
        $cidadeCV  = \Request::get('cidadeCV');
        $nomeCurriculo  = \Request::get('nomeCurriculo');

         $ramos = Ramo::orderBy('TT002CC001') -> get();
         $linguas = Lingua::orderBy('TT031CC001')->get();
         $cargos = Cargo::orderBy('TT003CC001') ->get();

        $curriculos = Curriculo:: where('TT014CC006','like','%'.$nomeCurriculo.'%')//buscar por nome usuario
            // BUSCA POR DEFICIENCIAS
           -> where(function($query)
                            {
                                $deficienciaCV  = \Request::get('deficienciaCV');
                                if (  $deficienciaCV == "ANY" ){
                                    $query->  with('Deficiencias')->whereHas('Deficiencias', function($q){ $q->where('TT024CC002', "MOT")->orWhere('TT024CC002', "AUD")->orWhere('TT024CC002', "VIS")->orWhere('TT024CC002', "INT");});
                                }

                                else if (  $deficienciaCV != "" ){
                                    $query->  with('Deficiencias')->whereHas('Deficiencias', function($q){$deficienciaCV  = \Request::get('deficienciaCV'); $q->where('TT024CC002', $deficienciaCV);});
                                }
                            })
            /// BUSCA POR DEFICIENCIAS
             // BUSCA POR CIDADES
            -> where(function($query)
                            {
                                $cidadeCV  = \Request::get('cidadeCV');
                                if (  $cidadeCV != "" ){
                                    $query->   where('TT021CC002','=', $cidadeCV  );
                                }
                            })
            /// BUSCA POR CIDADES
            //      BUSCA POR NIVEL DE FORMACAO
            -> where(function($query)
                            {
                                $formacaoCV  = \Request::get('formacaoCV');
                               if (  $formacaoCV != "" ){
                                   $query->  with('Formacoes')->whereHas('Formacoes', function($q){$formacaoCV = \Request::get('formacaoCV'); $q->where('TT023CC003', $formacaoCV);});
                                }
                            })
            ///      BUSCA POR NIVEL DE FORMACAO
            // BUSCA POR RAMO
            -> where(function($query)
                            {
                                $ramoCV  = \Request::get('ramoCV');
                                if (  $ramoCV != "" ){
                                    $query->  with('Experiencias')->whereHas('Experiencias', function($q){$ramoCV  = \Request::get('ramoCV'); $q->where('TT025CC002', $ramoCV);});
                                }
                            })
            /// BUSCA POR RAMO
            // BUSCA POR CARGO
            -> where(function($query)
                            {
                                $cargoCV  = \Request::get('cargoCV');
                                if (  $cargoCV != "" ){
                                    $query->  with('Experiencias')->whereHas('Experiencias', function($q){$cargoCV  = \Request::get('cargoCV'); $q->where('TT025CC003', $cargoCV);});
                                }
                            })
            /// BUSCA POR CARGO
            // BUSCA POR IDIOMAS
            -> where(function($query)
                            {
                                $idiomaCV  = \Request::get('idiomaCV');
                                if (  $idiomaCV != "" ){
                                    $query->  with('Idiomas')->whereHas('Idiomas', function($q){$idiomaCV  = \Request::get('idiomaCV'); $q->where('TT026CC002', $idiomaCV);});
                                }
                            })
            /// BUSCA POR IDIOMAS
            ->inRandomOrder()-> paginate (20) ; //pegar em ordem aleatória para não privilegiar ninguém


//

       return view('admin::curriculos.index', compact('curriculos', 'cidades', 'linguas','cargos', 'ramos',  'formacaoCV', 'deficienciaCV', 'idiomaCV', 'ramoCV', 'cargoCV', 'cidadeCV', 'nomeCurriculo'));





	}



    /**
    *
    * Mostrar Vaga
    *
    **/
	public function show( $id ) {

        if ( $curriculo = Curriculo::find( $id )) {


              $curriculo = Curriculo::where('TT021CC000','=', $id ) -> first ( );


        $deficiencias = Deficiencia::where('TT024CC001' , '=', $id)->get();
        $temdeficiencia = Deficiencia::groupBy('TT024CC001')->where('TT024CC001' , '=', $id)
            ->selectRaw('count(*) as total, TT024CC001')
            ->count();
        $teminteresse = Interesse::where('TT027CC002' , '=', $id) -> groupBy('TT027CC002')->where('TT027CC002' , '=', $id)
            ->selectRaw('count(*) as total, TT027CC002')
            ->count();
        $temexperiencia = Experiencia::where('TT025CC001' , '=', $id) -> groupBy('TT025CC001')->where('TT025CC001' , '=', $id)
            ->selectRaw('count(*) as total, TT025CC001')
            ->count();
        $temextracurso = Extracurso::where('TT032CC001' , '=', $id) -> groupBy('TT032CC001')->where('TT032CC001' , '=', $id)
            ->selectRaw('count(*) as total, TT032CC001')
            ->count();
        $temidioma = Idioma::where('TT026CC001' , '=', $id) -> groupBy('TT026CC001')->where('TT026CC001' , '=', $id)
            ->selectRaw('count(*) as total, TT026CC001')
            ->count();
        $temformacao = Formacao::where('TT023CC001' , '=', $id) -> groupBy('TT023CC001')->where('TT023CC001' , '=', $id)
            ->selectRaw('count(*) as total, TT023CC001')
            ->count();
//
//                                   var_dump($teminteresse ); die();
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
        $experiencias = Experiencia::where('TT025CC001' , '=', $id)->get();
        $extracursos = Extracurso::where('TT032CC001' , '=', $id)->get();
        $formacoes = Formacao::where('TT023CC001' , '=', $id)->get();
        $linguas = Lingua::orderBy('TT031CC001') -> pluck( 'TT031CC001', 'TT031CC000' );
        $idiomas = Idioma::where('TT026CC001' , '=', $id)->get();
        $interesses = Interesse::where('TT027CC002' , '=', $id)->get();



         return View ('admin::curriculos.show', compact( 'curriculo' , 'id', 'deficiencias', 'temexperiencia', 'temextracurso', 'temidioma', 'temformacao','temdeficiencia', 'teminteresse', 'experiencias', 'extracursos', 'formacoes', 'idiomas', 'linguas', 'interesses', 'cargos', 'ramos'));



        }
        return redirect() -> back() -> with( 'alert', 'Currículo não encontrado' );
	}

    /**
    *
    * Ativar Vaga
    *
    **/

    //função criada por liliana - usar para desabilitar vaga
	public function disable( $id ) {

         // Dados do Vaga
        if ( $vaga = Vaga::find( $id )) {


                $vaga -> TT014CC016 = false;

            $vaga -> save();
            return redirect() -> back() -> with( 'success', $msg );
        }

        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
	}
     //função criada por liliana - usar para desabilitar vaga



	public function active( $id ) {


        // Dados do Vaga
        if ( $vaga = Vaga::find( $id )) {
            // Vaga Habilitada?
            if ( $vaga -> TT014CC016 ) {
                $msg = 'Vaga desabilitada com sucesso';
                $vaga -> TT014CC016 = false;

            } else {
                $msg = 'Vaga habilitada com sucesso';
                $vaga -> TT014CC016 = true;
                $vaga -> TT014CC020 = 'DE';
                $vaga -> TT014CC019 = carbon::now();
                // ENVIAR EMAIL PARA O ANUNCIANTE

// manter ativo - inativo para testes # atenção ***********************************************************************************
                Mail::to( $vaga -> empresa -> TT010CC010, $vaga -> empresa ->  TT010CC005 ) -> send ( new PublicadaVagaMail( $vaga ));
            }

            $vaga -> save();
            $urlFiltroAtual = \Request::get('urlFiltroAtual');
            $paginacaoAtual = \Request::get('paginacaoAtual');
            return redirect('admin/vagas?page=' .  $paginacaoAtual . $urlFiltroAtual )-> with( 'success', $msg );

        }
        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
	}



    /**
    *
    * Editar Vagas
    *
    **/
    public function edit( $id ) {
        $vaga = Vaga::find( $id );
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
        $empresas = Empresa::select(
            DB::raw("concat(TT010CC004,' | ',
            concat( left(TT010CC003, 2), '.',
            mid(TT010CC003, 3, 3), '.',
            mid(TT010CC003, 6, 3), '/',
            mid(TT010CC003, 9, 4), '-',
            right(TT010CC003, 2))) as RazaoSocial"), 'TT010CC000')
            -> orderBy('TT010CC004')
            -> pluck('RazaoSocial', 'TT010CC000');
        return View ( 'admin::vagas.edit', compact([ 'vaga', 'ramos', 'cargos', 'empresas' ]));
    }





    /**
    *
    * Deletar Vaga
    *
    **/
    public function destroy( $id ) {

        if ( $vaga = Vaga::find( $id )) {

            if ( count( $vaga -> concurso )) {
                return redirect() -> back() -> with( 'alert','Concurso relacionado com a Vaga' );
            }

            $vaga -> delete();

            return redirect() -> back() -> with( 'success', 'Vaga deletada com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Vaga não encontrada' );
    }





}
