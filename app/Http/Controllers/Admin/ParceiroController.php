<?php

namespace App\Http\Controllers\Admin;

use File;
use App\Empresa;
use App\Parceiro;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class ParceiroController extends Controller {

    /**
    *
    * Mostrar Parceiros
    *
    **/
	public function index(Request $filters) {

        $parceiroSelected = \Request::get('parceiroSelected');
        $dataParceiro = \Request::get('dataParceiro');
        $modalidadeParceiro = \Request::get('modalidadeParceiro');
        $ativoParceiro = \Request::get('ativoParceiro');
        $parceiros = Parceiro::where('TT011CC001','like','%'.$parceiroSelected.'%')
          -> where('created_at','like','%'.$dataParceiro.'%')
          -> where(function($query)
                // Aqui coloquei pelo ID do tema, que deve ser identico e por isso utilizo o "=". Mas apenas executa a função se $temaArtigo não estiver vazia.
                            {
                                $ativoParceiro = \Request::get('ativoParceiro');
                                if (  $ativoParceiro != "" ){
                                    $query->   where('TT011CC005','=', $ativoParceiro );                                }
                            })
          -> where(function($query)
                // Aqui coloquei pelo ID do tema, que deve ser identico e por isso utilizo o "=". Mas apenas executa a função se $temaArtigo não estiver vazia.
                            {
                                $modalidadeParceiro = \Request::get('modalidadeParceiro');
                                if (  $modalidadeParceiro != "" ){
                                    $query->   where('TT011CC002','=', $modalidadeParceiro );
                                }
                            })
          -> orderBy ( 'TT011CC000', 'ASC' ) -> paginate ( 12 );

        // Request via Ajax?
        if ( $filters -> ajax() ) {
            return view( 'admin::parceiros.load', compact( 'parceiros', 'parceiroSelected', 'dataParceiro', 'modalidadeParceiro','ativoParceiro'));
        } else {
            return View ( 'admin::parceiros.index', compact([ 'parceiros' , 'parceiroSelected', 'dataParceiro', 'modalidadeParceiro', 'ativoParceiro']));
        }
	}


    /**
    *
    * Ativar Parceiro
    *
    **/
	public function active( $id ) {

        // Dados do Parceiro
        if ( $parceiro = Parceiro::find( $id )) {

            // Parceiro Habilitado?
            if ( $parceiro -> TT011CC005 ) {
                $msg = 'Parceiro desabilitado com sucesso';
                $parceiro -> TT011CC005 = false;
            } else {
                $msg = 'Parceiro habilitado com sucesso';
                $parceiro -> TT011CC005 = true;
            }

            $parceiro -> save();
//            return redirect() -> back() -> with( 'success', $msg );
            $urlFiltroAtual = \Request::get('urlFiltroAtual');
            $paginacaoAtual = \Request::get('paginacaoAtual');
            return redirect('admin/parceiros?page=' .  $paginacaoAtual . $urlFiltroAtual )-> with( 'success', $msg );
        }

        return redirect() -> back() -> with( 'alert', 'Parceiro não encontrado' );
	}


    /**
    *
    * Cadastrar Parceiros
    *
    **/
    public function create() {
        $empresas = Empresa::pluck( 'TT010CC005', 'TT010CC000' );
        return View ( 'admin::parceiros.create', compact( 'empresas' ));
    }

    /**
    *
    * Gravar Parceiro
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Parceiro::$rules, ['dimensions' => 'Dimensões 400 x 150'] );
        $parceiro = new Parceiro();
        $parceiro -> fill( $request -> all() );
        $parceiro -> save();

        //$parceiro -> TT011CC003 = 'img/parceiros/par-' . rand ( 111111, 999999 ) . '-' . $parceiro -> TT011CC000 . '.' . $request -> file( 'TT011CC003' ) -> getClientOriginalExtension();
        $parceiro -> TT011CC003 = 'img/parceiros/' . $request -> file( 'TT011CC003' ) -> getClientOriginalName();

        $request -> file( 'TT011CC003' ) -> move (
            base_path() . '/public/img/parceiros/', $parceiro -> TT011CC003
        );

        $parceiro -> save();

        return redirect() -> back() -> with( 'success', 'Parceiro cadastrado com sucesso' );
    }

    /**
    *
    * Editar Parceiros
    *
    **/
    public function edit( $id ) {
        $parceiro = Parceiro::find( $id );
        $empresas = Empresa::pluck( 'TT010CC005', 'TT010CC000' );
        return View ( 'admin::parceiros.edit', compact([ 'parceiro', 'empresas' ]));
    }

    /**
    *
    * Atualizar Parceiro
    *
    **/
    public function update( Request $request, $id ) {

        // Dados do Parceiro
        if ( $parceiro = Parceiro::find( $id )) {

            $this -> validate( $request, Parceiro::$updateRules, ['dimensions' => 'Dimensões 400 x 150'] );
            $validator = Validator::make(Input::all(), Parceiro::$imageRules);
            if ($validator->passes()){
                // Deletar imagem antiga
                if ( File::exists( $parceiro -> TT011CC003 )) {
                File::delete( $parceiro -> TT011CC003 );
            }}
            // Dados novos
            $parceiro -> fill( $request -> all() );
            //$parceiro -> TT011CC003 = 'img/parceiros/par-' . rand ( 111111, 999999 ) . '-' . $parceiro -> TT011CC000 . '.' . $request -> file( 'TT011CC003' ) -> getClientOriginalExtension();
            if ($validator->passes()){
                $parceiro -> TT011CC003 = 'img/parceiros/' . $request -> file( 'TT011CC003' ) -> getClientOriginalName();
                $request -> file( 'TT011CC003' ) -> move( base_path() . '/public/img/parceiros/', $parceiro -> TT011CC003 );
            }
            $parceiro -> save();
            return redirect() -> back() -> with( 'success', 'Parceiro atualizado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Parceiro não encontrado' );
    }





    /**
    *
    * Deletar Parceiro
    *
    **/
    public function destroy( $id ) {

        if ( $parceiro = Parceiro::find( $id )) {

            if ( count( $parceiro -> descontos )) {
                return redirect() -> back() -> with( 'alert', 'Descontos relacionados com o Parceiro' );
            } elseif ( count( $parceiro -> banners )) {
                return redirect() -> back() -> with( 'alert', 'Banners relacionados com o Parceiro' );
            }

            $parceiro -> delete();

            return redirect() -> back() -> with( 'success', 'Parceiro deletado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Parceiro não encontrado' );
    }
}
