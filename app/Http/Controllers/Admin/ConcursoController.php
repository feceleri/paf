<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Estado;
use App\Cidade;
use App\Concurso;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ConcursoController extends Controller {

    /**
    *
    * Mostrar Concursos
    *
    **/
	public function index(Request $filters) {
        $hoje = Carbon::today();
        $cidConcurso = \Request::get('cidConcurso');
        $estConcurso = \Request::get('estConcurso');
        $instituicaoConcurso = \Request::get('instituicaoConcurso');

        $concursos = Concurso::where('TT015CC003','like','%'.$instituicaoConcurso.'%')
            -> where('TT015CC012','like','%'.$estConcurso.'%')
            -> where('TT015CC013','like','%'.$cidConcurso.'%')
            ->  orderBy ( 'TT015CC009', 'DESC' ) -> paginate ( 30 );

        // Request via Ajax?
        if ( $filters -> ajax() ) {
            return view( 'admin::concursos.load', compact( 'concursos', 'cidConcurso', 'estConcurso', 'instituicaoConcurso' ));
        } else {
            return View ( 'admin::concursos.index', compact([ 'concursos', 'cidConcurso', 'estConcurso', 'instituicaoConcurso'  ]));
        }
	}


    /**
    *
    * Mostrar Concurso
    *
    **/
	public function show( $id ) {

        if ( $concurso = Concurso::find( $id )) {
            return View ( 'admin::concursos.show', compact([ 'concurso' ]));
        }
        return redirect() -> back() -> with( 'alert', 'Concurso não encontrado' );
	}

    /**
    *
    * Ativar Concurso
    *
    **/
	public function active( $id ) {

        // Dados do Concurso
        if ( $concurso = Concurso::find( $id )) {

            // Concurso Habilitada?
            if ( $concurso -> TT015CC010 ) {
                $msg = 'Concurso desabilitado com sucesso';
                $concurso -> TT015CC010 = false;
            } else {
                $msg = 'Concurso habilitado com sucesso';
                $concurso -> TT015CC010 = true;
            }

            $concurso -> save();
//            return redirect() -> back() -> with( 'success', $msg );
            $urlFiltroAtual = \Request::get('urlFiltroAtual');
            $paginacaoAtual = \Request::get('paginacaoAtual');
            return redirect('admin/concursos?page=' .  $paginacaoAtual . $urlFiltroAtual )-> with( 'success', $msg );
        }

        return redirect() -> back() -> with( 'alert', 'Concurso não encontrada' );
	}

    /**
    *
    * Cadastrar Concursos
    *
    **/
    public function create() {
        $estados = Estado::pluck('TT007CC001', 'TT007CC000');
        $cidades = Cidade::select(
            DB::raw("concat(TT008CC003,' - ', TT008CC001) as Name"),'TT008CC000')
            -> orderBy('TT008CC001')
            -> orderBy('TT008CC003')
            -> pluck('Name', 'TT008CC000');
        return View ( 'admin::concursos.create', compact([ 'estados', 'cidades' ]) );
    }

    /**
    *
    * Gravar Concurso
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Concurso::$rules );

        // Dados do Concurso
        $concurso = new Concurso();
        $concurso -> fill($request -> all());

        // Abrangência Nacional?
        if ($request -> get('TT015CC011') == 'on') {
            $concurso -> TT015CC011 = true;
            $concurso -> save();
        } else {
            $concurso -> TT015CC011 = false;
            $concurso -> save();
            $concurso -> estados() -> attach($request -> get( 'estados' ));
            $concurso -> cidades() -> attach($request -> get( 'cidades' ));
        }

        return redirect() -> back() -> with( 'success', 'Concurso cadastrado com sucesso' );
    }

    /**
    *
    * Editar Concursos
    *
    **/
    public function edit( $id ) {

        // Dados do Concurso
        if ( $concurso = Concurso::find( $id )) {
            $selectedEstados = [];
            $selectedCidades = [];
            $estados = Estado::pluck('TT007CC001', 'TT007CC000');
            $cidades = Cidade::select(
                DB::raw("concat(TT008CC003,' - ', TT008CC001) as Name"),'TT008CC000')
                -> orderBy('TT008CC001')
                -> orderBy('TT008CC003')
                -> pluck('Name', 'TT008CC000');

            // Estados Selecionados
            foreach($concurso -> estados as $estado) {
                $selectedEstados[] = $estado -> TT007CC000;
            }

            // Cidades Selecionadas
            foreach($concurso -> cidades as $cidade) {
                $selectedCidades[] = $cidade -> TT008CC000;
            }

            return View ( 'admin::concursos.edit', compact([ 'concurso', 'estados', 'cidades', 'selectedEstados' , 'selectedCidades' ]));
        }
        return redirect() -> back() -> with( 'alert', 'Concurso não encontrada' );
    }

    /**
    *
    * Atualizar Concurso
    *
    **/
    public function update( Request $request, $id ) {
        // Dados do Concurso
        if ( $concurso = Concurso::find( $id )) {

            $this -> validate( $request, Concurso::$rules );

            // Dados novos
            $concurso -> fill( $request -> all() );

            // Zerar abrangência
            $concurso -> estados() -> detach();
            $concurso -> cidades() -> detach();

            // Abrangência Nacional?
            if ($request -> get('TT015CC011') == 'on') {
                $concurso -> TT015CC011 = true;
            } else {
                $concurso -> TT015CC011 = false;
                $concurso -> estados() -> attach($request -> get( 'estados' ));
                $concurso -> cidades() -> attach($request -> get( 'cidades' ));
            }

            $concurso -> save();

            return redirect() -> back() -> with( 'success', 'Concurso atualizado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Concurso não encontrado' );
    }

    /**
    *
    * Deletar Concurso
    *
    **/
    public function destroy( $id ) {

        if ( $concurso = Concurso::find( $id )) {

            // Zerar abrangência
            $concurso -> estados() -> detach();
            $concurso -> cidades() -> detach();

            $concurso -> delete();

            return redirect() -> route( 'admin.concursos.index' ) -> with( 'success', 'Concurso deletado com sucesso' );
        }

        return redirect() -> back() -> with( 'alert', 'Concurso não encontrado' );
    }
}
