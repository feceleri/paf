<?php

namespace App\Http\Controllers;

use App\Tema;
use App\Artigo;
use Illuminate\Http\Request;
use App\Search\Artigo\ArtigoSearch;

class ArtigoController extends Controller {

    /**
    *
    * Carregar página de Artigos
    *
    **/
	public function index( Request $filters ) {

        $artigos = ArtigoSearch::apply( $filters );

        // Request via Ajax?
        if ( $filters -> ajax() ) {
            return view( 'site::Artigos.load', compact( 'artigos' ));
        } else {
            $temas = Tema::orderBy( 'TT004CC002', 'DESC' ) -> get();
            return view( 'site::Artigos.index', compact( 'artigos', 'temas' ));
        }
	}

    /**
    * Mostrar página do Artigo
    *
    * @param  int  $id
    * @return View artigos.show
    */
    public function show( $id ) {

        if ( $artigo = Artigo::where( 'TT009CC005', true ) -> find( $id )) {

            $artigos = Artigo::where( 'TT009CC005', true ) -> orderBy( 'created_at', 'desc' ) -> limit( 6 ) -> get();
            return view ( 'site::Artigos.show', compact( 'artigo', 'artigos' ));
        }
        return redirect() -> back();
    }
}
