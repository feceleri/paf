<?php

namespace App\Http\Controllers;

use DB;
use Route;
use App\Cidade;
use Illuminate\Http\Request;
use App\Mail\AnuncioVagaMail;
use App\Perito;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class PeritoController extends Controller {


    /**
    *
    * Mostrar view de Cadastro de Vagas
    *
    **/
    public function create() {

        return view ('site::perito');
    }


    /**
    *
    * Gravar cadastro de perito
    *
    **/
    public function store( Request $request ) {

        $this -> validate( $request, Perito::$rules );

        // Dados da Vaga
        $perito = new Perito();
        $perito -> fill( $request -> all() );

        // Limpar Formatação



         $perito -> TT030CC003 = preg_replace( '/\D/', '', $request -> get( 'TT030CC003' ));

         $perito -> save();
        //ENVIAR EMAIL INTERNO PARA AVISAR
//       Mail::to( 'paf.farmacia@gmail.com' )
//            -> send( new AnuncioVagaMail( $vaga ));

        // ENVIAR EMAIL PARA O ANUNCIANTE



        return redirect() -> route( 'perito' )
            -> with( 'success','Cadastro enviado com sucesso!' );
    }
}
