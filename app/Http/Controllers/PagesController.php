<?php

namespace App\Http\Controllers;

use DB;
use Route;
use App\Vaga;
use App\Artigo;
use App\Concurso;
use App\Desconto;
use App\Seccional;

class PagesController extends Controller {

	/**
    *
    * Mostrar a view Sobre
    *
    **/
	public function sobre() {
		return View ( 'site::sobre' );
	}

    /**
    *
    * Mostrar as Seccinais
    *
    **/
	public function seccional() {
        $id = Route::current() -> getParameter('id');
        $seccionais = Seccional::
            where('TT006CC000', '!=', 1 ) // não listar Adamantina
            -> where('TT006CC000', '!=', 4 ) // não listar Avaré
                -> where('TT006CC000', '!=', 5 ) // não listar Barretos
                -> where('TT006CC000', '!=', 9 ) // não listar Caraguatatuba
                -> where('TT006CC000', '!=', 19) // não listar registro
            ->orderBy('TT006CC001') -> pluck('TT006CC001', 'TT006CC000');
        $artigos = Artigo::where( 'TT009CC005', true ) -> orderBy( 'created_at', 'desc' ) -> get() -> take( 3 );
        $desconto = Desconto::where( 'TT012CC006', true ) -> get() -> random( 1 );

        $concurso = Concurso::select( DB::raw('TT015.*'))
                -> join('TT029', 'TT015CC000', '=', 'TT029CC002')
                -> join('TT008', 'TT029CC001', '=', 'TT008CC000')
                -> join('TT006', 'TT008CC002', '=', 'TT006CC000')
                -> where('TT015CC010', true)
                -> where('TT006CC000', $id)
                -> orderBy('TT015CC009')
                -> orderBy('TT006CC001')
                -> get()
                -> first();

        $vaga = Vaga::select( DB::raw('TT014.*'))
                -> join('TT008', 'TT014CC003', '=', 'TT008CC000')
                -> join('TT006', 'TT008CC002', '=', 'TT006CC000')
                -> where('TT014CC016', true)
                -> where('TT006CC000', $id)
                -> orderBy('TT014CC019')
                -> orderBy('TT006CC001')
                -> get()
                -> last();

        // Verificar Concursos Nacionais
        if (!$concurso) {
            $concurso = Concurso::where('TT015CC010', true)
                -> where('TT015CC011', true)
                -> get()
                -> first();
        }

		return View( 'site::PAF-Regional.index', compact(['id', 'artigos', 'vaga', 'concurso', 'desconto', 'seccionais']));
	}

    /**
    *
    * Testar os E-mails
    *
    **/
	public function email() {
        $vaga = Vaga::firstOrNew(['TT014CC000' => 350]);
		return View ( 'emails.anuncio-vaga', compact( 'vaga' ));
	}


    	/**
    *
    * Mostrar a view usuario inativo
    *
    **/
	public function usuarioinativo() {
		return View ( 'site::usuarioinativo' );
	}
}
