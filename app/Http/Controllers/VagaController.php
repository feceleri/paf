<?php

namespace App\Http\Controllers;

use DB;
use Route;
use App\Ramo;
use App\Vaga;
use App\Cargo;
use App\Cidade;
use App\Empresa;
use App\BloqueiosTemporarios;
use Illuminate\Http\Request;
use App\Mail\AnuncioVagaMail;
use App\Search\Vaga\VagaSearch;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;


class VagaController extends Controller {

	/**
    *
    * Mostrar view de Vagas
    *
    **/
	public function index( Request $filters ) {

        $vagas = VagaSearch::apply( $filters );

        // Request via Ajax?
        if ( $filters -> ajax()) {
            return view( 'site::PAF-Empregos.vagas.load', compact( 'vagas' ));
        } else {
         //   $encerra_vaga = Carbon::now()->subDays(15);
            $cidades = Vaga::select( DB::raw('TT008CC000, TT008CC001, TT008CC003, sum(TT014CC013) as qtde'))
                -> join('TT008', 'TT014.TT014CC003', '=', 'TT008.TT008CC000')
                -> where('TT014CC016', true)
               // -> where( 'TT014CC019', '>', $encerra_vaga )
                -> groupBy('TT008CC000', 'TT008CC001', 'TT008CC003')
                -> orderBy('TT008CC003')
                -> get();

            $seccionais = Vaga::select( DB::raw( 'TT006CC000, TT006CC001, sum(TT014CC013) as qtde'))
                -> join('TT008', 'TT014.TT014CC003', '=', 'TT008.TT008CC000')
                -> join('TT006', 'TT008.TT008CC002', '=', 'TT006.TT006CC000')
                -> where('TT014CC016', true)
              //  -> where( 'TT014CC019', '>', $encerra_vaga )
                -> groupBy('TT006CC000', 'TT006CC001')
                -> orderBy('TT006CC001')
                -> get();

            $ramos = Vaga::select( DB::raw('TT002CC000, TT002CC001, sum( TT014CC013 ) as qtde'))
                -> join('TT002', 'TT014.TT014CC001', '=', 'TT002.TT002CC000')
                -> where('TT014CC016', true)
               // -> where( 'TT014CC019', '>', $encerra_vaga )
                -> groupBy('TT002CC000', 'TT002CC001')
                -> orderBy('TT002CC001')
                -> get();

            return view('site::PAF-Empregos.vagas.index', compact(['vagas','cidades', 'seccionais', 'ramos']));
        }
	}
    
    public function totalvagas(){
       
        $encerra_vaga = Carbon::now()->subDays(30);
        $qtdVagas = Vaga::where( 'TT014CC016', true )-> where( 'TT014CC019', '>', $encerra_vaga ) -> sum( 'TT014CC013' );
        return view('site::PAF-Empregos.vagas.totalvagas', compact(['qtdVagas']));

    }

    /**
    *
    * Mostrar view de Cadastro de Vagas
    *
    **/
    public function create() {
        // no model está a condição
         $bloqueios = BloqueiosTemporarios::first();
          if($bloqueios->estaBloqueado()){

           return view ( 'site::bloqueiotemporario');
        }

        else {

            $tipoVaga = 'EM';
            $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
            $cargos = Cargo::orderBy('TT003CC001')->where('TT003CC002', true ) -> pluck( 'TT003CC001', 'TT003CC000' );
            return view ( 'site::PAF-Empregos.vagas.create', compact(['cargos', 'ramos', 'tipoVaga']));
        }



    }

    /**
    *
    * Mostrar view de Cadastro de Estágios
    *
    **/
    public function estagio() {

         // no model está a condição
         $bloqueios = BloqueiosTemporarios::first();
          if($bloqueios->estaBloqueado()){

           return view ( 'site::bloqueiotemporario');
        }

        else {
              $tipoVaga = 'ES';
        $ramos = Ramo::orderBy('TT002CC001') -> pluck( 'TT002CC001', 'TT002CC000' );
        $cargos = Cargo::orderBy('TT003CC001') -> pluck( 'TT003CC001', 'TT003CC000' );
        return view ( 'site::PAF-Empregos.vagas.create', compact(['cargos', 'ramos', 'tipoVaga']));
        }


    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id) {
        if ( $vaga = Vaga::where( 'TT014CC016', true ) -> find( $id )) {
            $vagas = Vaga::where( 'TT014CC016', true )  -> orderBy ( 'TT014CC019', 'DESC' ) -> get() -> take( 2 );
            return view ('site::PAF-Empregos.vagas.show', compact(['vaga', 'vagas']));
        }
        return redirect() -> back();
    }

    /**
    *
    * Gravar anúncio de vaga
    *
    **/
    public function store( Request $request ) {
        $vaga = new Vaga();

        $vaga -> TT014CC008 = preg_replace( '/\D/', '', $request -> get( 'TT014CC008' ));
        $vaga -> TT014CC014 = floatval( str_replace(',', '.', str_replace( '.', '', $request -> get( 'TT014CC014' ))));

        $tipoVaga   = $request->input('tipoVaga');

         if ( $tipoVaga == 'ES' ) { $rules = Vaga::$estagiorules;

        } else {$rules = Vaga::$empregorules;}


        $rules += Empresa::$rules;
        $rules += ['termo' => 'required'];
        unset($rules['TT014CC004']);

        $this->validate( $request, $rules );

        // Dados da Vaga
        $vaga = new Vaga();
        $vaga -> fill( $request -> all() );

        // Dados da Empresa
        $empresa = Empresa::firstOrNew( ['TT010CC003' => preg_replace( '/\D/', '', $request -> get( 'TT010CC003' ))]);
        $empresa -> fill( $request -> all() );

        // Limpar Formatação
        $vaga -> TT014CC014 = floatval( str_replace(',', '.', str_replace( '.', '', $request -> get( 'TT014CC014' ))));
        $vaga -> TT014CC008 = preg_replace( '/\D/', '', $request -> get( 'TT014CC008' ));
        $empresa -> TT010CC003 = preg_replace( '/\D/', '', $request -> get( 'TT010CC003' ));
        $empresa -> TT010CC006 = preg_replace( '/\D/', '', $request -> get( 'TT010CC006' ));
        $empresa -> TT010CC009 = preg_replace( '/\D/', '', $request -> get( 'TT010CC009' ));


        // Empresa já cadastrada?
        if ( !$empresa -> TT010CC000 > 0 ) {
            $empresa -> save();
        }

        $vaga -> TT014CC004 = $empresa -> TT010CC000;
        $vaga -> TT014CC005 = $vaga -> cargo -> TT003CC001;
        $vaga -> save();



        //ENVIAR EMAIL INTERNO PARA AVISAR
//       Mail::to( 'paf@crfsp.org.br' )
//            -> send( new AnuncioVagaMail( $vaga ));
//oculto para arrumar servidor
        // ENVIAR EMAIL PARA O ANUNCIANTE
//
//        Mail::to( $empresa -> TT010CC010, $empresa -> TT010CC005 ) -> send ( new AnuncioVagaMail( $vaga ));
//
//        return redirect() -> route( 'vagas.create' )
//            -> with( 'success','Anúncio enviado com sucesso! Aguarde a validação dos dados para a publicação' );
        //oculto para arrumar servidor
    }
}
