<?php

namespace App\Http\Controllers;

use DB;
use App\Estado;
use App\Cidade;
use App\Concurso;
use App\Seccional;
use Illuminate\Http\Request;
use App\Search\Concurso\ConcursoSearch;
use Carbon\Carbon;


class ConcursoController extends Controller {

    /**
    *
    * Mostrar view de Concursos
    *
    **/
	public function index( Request $filters ) {
        $hoje = Carbon::today();
        $concursos = ConcursoSearch::apply( $filters );

        // Request via Ajax?
        if ( $filters -> ajax()) {
            return view( 'site::PAF-Empregos.concursos.load', compact( 'concursos' ));
        } else {

            $estados = Estado::select( DB::raw('TT007CC000, TT007CC001, count(distinct TT028CC002) as qtde'))
                -> join('TT028', 'TT007CC000', '=', 'TT028CC001')
                -> join ('TT015', 'TT015CC000', '=', 'TT028CC002')
                -> where ('TT015CC010', true)
                -> groupBy('TT007CC000', 'TT007CC001')
                -> orderBy('TT007CC001')
                -> orderBy('qtde', 'DESC')

                -> get();

            $cidades = Cidade::select( DB::raw('TT008CC000, TT008CC003, count(distinct TT029CC002) as qtde'))
                -> join('TT029', 'TT008CC000', '=', 'TT029CC001')
                -> join ('TT015', 'TT015CC000', '=', 'TT029CC002')
                -> where ('TT015CC010', true)
                -> groupBy('TT008CC000', 'TT008CC003')
                -> orderBy('TT008CC003')
                -> orderBy('qtde', 'DESC')
                -> get();

            $seccionais = Seccional::select( DB::raw( 'TT006CC000, TT006CC001, count(distinct TT029CC002) as qtde' ))
                -> join( 'TT008', 'TT006CC000', '=', 'TT008CC002' )
                -> join( 'TT029', 'TT008CC000', '=', 'TT029CC001' )
                 -> join ('TT015', 'TT015CC000', '=', 'TT029CC002')
                -> where ('TT015CC010', true)
                -> groupBy( 'TT006.TT006CC000', 'TT006.TT006CC001' )
                -> orderBy( 'TT006.TT006CC001' )
                -> get();

            return view( 'site::PAF-Empregos.concursos.index', compact([ 'concursos', 'estados', 'cidades', 'seccionais' ]));
        }
	}

    /**
    * Mostrar Concurso
    *
    * @param  int  $id
    * @return Response
    */
    public function show( $id ) {
        if ( $concurso = Concurso::where( 'TT015CC010', true ) -> find( $id )) {
            $concursos = Concurso::where( 'TT015CC010', true ) -> orderBy( 'created_at', 'DESC' ) -> limit( 2 ) -> get();
            return view ( 'site::PAF-Empregos.concursos.show', compact( 'concurso' , 'concursos' ));
        }
        return redirect() -> back();
    }
}
