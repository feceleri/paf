<?php

namespace App\Http\Controllers;

use DB;
use App\Estado;
use App\Cidade;
use App\Desconto;
use App\Seccional;
use Illuminate\Http\Request;
use App\Search\Desconto\DescontoSearch;

class DescontoController extends Controller {

    /**
    *
    * Mostrar view de empresa
    *
    **/
    public function empresa(Request $filters) {
        $tipoDesc = 'PJ';
        return $this->load($filters, $tipoDesc);
    }

    /**
    *
    * Mostrar view de Descontos
    *
    **/
    public function index(Request $filters) {
        $tipoDesc = 'PF';
        return $this->load($filters, $tipoDesc);
    }

    /**
    *
    * Mostrar view de Descontos
    *
    **/
	public function load(Request $filters, $tipoDesc) {

        $descontos = DescontoSearch::apply($filters);

        // Request via Ajax?
        if  ($filters -> ajax()) {
            return view('site::PAF-Descontos.descontos.load', compact('descontos'));
        } else {

             if  ( $tipoDesc == 'PF') {

            $categorias = Desconto::select( DB::raw('TT005CC000, TT005CC001, count(*) as qtde'))
                -> join('TT005', 'TT012CC002', '=', 'TT005CC000')
                -> where('TT012CC006', true)
                -> where('TT012CC007', '!=' , 'PJ')
                -> groupBy('TT005CC000', 'TT005CC001')
                -> orderBy('TT005CC001')
                -> orderBy('qtde', 'DESC')
                -> get();

            $estados = Estado::select( DB::raw('TT007CC000, TT007CC001, count(distinct TT019CC002) as qtde'))
                -> join('TT019', 'TT007CC000', '=', 'TT019CC001')
                -> groupBy('TT007CC000', 'TT007CC001')
                -> orderBy('TT007CC001')
                -> orderBy('qtde', 'DESC')
                -> get();

            $cidades = Cidade::select( DB::raw('TT008CC000, TT008CC001, TT008CC003, count(distinct TT020CC002) as qtde'))
                -> join('TT020', 'TT008CC000', '=', 'TT020CC001')
                -> groupBy('TT008CC000', 'TT008CC001', 'TT008CC003')
                -> orderBy('TT008CC003')
                -> orderBy('qtde', 'DESC')
                -> get();

            $seccionais = Seccional::select( DB::raw( 'TT006CC000, TT006CC001, count(distinct TT020CC002) as qtde' ))
                -> join( 'TT008', 'TT006CC000', '=', 'TT008CC002' )
                -> join( 'TT020', 'TT008CC000', '=', 'TT020CC001' )
                -> groupBy( 'TT006.TT006CC000', 'TT006.TT006CC001' )
                -> orderBy( 'TT006.TT006CC001' )
                -> get();

            $parceiros = Desconto::select( DB::raw('TT011CC000, TT011CC001'))
                -> join('TT011', 'TT012CC001', '=', 'TT011CC000')
                -> where('TT012CC006', true)
                 -> where('TT012CC007', '!=' , 'PJ')
                -> groupBy('TT011CC000', 'TT011CC001')
                -> orderBy('TT011CC001')
                -> get();

             }


             else {

            $categorias = Desconto::select( DB::raw('TT005CC000, TT005CC001, count(*) as qtde'))
                -> join('TT005', 'TT012CC002', '=', 'TT005CC000')
                -> where('TT012CC006', true)
                -> where('TT012CC007', '!=' , 'PF')
                -> groupBy('TT005CC000', 'TT005CC001')
                -> orderBy('TT005CC001')
                -> orderBy('qtde', 'DESC')
                -> get();

            $estados = Estado::select( DB::raw('TT007CC000, TT007CC001, count(distinct TT019CC002) as qtde'))
                -> join('TT019', 'TT007CC000', '=', 'TT019CC001')
                -> groupBy('TT007CC000', 'TT007CC001')
                -> orderBy('TT007CC001')
                -> orderBy('qtde', 'DESC')
                -> get();

            $cidades = Cidade::select( DB::raw('TT008CC000, TT008CC001, TT008CC003, count(distinct TT020CC002) as qtde'))
                -> join('TT020', 'TT008CC000', '=', 'TT020CC001')
                -> groupBy('TT008CC000', 'TT008CC001', 'TT008CC003')
                -> orderBy('TT008CC003')
                -> orderBy('qtde', 'DESC')
                -> get();

            $seccionais = Seccional::select( DB::raw( 'TT006CC000, TT006CC001, count(distinct TT020CC002) as qtde' ))
                -> join( 'TT008', 'TT006CC000', '=', 'TT008CC002' )
                -> join( 'TT020', 'TT008CC000', '=', 'TT020CC001' )
                -> groupBy( 'TT006.TT006CC000', 'TT006.TT006CC001' )
                -> orderBy( 'TT006.TT006CC001' )
                -> get();

            $parceiros = Desconto::select( DB::raw('TT011CC000, TT011CC001'))
                -> join('TT011', 'TT012CC001', '=', 'TT011CC000')
                -> where('TT012CC006', true)
                -> where('TT012CC007', '!=' , 'PF')
                -> groupBy('TT011CC000', 'TT011CC001')
                -> orderBy('TT011CC001')
                -> get();

             }


            return view('site::PAF-Descontos.descontos.index', compact([ 'descontos', 'estados', 'cidades', 'categorias', 'parceiros', 'seccionais', 'modalidade', 'tipoDesc']));
        }
	}

    /**
    * Mostrar Desconto
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id) {
        if ($desconto = Desconto::where('TT012CC006', true) -> find($id)) {
            return view ('site::PAF-Descontos.descontos.show', compact('desconto'));
        }
        return redirect() -> back();
    }
}
