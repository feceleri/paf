<?php

namespace App;

use Illuminate\Database\Eloquent\Model; //estou importando as funcionalidades do model

class EstatisticaVagas extends Model {

	protected $table = 'TT033'; //Por defaul ele coloca no plural da classe, que seria 'vagas', mas aqui definimos com o nome usado no banco de dados.
    protected $primaryKey = 'TT033CC000'; // aqui estou definindo qual coluna é o primarykey, já que neste caso não de chama id (default)

	protected $fillable = [

            'TT033CC001', 'TT033CC002', 'TT033CC003'
	];


    public static $rules = [


            'TT033CC005' => 'required', // Preenchido pelo PAF? SI,NO, AD,NP


    ];



    // Relacionemento 1:Vaga - N:Registros de Pesquisa
	public function vaga() {
		return $this -> belongsTo ( 'App\Vaga', 'TT033CC001','TT014CC000' );
	}




}
