<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model {

    protected $table = 'TT007';
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = 'TT007CC000';

	protected $fillable = [

            'TT007CC000', 'TT007CC001',
	];

    // Relacionemento 1:Cidade - N:Vagas
	public function cidades() {
		return $this -> hasMany ( 'App\Cidade', 'TT08CC001', 'TT007CC000' );
	}
}
