<?php

namespace App\Console;
use DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
//use App\Console\Commands\desabilitarConcursos;
// caso volte a utilizar a funcao desabilitar concursos, descomente a linha acima, protected commands e protected funcions relacionadas ao concurso
use App\Console\Commands\desabilitarVagas;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
       '\App\Console\Commands\desabilitarVagas'
//        , '\App\Console\Commands\desabilitarConcursos'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule-> command('desabilitarVagas:update')->withoutOverlapping()
                ->everyFiveMinutes() ;
//        $schedule-> command('desabilitarConcursos:update')->withoutOverlapping()
//                ->everyFiveMinutes() ;


        }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
