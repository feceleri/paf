<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use App\Concurso;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class desabilitarConcursos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'desabilitarConcursos:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Desabilita concursos que não estão mais abertos para inscrição';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $hoje = Carbon::today();
       DB::table('TT015')-> where( 'TT015CC009', '<', $hoje )-> update(['TT015CC010'=> false]);
    }
}
