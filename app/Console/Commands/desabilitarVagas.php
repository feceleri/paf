<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use App\Vagas;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class desabilitarVagas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'desabilitarVagas:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Desabilita vagas que foram publicadas há mais de 15 dias';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $encerra_vaga = Carbon::now()->subDays(15);
       DB::table('TT014')-> where( 'TT014CC019', '<', $encerra_vaga )-> update(['TT014CC016'=> false]);
    }
}
