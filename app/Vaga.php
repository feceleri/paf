<?php

namespace App;

use Illuminate\Database\Eloquent\Model; //estou importando as funcionalidades do model


class Vaga extends Model {

	protected $table = 'TT014'; //Por defaul ele coloca no plural da classe, que seria 'vagas', mas aqui definimos com o nome usado no banco de dados.
    protected $primaryKey = 'TT014CC000'; // aqui estou definindo qual coluna é o primarykey, já que neste caso não de chama id (default)

	protected $fillable = [

            'TT014CC001', 'TT014CC002', 'TT014CC003', 'TT014CC004', 'TT014CC005', 'TT014CC006',
            'TT014CC007', 'TT014CC008', 'TT014CC009', 'TT014CC010', 'TT014CC011', 'TT014CC012',
            'TT014CC013', 'TT014CC014', 'TT014CC015','TT014CC018','TT014CC020','TT014CC021','TT014CC022'
	];


    public static $rules = [

            'TT014CC001' => 'required', // ID Ramo
            'TT014CC002' => 'required', // ID Cargo
            'TT014CC003' => 'required', // ID Cidade
            'TT014CC004' => 'required', // ID Empresa
            'TT014CC006' => 'required|email|max:150', // E-mail
            'TT014CC007' => 'required|max:150', // Contato
            'TT014CC008' => 'required|tel', // Telefone
            'TT014CC009' => 'required', // Descritivo
            'TT014CC010' => 'required', // Requisitos
            'TT014CC011' => 'max:1000', // benefícios
            'TT014CC012' => 'required', // Instruções
            'TT014CC013' => 'required|numeric|min:1|max:9999', // Número de Vagas
            'TT014CC014' => 'required|salario|max:99999', // Remuneração
            'TT014CC015' => 'max:150', // Carga Horária
            'TT014CC021' => 'max:250' // Informações adicionais do administrador
    ];

    public static $empregorules = [

            'TT014CC001' => 'required', // ID Ramo
            'TT014CC002' => 'required', // ID Cargo
            'TT014CC003' => 'required', // ID Cidade
            'TT014CC004' => 'required', // ID Empresa
            'TT014CC006' => 'required|email|max:150', // E-mail
            'TT014CC007' => 'required|max:150', // Contato
            'TT014CC008' => 'required|tel', // Telefone
            'TT014CC009' => 'required|max:1000', // Descritivo
            'TT014CC010' => 'required|max:1000', // Requisitos
            'TT014CC011' => 'max:1000', // benefícios
            'TT014CC012' => 'required|max:1000', // Instruções
            'TT014CC013' => 'required|numeric|min:1|max:9999', // Número de Vagas
            'TT014CC014' => 'required|salario|min:1|max:99999', // Remuneração
            'TT014CC015' => 'max:150', // Carga Horária
            'TT014CC021' => 'max:250' // Informações adicionais do administrador
    ];

     public static $estagiorules = [

            'TT014CC001' => 'required', // ID Ramo
            'TT014CC002' => 'required', // ID Cargo
            'TT014CC003' => 'required', // ID Cidade
            'TT014CC004' => 'required', // ID Empresa
            'TT014CC006' => 'required|email|max:150', // E-mail
            'TT014CC007' => 'required|max:150', // Contato
            'TT014CC008' => 'required|tel', // Telefone
            'TT014CC009' => 'required|max:1000', // Descritivo
            'TT014CC010' => 'required|max:1000', // Requisitos
            'TT014CC011' => 'max:1000', // benefícios
            'TT014CC012' => 'required|max:1000', // Instruções
            'TT014CC013' => 'required|numeric|min:1|max:9999', // Número de Vagas
            'TT014CC014' => 'required|max:99999', // Remuneração
            'TT014CC015' => 'max:150', // Carga Horária
            'TT014CC021' => 'max:250' // Informações adicionais do administrador
    ];



    // Relacionemento 1:Ramo - N:Vagas
	public function ramo() {
		return $this -> belongsTo ( 'App\Ramo', 'TT014CC001','TT002CC000' );
	}

    // Relacionemento 1:Cargo - N:Vagas
	public function cargo() {
		return $this -> belongsTo ( 'App\Cargo', 'TT014CC002','TT003CC000' );
	}

    // Relacionemento 1:Cidade - N:Vagas
	public function cidade() {
		return $this -> belongsTo ( 'App\Cidade', 'TT014CC003','TT008CC000' );
	}

    // Relacionemento 1:Empresa - N:Vagas
    public function empresa() {
		return $this -> belongsTo ( 'App\Empresa', 'TT014CC004', 'TT010CC000' );
	}




}
