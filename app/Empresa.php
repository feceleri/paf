<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model {

	protected $table = 'TT010';
    protected $primaryKey = 'TT010CC000';

	protected $fillable = [

			'TT010CC001', 'TT010CC002', 'TT010CC003', 'TT010CC004', 'TT010CC005', 'TT010CC006', 'TT010CC007', 'TT010CC008',
            'TT010CC009', 'TT010CC010', 'TT010CC011', 'TT010CC012'
	];




    public static $rules = [

        'TT010CC001' => 'required', // ID Ramo
        'TT010CC002' => 'required', // ID Cidade
        'TT010CC003' => 'required|cnpj', // CNPJ
        'TT010CC004' => 'required|max:150', // Razão social
        'TT010CC005' => 'required|max:150', // Nome fantasia
        'TT010CC006' => 'required|cep', // CEP
        'TT010CC007' => 'required|max:150', // Endereço
        'TT010CC008' => 'required|max:150', // Bairro
        'TT010CC009' => 'required|tel', // Telefone
        'TT010CC010' => 'required|max:150|email', // E-mail
        'TT010CC011' => 'max:150|url', // Link
        'TT010CC012' => 'max:250', // Observação
    ];

	// Relacionemento 1:Ramo - N:Empresas
	public function ramo() {
		return $this -> belongsTo ( 'App\Ramo', 'TT010CC001', 'TT002CC000' );
	}

    // Relacionemento 1:Cidade - N:Empresas
	public function cidade() {
		return $this -> belongsTo ( 'App\Cidade', 'TT010CC002', 'TT008CC000' );
	}

    // Relacionemento 1:Empresa - N:Vagas
	public function vagas() {
		return $this -> hasMany ( 'App\Vaga', 'TT014CC004', 'TT010CC000');
	}
}
