<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model {

    protected $table = 'TT017';
    protected $primaryKey = 'TT017CC000';

	protected $fillable = [

			'TT017CC001', // ID Tipo de Documento
            'TT017CC002', // Nome
	];

    public static $rules = [

            'TT017CC001' => 'required' // ID Tipo de Documento
            'TT017CC002' => 'required|max:150' // Nome
    ];
}
