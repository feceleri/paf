<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model {

	protected $table = 'TT003';
    protected $primaryKey = 'TT003CC000';

	protected $fillable = [

			'TT003CC001','TT003CC002','TT003CC003'
	];

    public static $rules = [

            'TT003CC001' => 'required|max:150' // Titulo
    ];

	// Relacionamento 1:Cargo - N:Vagas
	public function vagas() {
		return $this -> hasMany ( 'App\Vaga', 'TT014CC002', 'TT003CC000' );
	}
}
