<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\BloqueiosTemporarios;

class BloqueiosTemporarios extends Model {

	protected $table = 'TT034';
    protected $primaryKey = 'TT034CC000';

	protected $fillable = [

            'TT034CC001', 'TT034CC002', 'TT034CC003', 'TT034CC004'
    ];

    public static $rules = [

            'TT034CC001' => 'required|max:50', // nome
			'TT034CC002' => 'required|max:500', // Texto do aviso
			'TT034CC003' => 'required|date', // Início
            'TT034CC004' => 'required|date', // Data Final
    ];


     public function estaBloqueado(){

        $hoje = Carbon::now();
        $bloqueios = BloqueiosTemporarios::select('TT034CC003','TT034CC004')-> first(  );
        $inicio = $bloqueios->TT034CC003;
        $final = $bloqueios->TT034CC004;

        if ($inicio < $hoje &&  $hoje < $final ){
           return true;
        }

        return false;

    }


}
