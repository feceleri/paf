<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formacao extends Model {

    protected $table = 'TT023';
    protected $primaryKey = 'TT023CC000';

	protected $fillable = [

            'TT023CC001', // ID Currículo
			'TT023CC002', // Curso
			'TT023CC003', // Tipo
            'TT023CC004', // Instituição
            'TT023CC005', // País
            'TT023CC006', // Data de Inicio
            'TT023CC007', // Data de Encerramento
	];

    public static $rules = [

			'TT023CC001' => 'required', // ID Currículo
			'TT023CC002' => 'required|max:150', // Curso
			'TT023CC003' => 'required|max:150', // Tipo
            'TT023CC004' => 'required|max:150', // Instituição
            'TT023CC005' => 'required|max:150', // País
            'TT023CC006' => 'required|date', // Data de Inicio
            'TT023CC007' => 'date|after:TT023CC006', // Data de Encerramento
    ];

	// Relacionamento 1:Curriculo - N:Formações
	public function curriculo() {
		return $this -> belongsTo ( 'App\Curriculo', 'TT023CC001', 'TT021CC000' );
	}
}
