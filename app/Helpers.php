<?php

/**
* Limitar o tamanho do texto
* @param $text: String
* @param $limit: Int
* @return texto String
*/
function limitText($texto, $limite) {
  $contador = strlen($texto);
  if( $contador >= $limite ) {
      $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . ' ...';
      return $texto;
  }
  else {
    return $texto;
  }
}

/**
* Formatar Telefone
* @param $number: String
* @return String
*/
function formatTel( $number ) {
    return preg_replace( '/([1-9]{2})([1-9][0-9]{3,4})([0-9]{4})/', '($1) $2-$3',  $number );
}

/**
* Formatar CEP
* @param $number: String
* @return String
*/
function formatCep( $number ) {
    return preg_replace( '/([0-9]{5})([0-9]{3})/', '$1-$2',  $number );
}

/**
* Formatar Moeda BR
* @param $value: Float
* @return Float
*/
function formatMoney( $value ){
    return number_format ( $value, 2, ',' , '.' );
}


/**
* Formatar Data
* @param $date: Date
* @return Date
*/
function formatDate( $date ){
    return date_format( date_create( $date ),'d/m/Y' );
}

?>
