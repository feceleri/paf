<?php

namespace App\Mail;

use App\Contato;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContatoMail extends Mailable {

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Contato $contato) {
        $this -> contato = $contato;
    }

    /**
     *
     * @return $this
     */
    public function build() {
        return $this -> view('emails.contato')
                -> with( 'contato', $this -> contato )
                -> replyto( 'paf@crfsp.org.br', 'PAF - Programa de Assistência ao Farmacêutico' )
                -> subject( $this -> contato -> assunto );
    }
}
