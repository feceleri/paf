<?php

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CadastrocvMail extends Mailable {

    use Queueable, SerializesModels;

    
    public $nome;
    public $email;
    public $cpf;
    public $crf; //array data
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $cpf,  $crf, $email, $nome ) {
        $this -> nome = $nome;
        $this-> email = $email;
        $this-> crf = $crf;
        $this-> cpf = $cpf;
    }

    /**
     *
     * @return $this
     */
    
    
    public function build() {
       
        return $this -> view('emails.cadastro-cv') // pode acabar aqui se quiser
                ->to($this->email)
            
                ->subject('PAF | Cadastro de Currículo')
                ->with(['email' => $this->email, 'crf' => $this->crf,  'cpf' => $this->cpf,  'nome' => $this->nome])
                -> replyto( 'paf@crfsp.org.br', 'PAF - Programa de Assistência ao Farmacêutico' )                
            ;
    }
}
