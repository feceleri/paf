<?php

namespace App\Mail;

use App\Vaga;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AnuncioVagaMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Vaga $vaga ) {
        $this -> vaga = $vaga;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this -> view('emails.anuncio-vaga')
                -> with( 'vaga', $this -> vaga )
                -> subject( 'PAF - Nova proposta de vaga  ');
    }
}
