<?php

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestMail extends Mailable {

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Contato $contato) {
        $this -> contato = $contato;
    }

    /**
     *
     * @return $this
     */
    public function build() {
        return $this -> view('emails.testmail')
                -> replyto( 'paf@crfsp.org.br', 'PAF - Programa de Assistência ao Farmacêutico' )
                -> subject( "teste email paf" );
    }
}
