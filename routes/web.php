<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::resource('demo', 'DemoController');
/**
 * Home
 */
Route::get( '/', 'HomeController@index' ); /* aqui poderia ser uma função - Route::get( '/',  function () { return view ('texto'); });  com texto ao invés de chamar um controller. */

/**
 * Sobre
 */
Route::get( 'sobre', 'PagesController@sobre' );

/**
 * Artigos
 */
Route::resource( 'artigos', 'ArtigoController', ['only' => ['index', 'show']]);

/***********************************************************
 * -------------------------------------------------------------------------
 *  PAF-Regional
 * -------------------------------------------------------------------------
 */

/**
 * Seccionais
 */
Route::get( 'seccionais/{id?}', ['uses' => 'PagesController@seccional', 'as' => 'seccionais.index']);


/***********************************************************
 * -------------------------------------------------------------------------
 *  PAF-Empregos
 * -------------------------------------------------------------------------
 */

/**
 * Vagas
 */
Route::resource( 'vagas', 'VagaController' );
Route::resource( 'totalvagas', 'VagaController@totalvagas' );
Route::get( 'estagios/create', ['uses' => 'VagaController@estagio', 'as' => 'estagios']);


/**
 * Peritos cadastro
 */

Route::get('perito', ['as' => 'perito', 'uses' => 'PeritoController@create' ]);
Route::post('perito', ['as' => 'perito.store', 'uses' => 'PeritoController@store' ]);




/**
 * Concursos
 */
Route::resource('concursos', 'ConcursoController');
/**
 * Logout
 */
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

/**
 * Currículos
 */

Route::group( ['as' => 'curriculos.', 'prefix' => 'curriculos', 'middleware' => ['auth']], function() {



    /**
     * Home
     */
    Route::get('/',['as' => 'home', function() {
        return View::make('site::Curriculos.home');
    }]);

    /**
     * Curriculos/Farmaceuticos com login
     */
    Route::group( [ 'as' => 'farmaceuticos.', 'prefix' => 'farmaceuticos',  'middleware' => 'IsFarmaceutico'], function() {

        /**
         * Currículo
         */
        Route::resource( 'curriculo', 'Curriculos\CurriculoController', ['except' => [ 'destroy' ]]);
        Route::resource( 'formacao', 'Curriculos\Farmaceuticos\FormacaoController');
        Route::post('formacao/update', ['uses' => 'Curriculos\Farmaceuticos\FormacaoController@update']);
        Route::resource( 'deficiencia', 'Curriculos\Farmaceuticos\DeficienciaController');
        Route::post('deficiencia/update', ['uses' => 'Curriculos\Farmaceuticos\DeficienciaController@update']);
        Route::resource( 'extracurso', 'Curriculos\Farmaceuticos\ExtracursoController');
        Route::post('extracurso/update', ['uses' => 'Curriculos\Farmaceuticos\ExtracursoController@update']);
        Route::resource( 'idioma', 'Curriculos\Farmaceuticos\IdiomaController');
        Route::post('idioma/update', ['uses' => 'Curriculos\Farmaceuticos\IdiomaController@update']);
        Route::resource( 'interesse', 'Curriculos\Farmaceuticos\InteresseController');
        Route::post('interesse/update', ['uses' => 'Curriculos\Farmaceuticos\InteresseController@update']);
        Route::resource( 'experiencia', 'Curriculos\Farmaceuticos\ExperienciaController');
        Route::post('experiencia/update', ['uses' => 'Curriculos\Farmaceuticos\ExperienciaController@update']);
        Route::get( 'meusdados', ['uses' => 'Curriculos\Farmaceuticos\CurriculoController@perfilfarmaceutico', 'as' => 'Curriculo.perfilfarmaceutico']);


    });

    /**
     * Curriculos/Empresas  com login
     */
    Route::group( [ 'as' => 'empresas.', 'prefix' => 'empresas', 'middleware' => 'IsEmpresa'], function() {

        /**
         * Home
         */
        Route::get('/',['as' => 'home', function() {
            return View::make('site::Curriculos.empresas.home');
        }]);
        Route::resource( 'empresa', 'Curriculos\Empresas\EmpresaController');
        Route::post('empresa/update', ['uses' => 'Curriculos\Empresas\EmpresaController@update']);
        Route::post('empresa/{id}/disable', ['uses' => 'Curriculos\Empresas\EmpresaController@disable', 'as' => 'empresa.disable']);

        Route::resource( 'vaga', 'Curriculos\Empresas\VagaController');
        Route::post('vaga/update', ['uses' => 'Curriculos\Empresas\VagaController@update']);
        Route::resource( 'pesquisa', 'Curriculos\Empresas\PesquisacvController');
        Route::post('pesquisa/update', ['uses' => 'Curriculos\Empresas\PesquisacvController@update']);
        Route::resource( 'estatisticavagas', 'Curriculos\Empresas\EstatisticavagasController');
        Route::post('desabilitarvaga/{id}/active', ['uses' => 'Curriculos\Empresas\EstatisticavagasController@disable', 'as' => 'estatisticavagas.disable']);


        /**
        * Sobre
        */
        Route::get( 'sobre', 'PagesController@sobre' );
        });
        });






/***********************************************************
 * -------------------------------------------------------------------------
 *  PAF-Descontos
 * -------------------------------------------------------------------------
 */

/**
 * Descontos
 */
Route::resource('descontos', 'DescontoController');
Route::get( 'empresas', ['uses' => 'DescontoController@empresa', 'as' => 'empresa']);

 /**
  * Usuario Inativo
  */
    Route::get( 'usuarioinativo', 'PagesController@usuarioinativo' );


 /***********************************************************
 * -------------------------------------------------------------------------
 *  Envio de Contatos
 * -------------------------------------------------------------------------
 */
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@create' ]);
Route::get( 'contato/serparceiro', ['uses' => 'ContatoController@serparceiro', 'as' => 'serparceiro']);
Route::get( 'contato/indicarparceiro', ['uses' => 'ContatoController@indicarparceiro', 'as' => 'indicarparceiro']);
Route::get( 'contato/retiraranuncio', ['uses' => 'ContatoController@retiraranuncio', 'as' => 'retiraranuncio']);
Route::post('contato', ['as' => 'contato.store', 'uses' => 'ContatoController@store' ]);

/***********************************************************
 * -------------------------------------------------------------------------
 *  Cidades do Brasil
 * -------------------------------------------------------------------------
 */
Route::get( '/ufs/', function( $uf = null ) {
    return response() -> json( App\Cidade::select( 'TT008CC001' ) -> distinct('TT008CC001') -> orderBy('TT008CC001') -> get());
});

Route::get( '/cidades/{uf}', function( $uf = null ) {
    return response() -> json( App\Cidade::where( 'TT008CC001', $uf ) -> orderBy('TT008CC003') -> get());
});

/**
 * Teste Email
 */
Route::get( 'email', 'PagesController@email' );



//TESTES DE LOGIN E PERFIL////////////////////////////////////////////////////////////


Route::get( '/teste', function () {

 $user = Auth::user();

    if($user->isAdmin()){
        echo "Logado como Admin";

    }
    if($user->isFarmaceutico()){
        echo "Logado como Farmaceutico";

    }
    if($user->isEmpresa()){
        echo "Logado como Empresa";

    }

});

/***********************************************************
 * -------------------------------------------------------------------------
 *  Cadastro de Farmaceuticos
 * -------------------------------------------------------------------------
 */ 

        /**
         * Digitar o CPF
         */
Route::resource( 'cadastro', 'Soap\SoapController' );
Route::post('inserirCRF', 'Soap\SoapController@buscasoap')->name('cadastro.buscasoap');
Route::post('inserirEmail', 'Soap\SoapController@validacrf')->name('cadastro.validacrf');
Route::get('pfs', 'Soap\SoapController@pfs');
Route::post('enviaemail', ['as' => 'cadastro.enviaemail', 'uses' => 'Soap\SoapController@enviaemail' ]);
//Route::get( 'enviaemail', 'Soap\SoapController@enviaemail')->name('cadastro.enviaemail');


//Route::get( 'admin2/user/roles', ['middleware'=> ['web'], function () {
//
//    return "Middleware role";
//}  ]);

Route::get( '/admin2', 'AdminController@index');

//TESTES DE LOGIN E PERFIL////////////////////////////////////////////////////////////


/***********************************************************
 * -------------------------------------------------------------------------
 *  Painel Administrador
 * -------------------------------------------------------------------------
 */
Route::group([ 'as' => 'admin.', 'prefix' => 'admin',  'middleware' => ['auth', 'IsAdmin']], function() {

    /**
    * Home
    */
    Route::get('/',['as' => 'home', function() {
        return View::make('admin::home');
    }]);

    /**
    * Artigos
    */
    Route::resource( 'artigos', 'Admin\ArtigoController' );
    Route::post('artigo/{id}/active', ['uses' => 'Admin\ArtigoController@active', 'as' => 'artigos.active']);

    /**
    * Banners
    */
    Route::resource( 'banners', 'Admin\BannerController' );

     /**
    * Bloqueios Temporários
    */
    Route::resource( 'bloqueios', 'Admin\BloqueiosController' );

    /**
    * Descontos
    */
    Route::resource( 'descontos', 'Admin\DescontoController' );
    Route::post('descontos/{id}/active', ['uses' => 'Admin\DescontoController@active', 'as' => 'descontos.active']);

    /**
    * Cargos
    */
    Route::resource( 'cargos', 'Admin\CargoController' );
    Route::post('cargo/{id}/active', ['uses' => 'Admin\CargoController@active', 'as' => 'cargos.active']);
    Route::post('cargo/{id}/activecv', ['uses' => 'Admin\CargoController@activecv', 'as' => 'cargos.activecv']);


    /**
    * Concursos
    */
    Route::resource( 'concursos', 'Admin\ConcursoController' );
    Route::post('concurso/{id}/active', ['uses' => 'Admin\ConcursoController@active', 'as' => 'concurso.active']);


    /**
    * Categorias
    */
    Route::resource( 'categorias', 'Admin\CategoriaController' );

    /**
    * Curriculos
    */
    Route::resource( 'curriculos', 'Admin\CurriculoController' );



    /**
    * Empresas
    */
    Route::resource( 'empresas', 'Admin\EmpresaController' );

     /**
    * Estatísticas
    */
    Route::resource( 'estatisticas', 'Admin\EstatisticasController' );


    /**
    * Linguas
    */
    Route::resource( 'linguas', 'Admin\LinguaController' );



    /**
    * Parceiros
    */
    Route::resource( 'parceiros', 'Admin\ParceiroController' );
    Route::post('parceiro/{id}/active', ['uses' => 'Admin\ParceiroController@active', 'as' => 'parceiro.active']);

    /**
    * Ramos
    */
    Route::resource( 'ramos', 'Admin\RamoController' );

    /**
    * Temas
    */
    Route::resource( 'temas', 'Admin\TemaController' );

     /**
    * Testmail
    */
    Route::get('testmail', ['as' => 'testmail', 'uses' => 'TestmailController@create' ]);
    Route::post('testmail', ['as' => 'testmail.store', 'uses' => 'TestmailController@store' ]);

    /**
    * Vagas
    */
    Route::resource( 'vagas', 'Admin\VagaController' );
    Route::post('vaga/{id}/active', ['uses' => 'Admin\VagaController@active', 'as' => 'vagas.active']);
    Route::post('vaga/{id}/disable', ['uses' => 'Admin\VagaController@disable', 'as' => 'vagas.disable']);

     /**
    * Peritos
    */
    Route::resource( 'peritos', 'Admin\PeritoController' );
    Route::get('peritos/excel/{type}', 'Admin\PeritoController@excel');
    Route::get('peritos/downloadExcel/{type}', 'Admin\PeritoController@downloadExcel');
    

         /**
    * Usuários
    */
    Route::resource( 'usuarios', 'Admin\UsuarioController' );
    Route::get('usuarios/downloadExcel/{type}', 'Admin\UsuarioController@downloadExcel');
    Route::post('usuarios/{id}/active', ['uses' => 'Admin\UsuarioController@active', 'as' => 'usuarios.active']);





});

//
//Route::group([ 'as' => 'farmaceutico.', 'prefix' => 'farmaceutico',  'middleware' => ['auth', 'IsFarmaceutico']], function() {
//
//    /**
//    * Home
//    */
//    Route::get('/',['as' => 'home', function() {
//        return View::make('farmaceutico::home');
//    }]);
//
//});
//
//
//Route::group([ 'as' => 'empresa.', 'prefix' => 'empresa',  'middleware' => ['auth', 'IsEmpresa']], function() {
//
//    /**
//    * Home
//    */
//    Route::get('/',['as' => 'home', function() {
//        return View::make('empresa::home');
//    }]);
//

//});

Auth::routes();
