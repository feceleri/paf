$(document).ready(function () {


    /***********************************************************
     * -------------------------------------------------------------------------
     * Hash Change
     * -------------------------------------------------------------------------
     */
    $(window).on('hashchange', function () {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }
            else {
                getData(page);
            }
        }
    });
    /***********************************************************
     * -------------------------------------------------------------------------
     * Pagination responsive
     * -------------------------------------------------------------------------
     */
    $(document).ready(function () {
        $(".paginationX").rPage();
    });
    /***********************************************************
     * -------------------------------------------------------------------------
     * Paginação
     * -------------------------------------------------------------------------
     */
    $(document).on('click', '.pagination a', function (event) {
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        getData(page);
    });
    /***********************************************************
     * -------------------------------------------------------------------------
     * Aplicar Filtros
     * -------------------------------------------------------------------------
     */
    $(document).on('click', '.apply-filter', function (event) {
        event.preventDefault();
        var page = $('.pagination a').attr('href').split('page=')[1];
        getData(page);
    });



    /***********************************************************
     * -------------------------------------------------------------------------
     * Pegar Dados
     * -------------------------------------------------------------------------
     */
    function getData(page) {
        $.ajaxSetup({
            header: $('meta[name="_token"]').attr('content')
        }), $.ajax({
            url: '?page=' + page
            , data: $('.filter-form').serialize()
            , type: 'GET'
            , datatype: "html"
            , beforeSend: function () {
                //$('.load-ajax').append('<img class="load-img" src="/img/loading.gif"/>');
            }
        }).done(function (data) {
            console.log(data);
            $(".load-ajax").empty().html(data);
            location.hash = page;
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            alert('No response from server');
        });
    }
});
