$(document).ready(function () {
    /***********************************************************
     * -------------------------------------------------------------------------
     * Toggler Menu
     * -------------------------------------------------------------------------
     */
    $('.menu-toggler').on('click', function () {
        if (this.classList.contains("menu-open") === true) {
            this.classList.remove("menu-open");
            $("body").removeClass("menu-open");
        }
        else {
            this.classList.add("menu-open");
            $("body").toggleClass("menu-open");
            $("body").removeClass("search-open");
            $("body").removeClass("filter-open");
            $("#search-toggler").removeClass("search-open");
            $("#filter-button").removeClass("filter-open");
        }
    });



    /***********************************************************
     * -------------------------------------------------------------------------
     * Toggler Search
     * -------------------------------------------------------------------------
     */
    $('#search-toggler').on('click', function () {
        if (this.classList.contains("search-open") === true) {
            this.classList.remove("search-open");
            $("body").removeClass("search-open");
        }
        else {
            this.classList.add("search-open");
            $("body").toggleClass("search-open");
            $("body").removeClass("menu-open");
            $("body").removeClass("filter-open");
            $(".menu-toggler").removeClass("menu-open");
            $("#filter-button").removeClass("filter-open");
        }
    });
    /***********************************************************
     * -------------------------------------------------------------------------
     * Toggler Filter
     * -------------------------------------------------------------------------
     */
    $('#filter-button').on('click', function () {
        if (this.classList.contains("filter-open") === true) {
            this.classList.remove("filter-open");
            $("body").removeClass("filter-open");
        }
        else {
            this.classList.add("filter-open");
            $("body").toggleClass("filter-open");
            $("body").removeClass("search-open");
            $("body").removeClass("menu-open");
            $(".menu-toggler").removeClass("menu-open");
            $("#search-toggler").removeClass("search-open");
        }
    });


    /***********************************************************
     * -------------------------------------------------------------------------
     * Toggles - Fechar ao Redimencionar
     * -------------------------------------------------------------------------
     */
    var widthInicial = $(window).width();
    $(window).resize(function () {
        if ($(window).width() != widthInicial) {
            widthInicial = $(window).width();
            $("body").removeClass("menu-open");
            $("body").removeClass("search-open");
            $("body").removeClass("filter-open");
            $(".menu-toggler").removeClass("menu-open");
            $("#search-toggler").removeClass("search-open");
            $("#filter-button").removeClass("filter-open");
        }
    });
    /***********************************************************
     * -------------------------------------------------------------------------
     * Toggles - Fechar ao Tocar o Blur
     * -------------------------------------------------------------------------
     */
    $('#blur').click(function (e) {
        $("body").removeClass("menu-open");
        $("body").removeClass("search-open");
        $("body").removeClass("filter-open");
        $(".menu-toggler").removeClass("menu-open");
        $("#search-toggler").removeClass("search-open");
        $("#filter-button").removeClass("filter-open");
    });
    /***********************************************************
     * -------------------------------------------------------------------------
     * BTN - Cadastrar Curriculos
     * -------------------------------------------------------------------------
     */
    $('#create-curriculos').on('mousedown', function () {
        $(".atalho-BE").toggleClass("press-left");
    });
    $('#create-curriculos').on('mouseup', function () {
        $(".atalho-BE").removeClass("press-left");
    });
    $('#create-curriculos').on('mouseleave', function () {
        $(".atalho-BE").removeClass("press-left");
    });
    /***********************************************************
     * -------------------------------------------------------------------------
     * BTN - Consultar Curriculos
     * -------------------------------------------------------------------------
     */
    $('#search-curriculos').on('mousedown', function () {
        $(".atalho-BE").toggleClass("press-right");
    });
    $('#search-curriculos').on('mouseup', function () {
        $(".atalho-BE").removeClass("press-right");
    });
    $('#search-curriculos').on('mouseleave', function () {
        $(".atalho-BE").removeClass("press-right");
    });

    /******************************************************************************
    */

});
