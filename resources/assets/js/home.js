$(document).ready(function () {
    /***********************************************************
     * -------------------------------------------------------------------------
     * Contador dos Atalhos
     * -------------------------------------------------------------------------
     */
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1500
            , easing: 'swing'
            , step: function (now) {
                $(this).text(Math.ceil(this.Counter));
            }
        });
    });




});

