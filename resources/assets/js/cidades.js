(function ($) {
    $.fn.ufs = function (options) {
        var select = $(this);
        var settings = $.extend({
            'default': select.attr('default')
            , 'onChange': function (TT008CC001) {}
        }, options);
        $.get("/ufs", null, function (json) {
            $.each(json, function (key, value) {
                select.append('<option value="' + value.TT008CC001 + '" ' + (settings.default == value.TT008CC001 ? 'selected' : '') + '>' + value.TT008CC001 + '</option>');
            })
            settings.onChange(select.val());
        }, 'json');
        select.change(function () {
            settings.onChange(select.val());
        });
    };
    $.fn.cidades = function (options) {
        var select = $(this);
        var settings = $.extend({
            'default': select.attr('default')
            , 'TT008CC001': null
        }, options);
        if (!settings.TT008CC001 == null) select.html('<option>Carregando..</option>');
        $.get("/cidades/" + settings.TT008CC001, null, function (json) {
            select.html('<option value="">Selecione...</option>');
            $.each(json, function (key, value) {
                select.append('<option value="' + value.TT008CC000 + '" ' + ((settings.default == value.TT008CC000 || settings.default == value.TT008CC003) ? 'selected' : '') + '>' + value.TT008CC003 + '</option>');
            })
        }, 'json');
    };
}(jQuery));
