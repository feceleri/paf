$(document).ready(function () {
    /***********************************************************
     * -------------------------------------------------------------------------
     * Cidades BR
     * -------------------------------------------------------------------------
     */
    // Cidades
    $('#uf').ufs({
        onChange: function (uf) {
            $('#cidade').cidades({
                TT008CC001: uf
            });
        }
    });
    // Cidades-Vaga
    $('#uf-vaga').ufs({
        onChange: function (uf) {
            $('#cidade-vaga').cidades({
                TT008CC001: uf
            });
        }
    });

    // Cidades-Empresa
    $('#uf-empresa').ufs({
        onChange: function (uf) {
            $('#cidade-empresa').cidades({
                TT008CC001: uf
            });
        }
    });
});
