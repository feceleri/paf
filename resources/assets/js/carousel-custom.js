$(document).ready(function () {
    /***************************************************************************
     * -------------------------------------------------------------------------
     * Carousel Banner
     * -------------------------------------------------------------------------
     */
    $("#owlBanner").owlCarousel({
        items: 1
        , margin: 24
        , loop: true
        , autoplay: true
        , autowidth: true
        , nav: true
        , navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
    });
    /***************************************************************************
     * -------------------------------------------------------------------------
     * Carousel Artigos
     * -------------------------------------------------------------------------
     */
    $("#owlArtigos").owlCarousel({
        items: 3
        , margin: 24
        , loop: true
        , autoplay: false
        , autowidth: true
        , responsiveClass: true
        , responsive: {
            0: {
                items: 1
            , }
            , 768: {
                items: 2
            , }
            , 992: {
                items: 3
            , }
            , 1200: {
                items: 1
            , }
        }
    });
    /***************************************************************************
     * -------------------------------------------------------------------------
     * Carousel Parceiros CB
     * -------------------------------------------------------------------------
     */
    $("#owlPafDescontos").owlCarousel({
        loop: true
        , items: 4
        , autoplay: true
        , autowidth: true
        , margin: 24
        , responsiveClass: true
        , responsive: {
            0: {
                items: 2
            , }
            , 992: {
                items: 3
            , }
        }
    });
    /***************************************************************************
     * -------------------------------------------------------------------------
     * Carousel Parceiros BE
     * -------------------------------------------------------------------------
     */
    $("#owlPafEmpregos").owlCarousel({
        loop: true
        , items: 4
        , autoplay: true
        , autowidth: true
        , margin: 24
        , responsiveClass: true
        , responsive: {
            0: {
                items: 2
            , }
            , 992: {
                items: 3
            , }
        }
    });
});
