<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':Attribute deve ser aceito',
    'active_url'           => ':Attribute não é uma URL válida',
    'after'                => 'Informe uma data maior',
    'alpha'                => ':Attribute deve conter somente letras',
    'alpha_dash'           => ':Attribute deve conter letras, números e traços',
    'alpha_num'            => ':Attribute deve conter somente letras e números',
    'array'                => ':Attribute deve ser um array',
    'before'               => 'Informe uma data menor',
    'between'              => [
        'numeric' => ':Attribute deve estar entre :min e :max',
        'file'    => ':Attribute deve estar entre :min e :max kilobytes',
        'string'  => ':Attribute deve estar entre :min e :max caracteres',
        'array'   => ':Attribute deve ter entre :min e :max itens',
    ],
    'boolean'              => ':Attribute deve ser verdadeiro ou falso',
    'cep'                  => 'Cep inválido',
    'confirmed'            => 'A confirmação de :attribute não confere',
    'cnpj'                 => 'CNPJ inválido',
    'date'                 => ':Attribute não é uma data válida',
    'date_format'          => ':Attribute não confere com o formato :format',
    'different'            => ':Attribute e :other devem ser diferentes',
    'digits'               => ':Attribute deve ter :digits dígitos',
    'digits_between'       => ':Attribute deve ter entre :min e :max dígitos',
    'dimensions'           => 'Dimenções inválidas',
    'email'                => 'Informe um endereço de e-mail válido',
    'exists'               => 'O :attribute selecionado é inválido',
    'filled'               => ':Attribute é um campo obrigatório',
    'image'                => 'Informe uma imagem',
    'in'                   => ':Attribute é inválido',
    'integer'              => ':Attribute deve ser um inteiro',
    'ip'                   => ':Attribute deve ser um endereço IP válido',
    'json'                 => ':Attribute deve ser um JSON válido',
    'max'                  => [
        'numeric' => 'Máximo :max',
        'file'    => 'Máximo :max KB',
        'string'  => 'Máximo :max caracteres',
        'array'   => ':Attribute não deve ter mais que :max itens',
    ],
    'mimes'                => 'Informe arquivos do tipo: :values',
    'min'                  => [
        'numeric' => 'Mínimo :min',
        'file'    => ':Attribute deve ter no mínimo :min kilobytes',
        'string'  => ':Attribute deve ter no mínimo :min caracteres',
        'array'   => ':Attribute deve ter no mínimo :min itens',
    ],
    'not_in'               => 'O :attribute selecionado é inválido',
    'numeric'              => 'Campo numérico',
    'regex'                => 'O formato de :attribute é inválido',
    'required'             => 'Campo obrigatório',
    'required_if'          => 'O campo :attribute é obrigatório quando :other é :value',
    'required_unless'      => 'O :attribute é necessário a menos que :other esteja em :values',
    'required_with'        => 'O campo :attribute é obrigatório quando :values está presente',
    'required_with_all'    => 'O campo :attribute é obrigatório quando :values estão presentes',
    'required_without'     => 'O campo :attribute é obrigatório quando :values não está presente',
    'required_without_all' => 'O campo :attribute é obrigatório quando nenhum destes estão presentes: :values',
    'same'                 => ':Attribute e :other devem ser iguais',
    'size'                 => [
        'numeric' => ':Attribute deve ser :size',
        'file'    => ':Attribute deve ter :size kilobytes',
        'string'  => ':Attribute deve ter :size caracteres',
        'array'   => ':Attribute deve conter :size itens',
    ],
    'string'               => ':Attribute deve ser uma string',
    'tel'                  => 'Telefone inválido',
    'timezone'             => ':Attribute deve ser uma timezone válida',
    'unique'               => ':Attribute já está em uso',
    'url'                  => 'URL inválida',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
//
//    'custom' => [
//        'attribute-name' => [
//            'rule-name' => 'custom-message',
//        ],
//    ],

    'custom' => [
        'TT014CC014' => [
            'salario' => 'Insira um salário válido',
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
