<nav class="text-center" aria-label="Page navigation">
    <ul class="pagination justify-content-center">
        <!-- Previous Page Link -->
        @if ( $paginator -> onFirstPage() )
            <li class="page-item pagination-prev disabled">
                <a class="page-link" href="#" tabindex="-1" aria-label="Previous">
                    <span aria-hidden="true">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
        @else
            <li class="page-item pagination-prev">
                <a class="page-link" href="{!! $paginator -> previousPageUrl() !!}" tabindex="-1" aria-label="Previous">
                    <span aria-hidden="true">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
        @endif
        <!-- /Previous Page Link -->

        <!-- Pagination Elements -->
        @foreach ( $elements as $element )
            <!-- "Three Dots" Separator -->
            @if ( is_string( $element ))
                <li class="page-item disabled">
                    <a class="page-link" href="#">{!! $element !!}</a>
                </li>
            @endif
            <!-- /"Three Dots" Separator -->

            <!-- Array Of Links -->
            @if (is_array( $element ))
                @foreach ( $element as $page => $url)
                    @if ( $page == $paginator -> currentPage() )
                        <li class="page-item active">
                            <a class="page-link" href="{!! $url !!}">{!! $page !!}</a>
                        </li>
                    @else
                        <li class="page-item hidden-sm-down">
                            <a class="page-link" href="{!! $url !!}">{!! $page !!}</a>
                        </li>
                    @endif
                @endforeach
            @endif
            <!-- /Array Of Links -->
        @endforeach
        <!-- /Pagination Elements -->

        <!-- Next Page Link -->
        @if ($paginator->hasMorePages())
            <li class="page-item pagination-next">
                <a class="page-link" href="{!! $paginator->nextPageUrl() !!}" aria-label="Next">
                    <span aria-hidden="true">
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        @else
            <li class="page-item pagination-next disabled">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
        @endif
        <!-- /Next Page Link -->
    </ul>
</nav>
