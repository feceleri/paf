<!-- layout Master -->
@extends('emails.email')

<!-- Conteúdo -->
@section('conteudo')

    <p>
        A vaga cadastrada pela empresa <strong>{!! $vaga -> empresa -> TT010CC005 !!}</strong> em {!! date_format( date_create( $vaga -> created_at ),'d/m/Y' ) !!} foi publicada e ficará disponível no site do <a href="http://paf.crfsp.org.br/vagas">PAF</a> por 15 dias.

    </p>

     <p><span style="color:#E74C3Cbn"><strong>
         A fim de que possamos prestar melhor serviço, solicitamos a gentileza de nos comunicar se a vaga publicada foi preenchida  por candidato proveniente do PAF-EMPREGOS do CRF-SP.</strong> </span>

    </p>


    <h3>Dados da Empresa</h3>

    <p>
        <strong>Razão Social:</strong> {!! $vaga -> empresa -> TT010CC004 !!}
    </p>

    <p>
        <strong>Nome Fantasia:</strong> {!! $vaga -> empresa -> TT010CC005 !!}
    </p>

    <p>
        <strong>CNPJ:</strong> {!! $vaga -> empresa -> TT010CC003 !!}
    </p>

    <p>
        <strong>Ramo de Atividade:</strong> {!! $vaga -> empresa -> ramo -> TT002CC001 !!}
    </p>

    <p>
        <strong>Telefone:</strong> {!! $vaga -> empresa -> TT002CC009 !!}
    </p>

    <p>
        <strong>E-mail:</strong> {!! $vaga -> empresa -> TT010CC010 !!}
    </p>

    <p>
        <strong>Site:</strong> {!! $vaga -> empresa -> TT010CC011 !!}
    </p>

    <p>
        <strong>Endereço:</strong>
        {!! $vaga -> empresa -> TT010CC007 !!} -
        {!! $vaga -> empresa -> TT010CC008 !!} -
        {!! $vaga -> empresa -> TT010CC006 !!} -
        {!! $vaga -> empresa -> cidade -> TT008CC003 !!} /
        {!! $vaga -> empresa -> cidade -> TT008CC001 !!}
    </p>

    <h3>Dados da Vaga:</h3>

    <p>
        <strong>Cargo:</strong> {!! $vaga -> cargo -> TT003CC001 !!}
    </p>

    <p>
        <strong>Ramo de atividade:</strong> {!! $vaga -> ramo -> TT002CC001 !!}
    </p>

    <p>
        <strong>Remuneração:</strong> {!! $vaga -> TT014CC014 !!}
    </p>

    <p>
        <strong>Vagas:</strong> {!! $vaga -> TT014CC013 !!}
    </p>

    <p>
        <strong>Horário de Trabalho:</strong> {!! $vaga -> TT014CC015 !!}
    </p>

    <p>
        <strong>Localização:</strong>
        {!! $vaga -> cidade -> TT008CC003 !!} / {!! $vaga -> cidade -> TT008CC001 !!}
    </p>

    <p>
        <strong>Contato:</strong> {!! $vaga -> TT014CC007 !!}
    </p>

    <p>
        <strong>Telefone:</strong> {!! $vaga -> TT014CC008 !!}
    </p>

    <p>
        <strong>E-mail:</strong> {!! $vaga -> TT014CC006 !!}
    </p>

    <p>
        <strong>Descrição das Atividades:</strong> {!! $vaga -> TT014CC009 !!}
    </p>

    <p>
        <strong>Requisitos:</strong> {!! $vaga -> TT014CC010 !!}
    </p>

    <p>
        <strong>Descontos:</strong> {!! $vaga -> TT014CC011 !!}
    </p>

    <p>
        <strong>Como se candidatar:</strong> {!! $vaga -> TT014CC012 !!}
    </p>

     <p>
        <strong>Código da Vaga:</strong> {!! $vaga -> TT014CC000 !!}
    </p>

@stop
