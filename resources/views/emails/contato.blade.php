<!-- layout Master -->
@extends('emails.email')

<!-- Conteúdo -->
@section('conteudo')

<p>
    Esta é a cópia da mensagem enviada por <strong> {!! $contato -> nome !!}</strong> em {!! date_format( date_create(  ),'d/m/Y' ) !!}
</p>

<p>
    <strong>Telefone:</strong> {!! $contato -> telefone !!}
</p>

<p>
    <strong>E-mail:</strong> <a href="{!! $contato -> email !!}"> {!! $contato -> email !!} </a>
</p>

<h4>
    {!! $contato -> assunto !!}:
</h4>


  @if( $contato -> assunto == 'Retirar anúncio')
<p>
  <strong>Razão Social:</strong>   {!! $contato -> razao !!}
</p>
<p>
    <strong>CNPJ:</strong>  {!! $contato -> cnpj !!}
</p>
<p>
    <strong>Título Cargo:</strong> {!! $contato -> titulocargo !!}
</p>
@endif


<p>
    {!! $contato -> mensagem !!}
</p>

@stop
