<!-- layout Master -->
@extends('site::app')

<!-- Scripts -->
@section('script')
    <script asinc src="{!! elixir('js/all.js') !!}"></script>
@stop

<!-- Título -->
@section('titulo') PAF : 404 @stop

<!-- Conteúdo -->
@section('conteudo')

<div class="row">
    <div class="col-12">
        <div class="card error-404">
            <div class="card-block text-center cor-paf">
                <h1 class="card-title">404</h1>
                <p class="card-text">Ops, Página não encontrada</p>
                <i class="fa fa-exclamation-triangle fa-5x" aria-hidden="true"></i>
            </div>
        </div>
    </div>
</div>

@stop
