<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Farmacêuticos | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ URL::Route('curriculos.home') }}">Banco de Currículos</a></li>
            <li class="breadcrumb-item active"><a href="{{ URL::Route('curriculos.farmaceuticos.curriculo.index',  $id) }}">Acesso do Farmacêutico</a></li>
            <li class="breadcrumb-item active">Editar Currículo</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Info -->
<div class="row">
    <div class="col-12 my-std">
        <div class="alert alert-info m-0">
            <div class="text-justify">
                <p class="m-0"> Caso enfrente dificuldades técnicas ou o sistema apresente falhas ao salvar seu currículo, por favor, entre em contato <a class="alert-link" href="{{ URL::to('contato') }}">conosco</a> reportando seu problema para que possamos verificar. </p>
            </div>
        </div>
    </div>
</div>
<!-- /Info -->
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Mensagens de Erro -->@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div> @endif
<!-- /Mensagens de Erro -->
<!-- Currículos -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <!-- Nav tabs -->
                <div class="row">
                    <div class="col-12 my-std">
                        <ul class="nav nav-tabs nav-fill">
                            <li class="nav-item"> <a class="nav-link active" href="{{ URL::Route('curriculos.farmaceuticos.curriculo.edit', $curriculo -> TT021CC000 ) }}" role="tab">Dados Pessoais</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.interesse.create' ) }}">Interesses Profisionais</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.experiencia.create' ) }}">Histórico Profisional</a> </li>
                            <li class="nav-item"> <a class="nav-link " href="{{ URL::Route( 'curriculos.farmaceuticos.formacao.create' ) }}">Formação Acadêmica</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.extracurso.create' ) }}">Cursos e Participações</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.idioma.create' ) }}">Idiomas</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.deficiencia.create' ) }}">Deficiências</a> </li>
                        </ul>
                    </div>
                </div>
                <!-- /Nav tabs -->
                <!-- Tabs -->
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content">
                            <!-- Dados Pessoais -->
                            <div class="tab-pane active" id="dados-pessoais" role="tabpanel">
                                <!-- Aviso -->
                                <div class="row">
                                    <div class="col-12 my-std">
                                        <div class="alert alert-info m-0">
                                            <div class="text-justify">
                                                <p class="m-0"> As informações de seu currículo são independentes dos dados de seu cadastro no CRF-SP e as atualizações aqui realizadas não são de nenhuma forma vinculadas ao seu registro profissional no Conselho. Logo, caso queira realizar atualizações dos seus dados cadastrais junto ao CRF-SP, entre em contato com nossa <a class="alert-link" href="mailto:atendimento@crfsp.org.br">Central de Atendimento</a>, ou realize as atualizações on-line pelo <a class="alert-link" href="http://services.crfsp.org.br/REQNET/lstLogar.aspx?ReturnUrl=%2fREQNET%2fDefault.aspx" target="_blank">Atendimento Eletrônico</a> disponível no Portal do CRF-SP. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Aviso -->
                                <div class="row">
                                    <div class="col-12 my-std"> {{ Form::model( $curriculo, [ 'method' => 'PATCH', 'route' => [ 'curriculos.farmaceuticos.curriculo.update', $curriculo -> TT021CC000 ]]) }}
                                        <div class="row">
                                            <div class="col-12 col-md-5"> {{ $curriculo -> usuario -> TT001CC001 }} | CRF: {{ $curriculo -> usuario -> TT001CC002 }}
                                                <br> Data de nascimento: {{ $curriculo -> TT021CC007 }} | Sexo: @if ( $curriculo -> TT021CC013 == 'M' ) Masculino @elseif ( $curriculo -> TT021CC013 == 'F' ) Feminino | Estado civil: {{ $curriculo -> TT021CC008 }} @endif | Nacionalidade: {{ $curriculo -> TT021CC009 }}
                                                <br> Endereço: {{ $curriculo -> TT021CC004 }} - {{ $curriculo -> cidade -> TT008CC003 }}/{{ $curriculo -> cidade -> TT008CC001 }}
                                                <br> Telefone: {{ $curriculo -> TT021CC005 }}
                                                <br>
                                                <br> </div>

                                                  <div class="col-12 col-md-5">
                                                </div>



                                        </div>
                                        <div class="row">
                                            <!-- Linkedin -->
                                            <div class="col-12 col-md-4">
                                                <div class="form-group @if ( $errors -> has( 'TT021CC006' )) has-danger @endif"> {{ Form::label('TT021CC006', 'Linkedin', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT021CC006', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                                                    <!-- Erro -->@if ($errors->has('TT021CC006'))
                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC006') }} </div> @endif
                                                    <!-- /Erro -->
                                                </div>
                                            </div>
                                            <!-- /Linkedin -->
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input name="TT021CC011" type="checkbox" class="custom-control-input" "@if ($curriculo -> TT021CC011) checked @endif"> <span class="custom-control-indicator"></span><strong> Currículo visível para as empresas </strong></label>
                                            </div>
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input name="TT021CC012" type="checkbox" class="custom-control-input" "@if ($curriculo -> TT021CC012) checked @endif"> <span class="custom-control-indicator"></span> Desejo receber alertas </label>
                                            </div>
                                        </div>
                                        <div class="row mb-1">
                                            <div class="col-12">
                                                <p class="card-text text-uppercase">( * ) Campos obrigatórios</p>
                                            </div>
                                        </div>
                                        <!-- Controle -->
                                        <div class="row">
                                            <div class="col-12 col-md-2"></div>
                                            <!-- Salvar -->
                                            <div class="col-12 col-md-8"> {{ Form::button ( '
                                                <h5 class="m-0 text"><strong>Salvar</strong></h5>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} </div>
                                            <!-- /Salvar -->
                                            <div class="col-12 col-md-2"></div>
                                        </div>
                                        <!-- /Controle -->{{ Form::close() }} </div>
                                </div>
                            </div>
                            <!-- /Dados Pessoais -->
                        </div>
                    </div>
                </div>
                <!-- /Tabs -->
            </div>
        </div>
    </div>
</div>
<!-- /Currículos -->@stop
