<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Farmacêuticos | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ URL::Route('curriculos.home') }}">Banco de Currículos</a></li>
            <li class="breadcrumb-item active"><a href="{{ URL::Route('curriculos.farmaceuticos.curriculo.index',  $id) }}">Acesso do Farmacêutico</a></li>
            <li class="breadcrumb-item active">Editar Currículo</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Info -->
<div class="row">
    <div class="col-12 my-std">
        <div class="alert alert-info m-0">
            <div class="text-justify">
                <p class="m-0"> Caso enfrente dificuldades técnicas ou o sistema apresente falhas ao salvar seu currículo, por favor, entre em contato <a class="alert-link" href="{{ URL::to('contato') }}">conosco</a> reportando seu problema para que possamos verificar. </p>
            </div>
        </div>
    </div>
</div>
<!-- /Info -->
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Mensagens de Erro -->@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div> @endif
<!-- /Mensagens de Erro -->
<!-- Currículos -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <!-- Nav tabs -->
                <div class="row">
                    <div class="col-12 my-std">
                        <ul class="nav nav-tabs nav-fill">
                            <li class="nav-item"> <a class="nav-link active" href="{{ URL::Route('curriculos.farmaceuticos.curriculo.edit', $curriculo -> TT021CC000 ) }}" role="tab">Dados Pessoais</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="#" >Interesses Profisionais</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.experiencia.create' ) }}" >Histórico Profisional</a> </li>
                            <li class="nav-item"> <a class="nav-link "  href="{{ URL::Route( 'curriculos.farmaceuticos.formacao.create' ) }}">Formação Acadêmica</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.extracurso.create' ) }}">Cursos e Participações</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.idioma.create' ) }}">Idiomas</a> </li>
                            <li class="nav-item"> <a class="nav-link"  href="#">Deficiências</a> </li>
                        </ul>
                    </div>
                </div>
                <!-- /Nav tabs -->
                <!-- Tabs -->
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content">
                            <!-- Dados Pessoais -->
                            <div class="tab-pane active" id="dados-pessoais" role="tabpanel">
                                <!-- Aviso -->
                                <div class="row">
                                    <div class="col-12 my-std">
                                        <div class="alert alert-info m-0">
                                            <div class="text-justify">
                                                <p class="m-0"> As informações de seu currículo são independentes dos dados de seu cadastro no CRF-SP e as atualizações aqui realizadas não são de nenhuma forma vinculadas ao seu registro profissional no Conselho. Logo, caso queira realizar atualizações dos seus dados cadastrais junto ao CRF-SP, entre em contato com nossa <a class="alert-link" href="mailto:atendimento@crfsp.org.br">Central de Atendimento</a>, ou realize as atualizações on-line pelo <a class="alert-link" href="http://services.crfsp.org.br/REQNET/lstLogar.aspx?ReturnUrl=%2fREQNET%2fDefault.aspx" target="_blank">Atendimento Eletrônico</a> disponível no Portal do CRF-SP. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Aviso -->
                                <div class="row">
                                    <div class="col-12 my-std"> {{ Form::model( $curriculo, [ 'method' => 'PATCH', 'route' => [ 'curriculos.farmaceuticos.curriculo.update', $curriculo -> TT021CC000 ]]) }}
                                        <div class="row">
                                            <div class="col-5 col-md-12"> Nome: {{ $curriculo -> usuario -> TT001CC001 }}
                                                <br> CRF: {{ $curriculo -> usuario -> TT001CC002 }}
                                                <br> Sexo:
                                                 @if ( $curriculo -> TT021CC013 == 'M' ) Masculino
                                                     @elseif (  $curriculo -> TT021CC013 == 'F'  ) Feminino
                                                     @endif
                                                <br>
                                                <br> </div>
                                        </div>
                                        <div class="row">
                                            <!-- Sexo -->
                                            <div class="col-5 col-md-3 col-lg-2">
                                                <div class="form-group @if ( $errors -> has( 'TT021CC013' )) has-danger @endif"> {{ Form::label('TT021CC013', 'Sexo *', ['class' => 'form-con trol-label']) }} {{ Form::select ( 'TT021CC013', [ 'M' => 'Masculino', 'F' => 'Feminino' ], null, [ 'class' => 'form-control', 'placeholder' => 'Selecione...' ] ) }}
                                                    <!-- Erro -->@if ($errors->has('TT021CC013'))
                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC013') }} </div> @endif
                                                    <!-- /Erro -->
                                                </div>
                                            </div>
                                            <!-- /Sexo -->
                                            <!-- UF -->
                                            <div class="col-4 col-sm-3 col-md-2">
                                                <div class="form-group"> {{ Form::label('uf', 'UF *', ['class' => 'form-control-label']) }} {{ Form::select ( 'uf', [''], null, [ 'id' => 'uf', 'class' => 'form-control', 'default' => old('uf', $curriculo -> cidade -> TT008CC001) ] ) }} </div>
                                            </div>
                                            <!-- /UF -->
                                            <!-- Cidade -->
                                            <div class="col-8 col-sm-9 col-md-4">
                                                <div class="form-group @if ( $errors -> has( 'TT021CC002' )) has-danger @endif"> {{ Form::label('TT021CC002', 'Cidade *', ['class' => 'form-control-label']) }} {{ Form::select ( 'TT021CC002', [''], null, [ 'id' => 'cidade', 'class' => 'form-control', 'default' => old('TT021CC002', $curriculo -> TT021CC002) ] ) }}
                                                    <!-- Erro -->@if ($errors->has('TT021CC002'))
                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC002') }} </div> @endif
                                                    <!-- /Erro -->
                                                </div>
                                            </div>
                                            <!-- /Cidade -->
                                            <!-- Nível Escolar -->
                                            <div class="col-5 col-md-3 col-lg-3">
                                                <div class="form-group @if ( $errors -> has( 'TT021CC010' )) has-danger @endif"> {{ Form::label('TT021CC010', 'Nível Escolar *', ['class' => 'form-con trol-label']) }} {{ Form::select ( 'TT021CC010', [ 'MI' => 'Ensino Médio Incompleto', 'MC' => 'Ensino Médio Completo', 'SI' => 'Superior Incompleto', 'SC' => 'Superior Completo', 'PO' => 'Pós-graduação', 'ME' => 'Mestrado', 'DO' => 'Doutorado', 'PD' => 'Pós-Doutorado' ], null, [ 'class' => 'form-control', 'placeholder' => 'Selecione...' ] ) }}
                                                    <!-- Erro -->@if ($errors->has('TT021CC010'))
                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC010') }} </div> @endif
                                                    <!-- /Erro -->
                                                </div>
                                            </div>
                                            <!-- /Nível Escolar -->
                                        </div>
                                        <div class="row">
                                            <!-- Endereço -->
                                            <div class="col-12 col-md-6">
                                                <div class="form-group @if ( $errors -> has( 'TT021CC004' )) has-danger @endif"> {{ Form::label('TT021CC004', 'Endereço *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT021CC004', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                                                    <!-- Erro -->@if ($errors->has('TT021CC004'))
                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC004') }} </div> @endif
                                                    <!-- /Erro -->
                                                </div>
                                            </div>
                                            <!-- /Endereço -->
                                            <!-- Telefone -->
                                            <div class="col-12 col-md-3">
                                                <div class="form-group @if ( $errors -> has( 'TT021CC005' )) has-danger @endif"> {{ Form::label('TT021CC005', 'Telefone *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT021CC005', null, [ 'class' => 'form-control tel', 'maxlength' => '15' , 'placeholder' => '(00) 0000-0000' ] ) }}
                                                    <!-- Erro -->@if ($errors->has('TT021CC005'))
                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC005') }} </div> @endif
                                                    <!-- /Erro -->
                                                </div>
                                            </div>
                                            <!-- /Telefone -->
                                            <!-- Linkedin -->
                                            <div class="col-12 col-md-4">
                                                <div class="form-group @if ( $errors -> has( 'TT021CC006' )) has-danger @endif"> {{ Form::label('TT021CC006', 'Linkedin', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT021CC006', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                                                    <!-- Erro -->@if ($errors->has('TT021CC006'))
                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC006') }} </div> @endif
                                                    <!-- /Erro -->
                                                </div>
                                            </div>
                                            <!-- /Linkedin -->
                                            <!-- Data de Nascimento -->
                                            <div class="col-12 col-lg-3">
                                                <div class="form-group @if ( $errors -> has( 'TT021CC007' )) has-danger @endif"> {{ Form::label('TT021CC007', 'Data de Nascimento *', ['class' => 'form-control-label']) }} {{ Form::date ( 'TT021CC007', null , [ 'max' => '2010-12-31', 'class' => 'form-control' ] ) }}
                                                    <!-- Erro -->@if ($errors->has('TT021CC007'))
                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC007') }} </div> @endif
                                                    <!-- /Erro -->
                                                </div>
                                            </div>
                                            <!-- Data de Nascimento -->
                                            <!-- Estado Civil -->
                                            <div class="col-12 col-md-2">
                                                <div class="form-group @if ( $errors -> has( 'TT021CC008' )) has-danger @endif"> {{ Form::label('TT021CC008', 'Estado Civil', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT021CC008', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                                                    <!-- Erro -->@if ($errors->has('TT021CC008'))
                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC008') }} </div> @endif
                                                    <!-- /Erro -->
                                                </div>
                                            </div>
                                            <!-- /Estado Civil -->
                                            <!-- Nacionalidade -->
                                            <div class="col-12 col-md-2">
                                                <div class="form-group @if ( $errors -> has( 'TT021CC009' )) has-danger @endif"> {{ Form::label('TT021CC009', 'Nacionalidade', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT021CC009', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                                                    <!-- Erro -->@if ($errors->has('TT021CC009'))
                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC009') }} </div> @endif
                                                    <!-- /Erro -->
                                                </div>
                                            </div>
                                            <!-- /Nacionalidade -->
                                            <!-- Novo -->
                                            <div class="float-left">
                                                <a class="btn btn-admin" href="{{ URL::Route( 'curriculos.farmaceuticos.formacao.create' ) }}"> <i class="fa fa-plus fa-2x" aria-hidden="true"></i> </a>
                                            </div>
                                            <!-- /Novo -->
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input name="TT021CC011" type="checkbox" class="custom-control-input" "@if ($curriculo -> TT021CC011) checked @endif"> <span class="custom-control-indicator"></span> Currículo visível para as empresas </label>
                                            </div>
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input name="TT021CC012" type="checkbox" class="custom-control-input" "@if ($curriculo -> TT021CC012) checked @endif"> <span class="custom-control-indicator"></span> Desejo receber alertas </label>
                                            </div>
                                        </div>
                                        <div class="row mb-1">
                                            <div class="col-12">
                                                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
                                            </div>
                                        </div>
                                        <!-- Controle -->
                                        <div class="row">
                                            <div class="col-12 col-md-2"></div>
                                            <!-- Salvar -->
                                            <div class="col-12 col-md-8"> {{ Form::button ( '
                                                <h5 class="m-0 text"><strong>Salvar</strong></h5>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} </div>
                                            <!-- /Salvar -->
                                            <div class="col-12 col-md-2"></div>
                                        </div>
                                        <!-- /Controle -->{{ Form::close() }} </div>
                                </div>
                            </div>
                            <!-- /Dados Pessoais -->

                            </div>
                        </div>
                    </div>
                    <!-- /Tabs -->
                </div>
            </div>
        </div>
    </div>
    <!-- /Currículos -->@stop
