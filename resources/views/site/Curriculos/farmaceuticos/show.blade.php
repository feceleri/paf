<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Farmacêuticos | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ URL::Route('curriculos.home') }}">Banco de Currículos</a></li>
            <!--            <li class="breadcrumb-item active"><a href="{{ URL::Route('curriculos.farmaceuticos.curriculo.index',  $id) }}">Acesso do Farmacêutico</a></li>-->
            <li class="breadcrumb-item active">Visualizar Currículo</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Info -->
<div class="row">
    <div class="col-12 my-std">
        <div class="alert alert-info m-0">
            <div class="text-justify">
                <p class="m-0"> Caso enfrente dificuldades técnicas ou o sistema apresente falhas ao salvar seu currículo, por favor, entre em contato <a class="alert-link" href="{{ URL::to('contato') }}">conosco</a> reportando seu problema para que possamos verificar. </p>
            </div>
        </div>
    </div>
</div>
<!-- /Info -->
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Mensagens de Erro -->@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div> @endif
<!-- /Mensagens de Erro -->
<!-- Currículos -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="dados-pessoais" role="tabpanel">
                                <div class="row">
                                    <div class="col-12 my-std">
                                        <div class="row">
                                            <div class="col-5 col-md-12">
                                                <h5>
                                                   {{ $curriculo -> usuario -> TT001CC001 }}
                                               </h5>
                                                <h6>CRF: {{ $curriculo -> usuario -> TT001CC002 }}</h6>
                                                <br>
                                                <h5>DADOS PESSOAIS</h5>
                                                <p> <b>Idade:</b> {{ $curriculo -> TT021CC007 }}
                                                    <br><b>Sexo:</b> @if ( $curriculo -> TT021CC013 == 'M' ) Masculino @elseif ( $curriculo -> TT021CC013 == 'F' ) Feminino
                                                    <br> <b>Estado civil:</b> {{ $curriculo -> TT021CC008 }} @endif
                                                    <br> <b>Nacionalidade:</b> {{ $curriculo -> TT021CC009 }}
                                                    <br> <b>Endereço:</b> {{ $curriculo -> TT021CC004 }} - {{ $curriculo -> cidade -> TT008CC003 }}/{{ $curriculo -> cidade -> TT008CC001 }}
                                                    <br> <b>Telefone:</b> {{ $curriculo -> TT021CC005 }}
                                                    <br><b>E-mail:</b> {{ $curriculo -> usuario -> TT001CC004 }}
                                                    <br><b>Linkedin:</b> {{ $curriculo -> TT021CC006 }}</p>
                                                <br>
                                                <h5>INTERESSES PROFISSIONAIS</h5> @foreach($interesses as $interesse )
                                                <p class="card-text"> {{ $interesse -> cargo -> TT003CC001 }} </p> @endforeach
                                                <br>
                                                <h5>HISTÓRICO PROFISSIONAL</h5> @foreach($experiencias->sortBy('TT027CC007')->sortByDesc('TT025CC006') as $experiencia )
                                                <p> <b>Empresa:</b> {{ $experiencia -> TT025CC004 }}
                                                    <br> <b>Cargo: </b>{{ $experiencia -> cargo -> TT003CC001 }}
                                                    <br> <b>Ramo: </b>{{ $experiencia -> ramo -> TT002CC001 }}
                                                    <br> <b>Início:</b> {{ $experiencia -> TT025CC005 }}
                                                    <br> @if($experiencia -> TT025CC007 == true) <b>Cargo Atual </b>@else<b>Conclusão </b>{{ $experiencia -> TT025CC006 }} @endif
                                                    <br> @if ( $experiencia -> TT025CC008 ==! NULL ) <b>Descrição:</b> {{ $experiencia -> TT025CC008 }}
                                                    <br> @endif</p> @endforeach
                                                <br>
                                                <h5>FORMAÇÃO ACADÊMICA</h5>
                                                <br> @foreach($formacoes->sortByDesc('TT023CC007') as $formacao )
                                                <p> <strong> @if ( $formacao -> TT023CC003  == 'TC' ) Técnico:
                                                     @elseif ( $formacao -> TT023CC003  == 'BA' ) Bacharel:
                                                     @elseif ( $formacao -> TT023CC003  == 'LI' ) Licenciatura:
                                                     @elseif ( $formacao -> TT023CC003  == 'TN' ) Tecnólogo:
                                                     @elseif ( $formacao -> TT023CC003  == 'PL' ) Pós-graduação Latu Sensu:
                                                     @elseif ( $formacao -> TT023CC003  == 'ME' ) Mestrado:
                                                     @elseif ( $formacao -> TT023CC003  == 'DO' ) Doutorado:
                                                     @elseif ( $formacao -> TT023CC003  == 'PD' ) Pós-doutorado:
                                                     @endif {{ $formacao -> TT023CC002 }}</strong>
                                                    <br> <b>Instituição:</b> {{ $formacao -> TT023CC004 }}
                                                    <br> <b>País: </b>{{ $formacao -> TT023CC005 }}
                                                    <br> <b>Início:</b> {{ $formacao -> TT023CC006 }}
                                                    <br> <b>Conclusão </b>{{ $formacao -> TT023CC007 }}
                                                    <br> </p> @endforeach
                                                <br>
                                                <h5>CURSOS E PARTICIPAÇÕES</h5>
                                                <br> @foreach($extracursos->sortByDesc('TT032CC007') as $extracurso )
                                                <p> <strong> @if ( $extracurso -> TT032CC003  == 'CU' ) Curso:
                                                     @elseif ( $extracurso -> TT032CC003  == 'CO' ) Congresso:
                                                     @elseif ( $extracurso -> TT032CC003  == 'PA' ) Palestra: @endif </strong> {{ $extracurso -> TT032CC002 }}
                                                    <br><b>Instituição:</b> {{ $extracurso -> TT032CC004 }}
                                                    <br><b>País: </b>{{ $extracurso -> TT032CC005 }}
                                                    <br> <b>Início:</b> {{ $extracurso -> TT032CC006 }}
                                                    <br> <b>Conclusão </b>{{ $extracurso -> TT032CC007 }}
                                                    <br> </p> @endforeach
                                                <br>
                                                <h5>IDIOMAS</h5>
                                                <br> @foreach($idiomas->sortByDesc('TT026CC002') as $idioma )
                                                <p> <strong> {{ $idioma -> lingua -> TT031CC001 }} -  @if ( $idioma -> TT026CC006  == 'BAS' ) Básico
                                                     @elseif ( $idioma -> TT026CC006  == 'INT' ) Intermediário
                                                     @elseif ( $idioma -> TT026CC006  == 'AVA' ) Avançado
                                                     @elseif ( $idioma -> TT026CC006  == 'FLU' ) Fluente
                                                      @endif </strong> | <b>Instituição </b>{{ $idioma -> TT026CC003 }} </p>
                                                <br> @endforeach @if ( $temdeficiencia > 1 )
                                                <h5>DEFICIÊNCIAS</h5>
                                                <br> @elseif ( $temdeficiencia > 0 )
                                                <h5>DEFICIÊNCIA</h5>
                                                <br> @endif @foreach($deficiencias->sortByDesc('TT024CC002') as $deficiencia )
                                                <p> <b>   @if ( $deficiencia -> TT024CC002 == 'MOT' ) Motora @elseif ( $deficiencia -> TT024CC002 == 'INT' ) Intelectual @elseif ( $deficiencia -> TT024CC002 == 'AUD' ) Autitiva @elseif ( $deficiencia -> TT024CC002 == 'VIS' ) Visual @endif </b> @if ( $deficiencia -> TT024CC003 ==! NULL ) | {{ $deficiencia -> TT024CC003}} @endif </p> @endforeach </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Currículos -->@stop
