<!-- layout Master -->
@extends('site::app')

<!-- Título -->
@section('titulo') Farmacêuticos | PAF @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ URL::Route('curriculos.home') }}">Banco de Currículos</a></li>
            <li class="breadcrumb-item active">Acesso do Farmacêutico</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Aviso -->
<div class="row">
    <div class="col-12">
        <div class="alert alert-info my-std">
            <div class="text-justify">
                <h5 class="text-uppercase text-center">Bem-vindo ao Banco de currículos do PAF!</h5>
                <p>
                    Neste espaço você pode registrar seu currículo profissional e disponibilizá-lo às empresas ofertantes de vagas no PAF Empregos.
                </p>
                <p>
                    É importante que você mantenha seu currículo sempre atualizado com suas informações profissionais mais recentes, de modo que tenha mais chances de sucesso na conquista da vaga desejada.
                </p>
                <p>
                    Caso queira verificar as oportunidades de trabalho, acesse o <a class="color-paf-empregos" href="{{ URL::Route('vagas.index') }}">PAF Empregos</a>.
                </p>
            </div>
        </div>
    </div>
</div>
<!-- Aviso -->

<div class="row">
    <div class="col-12">
        <div class="row">

            <!-- Editar -->
            <div class="col-12 col-md-4 my-std">
                <a href="{{ URL::Route('curriculos.farmaceuticos.curriculo.edit', $curriculo -> TT021CC000 ) }}">
                    <div class="card">
                        <div class="card-block text-center color-paf-primary">
                            <h1 class="mb-5">
                                <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                            </h1>
<!--                            {{ $id}}-->
                            <h5 class="m-0"> Editar Currículo </h5>
                        </div>
                    </div>
                </a>
            </div>
            <!-- /Editar -->

            <!-- Visualizar -->
            <div class="col-12 col-md-4 my-std">
                <a href="{{ URL::Route('curriculos.farmaceuticos.curriculo.show',  $curriculo -> TT021CC000) }}">
                    <div class="card">
                        <div class="card-block text-center color-paf-primary">
                            <h1 class="mb-5">
                                <i class="fa fa-file-text fa-2x color-paf-primary" aria-hidden="true"></i>
                            </h1>
                            <h5 class="m-0"> Visualizar Currículo </h5>
                        </div>
                    </div>
                </a>
            </div>
            <!-- /Visualizar -->

            <!-- Título -->
<!--
            <div class="col-12 col-md-4 my-std">
                <a href="{{ URL::Route('curriculos.farmaceuticos.curriculo.index') }}">
                    <div class="card">
                        <div class="card-block text-center color-paf-primary">
                            <h1 class="mb-5">
                                <i class="fa fa-key fa-2x" aria-hidden="true"></i>
                            </h1>
                            <h5 class="m-0"> Alterar senha </h5>
                        </div>
                    </div>
                </a>
            </div>
-->
            <!-- /Título -->
        </div>
    </div>
</div>

@stop
