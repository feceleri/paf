<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Farmacêuticos | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ URL::Route('curriculos.home') }}">Banco de Currículos</a></li>
            <li class="breadcrumb-item active"><a href="{{ URL::Route('curriculos.farmaceuticos.curriculo.index') }}">Acesso do Farmacêutico</a></li>
            <li class="breadcrumb-item active">Criar atividade complementar</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Info -->
<div class="row">
    <div class="col-12 my-std">
        <div class="alert alert-info m-0">
            <div class="text-justify">
                <p class="m-0"> Caso enfrente dificuldades técnicas ou o sistema apresente falhas ao salvar seu currículo, por favor, entre em contato <a class="alert-link" href="{{ URL::to('contato') }}">conosco</a> reportando seu problema para que possamos verificar. </p>
            </div>
        </div>
    </div>
</div>
<!-- /Info -->
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Mensagens de Erro -->@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div> @endif
<!-- /Mensagens de Erro -->
<!-- Currículos -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <!-- Nav tabs -->
                <div class="row">
                    <div class="col-12 my-std">
                        <ul class="nav nav-tabs nav-fill">
                            <li class="nav-item"> <a class="nav-link " href="{{ URL::Route('curriculos.farmaceuticos.curriculo.edit', $curriculo -> TT021CC000 ) }}" role="tab">Dados Pessoais</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.interesse.create' ) }}" >Interesses Profisionais</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.experiencia.create' ) }}" >Histórico Profisional</a> </li>
                            <li class="nav-item"> <a class="nav-link "  href="{{ URL::Route( 'curriculos.farmaceuticos.formacao.create' ) }}">Formação Acadêmica</a> </li>
                            <li class="nav-item"> <a class="nav-link active" href="{{ URL::Route( 'curriculos.farmaceuticos.extracurso.create' ) }}">Cursos e Participações</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.idioma.create' ) }}">Idiomas</a> </li>
                            <li class="nav-item"> <a class="nav-link"  href="{{ URL::Route( 'curriculos.farmaceuticos.deficiencia.create' ) }}">Deficiências</a> </li>
                        </ul>
                    </div>
                </div>
                <!-- /Nav tabs -->
                <!-- Tabs -->
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content">
                            <!-- Dados Pessoais -->
                            <div class="tab-pane active" id="dados-pessoais" role="tabpanel">
                                <!-- Formação Acadêmica -->
                                <div class="tab-pane" id="formacao-academica" role="tabpanel">
                                    <!-- Aviso -->
                                    <div class="row">
                                        <div class="col-12 my-std">
                                            <div class="alert alert-info m-0">
                                                <div class="text-justify">
                                                    <!--                                                    <p class="m-0"> Descreva quantos cursos julgar relevantes. Não se preocupe com a ordem, quando o currículo for gerado, eles serão dispostas em ordem cronológica. </p>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Aviso -->
                                    <div class="row"> {{ Form::open(['route' => 'curriculos.farmaceuticos.extracurso.store','method'=>'POST']) }}
                                        <div class="row">
                                            <!-- Dados da Formação -->
                                            <div class="col-12 my-std">
                                                <div class="card">
                                                    <div class="card-block">
                                                        <h4 class="card-title text text-uppercase text-center">
                                                           Incluir atividade complementar
                                                         </h4>
                                                        <div class="row">
                                                            <!-- id curriculo -->{{ Form::hidden('TT032CC001', $curriculo -> TT021CC000) }}
                                                            <!-- /id curriculo -->
                                                             <!-- Tipo -->
                                                            <div class="col-5 col-md-3 col-lg-3">
                                                                <div class="form-group @if ( $errors -> has( 'TT021CC010' )) has-danger @endif"> {{ Form::label('TT032CC003', 'Tipo *', ['class' => 'form-con trol-label']) }} {{ Form::select ( 'TT032CC003', ['CU'=> 'Curso','PA' => 'Palestra','CO'=> 'Congresso' ], null, [ 'class' => 'form-control', 'placeholder' => 'Selecione...' ] ) }}
                                                                    <!-- Erro -->@if ($errors->has('TT021CC010'))
                                                                    <div class="form-control-feedback"> {{ $errors->first('TT021CC010') }} </div> @endif
                                                                    <!-- /Erro -->
                                                                </div>
                                                            </div>
                                                            <!-- /Tipo -->
                                                            <!-- Instituição -->
                                                            <div class="col-12 col-md-6">
                                                                <div class="form-group @if ( $errors -> has( 'TT032CC004' )) has-danger @endif"> {{ Form::label('TT032CC004', 'Instituição *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT032CC004', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                                                                    <!-- Erro -->@if ($errors->has('TT032CC004'))
                                                                    <div class="form-control-feedback"> {{ $errors->first('TT032CC004') }} </div> @endif
                                                                    <!-- /Erro -->
                                                                </div>
                                                            </div>
                                                            <!-- /Instituição -->
                                                            <!-- País -->
                                                            <div class="col-12 col-md-3">
                                                                <div class="form-group @if ( $errors -> has( 'TT032CC005' )) has-danger @endif"> {{ Form::label('TT032CC005', 'País *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT032CC005', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                                                                    <!-- Erro -->@if ($errors->has('TT032CC005'))
                                                                    <div class="form-control-feedback"> {{ $errors->first('TT032CC005') }} </div> @endif
                                                                    <!-- /Erro -->
                                                                </div>
                                                            </div>
                                                            <!-- /País -->
                                                            <!-- Curso -->
                                                            <div class="col-5 col-md-12">
                                                                <div class="form-group @if ( $errors -> has( 'TT032CC002' )) has-danger @endif"> {{ Form::label('TT032CC002', 'Atividade *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT032CC002', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                                                                    <!-- Erro -->@if ($errors->has('TT032CC002'))
                                                                    <div class="form-control-feedback"> {{ $errors->first('TT032CC002') }} </div> @endif
                                                                    <!-- /Erro -->
                                                                </div>
                                                            </div>
                                                            <!-- /Curso -->


                                                            <!-- Data de Inicio -->
                                                            <div class="col-12 col-sm-6 col-lg-3">
                                                                <div class="form-group @if ( $errors -> has( 'TT032CC006' )) has-danger @endif"> {{ Form::label('TT032CC006', 'Inicio *', ['class' => 'form-control-label']) }} {{ Form::date ( 'TT032CC006', \Carbon\Carbon::now(), [ 'max' => '2100-12-31', 'class' => 'form-control', ] ) }}
                                                                    <!-- Erro -->@if ($errors->has('TT032CC006'))
                                                                    <div class="form-control-feedback"> {{ $errors->first('TT032CC006') }} </div> @endif
                                                                    <!-- /Erro -->
                                                                </div>
                                                            </div>
                                                            <!-- /Data de Inicio -->
                                                            <!-- Data de Encerramento -->
                                                            <div class="col-12 col-sm-6 col-lg-3">
                                                                <div class="form-group @if ( $errors -> has( 'TT032CC007' )) has-danger @endif"> {{ Form::label('TT032CC007', 'Encerramento *', ['class' => 'form-control-label']) }} {{ Form::date ( 'TT032CC007', \Carbon\Carbon::tomorrow(), [ 'max' => '2100-12-31', 'class' => 'form-control', ] ) }}
                                                                    <!-- Erro -->@if ($errors->has('TT032CC007'))
                                                                    <div class="form-control-feedback"> {{ $errors->first('TT032CC007') }} </div> @endif
                                                                    <!-- /Erro -->
                                                                </div>
                                                            </div>
                                                            <!-- /Data de Encerramento -->
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <p class="card-text text-uppercase"><strong>(*) Campos obrigatórios</strong></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /Dados da  Formação -->
                                    </div>
                                    <!-- Publica curso -->
                                    <div class="row">
                                        <div class="col-12 my-std">
                                            <div class="card">
                                                <div class="card-block">
                                                    <div class="row">
                                                        <div class="col-12 col-md-2"></div>
                                                        <div class="col-12 col-md-8"> {{ Form::button ( '
                                                            <h5 class="m-0 text"><strong>Enviar</strong></h5>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} </div>
                                                        <div class="col-12 col-md-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Publica curso -->{{ Form::close() }}
                                    <div class="row"> @foreach($extracursos->sortByDesc('TT032CC007') as $extracurso )
                                        <div class="col-12 col-lg-6 my-std">
                                            <div class="card mural h-auto">
                                                <div class="card-block text-left">
                                                    <p class="card-text text-uppercase color-paf-primary"> <strong> @if ( $extracurso -> TT032CC003  == 'CU' ) Curso
                                                     @elseif ( $extracurso -> TT032CC003  == 'CO' ) Congresso
                                                     @elseif ( $extracurso -> TT032CC003  == 'PA' ) Palestra @endif : {{ $extracurso -> TT032CC002 }}</strong> </p>
                                                     <p class="card-text"><b>Instituição:</b>  {{ $extracurso -> TT032CC004  }}  </p>
                                                    <p class="card-text"><b>País: </b>{{ $extracurso -> TT032CC005 }} </p>
                                                    <p class="card-text"><b>Início:</b> {{ $extracurso -> TT032CC006 }} </p>
                                                    <p class="card-text"><b>Conclusão </b>{{ $extracurso -> TT032CC007 }} </p>
                                                    <div class="row">
                                                        <!-- Editar -->
                                                        <div class="col-12 col-sm-4">
                                                            <a class="btn btn-block btn-admin mb-1" href="{{ route( 'curriculos.farmaceuticos.extracurso.edit',    $extracurso -> TT032CC000 )}}"> <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i> </a>
                                                        </div>
                                                        <!-- Editar -->
                                                        <!-- Deletar -->
                                                        <div class="col-12 col-sm-4"> {{ Form::open([ 'method' => 'Delete','route' => [ 'curriculos.farmaceuticos.extracurso.destroy', $extracurso -> TT032CC000 ]]) }} {{ Form::button ( '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} {{ Form::close() }} </div>
                                                        <!-- Deletar -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div> @endforeach </div>
                                    <!-- /Tabs -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Currículos -->@stop
