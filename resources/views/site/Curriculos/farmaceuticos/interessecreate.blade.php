<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Farmacêuticos | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ URL::Route('curriculos.home') }}">Banco de Currículos</a></li>
            <li class="breadcrumb-item active"><a href="{{ URL::Route('curriculos.farmaceuticos.curriculo.index') }}">Acesso do Farmacêutico</a></li>
            <li class="breadcrumb-item active">Incluir Interesse Profissional</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Info -->
<div class="row">
    <div class="col-12 my-std">
        <div class="alert alert-info m-0">
            <div class="text-justify">
                <p class="m-0"> Caso enfrente dificuldades técnicas ou o sistema apresente falhas ao salvar seu currículo, por favor, entre em contato <a class="alert-link" href="{{ URL::to('contato') }}">conosco</a> reportando seu problema para que possamos verificar. </p>
            </div>
        </div>
    </div>
</div>
<!-- /Info -->
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Mensagens de Erro -->@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div> @endif
<!-- /Mensagens de Erro -->
<!-- Currículos -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <!-- Nav tabs -->
                <div class="row">
                    <div class="col-12 my-std">
                        <ul class="nav nav-tabs nav-fill">
                            <li class="nav-item"> <a class="nav-link " href="{{ URL::Route('curriculos.farmaceuticos.curriculo.edit', $curriculo -> TT021CC000 ) }}" role="tab">Dados Pessoais</a> </li>
                            <li class="nav-item"> <a class="nav-link active" href="{{ URL::Route( 'curriculos.farmaceuticos.interesse.create' ) }}">Interesses Profisionais</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.experiencia.create' ) }}">Histórico Profisional</a> </li>
                            <li class="nav-item"> <a class="nav-link " href="{{ URL::Route( 'curriculos.farmaceuticos.formacao.create' ) }}">Formação Acadêmica</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.extracurso.create' ) }}">Cursos e Participações</a> </li>
                            <li class="nav-item"> <a class="nav-link" href="{{ URL::Route( 'curriculos.farmaceuticos.idioma.create' ) }}">Idiomas</a> </li>
                            <li class="nav-item"> <a class="nav-link " href="{{ URL::Route( 'curriculos.farmaceuticos.deficiencia.create' ) }}">Deficiências</a> </li>
                        </ul>
                    </div>
                </div>
                <!-- /Nav tabs -->
                <!-- Tabs -->
                <div class="row">
                    <div class="col-12">
                        <div class="tab-content">
                            <!-- Dados Pessoais -->
                            <div class="tab-pane active" id="dados-pessoais" role="tabpanel">
                                <!-- Formação Acadêmica -->
                                <div class="tab-pane" id="formacao-academica" role="tabpanel">
                                    <!-- Aviso -->
                                    <div class="row">
                                        <div class="col-12 my-std">
                                            <div class="alert alert-info m-0">
                                                <div class="text-justify">
                                                    <!--                                                    <p class="m-0"> Descreva quantos cursos julgar relevantes. Não se preocupe com a ordem, quando o currículo for gerado, eles serão dispostas em ordem cronológica. </p>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Aviso -->
                                    <div class="row"> {{ Form::open(['route' => 'curriculos.farmaceuticos.interesse.store','method'=>'POST']) }}
                                        <!-- Dados da Formação -->
                                        <div class="col-12 my-std">
                                            <div class="card">
                                                <div class="card-block">
                                                    <h4 class="card-title text text-uppercase text-center">
                                                            Incluir interesse
                                                         </h4>
                                                    <div class="row">
                                                        <!-- id curriculo -->{{ Form::hidden('TT027CC002', $curriculo -> TT021CC000) }}
                                                        <!-- /id curriculo -->
                                                        <!-- Cargo -->
                                                        <div class="col-12 col-md-9 col-lg-6">
                                                            <div class="form-group @if ( $errors -> has( 'TT027CC001' )) has-danger @endif"> {{ Form::label('TT027CC001', 'Cargo *', ['class' => 'form-control-label']) }} {{ Form::select ( 'TT027CC001', $cargos, null, [ 'class' => 'form-control', 'placeholder' => 'Selecione...' ] ) }}
                                                                <!-- Erro -->@if ($errors->has('TT027CC001'))
                                                                <div class="form-control-feedback"> {{ $errors->first('TT027CC001') }} </div> @endif
                                                                <!-- /Erro -->
                                                            </div>
                                                        </div>
                                                        <!-- /Cargo -->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <p class="card-text text-uppercase"><strong>(*) Campos obrigatórios</strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /Dados da  Interesse -->
                                    </div>
                                    <!-- Publica formação -->
                                    <div class="row">
                                        <div class="col-12 my-std">
                                            <div class="card">
                                                <div class="card-block">
                                                    <div class="row">
                                                        <div class="col-12 col-md-2"></div>
                                                        <div class="col-12 col-md-8"> {{ Form::button ( '
                                                            <h5 class="m-0 text"><strong>Enviar</strong></h5>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} </div>
                                                        <div class="col-12 col-md-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Publica interesse -->{{ Form::close() }}
                                    <div class="row"> @foreach($interesses as $interesse )
                                        <div class="col-12 col-lg-6 my-std">
                                            <div class="card mural h-auto">
                                                <div class="card-block text-left">
                                                    <p class="card-text">Cargo: <b>{{ $interesse -> cargo -> TT003CC001 }} </b></p>
                                                    <!-- Deletar -->
                                                    <div class="col-12 col-sm-4"> {{ Form::open([ 'method' => 'Delete','route' => [ 'curriculos.farmaceuticos.interesse.destroy', $interesse -> TT027CC000 ]]) }} {{ Form::button ( '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} {{ Form::close() }} </div>
                                                    <!-- Deletar -->
                                                </div>
                                            </div>
                                        </div> @endforeach </div>
                                    <!-- /Tabs -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Currículos -->@stop
