<!-- layout Master -->
@extends('site::app')

<!-- Título -->
@section('titulo') Banco de Currículos | PAF @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{!! URL::to('/') !!}">Home</a></li>
            <li class="breadcrumb-item active"> Banco de Currículos</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->


<div class="row">
    <div class="col-12">
        <!-- Título -->
        <div class="row">
            <div class="col-12 my-std">
                <div class="card">
                    <div class="card-block text-uppercase">
                        <h5 class="color-paf-empregos m-0"> Banco de Currículos </h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Título -->

        <div class="row">
            <!-- Acesso Farmacéutico -->
            <div class="col-12 col-md-6 my-std">
                <div class="card">
                    <div class="card-header p-2 bg-paf-primary text-center">
                        <h4 class="color-white m-1">Acesso do Farmacéutico</h4>
                    </div>
                    <div class="card-block text-center">
                        <h1 class="mb-5"><br>
                            <i class="fa fa-user-circle fa-3x color-paf-primary" aria-hidden="true"></i>
                        </h1>
                        <a href="{{ URL::Route('curriculos.farmaceuticos.curriculo.index') }}">
                            <div class="btn btn-mural text-uppercase"> Entrar </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /Acesso Farmacéutico -->

            <!-- Acesso Empresa -->
            <div class="col-12 col-md-6 my-std">
                <div class="card">
                    <div class="card-header p-2 bg-paf-primary text-center">
                        <h4 class="color-white m-1">Acesso da Empresa</h4>
                    </div>
                    <div class="card-block text-center">
                        <h1 class="mb-5"><br>
                            <i class="fa fa-university fa-3x color-paf-primary" aria-hidden="true"></i>
                        </h1>
                        <a href="{{ URL::Route('curriculos.empresas.home') }}">
                            <div class="btn btn-mural text-uppercase"> Entrar </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Acesso Empresa -->
        </div>
    </div>
</div>

@stop
