<script>
    function mudarCoresLidos() {
        for (i = 0; i < ids.length; i++) {
            var minhadiv = 'div-' + ids[i];
            if (document.getElementById(minhadiv) != null) {
                document.getElementById(minhadiv).style.borderBottomColor = "#F4B01B";
            }
        }
    }
</script>
<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Farmacêuticos | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ URL::Route('curriculos.home') }}">Banco de Currículos</a></li>
             <li class="breadcrumb-item"><a href="{{ URL::to('curriculos/empresas/empresa') }}">Minhas Vagas</a></li>
            <!--            <li class="breadcrumb-item active"><a href="{{ URL::Route('curriculos.farmaceuticos.curriculo.index',  $id) }}">Acesso do Farmacêutico</a></li>-->
            <li class="breadcrumb-item active">Vaga</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Info -->
<div class="row">
    <div class="col-12 my-std">
        <div class="alert alert-info m-0">
            <div class="text-justify">
                <p class="m-0"> Caso enfrente dificuldades técnicas ou o sistema apresente falhas ao salvar seu currículo, por favor, entre em contato <a class="alert-link" href="{{ URL::to('contato') }}">conosco</a> reportando seu problema para que possamos verificar. </p>
            </div>
        </div>
    </div>
</div>
<!-- /Info -->
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Mensagens de Erro -->@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> Tente novamente. É necessário responder à pergunta antes de desativar a vaga. </p>
</div> @endif
<!-- /Mensagens de Erro -->
<!-- NOME CRF -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="title clearfix">
                        <div class="float-right">@if (  Carbon\Carbon::now()->subDays(30) >= $vaga -> TT014CC019   or $vaga -> TT014CC016 == 0 )



                                <div class="btn "> Vaga inativa</div>



<!--
                                  <a href="#" data-toggle="modal" data-target="#modal_show-{{  $vaga -> TT014CC000 }}-hab">

                                <div class="btn btn-mural"> Criar vaga similar</div>
                            </a>
-->

                           @else <a href="#"  data-toggle="modal" data-target="#modal_show-{{  $vaga -> TT014CC000 }}">

                                <div class="btn btn-mural"> Desabilitar vaga</div>
                            </a>  @endif
                        </div>
                    </div>
                    <h4 class="card-text text-uppercase color-paf-primary">
                    <strong>{{ $vaga -> cargo -> TT003CC001 }}</strong>
                </h4>
                    <p class="card-text"><a href="{{ route( 'admin.empresas.show', $vaga -> empresa -> TT010CC000)}}">
                    {{ $vaga -> empresa -> TT010CC004  }} ( {{ $vaga -> ramo -> TT002CC001 }} )
                    <br>CNPJ: {{ $vaga -> empresa -> TT010CC003  }} /
                    Telefone: {{ $vaga -> empresa -> TT010CC009  }}</a> </p>
                    <p class="card-text"> <strong>vagas: {{ $vaga -> TT014CC013 }}</strong> | {{ $vaga -> cidade -> TT008CC003 }} - {{ $vaga -> cidade -> TT008CC001 }} | {{ formatDate($vaga -> TT014CC019) }} </p>
                    <p> R$ {{ formatMoney( $vaga -> TT014CC014 ) }} @if ( $vaga -> TT014CC022 === 'HO' )/ hora @elseif ( $vaga -> TT014CC022 === 'ME' ) @endif | {{ $vaga -> TT014CC015 }} </p>
                    <!-- Mostrar endereço? -->@if ($vaga -> TT014CC017)
                    <p>
                        <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Endereço</strong>
                    </h6> {{ $vaga -> empresa -> TT010CC007 }} - {{ $vaga -> empresa -> TT010CC008 }} - CEP: {{ formatCep($vaga -> empresa -> TT010CC006) }} </p> @endif
                    <p>
                        <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Descrição das Atividades</strong>
                    </h6> {!! $vaga -> TT014CC009 !!} </p>
                    <p>
                        <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Requisitos</strong>
                    </h6> {!! $vaga -> TT014CC010 !!} </p>
                    <p>
                        <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Benefícios</strong>
                    </h6> {!! $vaga -> TT014CC011 !!} </p>
                    <p>
                        <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Como se candidatar</strong>
                    </h6> {!!$vaga -> TT014CC012 !!} </p>
                    <p>
                        <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Código da Vaga:</strong>
                    </h6> {!!$vaga -> TT014CC000 !!} </p>
                </div>
            </div>
        </div>
    </div>
    <!--Modal-->
    <div class="modal fade" id="modal_show-{{ $vaga -> TT014CC000  }}">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><strong>DESABILITAR VAGA - {!!$vaga -> TT014CC000 !!} - {{ $vaga -> cargo -> TT003CC001 }}</strong> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                    <!-- Aviso -->
                    <div class="alert alert-info my-std">
                        <div class="text-justify">
                            <h5 class="text-uppercase text-center">Prezado,</h5>
                            <p> Ao desativar a vaga, caso queira ativar novamente, será necessária nova análize por parte do <a class="cor-link" href="{{ URL::to('contato') }}">CRF-SP</a>. </p>
                        </div>
                    </div>
                    <!-- Aviso -->
                    <div class="card my-std">
                        <div class="card-block text-left">

                            <p> A vaga <strong> {!!$vaga -> TT014CC000 !!} - {{ $vaga -> cargo -> TT003CC001 }}</strong>, publicada em {{ date_format( date_create( $vaga -> TT014CC019 ),'d/m/Y' ) }} foi preenchida através do PAF, Programa de Assistência ao Farmacêutico? Nos informe o número de posições preenchidas através do PAF: *
                                </p>
                        </div>
                        <div> {{ Form::open(['route' =>[ 'curriculos.empresas.estatisticavagas.disable', $vaga -> TT014CC000 ],'method'=>'POST','id'=> 'myForm']) }}
                            <div class="col-12">
                                <!-- Preenchida através do PAF? -->
                                <div class="col-12 col-md-6">
                                    <div class="form-group @if ( $errors -> has( 'TT033CC005' )) has-danger @endif">

<!--

                                       {{ Form::radio ( 'TT033CC002', 'SI', false ) }} SIM <br>
                                       {{ Form::radio ( 'TT033CC002', 'NO', false ) }} NÃO <br>

-->

                                         {{ Form::selectRange('TT033CC005', $vaga->TT014CC013, 0,  null,[
                                        'class' => 'form-control',
                                        'placeholder'=>'Selecione...'
                                    ])}}

                                       <!-- Erro -->@if ($errors->has('TT033CC005'))
                                        <div class="form-control-feedback"> {{ $errors->first('TT033CC002') }} </div> @endif
                                        <!-- /Erro -->
                                         <br><br>* Preenchimento obrigatório.
                                    </div>
                                </div>
                                <!-- /Preenchida através do PAF? -->
                            </div> {{ Form::button ( '
                            <h5 class="m-0 text"><strong>Desabilitar vaga</strong></h5>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit', 'onclick' => 'btnClick()' ] ) }} <br>

                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Modal-->




    <!-- /NOME CRF -->@stop
