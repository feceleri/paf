<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Farmácias | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ URL::Route('curriculos.home') }}">Banco de Currículos</a></li>
            <li class="breadcrumb-item active">Pesquisa de Currículos</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Aviso -->
<div class="row">
    <div class="col-12">
        <div class="alert alert-info my-std">
            <div class="text-justify">
                <h5 class="text-uppercase text-center">Bem-vindo ao Banco de currículos do PAF!</h5>
                <p> Utilize abaixo os campos para uma busca facilitada. </p>
            </div>
        </div>
    </div>
</div>
<!-- Aviso -->
<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="row">
            <div class="col-12 col-md-12"> {!! Form::open(['method'=>'GET','url'=>'curriculos/empresas/pesquisa','class'=>'filter-form navbar-left','role'=>'search']) !!}


                 <!-- Cidade -->
<!--
            <div class="row">
                <div class="col-12 my-std">
                     <div class="card">
                        <div class="card-header bg-paf-primary color-white">
                            <h6 class="card-title text-center m-0"> Cidade </h6>
                        </div>
                        <div class="card-block scroll">
                            <div class="filter-item">
                                <div class="form-check">




                                    @foreach( $cidades as $cidade )

                                    <div class="row">
                                        <div class="col-12">



                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="cidadeCV" value="{{ $cidade -> TT008CC000 }}" class="custom-control-input"   >
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">
                                                    {{ $cidade -> TT008CC003 }}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
-->
            <!-- /Cidade -->








                <div class="col-12 col-md-12">
                    <div class="row">


                        <div class="col-12 col-md-2"> Cidade
                            <select class="form-control" name="cidadeCV">
                                <option value="">Todas</option> @foreach( $cidades as $cidade )
                                <option value="{{ $cidade -> TT008CC000 }}" @if ($cidadeCV==$cidade->TT008CC000) selected @endif >
                                {{ $cidade -> TT008CC003 }}
                                </option> @endforeach </select>
                        </div>

                         <div class="col-12 col-md-2"> Nível de Formação
                            <select class="form-control" name="formacaoCV">
                                <option value="">Todas</option>
<!--                                <option value="TC" @if ( $formacaoCV=="TC" ) selected @endif>Técnico</option>-->
                                <option value="BA" @if ( $formacaoCV=="BA" ) selected @endif>Bacharel</option>
                                <option value="LI" @if ( $formacaoCV=="LI" ) selected @endif>Licenciatura</option>
<!--                                <option value="TN" @if ( $formacaoCV=="TN" ) selected @endif>Tecnólogo</option>-->
                                <option value="PL" @if ( $formacaoCV=="PL" ) selected @endif>Pós Graduação Latu Sensu</option>
                                <option value="ME" @if ( $formacaoCV=="ME" ) selected @endif>Mestrado</option>
                                <option value="DO" @if ( $formacaoCV=="DO" ) selected @endif>Doutorado</option>
                                <option value="PD" @if ( $formacaoCV=="PD" ) selected @endif>Pós-Doutorado</option>
                            </select>
                        </div>


                        <div class="col-12 col-md-2"> Idiomas
                            <select class="form-control" name="idiomaCV">
                                <option value="">Todos</option> @foreach( $linguas as $lingua )
                                <option value="{{ $lingua -> TT031CC000 }}" @if ( $idiomaCV ==  $lingua->TT031CC000 ) selected @endif >{{ $lingua -> TT031CC001 }} </option> @endforeach </select>
                        </div>


                        <div class="col-12 col-md-2"> Ramo
                            <select class="form-control" name="ramoCV">
                                <option value="">Todas</option> @foreach( $ramos as $ramo )
                                <option value="{{ $ramo -> TT002CC000 }}" @if ( $ramoCV== $ramo->TT002CC000  ) selected @endif > {{ $ramo -> TT002CC001 }} </option> @endforeach </select>
                        </div>

                        <div class="col-12 col-md-2"> Cargo
                            <select class="form-control" name="cargoCV">
                                <option value="">Todas</option> @foreach( $cargos as $cargo )
                                <option value="{{ $cargo -> TT003CC000 }}" @if ( $cargoCV== $cargo->TT003CC000  ) selected @endif > {{ $cargo -> TT003CC001 }} </option> @endforeach </select>
                        </div>


                       <div class="col-12 col-md-2"> Deficiência
                            <select class="form-control" name="deficienciaCV">
                                <option value="">-</option>
                                <option value="ANY" @if ( $deficienciaCV=="ANY" ) selected @endif>Qualquer</option>
                                <option value="AUD" @if ( $deficienciaCV=="AUD" ) selected @endif>Auditiva</option>
                                <option value="INT" @if ( $deficienciaCV=="INT" ) selected @endif>Intelectual</option>
                                <option value="MOT" @if ( $deficienciaCV=="MOT" ) selected @endif>Motora</option>
                                <option value="VIS" @if ( $deficienciaCV=="VIS" ) selected @endif>Visual</option>
                            </select>
                        </div>





                        <div class="col-12 col-md-1">
                            <br> <span class="input-group-btn ">
                            <button class="btn btn-block btn-admin mb-1" title="Buscar" type="submit">
                            <i class="fa fa-search fa-2x"></i>
                            </button> </span> </div>
                        <div class="col-12 col-md-1">
                            <br>
                            <a href="/curriculos/empresas/pesquisa" title="Limpar todas as pesquisas" class="btn btn-block btn-admin mb-1" role="button">  <i class="fa fa-eraser fa-2x"></i></a>
                        </div>
                    </div>
                </div> {!! Form::close() !!} </div>
        </div>
    </div>
</div>
<!-- Buscas -->
<!-- PAF Descontos -->
<div class="row"> @foreach( $curriculos as $curriculo )
    <div class="col-12 col-md-6 col-lg-4">
        <div class="row">
            <div class="col-12 my-std">
                <div class="card ">
                    <div class="card-block text-left">
                        <h5>{{ $curriculo -> usuario -> TT001CC001 }}</h5>
                        <!-- Cidade -->{{ $curriculo -> cidade -> TT008CC003 }}/{{ $curriculo -> cidade -> TT008CC001 }}
                        <br>
                        <!-- /Cidade -->
                        <!-- Experiências -->@foreach( $curriculo -> experiencias->sortBy('TT027CC007')->sortByDesc('TT025CC006') as $experiencia )
                        <!-- Pegar apenas a primeira ocorrência -->@if ($loop->first) @if($experiencia -> TT025CC007 == true) Cargo Atual: @else Anterior: @endif
                        <!-- Se cargo atual ou anterior -->{{ $experiencia -> cargo -> TT003CC001 }} | {{ $experiencia -> TT025CC004 }} @endif
                        <!-- /Pegar apenas a primeira ocorrência -->@endforeach
                        <!-- /Experiências -->
                        <br>
                        <a href="{{ URL::Route('curriculos.empresas.pesquisa.show',  $curriculo -> TT021CC000) }}">
                            <div class="btn btn-mural">Ver Currículo</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /PAF Descontos -->@endforeach </div>
<div class="row">
    <div class="col-12">
        <div class="row">
            <!-- Paginação -->{{ $curriculos-> links( 'partials.pagination' ) }} </div>
    </div>
</div> @stop
