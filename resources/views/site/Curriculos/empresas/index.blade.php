<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Farmácias | PAF @stop

<!-- Script -->
@section('script')
<script type="text/javascript">
   window.onload = function() {
       document.getElementsByClassName("apply-filter")[0].click();
};
var ids = [];
</script>
  @stop


<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ URL::Route('curriculos.home') }}">Banco de Currículos</a></li>
            <li class="breadcrumb-item active">Minhas vagas</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Aviso -->
<div class="row">
    <div class="col-12">
        <div class="alert alert-info my-std">
            <div class="text-justify">
                <h5 class="text-uppercase text-center">VAGAS CADASTRADAS</h5>
<!--                <p> Estas são as suas vagas cadastradas.</p>-->
            </div>
        </div>
    </div>
</div>
<!-- Aviso -->
<div class="row">
<!--
    <div class="col-12">
        <div class="card">
            <div class="card-block">123
            </div></div>
</div>
-->

<div class="row">
    <div class="col-12">
        <div class="row">

               @foreach($vagas->sortByDesc('TT014CC019') as $vaga )
               <div class="col-12 col-lg-4 my-std">
                <div class="card">
                    <div class="card-block text-uppercase">




                        <h5 class="card-title">{{ $vaga -> TT014CC005 }}</h5>
                        <p class="card-text"> Salário: R$ {{ $vaga -> TT014CC014 }} @if ( $vaga -> TT014CC022 === 'HO' )/ hora @elseif ( $vaga -> TT014CC022 === 'ME' ) @endif
                            <!--definindo se por hora ou mês -->| Vagas: {{ $vaga -> TT014CC013 }}
                            <br> <b>{{ $vaga -> cidade -> TT008CC003 }} -
                    {{ $vaga -> cidade -> TT008CC001 }}</b></p>
                        <p class="card-text"> Criada em: {{ date_format( date_create( $vaga -> created_at ),'d/m/Y' ) }}
                            <br> @if ( $vaga -> TT014CC019 >= $vaga -> created_at ) Publicada em:<b>
                                @if ( $vaga -> TT014CC019 >=  Carbon\Carbon::now()->subDays(30) and $vaga -> TT014CC016 )
                                <span style="color:MediumSeaGreen  ">
                                @else
                                <span style="color:DarkGrey ">
                                @endif
                                {{ date_format( date_create( $vaga -> TT014CC019  ),'d/m/Y' ) }}<br>

                        </span></b> @endif
                             Código: {{ $vaga -> TT014CC000 }} </p>

                                 <a href="{{ URL::Route('curriculos.empresas.empresa.show',  $vaga -> TT014CC000 ) }}">
                            <div class="btn btn-mural">Ver Vaga</div>
                        </a>

                    </div>
                </div>
            </div>@endforeach
        </div>
    </div>
</div>



 @stop
