<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Farmacêuticos | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{!! URL::to('/') !!}">Home</a></li>
            <li class="breadcrumb-item active">Currículos</li>
            <li class="breadcrumb-item active">EMPRESAS</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Aviso -->
<div class="row">
    <div class="col-12">
        <div class="alert alert-info my-std">
            <div class="text-justify">
                <h5 class="text-uppercase text-center">Bem-vindo ao Banco de currículos do PAF!</h5>
                <p> Neste espaço você pode pesquisar profissionais para suas vagas de Empregos. </p>
                <p> Caso queira verificar, acesse o <a class="color-paf-empregos" href="{{ URL::Route('vagas.index') }}">PAF Empregos</a>. </p>
            </div>
        </div>
    </div>
</div>
<!-- Aviso -->
<div class="row">
    <!-- Nome da empresa -->
    <div class="col-12 col-lg-12">
        <!-- Título -->
        <div class="row">
            <div class="col-12 my-std">
                <div class="card">
                    <div class="card-block text-uppercase">
                        <h5 class="m-0"> Empresa: {{ Auth::user() -> TT001CC001   }}</h5>
                        <br>
                        <h6>CNPJ: {{ Auth::user() -> TT001CC002   }}</h6> </div>
                </div>
            </div>
        </div>
        <!-- /Nome da empresa -->
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="row">


               <div class="col-12 col-lg-4 my-std">
                <div class="card">
                    <div class="card-block text-uppercase">













                        123







                    </div>
                </div>
            </div>
        </div>
    </div>
</div> @stop
