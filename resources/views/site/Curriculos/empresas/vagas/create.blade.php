<!-- layout Master -->
@extends('site::app')

@section('script') <script asinc src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>


<script>


function tipoRem(){
    var tipoCargo = document.getElementById("tipo-cargo").value;//pegando o valor correspondente ao cargo
//    window.alert(tipoCargo);
    if (tipoCargo == 115){ // se o cargo for de professor, 115
    var x = document.getElementById("tipo-remuneracao");// cria a opção por hora
    var option = document.createElement("option");
    option.text = "Horista";
    option.value = "HO";
    x.add(option);
    x.value = "HO";
    document.getElementById("tipoRemuneracaoDiv").style.opacity = "1";// visivel
    }else // se não for professor 115 o cargo
    {
        document.getElementById("tipoRemuneracaoDiv").style.opacity = "0.2";// opacidade 20%
        var x = document.getElementById("tipo-remuneracao");
        if (x.length > 1) {//e tiver mais do que uma opção, remove a segunda opção da lista
           x.remove(1);
            }
    }
}
     window.onload = function (){//carrego aqui a função para que se der erro, possa aparecer as opções
             tipoRem();
        }
</script>

   @stop

<!-- Título -->
@section('titulo') Anunciar Vagas | PAF @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item active">
                <!-- Estágio? -->
                @if($tipoVaga == 'ES')
                    Anunciar Estágio
                @else
                    Anunciar Vagas
                @endif
            </li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Título -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block text-uppercase">
                <h5 class="color-paf-empregos m-0">
                    <!-- Estágio? -->
                    @if($tipoVaga == 'ES')
                        Anunciar Estágio
                    @else
                        Anunciar Vagas
                    @endif
                </h5>
            </div>
        </div>
    </div>
</div>
<!-- /Título -->

<!-- Aviso -->
<div class="alert alert-info my-std">
    <div>
        <p class="mb-0">
            <!-- Estágio? -->
            @if($tipoVaga == 'ES')
                Para anunciar estágios,
            @else
                Para anunciar vagas,
            @endif

            preencha o formulário abaixo e verifique as condições do termo de adesão<br>

        </p>
    </div>
</div>
<!-- /Aviso -->

<!-- Termo de adesão -->
@if ( $errors->has( 'termo' ))
<div class="alert alert-warning alert-dismissible fade show my-std" role="alert">
    <button type="button" class="close pb-0" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        Verifique as condições do <strong>Termo de Adesão</strong>
    </p>
</div>
@endif
<!-- /Termo de adesão -->

<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show my-std" role="alert">
    <button type="button" class="close pb-0" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de erro -->
@if (( count( $errors ) > 0 ) && !( count( $errors ) == 1 && ( $errors->has( 'termo' ))))
<div class="alert alert-danger alert-dismissible fade show my-std" role="alert">
    <button type="button" class="close pb-0" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
   <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de erro -->

{{ Form::open(['route' => 'curriculos.empresas.vaga.store','method'=>'POST','id'=> 'myForm']) }}


{{ Form::hidden('TT014CC018', $tipoVaga) }}

{{ Form::hidden('TT014CC004', $empresa -> TT010CC000) }}

<div class="row">


    <!-- Dados da Vaga -->
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title text text-uppercase text-center">
                    @if($tipoVaga == 'ES')
                        Dados do Estágio
                    @else
                        Dados da Vaga
                    @endif
                </h4>
                <div class="row">

                    <!-- Tipo de Vaga -->
                    {{ Form::hidden('TT014CC018', $tipoVaga) }}
                    <!-- /Tipo de Vaga -->
 <!-- Ramo de Atividade -->
                    <div class="col-12 col-md-9 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT014CC001' )) has-danger @endif">
                            {{ Form::label('TT014CC001', 'Ramo de Atividade *', ['class' => 'form-control-label']) }}
                            {{ Form::select
                                (
                                    'TT014CC001',
                                    $ramos,
                                    null,

                                    [
                                        'class' => 'form-control',
                                        'placeholder' => 'Selecione...'
                                    ]
                                )
                            }}

                            <!-- Erro -->
                            @if ($errors->has('TT014CC001'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC001') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Ramo de Atividade -->
                    <!-- Cargo -->
                     @if($tipoVaga == 'ES')
                              {{ Form::label('TT014CC002', ' ', ['class' => 'form-control-label']) }}
                            {{ Form::hidden('TT014CC002', '4') }}
                               @else

                    <div class="col-12 col-md-9 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT014CC002' )) has-danger @endif">




                              {{ Form::label('TT014CC002', 'Cargo *', ['class' => 'form-control-label']) }}
                            {{ Form::select
                                (
                                    'TT014CC002',
                                    $cargos,
                                    null,

                                    [
                                       'id' => 'tipo-cargo',
                                        'class' => 'form-control',
                                        'onchange' => 'tipoRem()',
                                        'placeholder' => 'Selecione...'
                                    ]
                                )
                            }}      </div>
                    </div>
                            @endif
                            <!-- Erro -->
                            @if ($errors->has('TT014CC002'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC002') }}
                            </div>
                            @endif
                            <!-- /Erro -->

                    <!-- /Cargo -->

                    <!-- Remuneração -->
                    <div class="col-12 col-md-3 col-lg-2">
                        <div class="form-group @if ( $errors -> has( 'TT014CC014' )) has-danger @endif">

                            <!-- Estágio? -->
                            @if($tipoVaga == 'ES')
                                {{ Form::label('TT014CC014', 'Bolsa Auxílio *', ['class' => 'form-control-label']) }}
                            @else
                                {{ Form::label('TT014CC014', 'Remuneração *', ['class' => 'form-control-label']) }}
                            @endif

                            {{ Form::text
                                (
                                    'TT014CC014',
                                    null,

                                    [
                                        'maxlength' => '9',
                                        'class' => 'form-control money',
                                    ]
                                )
                            }}

                            <!-- Erro -->
                            @if ($errors->has('TT014CC014'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC014') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Remuneração -->
                    <!-- Tipo de Remuneração -->


                @if($tipoVaga == 'ES')
               <!--vazio  -->
                @else
                <div class="col-12 col-md-2" id="tipoRemuneracaoDiv">
                    <div class="form-group @if ( $errors -> has( 'TT014CC022' )) has-danger @endif">
                    {{ Form::label('TT014CC022', 'Tipo Remuneração *', ['class' => 'form-control-label']) }}
                    {{ Form::select (
                    'TT014CC022',
                    [ 'ME' => 'Mensal' ],
                     null,
                     [
                       'id' => 'tipo-remuneracao',
                       'class' => 'form-control' ]
                     ) }}
                    <!-- Erro -->@if ($errors->has('TT014CC022'))
                    <div class="form-control-feedback"> {{ $errors->first('TT014CC022') }} </div> @endif
                    <!-- /Erro -->
                </div>
                </div>
                @endif
                    <!-- /Tipo de  Remuneração -->



                </div>

                <div class="row">
                    <!-- Vagas -->
                    <div class="col-12 col-md-3 col-lg-2">
                        <div class="form-group @if ( $errors -> has( 'TT014CC013' )) has-danger @endif">
                            {{ Form::label('TT014CC013', 'Nº de Vagas *', ['class' => 'form-control-label']) }}
                            {{ Form::number
                            (
                                'TT014CC013',
                                null,

                                [
                                    'class' => 'form-control',
                                ]
                            )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT014CC013'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC013') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Vagas -->
                    <!-- Horario de Trabalho -->
                    <div class="col-12 col-md-4">
                        <div class="form-group @if ( $errors -> has( 'TT014CC015' )) has-danger @endif">
                            {{ Form::label('TT014CC015', 'Horario de Trabalho', ['class' => 'form-control-label']) }}
                            {{ Form::text
                                (
                                    'TT014CC015',
                                    null,

                                    [
                                        'class' => 'form-control',
                                        'maxlength' => '150'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT014CC015'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC015') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Horario de Trabalho -->

                    <!-- UF -->



<!--
                    <div class="col-4 col-sm-3 col-md-2"> - Aqui está oculto, pois pediram para somente aceitar vagas de SP.
                        <div class="form-group">



                            {{ Form::label('uf-vaga', 'UF *', ['class' => 'form-control-label']) }}
                            {{ Form::select
                                (
                                    'uf-vaga',
                                    [''],
                                    null,

                                    [
                                        'id' => 'uf-vaga',
                                        'class' => 'form-control',
                                        'default' => old('uf-vaga', 'SP')
                                    ]
                                )
                            }}
                        </div>
                    </div>
-->
                    <!-- /UF -->

                    <!-- Cidade -->
                    <div class="col-8 col-sm-9 col-md-4">
                        <div class="form-group @if ( $errors -> has( 'TT014CC003' )) has-danger @endif">



                           {{ Form::hidden('uf-vaga', 'SP', array('id' => 'uf-vaga')) }}
                            {{ Form::label('TT014CC003', 'Cidade em SP *', ['class' => 'form-control-label']) }}
                            {{ Form::select
                                (
                                    'TT014CC003',
                                    [''],
                                    null,

                                    [
                                        'id' => 'cidade-vaga',
                                        'class' => 'form-control',
                                        'default' => old('TT014CC003')
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT014CC003'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC003') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Cidade -->
                </div>

                <div class="row">
                    <!-- Contato -->
                    <div class="col-12 col-md-8 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT014CC007' )) has-danger @endif">
                            {{ Form::label('TT014CC007', ' Nome do Contato *', ['class' => 'form-control-label']) }}
                            {{ Form::text
                                (
                                    'TT014CC007',
                                    null,

                                    [
                                        'class' => 'form-control',
                                        'maxlength' => '150'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT014CC007'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC007') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Contato -->

                    <!-- Telefone -->
                    <div class="col-12 col-md-4 col-lg-2">
                        <div class="form-group @if ( $errors -> has( 'TT014CC008' )) has-danger @endif">
                            {{ Form::label('TT014CC008', 'Telefone *', ['class' => 'form-control-label']) }}
                            {{ Form::text
                                (
                                    'TT014CC008',
                                    null,

                                    [
                                        'maxlength' => '16',
                                        'class' => 'form-control tel',
                                        'placeholder' => '(00) 0000-0000'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT014CC008'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC008') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Telefone -->

                    <!-- E-mail -->
                    <div class="col-12 col-lg-6">
                        <div class="form-group @if ( $errors -> has( 'TT014CC006' )) has-danger @endif">
                            {{ Form::label('TT014CC006', 'E-mail *', ['class' => 'form-control-label']) }}
                            {{ Form::text
                                (
                                    'TT014CC006',
                                    null,
                                    [
                                        'class' => 'form-control',
                                        'maxlength' => '150'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT014CC006'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC006') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /E-mail -->
                </div>

                <div class="row">

                    <!-- Descrição das Atividades -->
                    <div class="col-12 col-md-6">
                        <div class="form-group @if ( $errors -> has( 'TT014CC009' )) has-danger @endif">
                            {{ Form::label('TT014CC009', 'Descrição das Atividades *', ['class' => 'form-control-label']) }}
                            {{ Form::textArea
                                (
                                    'TT014CC009',
                                    null,

                                    [   'placeholder' => 'Inserir no máximo 1000 caracteres',
                                        'rows' => '6',
                                        'class' => 'form-control'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT014CC009'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC009') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Descrição das Atividades -->

                    <!-- Requisitos -->
                    <div class="col-12 col-md-6">
                        <div class="form-group @if ( $errors -> has( 'TT014CC010' )) has-danger @endif">
                            {{ Form::label('TT014CC010', 'Requisitos *', ['class' => 'form-control-label']) }}
                            {{ Form::textArea
                                (
                                    'TT014CC010',
                                    null,

                                    [
                                        'placeholder' => 'Inserir no máximo 1000 caracteres',
                                        'rows' => '6',
                                        'class' => 'form-control'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT014CC010'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC010') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Requisitos -->
                </div>

                <div class="row">
                    <!-- Benefícios -->
                    <div class="col-12 col-md-6">
                        <div class="form-group @if ( $errors -> has( 'TT014CC011' )) has-danger @endif">
                            {{ Form::label('TT014CC011', 'Benefícios', ['class' => 'form-control-label']) }}
                            {{ Form::textArea
                                (
                                    'TT014CC011',
                                    null,

                                    [
                                       'placeholder' => 'Inserir no máximo 1000 caracteres',
                                        'rows' => '6',
                                        'class' => 'form-control'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT014CC011'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC011') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Benefícios -->

                    <!-- Como de Candidatar -->
                    <div class="col-12 col-md-6">
                        <div class="form-group @if ( $errors -> has( 'TT014CC012' )) has-danger @endif">
                            {{ Form::label('TT014CC012', 'Como se Candidatar *', ['class' => 'form-control-label', 'maxlength' => '255']) }}
                            {{ Form::textArea
                                (
                                    'TT014CC012',
                                    null,

                                    [
                                       'placeholder' => 'Inserir no máximo 1000 caracteres',
                                        'rows' => '6',
                                        'class' => 'form-control'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT014CC012'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT014CC012') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Como de Candidatar -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <p class="card-text text-uppercase"><strong>(*) Campos obrigatórios</strong></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Dados da Vaga -->
</div>

<!-- Termo de Adesão -->
<div class="row">
    <div class="col-12 my-std">
        <div class="alert alert-warning mb-0">
            <div class="text-justify">
                <h4 class="text-uppercase text-center">Termo de adesão</h4>
                <p>
                    O <strong>PAF Empregos</strong> consiste em um serviço gratuito de divulgação de vagas para profissionais farmacêuticos e estudantes de graduação em Farmácia;
                </p>
                <p>
                    As vagas ficarão disponíveis no portal por 15 dias. Após esse período serão excluídas automaticamente. Caso a vaga seja preenchida antes desse prazo, é necessário comunicar ao <strong>CRF-SP</strong> para a retirada da oferta. Para entrar em contato com o Departamento de Relacionamento, envie um e-mail para <a class ="alert-link" href="mailto:paf@crfsp.org.br">paf@crfsp.org.br</a> ou ligue (11)3067-1467 / (11)3067-1867;
                </p>
                <p>
                    Somente poderão ser ofertadas as vagas que correspondam a salários iguais ou maiores ao piso salarial da categoria de acordo com os sindicatos;
                </p>
                <p>
                    A publicação das vagas não é imediata. As informações estão sujeitas à avaliação e deferimento da equipe de Relacionamento do <strong>CRF-SP</strong>;
                </p>
                <p>
                    A empresa deve fornecer a maior quantidade de dados possível para que o candidato entre em contato e concorra à vaga. Os dados como razão social, endereço, telefone, CNPJ e e-mail são de preenchimento obrigatório;
                </p>
                <p>
                    <!-- Estágio ? -->
                    @if($tipoVaga == 'ES')
                        É necessário preencher o campo Bolsa Auxílio;
                    @else
                        É necessário preencher o campo Remuneração;
                    @endif
                </p>
                <p>
                    O <strong>CRF-SP</strong> não se responsabiliza pela veracidade das informações contidas nas vagas de empregos veiculadas, sendo essa de total responsabilidade dos anunciantes, que arcarão com as sanções civis e penais que eventualmente ocasionarem;
                </p>
                <p>
                    A utilização dessa ferramenta acarreta a condição de <strong>USUÁRIO</strong> e implica na aceitação e concordância das condições gerais de utilização.
                </p>
                <p>
                    Ao <strong>USUÁRIO</strong> compete:
                </p>
                <p>
                    A responsabilidade pela veracidade, integridade e legalidade das informações por ele prestadas;
                </p>
                <p>
                   Permitir a divulgação do anúncio nas redes sociais e grupos dos quais o <strong>CRF-SP</strong> faça parte;
                </p>
                <p>
                    Aceitar o indeferimento do anúncio, caso sejam verificadas irregularidades, inclusive as relacionadas a discriminação e preconceito (referência a idade, sexo, raça, dentre outros);
                </p>
                <p>
                    <!-- Estágio ? -->
                    @if($tipoVaga == 'ES')
                        Cumprir a <strong>LEI DE ESTÁGIO</strong>
                        <a class ="alert-link" href="http://www.planalto.gov.br/ccivil_03/_ato2007-2010/2008/lei/l11788.htm">
                            (Lei Federal nº 11.788 de 25/09/2008)
                        </a>;
                    @else
                        Cumprir a legislação trabalhista e as convenções coletivas da categoria, inclusive as relativas ao piso salarial;
                    @endif
                </p>
                <p>
                    Estar ciente das funções privativas do farmacêutico;
                </p>
                <p>
                    Respeitar o Código de Ética do Farmacêutico e as Resoluções do Conselho Federal de Farmácia;
                </p>
                <!-- Estágio ? -->
                @if($tipoVaga == 'EM')
                    <p>
                        Conhecer a Resolução 417/2004 do Conselho Federal de Farmácia, em especial os artigos 8º e 9º : Art.8º : “A profissão farmacêutica, em qualquer circunstância ou de qualquer forma, não pode ser exercida exclusivamente com objetivo comercial”. Art. 9º : “ Em seu trabalho, o farmacêutico não pode se deixar explorar por terceiros, seja com objetivo de lucro, seja com finalidade política ou religiosa”.
                    </p>
                @endif
                <p>
                    Em relação às condições gerais de utilização:
                </p>
                <div class="row">
                    <div class="col-12">
                        <label class="custom-control custom-checkbox">
                            <input name="termo" type="checkbox" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            Li e estou de acordo com o termo de adesão
                        </label>
                    </div>
                    <!--<div class="col-12">
                        <label class="custom-control custom-checkbox">
                            <input name="copia-email" type="checkbox" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            Desejo receber uma cópia desta proposta de vaga no meu e-mail
                        </label>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Termo de Adesão -->

<!-- Anunciar Vaga -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">



                <div class="row">
                    <div class="col-12 col-md-2"></div>
                    <div class="col-12 col-md-8">
                        {{ Form::button
                           (
                                '<h5 class="m-0 text"><strong>Enviar</strong></h5>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                    </div>
                    <div class="col-12 col-md-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Anunciar Vaga -->

{{ Form::close() }}
<script>
  //  CKEDITOR.replace( 'TT014CC012' );
</script>
@stop
