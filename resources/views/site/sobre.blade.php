<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Sobre | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{!! URL::to('/') !!}">Home</a></li>
            <li class="breadcrumb-item active">Sobre o PAF</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Sobre o PAF -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto">Sobre o PAF</span></div>
                </div>
                <p class="text-justify"> O PAF - Programa de Assistência ao Farmacêutico é <strong>totalmente gratuito</strong> e tem como objetivos a inserção ou recolocação do farmacêutico inscrito regularmente no CRF-SP, no mercado de trabalho <strong>(PAF Empregos)</strong>, assim como propiciar descontos e condições exclusivas para compras de serviços e produtos <strong>(PAF Descontos)</strong>. </p>
            </div>
        </div>
    </div>
</div>
<!-- /Sobre o PAF -->
<!-- PAF Empregos -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="color-paf-empregos">PAF Empregos</span></div>
                </div>
                <p class="text-justify"> É uma ferramenta de inserção do farmacêutico, inscrito regularmente no CRF-SP, no mercado de trabalho, promovendo a aproximação entre candidatos e empregadores de diversos ramos de atividades, por meio da divulgação diária de oportunidades de vagas das mais diversas áreas da Farmácia em todo o estado. Neste espaço, também, são divulgadas oportunidades para <strong>estagiários (as)</strong> do Curso de Graduação em Farmácia. </p>
                <p class="text-justify"> Todas as vagas divulgadas são analisadas rigidamente e estão, no mínimo, dentro do piso salarial informado pelo Sinfar-SP. Além disso, pertencem a empresas regulares perante o cadastro e fiscalizações do CRF-SP. </p>
                <p class="text-justify"> Além disso, o CRF-SP divulga editais de <strong>Concursos Públicos</strong> que envolvam vagas de interesse do farmacêutico. É importante ressaltar que todos os editais são analisados e, após aprovação do departamento jurídico, disponibilizados no site. Nesse espaço, também, são divulgados os processos seletivos para obtenção de bolsas de estudos em <strong>Programas de Aprimoramento Profissional e Residência Multiprofissional</strong>. </p>
            </div>
        </div>
    </div>
</div>
<!-- /PAF Empregos -->
<!-- Currículos -->
<!--
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-empregos">Currículos</span></div>
                </div>
                <p class="text-justify"> O cadastramento de currículos no PAF também é uma maneira de ampliar a empregabilidade dos candidatos, pois proporciona às empresas a possibilidade de busca de currículos conforme o perfil desejado. </p>
            </div>
        </div>
    </div>
</div>
-->
<!-- /Currículos -->
<!-- PAF Descontos -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="color-paf-descontos">PAF Descontos</span></div>
                </div>
                <p class="text-justify"> O Clube de Benefícios do PAF Descontos oferece aos farmacêuticos inscritos regularmente no CRF-SP, uma série de condições especiais e descontos exclusivos em <strong>diversas categorias:</strong> academias, assistência funeral, brinquedos, cursos de idiomas, graduação e pós-graduação, eletrodomésticos, eletroeletrônicos, hotéis, joias e semijoias, móveis e decoração, óticas, perfumes, planos de saúde, seguros, turismo, dentre outros. </p>
            </div>
        </div>
    </div>
</div>
<!-- /PAF Descontos -->
<!-- PAF Empresa -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="color-paf-empresas">PAF Empresas</span></div>
                </div>
                <p class="text-justify"> É um programa especial que estende vantagens e benefícios oferecidos pelos parceiros comerciais PAF às empresas de propriedade de farmacêuticos e, também, aos farmacêuticos empreendedores que pretendem investir em seu próprio negócio. </p>
            </div>
        </div>
    </div>
</div>
<!-- /PAF Empresa -->
<!-- PAF Regional -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="color-paf-regional">PAF Regional</span></div>
                </div>
                <p class="text-justify"> Consiste em parcerias com empresas das regiões que compõem as seccionais, além de divulgação de vagas regionais. </p>
            </div>
        </div>
    </div>
</div>
<!-- /PAF Regional -->
<!-- Dicas e Artigos -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="color-paf-dicas">Dicas e artigos</span></div>
                </div>
                <p class="text-justify"> Os artigos publicados neste espaço são resultado de pesquisas realizadas pela equipe do Programa de Assistência ao Farmacêutico, em sites especializados. </p>
                <p class="text-justify"> São analisados temas que podem ser do interesse do profissional farmacêutico em busca de emprego e/ou de empreender seu próprio negócio. </p>
                <p class="text-justify">As opiniões aqui expressas refletem as ideias dos autores, não necessariamente o posicionamento do CRF-SP em relação aos temas abordados, isentando este Conselho de qualquer eventual responsabilidade.</p>
            </div>
        </div>
    </div>
</div>
<!-- /Dicas e Artigos -->

<!-- Sobre o PAF -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto">  <strong>POLÍTICA PAF – PROGRAMA DE ASSISTÊNCIA AO FARMACÊUTICO  </strong></span></div>
                </div>
                <p class="text-justify">
                 <strong>OBJETIVO</strong><br>
Padronizar as normas que regem o Programa de Assistência ao Farmacêutico (PAF).<br><br>
                    <strong>DEFINIÇÃO </strong><br>
                    O Programa de Assistência ao Farmacêutico destina-se à inserção ou recolocação do farmacêutico, regularmente inscrito no CRF-SP, no mercado de trabalho  <strong>(PAF empregos)</strong>, assim como propicia descontos e condições exclusivas para compras de produtos e serviços  <strong>(PAF descontos)</strong>. <br><br><br>
                    <strong>DOS CRITÉRIOS</strong><br><br>
                    <strong>PAF EMPREGOS</strong> <br><br>
1)         As vagas são cadastradas, sem custo, pelas empresas que pretendem admitir o profissional farmacêutico em seu quadro de funcionários.<br><br>
2)         Antes da publicação da vaga no site do PAF, o CRF-SP realiza análise rigorosa da empresa em seu cadastro, bem como da última visita fiscal.<br><br>
3)         Analisa-se também o salário oferecido pela empresa, que deve respeitar o piso salarial informado pelo SINFAR (acordo coletivo), assim como as atividades relativas ao âmbito farmacêutico.<br><br>
4)         O anúncio das vagas permanece ativo pelo período de 15 dias, sendo automaticamente desativado após o prazo. <br><br>
5)         Caso a vaga cadastrada tenha sido reprovada, a empresa será informada para regularização  perante o CRF-SP.<br><br>
                    <strong>PAF DESCONTOS</strong><br><br>
1)         A empresa parceira deve estar em situação regular perante os órgãos públicos competentes de acordo com cada ramo de atividade (Junta comercial, Receita Federal, Vigilância Sanitária etc.). <br><br>
2)         A documentação habilitatória deverá atender as exigências da Lei nº 8.666, de 21 de junho de 1993 (Licitações e Contratos).<br><br>
3)         A parceria obrigatoriamente propiciará vantagens para a categoria farmacêutica: descontos em serviços e produtos.<br><br>
4)         A contrapartida oferecida à empresa parceira será exclusivamente a divulgação de seus produtos e serviços nos meios de comunicação do CRF-SP, com conteúdo previamente analisado e aprovado  pela equipe PAF – CRF-SP.<br><br>
5)         A documentação será analisada pelo Departamento Jurídico do CRF-SP. Estando em termos, será elaborado um documento denominado “Termo de Parceria” entre o CRF-SP e a Empresa Parceira, tendo duração de 1 ano, podendo ser prorrogado por mais tempo ou cancelado no decorrer da validade, caso ocorra alguma irregularidade.<br><br>
6)         No caso de parceria com drogarias e farmácias, a divulgação será realizado por meio de chamamento público, com a publicação de edital no Diário Oficial e no Portal de Transparência do CRF-SP contendo as respectivas exigências especificas.<br><br>
7)         O CRF-SP não terá qualquer tipo de responsabilidade e vínculo pelos produtos/serviços divulgados através da parceria.<br><br>
 </p>
            </div>
        </div>
    </div>
</div>
<!-- /Sobre o PAF -->

@stop
