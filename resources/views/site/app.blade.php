<!DOCTYPE html>
<!-- Esta é a página principal do site -->
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ elixir('css/all.css') }}">
    <link rel="icon" href="{{ URL::asset('img/logos/ico.png') }}" type="image/x-icon">
    <script src="{{ elixir('js/all.js') }}"></script>
    <title>@yield('titulo')</title> @yield('script')
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o)
                , m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })
        (window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-89145960-2', 'paf.crfsp.org.br');
        ga('send', 'pageview');
    </script>
    <!--Remover após transferir site completo para o LIVE. O CSS está no sass _header -->
    <style>
        a.submenu {
            padding: 0 !important;
        }

        ul.submenu {
            list-style: none;
            padding-bottom: 10px;
        }

        li.submenu:before {
            content: "";
            border-color: transparent #fff;
            border-style: solid;
            border-width: 0.35em 0 0.35em 0.45em;
            display: block;
            height: 0;
            width: 0;
            left: -1em;
            top: 1em;
            position: relative;
        }
    </style>
    <!--/ Remover após transferir site completo para o LIVE. O CSS está no sass _header -->
    <noscript>
        <!-- anchor linking to external file -->
    </noscript>
</head>

<body>
    <!--  <div class="corner-ribbon top-right sticky">BETA</div>-->
    <!-- Header -->
    <header class="header bg-white">
        <div class="container">
            <div class="row align-items-center">
                <!-- Logo -->
                <div class="col-7 hidden-sm-down my-0"> <a href="{{ URL::to('/') }}" accesskey="1" title="Logo PAF">
            {{ Html::image( URL::asset('img/logos/positivo-horizontal.png' ), "PAF-Programa de Assistência ao Farmacêutico", array( 'class' => 'header-logo img-fluid' )) }}
          </a> </div>
                <!-- /Logo -->
                <!-- LOGIN -->
                <div class="col-4 text-right">

                    @if (Auth::check())  {{ Auth::user()->TT001CC001 }} @endif

                </div>
                <!-- /LOGIN -->
                <!-- LOGOUT -->
                 <div class="col-1 text-right">
                 <a class="btn btn-block btn-admin" href="{{ URL::to('/logout') }}" title="Logout" > <i class="fa fa-sign-out fa-1x" aria-hidden="true"> </i> </a>
                </div>
                <!-- /LOGOUT -->
                <div>
                    <!-- Search -->
                    <div class="col-12 col-md-4 my-0">
                        <div class="search hidden-xs-up">
                            <div class="search ">
                                <form class="search-form float-md-right">
                                    <div class="row align-items-center no-gutters">
                                        <div class="col-10">
                                            <input type="text" class="search-input form-control" placeholder="O que procura?" title="busca"> </div>
                                        <div class="col-2">
                                            <button class="btn btn-fa" type="button"> <i class="fa fa-search fa-lg fa-2x cor-paf" aria-hidden="true"></i> </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /Search -->
                    </div>
                </div>
            </div>
    </header>
    <!-- /Header -->
    <!-- Navbar -->
    <nav class="navbar navbar-toggleable-sm navbar-fixed-top navbar-dark bg-paf-primary">
        <div class="row align-items-center hidden-md-up">
            <!-- Menu-Toggler -->
            <div class="col-2 my-0">
                <div class="menu-toggler btn btn-fa hamburger hamburger-x"> <span>toggle menu</span> </div>
            </div>
            <!-- /Menu-Toggler -->
            <!-- Navbar-Brand -->
            <div class="col-8 px-0 text-center"> <a href="{{ URL::to('/') }}">
          {{ Html::image( URL::asset('img/logos/negativo-horizontal.png' ), "PAF-Programa de Assistência ao Farmacêutico", array( 'class' => 'navbar-logo img-fluid' )) }}
        </a> </div>
            <!-- /Navbar-Brand -->
            <!-- Search-Toggler -->
            <div class="col-2 pl-0 my-0">
                <div id="search-toggler" class="btn btn-fa hidden-xs-up"> <i class="fa fa-search fa-inverse fa-2x" aria-hidden="true"></i> </div>
            </div>
            <!-- /Search-Toggler -->
        </div>
        <!-- Menu -->
        <div class="container menu">
            <ul class="nav navbar-nav nav-fill-md-up">
                <li class="nav-item dropdown">
                    <a id="dropdownPAFEmpregos" class="nav-link dropdown-toggle" data-toggle="dropdown" href="{{ URL::Route('vagas.index') }}" role="button" aria-haspopup="true" aria-expanded="false" title="PAF Empregos"> <span class="color-paf-empregos">PAF Empregos</span> </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownPAFEmpregos"> <a class="dropdown-item" href="{{ URL::Route('vagas.index') }}" title="Buscar Vagas"> Buscar Vagas </a> <a class="dropdown-item" href="{{ URL::Route('concursos.index') }}" title="Concursos"> Concursos </a> <a class="dropdown-item" href="{{ URL::Route('vagas.create') }}" title="Anunciar Vagas"> Anunciar Vagas </a>
                        <!--            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal2" title="Anunciar Vagas"> Anunciar Vagas </a>  remover esta linha após 07 de janeiro -->
                        <!--            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal2" title="Anunciar Estágio"> Anunciar Estágio </a>  remover esta linha após 07 de janeiro --><a class="dropdown-item" href="{{ URL::Route('estagios') }}" title="Anunciar Estágios"> Anunciar Estágios </a>
                        <!--            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal2" title="Retirar Anúncio"> Retirar Anúncio  </a>  remover esta linha após 07 de janeiro --><a class="dropdown-item" href="{{ URL::to('contato/retiraranuncio') }}" title="Retirar Anúncio"> Retirar Anúncio </a>
                        <!--            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#myModal" title="Cadastro de Currículos "> Cadastro de Currículos </a>--><a class="dropdown-item" href="{{ URL::to('perito') }}" title="Cadastro de Peritos Judiciais"> Cadastro de Peritos Judiciais </a>
                         @if ( Auth::user()->isEmpresa())
                          <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ URL::to('curriculos/empresas/pesquisa') }}" title="Cadastro de Peritos Judiciais"> Buscar currículos</a>
                         <a class="dropdown-item" href="{{ URL::to('curriculos/empresas') }}" title="Cadastro de Peritos Judiciais"> Meus dados</a>
                         <a class="dropdown-item" href="{{ URL::to('curriculos/empresas/empresa') }}" title="Cadastro de Peritos Judiciais"> Minhas Vagas</a>
                        @endif
                          @if ( Auth::user()->isFarmaceutico())
                          <div class="dropdown-divider"></div>
                           <a class="dropdown-item" href="{{ URL::Route('curriculos.farmaceuticos.curriculo.show',  $curriculo -> TT021CC000) }}" title="Cadastro de Peritos Judiciais"> Visualizar currículo</a>
                        <a class="dropdown-item" href="{{ URL::Route('curriculos.farmaceuticos.curriculo.edit', $curriculo -> TT021CC000 ) }}" title="Cadastro de Peritos Judiciais"> Editar Currículo</a>

                        @endif
                        <!--<a class="dropdown-item" href="{{URL::to('http://curriculos.crfsp.org.br/paf/curriculos/?Inicio')}}" target="_blank"> Cadastro de Currículos </a>-->


                        </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="{{ URL::Route('descontos.index') }}" role="button" aria-haspopup="true" aria-expanded="false" title="PAF Descontos"> <span class="color-paf-descontos">PAF Descontos</span> </a>
                    <div class="dropdown-menu" aria-labelledby="Preview"> <a class="dropdown-item" href="{{ URL::Route( 'descontos.index') }}" title="Buscar descontos"> Buscar descontos </a> <a class="dropdown-item" href="{{ URL::to('contato/serparceiro') }}" title="Quero ser parceiro"> Quero ser parceiro </a> <a class="dropdown-item" href="{{ URL::to('contato/indicarparceiro') }}" title="Indicar um parceiro">Indicar um parceiro</a>
                        <div class="dropdown-divider"></div> <a class="dropdown-item" href="#" title="Escolha uma opção abaixo">Chamamento Público</a>
                        <ul class="submenu">
                            <li class="submenu"><a class="dropdown-item submenu" href="{{ URL::to('http://www.crfsp.org.br/documentos/transparencia/EditaldeChamamentoPublico03-2016.pdf') }}" title="EDITAL">EDITAL</a></li>
                            <li class="submenu"><a class="dropdown-item submenu" href="{{URL::to('http://www.crfsp.org.br/documentos/transparencia/ChamamentoPublico20161129.pdf')}}" title="COMUNICADO">COMUNICADO</a></li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a id="dropdownPAFEmpresa" class="nav-link dropdown-toggle" href="{{ URL::Route('descontos.index') }}" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false" title="PAF Empresas"> <span class="color-paf-empresas">PAF Empresas</span> </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown"> <a class="dropdown-item" href="{{ URL::Route('empresa') }}" title="Buscar descontos">Buscar descontos</a> <a class="dropdown-item" href="{{ URL::to('contato/serparceiro') }}" title="Quero ser parceiro">Quero ser parceiro</a> <a class="dropdown-item" href="{{ URL::to('contato/indicarparceiro') }}" title="Indicar um parceiro">Indicar um parceiro</a> </div>
                </li>
                <li class="nav-item"> <a class="nav-link" href="{{ URL::Route('seccionais.index', 1) }}" title="PAF Regional"><span class="color-paf-regional">PAF Regional</span></a> </li>
                <li class="nav-item dropdown hidden-lg-up"> <a id="dropdownPAFDesconto" class="nav-link dropdown-toggle text-uppercase" href="{{ URL::Route('descontos.index') }}" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false" title="Mais">
            Mais
          </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown"> <a class="dropdown-item" href="{{ URL::Route('artigos.index') }}" title="Artigos"> Artigos </a> <a class="dropdown-item" href="{{ URL::to('contato') }}" title="Contato"> Contato </a> <a class="dropdown-item" href="{{ URL::to('sobre') }}" title="Sobre o PAF"> Sobre o PAF </a> </div>
                </li>
                <li class="nav-item  hidden-md-down"><a class="nav-link" href="{{ URL::Route('artigos.index') }}" title="Dicas e Artigos"> Dicas e Artigos </a></li>
                <li class="nav-item  hidden-md-down"><a class="nav-link" href="{{ URL::to('contato') }}" title="Contato"> Contato </a></li>
                <li class="nav-item  hidden-md-down"><a class="nav-link" href="{{ URL::to('sobre') }}" title="Sobre o PAF"> Sobre o PAF </a></li>
            </ul>
        </div>
        <!-- Menu -->
    </nav>
    <!-- /Navbar -->
    <!-- Modal para acesso via clique
  <div class="modal fade" id="myModal">
    <div class="modal-dialog" role="document" style="width: 1134px;max-width: 100%;">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title"> Em desenvolvimento!</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Em desenvolvimento! O CRF-SP está trabalhando para aperfeiçoar este ambiente. Aguarde novidade em breve!

        </div>
      </div>
    </div>
  </div>
-->
    <!-- modal 2 -->
    <div class="modal fade" id="myModal2">
        <div class="modal-dialog" role="document" style="width: 1134px;max-width: 100%;">
            <div class="modal-content">
                <div class="modal-header">
                    <!--          <h6 class="modal-title"> Em desenvolvimento!</h6>-->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body"> Informamos que no período de <b>24 de dezembro de 2018 a 6 de janeiro de 2019</b> não haverá expediente no CRF-SP (Sede, Subsedes e Seccionais), por isso este serviço será desabilitado, com retorno a partir de <b>7 de janeiro de 2019.
O CRF-SP deseja a todos um Natal de muita paz e que 2019 seja repleto de novas conquistas, realizações e muita saúde!</b>
                    <!--		      <iframe  src="https://docs.google.com/forms/d/e/1FAIpQLSd-1aew-dFMu9ytognZ_E3RTpN2tbhlrg1FQQ-f_WeFkvabbA/viewform" height="900" width="100%" scrolling="yes" style="border: none;"></iframe>-->
                </div>
            </div>
        </div>
    </div>
    <!--/Modal2-->
    <!-- Main -->
    <div class="main">
        <div class="container"> @yield('conteudo')
            <!-- aqui carregará o conteúdo das páginas conforme rotas - nas views ele buscará o - @section('conteudo') - até o @stop -->
        </div>
        <div id="blur" class="blur"></div>
    </div>
    <!-- /Main -->
    <!-- Footer -->
    <footer class="navbar navbar-fixed-bottom navbar-light bg-paf-primary text-center text-white">
        <h4 class=" text-uppercase color-paf-secondary"> Programa de Assistência ao farmacêutico CRF-SP</h4>
        <h6>Sede: Rua Capote Valente, 487 - Jardim América - São Paulo/SP</h6>
        <!--    <h6>(11) 3067-1450</h6>-->
        <!-- <h6><a class="color-white" href="mailto:paf@crfsp.org.br"><strong>paf@crfsp.org.br</strong></a></h6> -->
        <figure class="figure card-img-top img-hrz-center my-3"> <a href="http://portal.crfsp.org.br/" target="_blank" title="Portal CRFSP">
        {{ Html::image( URL::asset('img/logos/CRFSP-horizontal.png' ), "CRF-SP", array( 'class' => 'figure-img img-fluid mb-0' )) }}
      </a> </figure>
    </footer>
</body>

</html>
