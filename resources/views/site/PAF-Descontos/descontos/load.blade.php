<script>

 function mudarCoresLidos() {

    for(i=0;i<ids.length;i++){
    var minhadiv = 'div-'+ids[i];
        if(document.getElementById(minhadiv) != null){
            document.getElementById(minhadiv).style.borderBottomColor = "#F4B01B";
        }
    }
}


</script>
<!-- Descontos -->
<div class="row">
    @foreach($descontos as $desconto)
    <script>
            function mudarcor(id) {
                ids.push(id);
                var minhadiv = 'div-'+id;
                document.getElementById(minhadiv).style.borderBottomColor = "#F4B01B";
            }
    </script>
    <div class="col-12 col-xl-6 my-std">
       <a href="#" data-toggle="modal" data-target="#modal_show-{{  $desconto -> TT012CC000 }}">
            <div id="div-{{  $desconto -> TT012CC000 }}"  class="card mural h-auto">
                <div class="card-block text-center">
                    <figure class="figure m-0">
                        {{ Html::image( $desconto -> TT012CC004, $desconto -> TT012CC003, array( 'class' => 'figure-img img-fluid m-0 desconto' )) }}
                    </figure>
                    <p class="card-text color-paf-descontos">
                        {{ $desconto -> TT012CC003 }}
                    </p>
                    <p class="card-text text-uppercase">
                        {{ $desconto -> categoria -> TT005CC001 }}
                    </p>


                     <a href="#" onclick="mudarcor('{{  $desconto -> TT012CC000 }}');" data-toggle="modal" data-target="#modal_show-{{  $desconto -> TT012CC000 }}">
                         <div class="btn btn-mural">Saiba Mais</div>
                     </a>
                </div>
            </div>
        </a>
    </div>
     <!--Modal-->
    <div class="modal fade" id="modal_show-{{ $desconto -> TT012CC000 }}">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">DESCONTO</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
         <div class="card my-std">
    <div class="card-block">
       <h2 class="card-title text-center">{{ $desconto -> TT012CC003 }}</h2>
        <figure class="figure desconto-img text-center">
            {{ Html::image( $desconto -> TT012CC004, $desconto -> TT012CC003, array('class' => 'figure-img img-fluid')) }}
        </figure>
       {!! $desconto -> TT012CC005 !!}
    </div>
</div>

          </div>
        </div>
      </div>
    </div>
   <!--Modal-->
    @endforeach
</div>
<script type="text/javascript">
   mudarCoresLidos();
</script>
<!-- /Descontos -->

<!-- Paginação -->
{{ $descontos -> links( 'partials.pagination' ) }}






