<!-- layout Master -->
@extends('site::app')

<!-- Título -->
@section('titulo') Desconto | PAF @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class=" breadcrumb-item"><a href="{{ URL::Route('descontos.index') }}"> Descontos </a></li>
            <li class=" breadcrumb-item active"> Desconto </li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Título -->
<div class="row">
    <!-- Voltar -->
    <div class="col-12 col-sm-1">
      <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'descontos.index' ) }}">
         <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
      </a>
    </div>
     <!-- /Voltar -->
    <div class="col-12 col-sm-11">
        <div class="card">
            <div class="card-block text-uppercase">
              <!-- <a class="btn btn-info" href="{{ URL::previous() }}">back</a> liliana testanto-->
                <h5 class="color-paf-descontos m-0"> Desconto </h5>
            </div>
        </div>
    </div>
</div>
<!-- /Título -->

<!-- Desconto -->
<div class="card my-std">
    <div class="card-block">
       <h2 class="card-title text-center">{{ $desconto -> TT012CC003 }}</h2>
        <figure class="figure desconto-img text-center">
            {{ Html::image( $desconto -> TT012CC004, $desconto -> TT012CC003, array('class' => 'figure-img img-fluid')) }}
        </figure>
        {!! $desconto -> TT012CC005 !!}
    </div>
</div>
<!-- /Desconto -->

@stop
