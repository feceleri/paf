<!-- layout Master -->
@extends('site::app')

<!-- Título -->
@section('titulo') Seccionais | PAF @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="breadcrumb-item active"> Seccionais </li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Título -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block text-uppercase">
                <h5 class="m-0"> Informe a Seccional </h5>
            </div>
        </div>
    </div>
</div>
<!-- /Título -->

 <!-- Seccional -->
 <div class="row">
     <div class="col-12 my-std">
         <div class="card">
             <div class="card-block">
                <!-- Seccionais -->
                {{ Form::select
                    (
                        'TT021CC021',
                        $seccionais,
                        $id,

                        [
                            'onChange' => 'location = this.value;',
                            'class' => 'form-control select-naked color-paf-regional',
                            'placeholder' => 'Informe a Seccional'
                        ]
                    )
                }}
                <!-- /Seccionais -->
             </div>
         </div>
     </div>
 </div>
 <!-- /Seccional -->

 <!-- Mural -->
<div class="row">
    <div class="col-12">
        <div class="row">
            <!-- PAF-Empregos -->
            <div class="col-12 col-lg-4">
                <div class="row">
                    <div class="col-12 my-std">
                        <div class="card mural">
                            <div class="card-header p-0 bg-paf-primary text-center">
                                <figure class="figure m-0">
                                    {{ Html::image( URL::asset('img/logos/btn-empregos.png'), 'Dicas e Artigos', array( 'class' => 'figure-img img-fluid m-0' )) }}
                                </figure>
                            </div>
                            <div class="card-block text-center">
                                <div class="row">
                                    <!-- Vaga de Emprego -->
                                    <div class="col-12">
                                        @if ($vaga)
                                            <p class="card-text color-paf-empregos">Última vaga de emprego cadastrada</p>
                                            <p class="card-text text-uppercase color-paf-primary">
                                                <strong>{{ $vaga -> cargo -> TT003CC001 }}</strong>
                                            </p>
                                            <p class="card-text">
                                                {{ $vaga -> empresa -> TT010CC004  }}
                                            </p>
                                            <p class="card-text">
                                                {{ $vaga -> cidade -> TT008CC003 }} - {{ $vaga -> cidade -> TT008CC001 }} |
                                                {{ formatDate( $vaga -> created_at ) }}
                                            </p>
                                            <a href="{{ URL::Route( 'vagas.show', $vaga -> TT014CC000 ) }}">
                                                <div class="btn btn-mural"> Saiba Mais </div>
                                            </a>
                                        @else
                                            <p class="card-text color-paf-empregos">Nenhuma vaga para a Seccional selecionada</p>

                                            <a href="{{ URL::Route('vagas.index') }}">
                                                <div class="btn btn-mural"> Ver outros Vagas </div>
                                            </a>
                                        @endif
                                    </div>
                                    <!-- Vaga de Emprego -->

                                    <div class="col-12"><hr></div>

                                    <!-- Concurso -->
                                    <div class="col-12">
                                        @if ($concurso)
                                            <p class="card-text color-paf-empregos">Concurso Público</p>
                                            <p class="card-text text-uppercase color-paf-primary-10">
                                                <strong>{{ $concurso -> TT015CC001 }}</strong>
                                            </p>
                                            <p class="card-text">
                                                {{ $concurso -> TT015CC003 }}
                                            </p>
                                            <p class="card-text">
                                                até {{ formatDate( $concurso -> TT015CC009 ) }}
                                            </p>
                                            <a href="{{ URL::Route('concursos.show', $concurso -> TT015CC000) }}">
                                                <div class="btn btn-mural"> Saiba Mais </div>
                                            </a>
                                        @else
                                            <p class="card-text color-paf-empregos">Nenhum Concurso para a Seccional selecionada</p>

                                            <a href="{{ URL::Route('concursos.index') }}">
                                                <div class="btn btn-mural"> Ver outros Concursos </div>
                                            </a>
                                        @endif
                                    </div>
                                    <!-- /Concurso -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /PAF-Empregos -->

            <!-- PAF Descontos -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="row">
                    <div class="col-12 my-std">
                        <div class="card mural">
                            <div class="card-header p-0 bg-paf-primary text-center">
                                <figure class="figure m-0">
                                    {{ Html::image( URL::asset('img/logos/btn-descontos.png'), 'Dicas e Artigos', array( 'class' => 'figure-img img-fluid m-0' )) }}
                                </figure>
                            </div>
                            <div class="card-block text-center">
                                <figure class="figure m-0">
                                    {{ Html::image( $desconto -> TT012CC004, $desconto -> TT012CC003, array( 'class' => 'figure-img img-fluid mb-1 desconto' )) }}
                                </figure>
                                <p class="card-text color-paf-descontos">
                                    {{ $desconto -> TT012CC003 }}
                                </p>
                                <p class="card-text text-uppercase color-paf-primary">
                                    <strong>{{ $desconto -> parceiro -> TT011CC001 }}</strong>
                                </p>
                                <a href="{{ URL::Route('descontos.show', $desconto -> TT012CC000) }}">
                                    <div class="btn btn-mural"> Saiba Mais </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /PAF Descontos -->

            <!-- Artigos -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="row">
                    <div class="col-12 my-std">
                        <div class="card mural">
                            <div class="card-header p-0 bg-paf-primary text-center">
                                <figure class="figure m-0">
                                    {{ Html::image( URL::asset('img/logos/btn-artigos.png'), 'Dicas e Artigos', array( 'class' => 'figure-img img-fluid m-0' )) }}
                                </figure>
                            </div>
                            <div class="card-block">
                                @foreach( $artigos as $artigo )
                                <a href="{{ URL::Route( 'artigos.show', $artigo -> TT009CC000 ) }}">
                                    <div class="media artigo">
                                        <figure class="figure d-flex align-self-center m-0">
                                            {{ Html::image( $artigo -> TT009CC003, $artigo -> TT009CC002, array( 'class' => 'figure-img img-fluid' )) }}
                                        </figure>
                                        <div class="media-body">
                                            <h6 class="mb-2">{{ formatDate($artigo -> created_at) }}</h6>
                                            <h6>{{ $artigo -> TT009CC002 }}</h6>
                                        </div>
                                    </div>
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Artigos -->
        </div>
    </div>
</div>
<!-- /Mural -->

@stop
