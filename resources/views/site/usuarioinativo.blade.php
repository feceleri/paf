<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') USUÁRIO INATIVO | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{!! URL::to('/') !!}">Home</a></li>
            <li class="breadcrumb-item active">USUÁRIO INATIVO</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Sobre o PAF -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto">USUÁRIO INATIVO</span></div>
                </div>
                <p class="text-justify"> O seu usuário encontra-se inativo. Para maiores informações entre em contato com o PAF no telefone xxxx ou email. </p>
            </div>
        </div>
    </div>
</div>
<!-- /Sobre o PAF -->







@stop
