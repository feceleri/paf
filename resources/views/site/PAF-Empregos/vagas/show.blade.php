<!-- Nesta view é mostrado todo o conteúdo da vaga individualmente  -->
<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') {{ $vaga -> TT014CC005 }} | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class=" breadcrumb-item"><a href="{{ URL::Route('vagas.index') }}">Vagas disponíveis</a></li>
            <li class=" breadcrumb-item active"> {{ $vaga -> TT014CC005 }} </li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<div class="row">
    <!-- Vaga -->
    <div class="col-12 col-md-8">
        <!-- Título -->
        <div class="row">
            <div class="col-12 col-sm-12">
                <div class="card">
                    <div class="card-block text-uppercase">
                        <h5 class="color-paf-empregos m-0"> {{ $vaga -> TT014CC005 }} </h5> </div>
                </div>
            </div>
        </div>
        <!-- /Título -->
        <!-- Aviso -->
        <div class="alert alert-info my-std">
            <div class="text-justify">
                <h5 class="text-uppercase text-center">Prezado Colega Farmacêutico</h5>
                <p> Comunique o <a class="cor-link" href="{{ URL::to('contato') }}">CRF-SP</a>, caso as informações contidas neste anúncio de vaga sejam diferentes das fornecidas pelo entrevistador. Sua identidade será mantida em sigilo e as providências cabíveis serão tomadas. </p>
            </div>
        </div>
        <!-- Aviso -->
        <!-- Content -->
        <div class="row">
            <div class="col-12 my-std">
                <div class="card">
                    <!-- Dados da Vaga -->
                    <div class="card-block text-left">
                        <h4 class="card-text text-uppercase color-paf-primary">
                            <strong>{{ $vaga -> cargo -> TT003CC001 }}</strong>
                        </h4>
                        <p class="card-text"> {{ $vaga -> empresa -> TT010CC004 }} ( {{ $vaga -> ramo -> TT002CC001 }} ) </p>
                        <p class="card-text"> <strong>vagas: {{ $vaga -> TT014CC013 }}</strong> | {{ $vaga -> cidade -> TT008CC003 }} - {{ $vaga -> cidade -> TT008CC001 }} | {{ formatDate($vaga -> TT014CC019) }} </p>
                        <p> R$ {{ formatMoney( $vaga -> TT014CC014 ) }}


                           @if ( $vaga -> TT014CC022 ===  'HO' )/ hora
                           @elseif ( $vaga -> TT014CC022 ===  'ME' )
                           @endif

                           @if ($vaga -> TT014CC015) | {{ $vaga -> TT014CC015 }} @endif </p>
                        <!-- Mostrar endereço? -->@if ($vaga -> TT014CC017)
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Endereço</strong>
                            </h6> {{ $vaga -> empresa -> TT010CC007 }} - {{ $vaga -> empresa -> TT010CC008 }} - CEP: {{ formatCep($vaga -> empresa -> TT010CC006) }} </p> @endif
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Descrição das Atividades</strong>
                            </h6> {!! $vaga -> TT014CC009 !!} </p>
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Requisitos</strong>
                            </h6> {!! $vaga -> TT014CC010 !!} </p>
                        <!-- Benefício preenchido? -->@if ($vaga -> TT014CC011)
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Benefícios</strong>
                            </h6> {!! $vaga -> TT014CC011 !!} </p> @endif
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Como se candidatar</strong>
                            </h6> {!! $vaga -> TT014CC012 !!} </p>
                    </div>
                    <!-- /Dados da Vaga -->
                </div>
            </div>
        </div>
        <!-- /Content -->
    </div>
    <!-- /Vaga -->
    <!-- Últimas -->
    <div class="col-12 col-md-4">
        <div class="sticky-top">
            <!-- Título -->
            <div class="row">
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block text-center text-uppercase">
                            <h5 class="color-paf-empregos m-0"> Últimas </h5> </div>
                    </div>
                </div>
            </div>
            <!-- /Título -->
            <!-- Vagas -->
            <div class="row"> @foreach( $vagas as $vaga )
                <div class="col-12 my-std">
                    <div class="card mural" style="height: auto">
                        <div class="card-block text-center">
                            <p class="card-text text-uppercase color-paf-primary"> <strong>{{ $vaga -> cargo -> TT003CC001 }}</strong> </p>
                            <p class="card-text"> {{ $vaga -> empresa -> TT010CC004 }} </p>
                            <p class="card-text"> {{ $vaga -> cidade -> TT008CC003 }} - {{ $vaga -> cidade -> TT008CC001 }} | {{ formatDate($vaga -> TT014CC019) }} </p>
                            <a href="{{ URL::Route('vagas.show', $vaga -> TT014CC000) }}">
                                <div class="btn btn-mural">Saiba Mais</div>
                            </a>
                        </div>
                    </div>
                </div> @endforeach
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block">
                            <a class="cor-texto" href="{{ URL::Route( 'vagas.index' ) }}">
                                <h6 class="text-center text-uppercase m-0">
                                    Todas <i class="fa fa-external-link ml-1" aria-hidden="true"></i>
                                </h6> </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Vagas -->
        </div>
    </div>
    <!-- /Últimas -->
</div> @stop
