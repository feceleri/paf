
<!-- Nesta view se busca cada vaga a ser mostrada e disponibiliza os filtros  -->

<!-- layout Master -->
@extends('site::app') <!-- Aqui ele diz que é uma extensão da página princial - a app.blade -->

<!-- Título -->
@section( 'titulo' ) Vagas Disponíveis | PAF @stop

<!-- Script -->
@section('script')
<script type="text/javascript">
   window.onload = function() {
       document.getElementsByClassName("apply-filter")[0].click();
};
var ids = [];
</script>
  @stop

<!-- Conteúdo -->
@section( 'conteudo' )

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class=" breadcrumb-item active">Vagas Disponíveis</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->

<div class="row">
    <div class="col-4 hidden-md-down">
        <div class="sticky-top">
            <!-- Título -->
            <div class="row">
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block text-uppercase">
                            <h5 class="color-paf-empregos m-0"> Busca Avançada </h5>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Título -->

            {{ Form::open(['class' => 'filter-form'],['route' => 'filter','method'=>'POST']) }}


               <!-- Cidade -->
            <div class="row">
                <div class="col-12 my-std">
                     <div class="card">
                        <div class="card-header bg-paf-primary color-white">
                            <h6 class="card-title text-center m-0"> Cidade </h6>
                        </div>
                        <div class="card-block scroll">
                            <div class="filter-item">
                                <div class="form-check">
                                    @foreach( $cidades as $cidade )
                                    <div class="row">
                                        <div class="col-12">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="TT014CC003[]" value="{{ $cidade -> TT008CC000 }}" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">
                                                    {{ $cidade -> TT008CC003 }} - {{ $cidade -> TT008CC001 }} <!--({{ $cidade -> qtde }})-->
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Cidade -->

            <!-- Ramo de Atividade -->
            <div class="row">
                <div class="col-12 my-std">
                     <div class="card">
                        <div class="card-header bg-paf-primary color-white">
                            <h6 class="card-title text-center m-0"> Ramo de Atividade </h6>
                        </div>
                        <div class="card-block scroll">
                            <div class="filter-item">
                                <div class="form-check">
                                    @foreach($ramos as $ramo) <!-- Para cada item em ramos, ele cria estes códigos - e cada item de ramos ele coloca este item em ramo-->
                                    <div class="row">
                                        <div class="col-12">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="TT014CC001[]" value="{{ $ramo -> TT002CC000 }}" class="custom-control-input" >
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">
                                                    {{ $ramo -> TT002CC001 }} <!--({{ $ramo -> qtde }})-->
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Ramo de Atividade -->



            <!-- Seccional -->
<!--
            <div class="row">
                <div class="col-12 my-std">
                     <div class="card">
                        <div class="card-header bg-paf-primary color-white">
                            <h6 class="card-title text-center m-0"> Seccional </h6>
                        </div>
                        <div class="card-block scroll">
                            <div class="filter-item">
                                <div class="form-check">
                                    @foreach( $seccionais as $seccional )
                                    <div class="row">
                                        <div class="col-12">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="TT006[]" value="{{ $seccional -> TT006CC000 }}" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">
                                                    {{ $seccional -> TT006CC001 }}
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
-->
            <!-- /Seccional -->


            <!-- Tipo de Vaga -->
            <div class="row">
                <div class="col-12 my-std">
                     <div class="card">
                        <div class="card-header bg-paf-primary color-white">
                            <h6 class="card-title text-center m-0"> Tipo de Vaga </h6>
                        </div>
                        <div class="card-block">
                            <div class="filter-item">
                                <div class="form-check">
                                    <div class="row">
                                        <!-- Estágio -->
                                        <div class="col-12">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="TT014CC018[]" value="ES" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">
                                                    Estágio
                                                </span>
                                            </label>
                                        </div>
                                        <!-- /Estágio -->

                                        <!-- Emprego -->
                                        <div class="col-12">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" name="TT014CC018[]" value="EM" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">
                                                    Emprego
                                                </span>
                                            </label>
                                        </div>
                                        <!-- /Emprego -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Tipo de Vaga -->

            {{ Form::close() }}

            <!-- Controle -->
            <div class="row">
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block">
                            <div class="row">
                                <!-- Aplicar Filtro -->
                                <div class="col-12 mb-2">
                                    {{ Form::button
                                       (
                                            '<h5 class="m-0 text"><strong>Aplicar Filtro</strong></h5>',

                                            [
                                                'class' => 'btn btn-block btn-admin apply-filter',
                                            ]
                                        )
                                    }}
                                </div>
                                <!-- /Aplicar Filtro -->

                                <!-- Limpar Filtro -->
                                <div class="col-12">
                                    {{ Form::button
                                       (
                                            '<h5 class="m-0 text"><strong>Limpar Filtro</strong></h5>',

                                            [
                                                'class' => 'btn btn-block btn-admin',
                                                'onclick' => 'location.reload();'
                                            ]
                                        )
                                    }}
                                </div>
                                <!-- Limpar Filtro -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Controle -->
        </div>
    </div>

    <div class="col-12 col-lg-8">
        <!-- Filter-Toggle -->
        <button class="btn btn-filter hidden-lg-up" data-toggle="modal" data-target="#filter-toggle">
            Filtros <i class="fa fa-sliders fa-lg ml-2" aria-hidden="true"></i>
        </button>
        <!-- Filter-Toggle -->

        <!-- Título -->
        <div class="row">
            <div class="col-12 my-std">
                <div class="card">
                    <div class="card-block text-uppercase">
                        <h5 class="color-paf-empregos m-0"> Vagas disponíveis </h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Título -->

        <!-- Vagas -->
        <section class="load-ajax">
            @include('site::PAF-Empregos.vagas.load')
        </section>
        <!-- /Vagas -->
    </div>
</div>

<!-- Filtros Mobile -->
<div class="modal fade" id="filter-toggle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- Header -->
            <div class="modal-header">
                <h5 class="modal-title text-uppercase"> Busca avançada </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- /Header -->

            <!-- Body -->
            <div class="modal-body">
                {{ Form::open(['class' => 'filter-form'],['route' => 'filter','method'=>'POST']) }}

                <!-- Cidade -->
                <div class="row">
                    <div class="col-12 my-std">
                         <div class="card">
                            <div class="card-header bg-paf-primary color-white">
                                <h6 class="card-title text-center m-0"> Cidade </h6>
                            </div>
                            <div class="card-block scroll">
                                <div class="filter-item">
                                    <div class="form-check">
                                        @foreach( $cidades as $cidade )
                                        <div class="row">
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="TT014CC003[]" value="{{ $cidade -> TT008CC000 }}" class="custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">
                                                        {{ $cidade -> TT008CC003 }} - {{ $cidade -> TT008CC001 }}  <!--({{ $cidade -> qtde }}) -->
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Cidade -->
                <!-- Ramo de Atividade -->
                <div class="row">
                    <div class="col-12 my-std">
                         <div class="card">
                            <div class="card-header bg-paf-primary color-white">
                                <h6 class="card-title text-center m-0"> Ramo de Atividade </h6>
                            </div>
                            <div class="card-block scroll">
                                <div class="filter-item">
                                    <div class="form-check">
                                        @foreach($ramos as $ramo)
                                        <div class="row">
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="TT014CC001[]" value="{{ $ramo -> TT002CC000 }}" class="custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">
                                                        {{ $ramo -> TT002CC001 }} <!--({{ $ramo -> qtde }})-->
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Ramo de Atividade -->


                 <!-- Seccional -->
                <div class="row">
                    <div class="col-12 my-std">
                         <div class="card">
                            <div class="card-header bg-paf-primary color-white">
                                <h6 class="card-title text-center m-0"> Seccional </h6>
                            </div>
                            <div class="card-block scroll">
                                <div class="filter-item">
                                    <div class="form-check">
                                        @foreach( $seccionais as $seccional )
                                        <div class="row">
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="TT006[]" value="{{ $seccional -> TT006CC000 }}" class="custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">
                                                        {{ $seccional -> TT006CC001 }}  <!-- ({{ $seccional -> qtde }}) -->
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Seccional -->
                 <!-- Tipo de Vaga -->
                <div class="row">
                    <div class="col-12 my-std">
                         <div class="card">
                            <div class="card-header bg-paf-primary color-white">
                                <h6 class="card-title text-center m-0"> Tipo de Vaga </h6>
                            </div>
                            <div class="card-block">
                                <div class="filter-item">
                                    <div class="form-check">
                                        <div class="row">
                                            <!-- Estágio -->
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="TT014CC018[]" value="ES" class="custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">
                                                        Estágio
                                                    </span>
                                                </label>
                                            </div>
                                            <!-- /Estágio -->

                                            <!-- Emprego -->
                                            <div class="col-12">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="TT014CC018[]" value="EM" class="custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">
                                                        Emprego
                                                    </span>
                                                </label>
                                            </div>
                                            <!-- /Emprego -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Tipo de Vaga -->

                {{ Form::close() }}
            </div>
            <!-- /Body -->

            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <!-- Aplicar Filtro -->
                        <div class="col-12 mb-2">
                            {{ Form::button
                               (
                                    '<h5 class="m-0 text"><strong>Aplicar Filtro</strong></h5>',

                                    [
                                        'data-dismiss' => 'modal',
                                        'class' => 'btn btn-block btn-admin apply-filter'
                                    ]
                                )
                            }}
                        </div>
                        <!-- /Aplicar Filtro -->

                        <!-- Limpar Filtro -->
                        <div class="col-12">
                            {{ Form::button
                               (
                                    '<h5 class="m-0 text"><strong>Limpar Filtro</strong></h5>',

                                    [
                                        'class' => 'btn btn-block btn-admin',
                                        'onclick' => 'location.reload();'
                                    ]
                                )
                            }}
                        </div>
                        <!-- Limpar Filtro -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Filtros Mobile -->

@stop
