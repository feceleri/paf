<script>

 function mudarCoresLidos() {

    for(i=0;i<ids.length;i++){
    var minhadiv = 'div-'+ids[i];
        if(document.getElementById(minhadiv) != null){
            document.getElementById(minhadiv).style.borderBottomColor = "#F4B01B";
        }
    }
}


</script>

<!--liliana-->
<!-- Nesta view insere dados dentro de cada box de vaga  -->
@foreach($vagas->sortByDesc('TT014CC019') as $vaga )
 <script>
            function mudarcor(id) {
                ids.push(id);
                var minhadiv = 'div-'+id;
                document.getElementById(minhadiv).style.borderBottomColor = "#F4B01B";
            }
    </script>
<div class="row">
    <div class="col-12  my-std">
        <div id="div-{{  $vaga -> TT014CC000 }}" class="card mural h-auto">
            <div class="card-block text-left">
                <p class="card-text text-uppercase color-paf-primary">
                    <strong>{{ $vaga -> cargo -> TT003CC001 }}</strong>
                </p>
                <p class="card-text">
                    {{ $vaga -> empresa -> TT010CC004  }} ( {{ $vaga -> ramo -> TT002CC001 }} )
                </p>
                <p class="card-text">
                    <strong>vagas: {{ $vaga -> TT014CC013 }}</strong> |
                    <strong>{{ $vaga -> cidade -> TT008CC003 }} - {{ $vaga -> cidade -> TT008CC001 }} </strong> |
                    {{ formatDate( $vaga -> TT014CC019 ) }}
                </p>
                <p class="card-text">
                    {!! $vaga -> TT014CC009 !!}
                </p>
                <a href="#" onclick="mudarcor('{{  $vaga -> TT014CC000 }}')" data-toggle="modal" data-target="#modal_show-{{  $vaga -> TT014CC000 }}">  <div class="btn btn-mural">Saiba Mais</div></a>
            </div>
        </div>
    </div>
</div>
  <!--Modal-->
    <div class="modal fade" id="modal_show-{{ $vaga -> TT014CC000  }}">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">VAGA</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <!-- Aviso -->
        <div class="alert alert-info my-std">
            <div class="text-justify">
                <h5 class="text-uppercase text-center">Prezado Colega Farmacêutico</h5>
                <p> Comunique o <a class="cor-link" href="https://www.participar.com.br/crfsp/users/sign_in" target="_blank">CRF-SP</a>, caso as informações contidas neste anúncio de vaga sejam diferentes das fornecidas pelo entrevistador. Sua identidade será mantida em sigilo e as providências cabíveis serão tomadas. </p>
            </div>
        </div>
        <!-- Aviso -->
         <div class="card my-std">
<div class="card-block text-left">
                        <h4 class="card-text text-uppercase color-paf-primary">
                            <strong>{{ $vaga -> cargo -> TT003CC001 }}</strong>
                        </h4>
                        <p class="card-text"> {{ $vaga -> empresa -> TT010CC004 }} ( {{ $vaga -> ramo -> TT002CC001 }} ) </p>
                        <p class="card-text"> <strong>vagas: {{ $vaga -> TT014CC013 }}</strong> | {{ $vaga -> cidade -> TT008CC003 }} - {{ $vaga -> cidade -> TT008CC001 }} | {{ formatDate($vaga -> TT014CC019) }} </p>
                        <p> R$ {{ formatMoney( $vaga -> TT014CC014 ) }}
                          @if ( $vaga -> TT014CC022 ===  'HO' )/ hora @elseif ( $vaga -> TT014CC022 ===  'ME' )  @endif  <!--definindo se por hora ou mês -->
                          @if ($vaga -> TT014CC015) | {{ $vaga -> TT014CC015 }} @endif </p>
                        <!-- Mostrar endereço? -->@if ($vaga -> TT014CC017)
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Endereço</strong>
                            </h6> {{ $vaga -> empresa -> TT010CC007 }} - {{ $vaga -> empresa -> TT010CC008 }} - CEP: {{ formatCep($vaga -> empresa -> TT010CC006) }} </p> @endif
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Descrição das Atividades</strong>
                            </h6> {!! $vaga -> TT014CC009 !!} </p>
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Requisitos</strong>
                            </h6> {!! $vaga -> TT014CC010 !!} </p>
                        <!-- Benefício preenchido? -->@if ($vaga -> TT014CC011)
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Benefícios</strong>
                            </h6> {!! $vaga -> TT014CC011 !!} </p> @endif
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Como se candidatar</strong>
                            </h6> {!! $vaga -> TT014CC012 !!} </p>
                    </div>
</div>

          </div>
        </div>
      </div>
    </div>
   <!--Modal-->
@endforeach
 <script type="text/javascript">
   mudarCoresLidos();
</script>
<!-- Paginação -->
{{ $vagas -> links( 'partials.pagination' ) }}
