<!-- layout Master -->
@extends('site::app')

<!-- Título -->
@section('titulo') {{ $concurso -> TT015CC001 }} | PAF @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class=" breadcrumb-item"><a href="{{ URL::Route('concursos.index') }}">Concursos Públicos</a></li>
            <li class=" breadcrumb-item active">{{ $concurso -> TT015CC001 }}</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->

<div class="row">
    <!-- Vaga -->
    <div class="col-12 col-md-8">
        <!-- Título -->
        <div class="row">

            <div class="col-12  col-sm-12">
                <div class="card">
                    <div class="card-block text-uppercase">
                        <h5 class="color-paf-empregos m-0">{{ $concurso -> TT015CC001 }}</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Título -->

        <!-- Content -->
        <div class="row">
            <div class="col-12 my-std">
                <div class="card">
                    <!-- Dados do Concurso -->
                    <div class="card-block text-left">

                        <p class="card-text text-uppercase color-paf-primary-10">
                            <strong>{{ $concurso -> TT015CC001 }}</strong>
                        </p>
                        <p class="card-text">
                            {{ $concurso -> TT015CC003 }}   @if ( $concurso -> TT015CC013 !=  "" )

                      - {{ $concurso ->  TT015CC013 }} -  {{ $concurso ->  TT015CC012 }}

                     @endif
                        </p>
                        <p class="card-text">
                            Inscrições até {{ formatDate( $concurso -> TT015CC009 ) }}
                        </p>
                         <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Descrição</strong>
                            </h6>
                            {!! $concurso -> TT015CC005 !!}
                        </p>
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Requisitos</strong>
                            </h6>
                            {!! $concurso -> TT015CC006 !!}
                        </p>
                        @if($concurso -> TT015CC007)
                        <p>
                            <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Informações Adicionais</strong>
                            </h6>
                            {!! $concurso -> TT015CC007 !!}
                        </p>
                        @endif
                    </div>
                    <!-- Dados do Concurso -->
                </div>
            </div>
        </div>
        <!-- /Content -->
    </div>
    <!-- /Vaga -->

    <!-- Últimas -->
    <div class="col-12 col-md-4">
        <div class="sticky-top">
            <!-- Título -->
            <div class="row">
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block text-uppercase">
                            <h5 class="color-paf-empregos m-0"> Últimas </h5>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Título -->

            <!-- Concursos -->
            <div class="row">
                @foreach( $concursos as $concurso )
                <div class="col-12 my-std">
                    <div class="card mural h-auto">
                        <div class="card-block text-center">
                            <p class="card-text text-uppercase color-paf-primary">
                                <strong>{{ $concurso -> TT015CC001 }}</strong>
                            </p>
                            <p class="card-text">
                                {{ $concurso -> TT015CC003 }}
                                 @if ( $concurso -> TT015CC013 !=  "" )

                      - {{ $concurso ->  TT015CC013 }} -  {{ $concurso ->  TT015CC012 }}

                     @endif


                            </p>
                            <p class="card-text">
                               Inscrições até {{ formatDate( $concurso -> TT015CC009 ) }}
                            </p>
                            <a href="{{ URL::Route('concursos.show', $concurso -> TT015CC000) }}">
                                <div class="btn btn-mural">Saiba Mais</div>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block">
                            <a class="cor-texto" href="{{ URL::Route( 'concursos.index' ) }}">
                                <h6 class="text-center text-uppercase m-0">
                                    Todos <i class="fa fa-external-link ml-1" aria-hidden="true"></i>
                                </h6>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Concursos -->
        </div>
    </div>
    <!-- /Últimas -->
</div>

@stop
