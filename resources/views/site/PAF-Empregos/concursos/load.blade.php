

<script>

 function mudarCoresLidos() {

    for(i=0;i<ids.length;i++){
    var minhadiv = 'div-'+ids[i];
        if(document.getElementById(minhadiv) != null){
            document.getElementById(minhadiv).style.borderBottomColor = "#F4B01B";
        }
    }
}


</script>
   <div class="row"> @foreach($concursos->sortBy('TT015CC009') as $concurso)
   <script>
            function mudarcor(id) {
                ids.push(id);
                var minhadiv = 'div-'+id;
                document.getElementById(minhadiv).style.borderBottomColor = "#F4B01B";
            }
    </script>
    <div class="col-12 col-lg-6 my-std ">
        <div id="div-{{ $concurso -> TT015CC000  }}" class="card mural h-auto">
            <div class="card-block text-left">
                <p class="card-text text-uppercase color-paf-primary-10"> <strong>{{ $concurso -> TT015CC001 }}</strong> </p>
                <p class="card-text"> {{ $concurso -> TT015CC003 }}
                 @if ( $concurso -> TT015CC013 !=  "" )

                      - {{ $concurso ->  TT015CC013 }} -  {{ $concurso ->  TT015CC012 }}

                     @endif


                   </p>
                <p class="card-text"> até {{ formatDate( $concurso -> TT015CC009 ) }} </p>
                <p class="card-text"> {!! $concurso -> TT015CC005 !!} </p>
                <a href="#"  onclick="mudarcor('{{ $concurso -> TT015CC000  }}')" data-toggle="modal" data-target="#modal_show-{{  $concurso -> TT015CC000 }}">
                    <div class="btn btn-mural">Saiba Mais</div>
                </a>
            </div>
        </div>
    </div>
    <!--Modal-->
    <div class="modal fade" id="modal_show-{{ $concurso -> TT015CC000  }}">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">CONCURSO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                </div>
                <div class="modal-body">
                    <div class="card my-std">
                        <div class="card-block text-left">
                            <p class="card-text text-uppercase color-paf-primary-10"> <strong>{{ $concurso -> TT015CC001 }}</strong> </p>
                            <p class="card-text"> {{ $concurso -> TT015CC003 }}
                            @if ( $concurso -> TT015CC013 !=  "" )

                      - {{ $concurso ->  TT015CC013 }} -  {{ $concurso ->  TT015CC012 }}

                     @endif
</p>
                            <p class="card-text"> Inscrições até {{ formatDate( $concurso -> TT015CC009 ) }} </p>
                            <p>
                                <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Descrição</strong>
                            </h6> {!! $concurso -> TT015CC005 !!} </p>
                            <p>
                                <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Requisitos</strong>
                            </h6> {!! $concurso -> TT015CC006 !!} </p> @if($concurso -> TT015CC007)
                            <p>
                                <h6 class="card-text mb-0">
                                <strong class="text-uppercase">Informações Adicionais</strong>
                            </h6> {!! $concurso -> TT015CC007 !!} </p>
                             @endif </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Modal-->@endforeach </div>
    <script type="text/javascript">
   mudarCoresLidos();
</script>
<!-- Paginação -->{{ $concursos -> links( 'partials.pagination' ) }}
