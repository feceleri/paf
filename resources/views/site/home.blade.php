@extends('site::app')

@section('titulo') Home | PAF @stop

@section('conteudo')

<!-- Header -->
<div class="row"> @if ($qtdBanners>0)
    <!-- Banner -->
    <div class="col-12 col-lg-8 my-std mb-owl">
        <div id="owlBanner" class="owl-carousel">
            @foreach($banners->sortByDesc('created_at') as $banner)
            <div class="slides">
                <figure class="figure m-0">
                    <a class="m-0" href="{{ $banner -> TT013CC003 }}" title="{{ $banner -> TT013CC002 }}">
                        <img class="figure-img banner m-0" src="{{ $banner -> TT013CC005 }}" alt="{{ $banner -> TT013CC002 }} - Logo">
                    </a>
                </figure>
                <div class="nav">
                    <label class="prev">&#x2039;</label>
                    <label class="next">&#x203a;</label>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- /Banner --> @endif

    <!-- Destaques -->
    <div class="col-12 col-lg-4 my-std">
        <div class="card destaques">
            <div class="text-center">
                <table class="table destaques">
                    <tbody>
                        <!-- PAF Empregos -->
                        <tr>
                            <td class="empregos align-middle p-0">@if ($qtdVagas>0)
                                <a href="{{ URL::Route('vagas.index') }}" title="PAF Empregos">
                                    <h4>
                                        <span >{{ $qtdVagas }}</span>
                                        @if ($qtdVagas>1) vagas de emprego @else  vaga de emprego @endif
                                    </h4>
                                    <h6 class="text-uppercase">PAF Empregos</h6>
                                </a>
                            </td>@endif
                        </tr>
                        <!-- /PAF Empregos -->

                        <!-- PAF Descontos -->
                        <tr>
                            <td class="descontos align-middle">
                                <a href="{{ URL::Route('descontos.index') }}" title="PAF Descontos">
                                    <h4>
                                        <span>{{ $qtdDescontos }}</span>
                                        descontos exclusivos
                                    </h4>
                                    <h6 class="text-uppercase">PAF Descontos</h6>
                                </a>
                            </td>
                        </tr>
                        <!-- /PAF Descontos -->

                        <!-- PAF Empregos -->
                        <tr>
                            <td class="concursos align-middle"> @if ($qtdConcursos > 0)
                                <a href="{{ URL::Route('concursos.index') }}" title="PAF Concursos">
                                    <h4>
                                        <span >{{ $qtdConcursos }}</span>    <!-- <span class="count"> classe serve para ativar contador visual da home -->
                                        @if ($qtdConcursos > 1) concursos públicos @else concurso público @endif
                                    </h4>
                                    <h6 class="text-uppercase">PAF Empregos</h6>
                                </a> @endif
                            </td>
                        </tr>
                        <!-- /PAF Empregos -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /Destaques -->
</div>
<!-- /Header -->

<!-- Mural -->
<div class="row">
    <div class="col-12">
        <div class="row">

            <!-- PAF-Empregos -->
            <div class="col-12 col-lg-4 ">
                <div class="row">
                    <div class="col-12 my-std">
                        <div class="card mural">
                            <div class="card-header p-0 bg-paf-primary text-center">
                                <figure class="figure m-0">
                                    {{ Html::image( URL::asset('img/logos/btn-empregos.png'), 'Empregos - Dicas e Artigos', array( 'class' => 'figure-img img-fluid m-0' )) }}
                                </figure>
                            </div>
                            <div class="card-block text-center">
                                <div class="row">@if ($qtdVagas>0)
                                    <div class="col-12">
                                        <p class="card-text color-paf-empregos">Última vaga de emprego cadastrada</p>
                                        <p class="card-text text-uppercase color-paf-primary">
                                            <strong>{{ $vaga -> cargo -> TT003CC001 }}</strong>
                                        </p>
                                        <p class="card-text">
                                            {{ $vaga -> empresa -> TT010CC004  }}
                                        </p>
                                        <p class="card-text">
                                            {{ $vaga -> cidade -> TT008CC003 }} - {{ $vaga -> cidade -> TT008CC001 }} |
                                            {{ formatDate( $vaga -> TT014CC019 ) }}
                                        </p>
                                        <a href="{{ URL::Route( 'vagas.show', $vaga -> TT014CC000 ) }}" title="Saiba Mais">
                                            <div class="btn btn-mural"> Saiba Mais </div>
                                        </a>
                                    </div>@endif
@if ($qtdConcursos > 0)
                                    <div class="col-12"><hr></div>

                                    <div class="col-12">
                                        <p class="card-text color-paf-empregos">Concurso Público</p>
                                        <p class="card-text text-uppercase color-paf-primary-10">
                                            <strong>{{ $concurso -> TT015CC001 }}</strong>
                                        </p>
                                        <p class="card-text">
                                            {{ $concurso -> TT015CC003 }}
                                        </p>
                                        <p class="card-text">
                                            Inscrições até {{ formatDate($concurso -> TT015CC009) }}
                                        </p>
                                        <a href="{{ URL::Route('concursos.show', $concurso -> TT015CC000) }}" title="Saiba Mais">
                                            <div class="btn btn-mural"> Saiba Mais </div>
                                        </a>
                                    </div>@endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /PAF-Empregos -->

            <!-- PAF Descontos -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="row">
                    <div class="col-12 my-std">
                        <div class="card mural">
                            <div class="card-header p-0 bg-paf-primary text-center">
                                <figure class="figure m-0">
                                    {{ Html::image( URL::asset('img/logos/btn-descontos.png'), 'Descontos - Dicas e Artigos', array( 'class' => 'figure-img img-fluid m-0' )) }}
                                </figure>
                            </div>
                            <div class="card-block text-center">
                                <figure class="figure m-0">
                                    {{ Html::image( $desconto -> TT012CC004, $desconto -> TT012CC003, array( 'class' => 'figure-img img-fluid mb-1 desconto' )) }}
                                </figure>
                                <p class="card-text color-paf-descontos">
                                    {{ $desconto -> TT012CC003 }}
                                </p>
                                <p class="card-text text-uppercase color-paf-primary">
                                    <strong>{{ $desconto -> categoria -> TT005CC001 }}</strong>
                                </p>
                                <a href="{{ URL::Route('descontos.show', $desconto -> TT012CC000) }}" title="Saiba Mais">
                                    <div class="btn btn-mural"> Saiba Mais </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /PAF Descontos -->

            <!-- Artigos -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="row">
                    <div class="col-12 my-std">
                        <div class="card mural">
                            <div class="card-header p-0 bg-paf-primary text-center">
                                <figure class="figure m-0">
                                    {{ Html::image( URL::asset('img/logos/btn-artigos.png'), 'Artigos - Dicas e Artigos', array( 'class' => 'figure-img img-fluid m-0' )) }}
                                </figure>
                            </div>
                            <div class="card-block">
                                @foreach( $artigos as $artigo )
                                <a href="{{ URL::Route( 'artigos.show', $artigo -> TT009CC000 ) }}" title="Leia">
                                    <div class="media artigo">
                                        <figure class="figure d-flex align-self-center m-0">
                                            {{ Html::image( $artigo -> TT009CC003, $artigo -> TT009CC002, array( 'class' => 'figure-img img-fluid' )) }}
                                        </figure>
                                        <div class="media-body">
                                            <h6 class="mb-2">{{ formatDate($artigo -> created_at) }}</h6>
                                            <h6>{{ $artigo -> TT009CC002 }}</h6>
                                        </div>
                                    </div>
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Artigos -->
        </div>
    </div>
</div>
<!-- /Mural -->

<!-- Parceiros -->
<div class="row">
    <!-- PAF Empregos -->
    <div class="col-12 col-md-6 my-std">
        <div class="card">
            <div class="card-block text-center">
                <h6 class="color-paf-empregos mb-3">Parceiros PAF Empregos</h6>
                <div id="owlPafEmpregos" class="owl-carousel">
                    @foreach( $parEmpregos as $parceiro )
                    <figure class="figure m-0">
                        <a class="m-0" href="{{ $parceiro -> TT011CC004 }}" title="{{ $parceiro -> TT011CC001 }}">
                            <img class="figure-img m-0" src="{{ $parceiro -> TT011CC003 }}" alt="{{ $parceiro -> TT011CC001 }} - Logo">
                        </a>
                    </figure>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- /PAF Empregos -->

    <!-- PAF Descontos -->
    <div class="col-12 col-md-6 my-std">
        <div class="card">
            <div class="card-block text-center">
                <h6 class="color-paf-descontos mb-3">Parceiros PAF Descontos</h6>
                <div id="owlPafDescontos" class="owl-carousel">
                    @foreach( $parDescontos as $parceiro )
                    <figure class="figure m-0">
                        <a class="m-0" href="{{ $parceiro -> TT011CC004 }}" title="{{ $parceiro -> TT011CC001 }}">
                            <img class="figure-img m-0" src="{{ $parceiro -> TT011CC003 }}" alt="{{ $parceiro -> TT011CC001 }} - Logo">
                        </a>
                    </figure>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- /PAF Descontos -->
</div>
<!-- /Parceiros -->

@stop
