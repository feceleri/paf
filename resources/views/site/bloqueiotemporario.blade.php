<!-- layout Master -->@extends('site::app')
<!-- Título -->@section('titulo') Sobre | PAF @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{!! URL::to('/') !!}">Home</a></li>
            <li class="breadcrumb-item active">Período de recesso</li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->
<!-- Sobre o PAF -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto">Aviso</span></div>
                </div>
                <p class="text-justify">
                    <b> Prezados:</b><br><br>

Devido à pandemia do novo coronavírus e à redução temporária do número de colaboradores, informamos que a Bolsa de Empregos do CRF-SP estará suspensa de 10/04/2020 a 03/05/2020.

<!--
                “Informamos que o atendimento estará suspenso no período de 19 de dezembro de 2019 a 3 de janeiro de 2020.  Retornaremos nossas atividades em  6 de janeiro de 2020.”
<br><br>
“O CRF-SP deseja a todos um Natal de muita paz e que 2019 seja repleto de novas conquistas, realizações e muita saúde!”
-->




 </p>
            </div>
        </div>
    </div>
</div>
<!-- /Sobre o PAF -->



@stop
