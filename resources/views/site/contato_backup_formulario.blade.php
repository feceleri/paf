<!-- layout Master -->
@extends('site::app')

<!-- Título -->
@section('titulo') Contato | PAF @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to( '/' ) }}"> Home </a></li>
            <li class="breadcrumb-item active"> Contato </li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Título -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block text-uppercase">
                <h5 class="m-0"> Contato </h5>
            </div>
        </div>
    </div>
</div>
<!-- /Título -->

<!-- Aviso -->
<div class="alert alert-info my-std">
    <div class="text-justify">
        <p class="mb-0">
          Caso tenha algum Comentário, crítica ou sugestão, favor enviar um e-mail para: <a href="mailto:paf@crfsp.org.br">paf@crfsp.org.br</a>.
<br>
Ou se preferir ligue para: (11)3067-1867 / (11)3067-1869 de segunda a sexta-feira das 8h às 17h.
<br>
Sua opinião é muito importante para nós.

           <!-- Caso tenha dúvidas, críticas, sugestões, denúncias ou outros assuntos em relação ao PAF - Programa de Assistência ao Farmacêutico,  envie-nos sua mensagem.-->
        </p>
    </div>
</div>
<!-- /Aviso -->

<!-- Mensagens de Sucesso -->
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show my-std" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p class="mb-0">
            {{ $message }}
        </p>
    </div>
@endif
<!-- /Mensagens de Sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0)
    <div class="alert alert-danger alert-dismissible fade show my-std" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
       <p class="mb-0"> Confira os campos em destaque </p>
    </div>
@endif
<!-- /Mensagens de erro -->

<!-- Form para Contato -->


{{ Form::open(['route' => 'contato.store','method'=>'POST']) }}

<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-12 col-md-9">
                        <div class="form-group @if ( $errors -> has( 'nome' )) has-danger @endif">
                            {{ Form::label('nome', 'Nome *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::text
                                (
                                    'nome',
                                    null,

                                    [
                                        'class' => 'form-control'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('nome'))
                            <div class="form-control-feedback">
                                {{ $errors->first('nome') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group @if ( $errors -> has( 'telefone' )) has-danger @endif">
                            {{ Form::label('telefone', 'Telefone *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::text
                                (
                                    'telefone',
                                    null,

                                    [
                                        'maxlength' => '15',
                                        'class' => 'form-control tel',
                                        'placeholder' => '(00) 0000-0000'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('telefone'))
                            <div class="form-control-feedback">
                                {{ $errors->first('telefone') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                </div>
                <div class="row">
                   <div class="col-12 col-md-5">
                        <div class="form-group @if ( $errors -> has( 'assunto' )) has-danger @endif">
                            {{ Form::label('assunto', 'Assunto *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::select
                               (
                                   'assunto',

                                    [
                                        'Elogio' => 'Elogio',
                                        'Dúvida' => 'Dúvida',
                                        'Crítica' => 'Crítica',
                                        'Denúncia' => 'Denúncia',
                                        'Sugestão' => 'Sugestão',
                                        'Quero ser um parceiro' => 'Quero ser um parceiro',
                                        'Quero indicar um parceiro' => 'Quero indicar um parceiro'
                                    ],
                                    null,

                                    [
                                        'class' => 'form-control',
                                        'placeholder' => 'Selecione o tipo de contato'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('assunto'))
                            <div class="form-control-feedback">
                                {{ $errors->first('assunto') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="form-group @if ( $errors -> has( 'email' )) has-danger @endif">
                            {{ Form::label('email', 'E-mail *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::text
                                (
                                    'email',
                                    null,

                                    [
                                        'class' => 'form-control'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('email'))
                            <div class="form-control-feedback">
                                {{ $errors->first('email') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group @if ( $errors -> has( 'mensagem' )) has-danger @endif">
                            {{ Form::label('mensagem', 'Mensagem *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::textArea
                                (
                                    'mensagem',
                                    null,

                                    [
                                        'class' => 'form-control'
                                        ,'rows' => '10'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('mensagem'))
                            <div class="form-control-feedback">
                                {{ $errors->first('mensagem') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p class="card-text text-uppercase"><strong>* Campos obrigatórios</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label class="custom-control custom-checkbox">
                            <input name="enviarCopia" type="checkbox" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            Desejo receber uma cópia desta mensagem no meu e-mail.
                        </label>
                    </div>
                </div>

                <!-- Enviar Mensagem -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                                             <script src="https://www.google.com/recaptcha/api.js" type="text/javascript"></script>
              <script type="text/javascript">
                   function btnClick()
                   {
                      if (!validData())
                          return false;

                   }
                   function validData()
                   {
                      var v = grecaptcha.getResponse();
                      if(v.length == 0)
                      {
                          event.preventDefault();
                          return false;
                      }
                      else
                      {
                          return true;
                      }
                   }
                </script>
               <center><div class="g-recaptcha" data-sitekey="6LfjOSYUAAAAAAOg7gOzqjtfp7Np1o_GVI1Q8akP"></div></center>
                                <div class="row">
                                    <div class="col-12 col-md-2"></div>
                                    <div class="col-12 col-md-8">
                                        {{ Form::button
                                           (
                                                '<h5 class="m-0 text"><strong>Enviar Mensagem</strong></h5>',

                                                [
                                                    'class' => 'btn btn-block btn-admin',
                                                    'type' => 'submit',
                                                    'onclick' => 'btnClick()'
                                                ]
                                            )
                                        }}
                                    </div>
                                    <div class="col-12 col-md-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Enviar Mensagem -->
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}
<!-- /Form para Contato -->

@stop
