<!-- layout Master -->
@extends('site::app')



<!-- Título -->
@section('titulo') Contato | PAF @stop



<!-- Conteúdo -->
@section('conteudo')

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to( '/' ) }}"> Home </a></li>
            <li class="breadcrumb-item active">  @if($tipoForm == 'CO')   Contato  @endif   @if($tipoForm == 'SP')   Ser um parceiro  @endif  @if($tipoForm == 'IP')   Indicar um parceiro  @endif  @if($tipoForm == 'RA')  Retirar anúncio  @endif  </li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Título -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block text-uppercase">
                <h5 class="m-0">  @if($tipoForm == 'CO')   Contato  @endif   @if($tipoForm == 'SP')   Ser um parceiro  @endif  @if($tipoForm == 'IP')   Indicar um parceiro  @endif  @if($tipoForm == 'RA')  Retirar anúncio  @endif </h5>
            </div>
        </div>
    </div>
</div>
<!-- /Título -->

<!-- Aviso -->
<div class="alert alert-info my-std">
    <div class="text-justify">
        <p class="mb-0">

          @if($tipoForm == 'CO')

           Para entrar em contato conosco, favor preencher o formulário abaixo ou enviar um e-mail para:
           <a href="mailto:paf@crfsp.org.br">paf@crfsp.org.br</a><br>
Se preferir ligue para: (11)3067-1869 / (11)3067-1867 de segunda a sexta-feira das 8h30 às 17h30."


           @endif

          @if($tipoForm == 'SP')
                      Se você é uma empresa com interesse em fazer parte do Clube de Benefícios do PAF, proporcionando descontos em seus produtos e serviços aos farmacêuticos inscritos, envie sua mensagem.
           @endif


           @if($tipoForm == 'IP')
                     Se você é farmacêutico ou funcionário do CRF-SP e tem interesse em indicar uma empresa para fazer parte do PAF descontos, envie sua mensagem.
           @endif
           @if($tipoForm == 'RA') As vagas ficam disponíveis por 15 dias, sendo automaticamente desabilitadas após esse período.<br>

Caso queira retirar o anúncio antes desse prazo, preencha o formulário abaixo informando a Razão Social, CNPJ da Empresa e o título do cargo.


<br>
 @endif

        </p>
    </div>

</div>
<div  class="col-12 my-std">

</div>
<!-- /Aviso -->

<!-- Aqui começa o formulário -->



<!-- Mensagens de Sucesso -->
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show my-std" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p class="mb-0">
            {{ $message }}
        </p>
    </div>
@endif
<!-- /Mensagens de Sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0)
    <div class="alert alert-danger alert-dismissible fade show my-std" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
       <p class="mb-0"> Confira os campos em destaque </p>
    </div>
@endif
<!-- /Mensagens de erro -->

<!-- Form para Contato -->


{{ Form::open(['route' => 'contato.store','method'=>'POST']) }}

<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-12 col-md-9">
                        <div class="form-group @if ( $errors -> has( 'nome' )) has-danger @endif">
                            {{ Form::label('nome', 'Nome *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::text
                                (
                                    'nome',
                                    null,

                                    [
                                        'class' => 'form-control'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('nome'))
                            <div class="form-control-feedback">
                                {{ $errors->first('nome') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group @if ( $errors -> has( 'telefone' )) has-danger @endif">
                            {{ Form::label('telefone', 'Telefone *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::text
                                (
                                    'telefone',
                                    null,

                                    [
                                        'maxlength' => '15',
                                        'class' => 'form-control tel',
                                        'placeholder' => '(00) 0000-0000'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('telefone'))
                            <div class="form-control-feedback">
                                {{ $errors->first('telefone') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                </div>
                <div class="row">
                   <div class="col-12 col-md-5">
                        <div class="form-group @if ( $errors -> has( 'assunto' )) has-danger @endif">

                         @if($tipoForm == 'CO')

                            {{ Form::label('assunto', 'Assunto *', [ 'class' => 'form-control-label' ])}}

                             {{ Form::text
                                (
                                    'assunto',
                                    null,

                                    [
                                        'class' => 'form-control'
                                    ]
                                )
                            }}



                            <!-- Erro -->
                            @if ($errors->has('assunto'))
                            <div class="form-control-feedback">
                                {{ $errors->first('assunto') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                           @endif



                            @if($tipoForm == 'SP')

                            {{ Form::label('assunto', 'Assunto *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::select
                               (
                                   'assunto',

                                    [

                                        'Quero ser um parceiro' => 'Quero ser um parceiro',

                                    ],
                                    null,

                                    [
                                        'class' => 'form-control',

                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('assunto'))
                            <div class="form-control-feedback">
                                {{ $errors->first('assunto') }}
                            </div>
                            @endif
                             <!-- /Erro -->
                           @endif


                           @if($tipoForm == 'IP')

                            {{ Form::label('assunto', 'Assunto *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::select
                               (
                                   'assunto',

                                    [

                                        'Quero indicar um parceiro' => 'Quero indicar um parceiro'

                                    ],
                                    null,

                                    [
                                        'class' => 'form-control',

                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('assunto'))
                            <div class="form-control-feedback">
                                {{ $errors->first('assunto') }}
                            </div>
                            @endif
                             <!-- /Erro -->
                           @endif


                             @if($tipoForm == 'RA')

                            {{ Form::label('assunto', 'Assunto *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::select
                               (
                                   'assunto',

                                    [

                                        'Retirar anúncio' => 'Retirar anúncio',

                                    ],
                                    null,

                                    [
                                        'class' => 'form-control',

                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('assunto'))
                            <div class="form-control-feedback">
                                {{ $errors->first('assunto') }}
                            </div>
                            @endif
                             <!-- /Erro -->
                           @endif


                        </div>
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="form-group @if ( $errors -> has( 'email' )) has-danger @endif">
                            {{ Form::label('email', 'E-mail *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::text
                                (
                                    'email',
                                    null,

                                    [
                                        'class' => 'form-control'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('email'))
                            <div class="form-control-feedback">
                                {{ $errors->first('email') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                </div>
                 @if($tipoForm == 'RA')
                  <div class="row">
                    <div class="col-4">
                        <div class="form-group @if ( $errors -> has( 'razao' )) has-danger @endif">
                            {{ Form::label('razao', 'Razão Social *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::text
                                (
                                    'razao',
                                    null,

                                    [
                                        'class' => 'form-control'

                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('razao'))
                            <div class="form-control-feedback">
                                {{ $errors->first('razao') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group @if ( $errors -> has( 'cnpj' )) has-danger @endif">
                            {{ Form::label('cnpj', 'CNPJ *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::text
                                (
                                    'cnpj',
                                    null,

                                    [
                                        'class' => 'form-control'

                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('cnpj'))
                            <div class="form-control-feedback">
                                {{ $errors->first('cnpj') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group @if ( $errors -> has( 'titulocargo' )) has-danger @endif">
                            {{ Form::label('titulocargo', 'Título do Cargo *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::text
                                (
                                    'titulocargo',
                                    null,

                                    [
                                        'class' => 'form-control'

                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('titulocargo'))
                            <div class="form-control-feedback">
                                {{ $errors->first('titulocargo') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="form-group @if ( $errors -> has( 'mensagem' )) has-danger @endif">
                            {{ Form::label('mensagem', 'Mensagem *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::textArea
                                (
                                    'mensagem',
                                    null,

                                    [
                                        'class' => 'form-control'
                                        ,'rows' => '10'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('mensagem'))
                            <div class="form-control-feedback">
                                {{ $errors->first('mensagem') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p class="card-text text-uppercase"><strong>* Campos obrigatórios</strong></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <label class="custom-control custom-checkbox">
                            <input name="enviarCopia" type="checkbox" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            Desejo receber uma cópia desta mensagem no meu e-mail.
                        </label>
                    </div>
                </div>

                <!-- Enviar Mensagem -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                                <script src="https://www.google.com/recaptcha/api.js" type="text/javascript"></script>
              <script type="text/javascript">
                   function btnClick()
                   {
                      if (!validData())
                          return false;

                   }
                   function validData()
                   {
                      var v = grecaptcha.getResponse();
                      if(v.length == 0)
                      {
                          event.preventDefault();
                          return false;
                      }
                      else
                      {
                          return true;
                      }
                   }
                </script>
               <center><div class="g-recaptcha" data-sitekey="6LfjOSYUAAAAAAOg7gOzqjtfp7Np1o_GVI1Q8akP"></div></center>
                                <div class="row">
                                    <div class="col-12 col-md-2"></div>
                                    <div class="col-12 col-md-8">
                                        {{ Form::button
                                           (
                                                '<h5 class="m-0 text"><strong>Enviar Mensagem</strong></h5>',

                                                [
                                                    'class' => 'btn btn-block btn-admin',
                                                    'type' => 'submit',
                                                    'onclick' => 'btnClick()'
                                                ]
                                            )
                                        }}
                                    </div>
                                    <div class="col-12 col-md-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Enviar Mensagem -->
            </div>
        </div>
    </div>
</div>

{{ Form::close() }}
<!-- /Form para Contato -->






@stop
