<!-- Artigos -->
<div class="row">
    @foreach( $artigos->sortByDesc('created_at')  as $artigo )
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="media">
                    <a href="{{ URL::Route( 'artigos.show', $artigo -> TT009CC000 ) }}">
                        <div class="row align-items-center">
                            <div class="col-12 col-lg-5 text-center">
                                <figure class="figure align-self-center m-0">
                                    {{ Html::image( $artigo -> TT009CC003, $artigo -> TT009CC002, array( 'class' => 'figure-img img-fluid m-0' )) }}
                                </figure>
                            </div>
                            <div class="col-12 col-lg-7">
                                <div class="media-body text-xs-center text-md-left m-4">
                                    <h4>{{ $artigo -> TT009CC002 }}</h4>
                                    <p class="m-0">{{ formatDate( $artigo -> created_at ) }}</p>
                                    <br>{!! limitText( $artigo -> TT009CC004, 180 ) !!}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
<!-- Artigos -->

<!-- Paginação -->
{{ $artigos -> links( 'partials.pagination' ) }}
