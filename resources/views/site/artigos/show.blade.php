<!-- layout Master -->
@extends('site::app')

@script()

<!-- Título -->
@section('titulo') Artigo | PAF @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class=" breadcrumb-item"><a href="{{ URL::Route('artigos.index') }}">Artigos</a></li>
            <li class=" breadcrumb-item active"> {{ $artigo -> TT009CC002 }} </li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->

<div class="row">
    <!-- Artigo -->
    <div class="col-12 col-lg-8">
        <!-- Título -->
        <div class="row">
            <div class="col-12 my-std">
                <div class="card">
                    <div class="card-block text-uppercase">
                        <h5 class="m-0"> {{ $artigo -> TT009CC002 }} - {{ formatDate( $artigo -> created_at ) }}</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Título -->

        <!-- Content -->
        <div class="row">
            <div class="col-12 my-std">
                <div class="card">
                    <div class="card-block">
                        <figure class="figure artigo-img text-center">
                            {{ Html::image( $artigo -> TT009CC003, $artigo -> TT009CC002, array('class' => 'figure-img img-fluid')) }}
                        </figure>
                        {!! $artigo -> TT009CC004 !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- /Content -->
    </div>
    <!-- /Artigo -->

    <!-- Artigos -->
    <div class="col-12 col-lg-4">
       <div class="sticky-top">
            <!-- Título -->
            <div class="row">
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block text-uppercase">
                            <h5 class="m-0"> Últimos </h5>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Título -->

            <!-- Útimos -->
            <div class="row">
                @foreach( $artigos as $artigo )
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block">
                            <a href="{{ URL::Route( 'artigos.show', $artigo -> TT009CC000 ) }}">
                                <div class="media artigo cor-texto">
                                    <figure class="figure align-self-center mb-0">
                                        {{ Html::image( $artigo -> TT009CC003, $artigo -> TT012CC003, array( 'class' => 'figure-img img-fluid mb-0' )) }}
                                    </figure>
                                    <div class="media-body">
                                        <h6 class="mb-2">{{ formatDate($artigo -> created_at) }}</h6>
                                        <h6>{{ $artigo -> TT009CC002 }}</h6>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block">
                            <a class="cor-texto" href="{{ URL::Route( 'artigos.index' ) }}">
                                <h6 class="text-center text-uppercase m-0">
                                    Todos <i class="fa fa-external-link ml-1" aria-hidden="true"></i>
                                </h6>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Útimos -->
        </div>
    </div>
    <!-- /Artigos -->
</div>

@stop
