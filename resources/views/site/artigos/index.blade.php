<!-- layout Master -->
@extends('site::app')

@section('script')
<script type="text/javascript">
   window.onload = function() {
       document.getElementsByClassName("apply-filter")[0].click();
};
</script>
  @stop

<!-- Título -->
@section('titulo') Dicas e Artigos | PAF @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Breadcrumb -->
<div class="card my-std">
    <div class="card-block">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ URL::to('/') }}">Home</a></li>
            <li class=" breadcrumb-item active"> Artigos </li>
        </ol>
    </div>
</div>
<!-- /Breadcrumb -->

<div class="row">
    <!-- Artigos -->
    <div class="col-12 col-md-8 col-lg-9">
        <!-- Título -->
        <div class="row">
            <div class="col-12 my-std">
                <div class="card">
                    <div class="card-block text-uppercase">
                        <h5 class="m-0"> Artigos </h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Título -->

         <!-- Load dos Artigos -->
        <section class="load-ajax">
            @include('site::Artigos.load')
        </section>
        <!-- Load dos Artigos -->
    </div>
    <!-- /Artigos -->

    <!-- Temas -->
    <div class="col-12 col-md-4 col-lg-3">
       <div class="sticky-top">
             <!-- Título -->
            <div class="row">
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block text-uppercase">
                            <h5 class="m-0"> Temas </h5>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Título -->

            <!-- Cidade -->
            <div class="row">
                <div class="col-12 my-std">
                     <div class="card">
                        <div class="card-block">

                            <div class="form-check">
                            {{ Form::open([ 'class' => 'filter-form' ], [ 'route' => 'filter', 'method'=>'GET' ]) }}
                                @foreach( $temas as $tema )
                                <div class="col-12">
                                    <label class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" name="TT009CC001[]" value="{{ $tema -> TT004CC000 }}">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description text-uppercase">
                                            {{ $tema -> TT004CC002 }}
                                        </span>
                                    </label>
                                </div>
                                @endforeach
                            {{ Form::close() }}
                            </div>

                            <div class="row">
                                <!-- Aplicar Filtro -->
                                <div class="col-12 mb-2">
                                    {{ Form::button
                                       (
                                            '<h5 class="m-0 text"><strong>Aplicar Filtro</strong></h5>',

                                            [
                                                'class' => 'btn btn-block btn-admin apply-filter',
                                            ]
                                        )
                                    }}
                                </div>
                                <!-- /Aplicar Filtro -->

                                <!-- Limpar Filtro -->
                                <div class="col-12">
                                    {{ Form::button
                                       (
                                            '<h5 class="m-0 text"><strong>Limpar Filtro</strong></h5>',

                                            [
                                                'class' => 'btn btn-block btn-admin',
                                                'onclick' => 'location.reload();'
                                            ]
                                        )
                                    }}
                                </div>
                                <!-- Limpar Filtro -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Cidade -->
        </div>
    </div>
    <!-- /Temas -->
</div>

@stop
