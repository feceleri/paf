<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link async rel="stylesheet" href="{{ elixir('css/all.css') }}">
    <link rel="icon" href="{{ URL::asset('img/logos/favicon.ico') }}" type="image/x-icon">
    <script async src="{{ elixir('js/all.js') }}"></script>
    <title> Dados do usuário | Currículos </title>
</head>

<body>
    <div class="container">
        <div class="card login mx-auto">
            <div class="card-header text-center bg-paf-primary">
                <h6 class="text-uppercase">Atenção!</h6> </div>
            <div class="row">
                <div class="col-12 my-std">
                    <div class="card">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-12  col-lg-12"> @if($crf == $crfdigitado)
                                    <div class="col-12">
                                        <spam class="form-control-label">Prezado(a) Dr.(a) {{ $nome }}, verifique seus dados pessoais. Caso necessite alterá-los, edite diretamente no portal e-cat.
                                            <br>
                                            <br>Nome: <b>{{ $nome }}</b>
                                            <br>CRF: <b>{{ $crf }} </b>
                                            <br>Sexo:<b> {{ $sexo }} </b>
                                            <br>Endereco: <b>{{ $endereco }} </b>
                                            <br>Cidade:<b> {{ $cidade }} </b>
                                            <br>Cep: <b>{{ $cep }} </b>
                                            <br>Email:<b> {{ $email }} </b>
                                            <br>Data de nascimento:<b> {{ $nascimento }} </b>
                                            <br>Nacionalidade:<b> {{ $nacionalidade }} </b>
                                            <br>Celular:<b> {{ $celular }} </b>
                                            <br>Telefone:<b> {{ $telefone }} </b>
                                            <br>

                               <br>

                                            <div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-block">
                                                                <div class="row">
                                                                    <div class="col-12 col-md-3"></div>
                                                                    <div class="col-12 col-md-6">
                                                                        <form action="{{ route('cadastro.enviaemail') }}" method="POST" id="myForm">
                                                                            <input type="hidden" name="cpf" value="{{ $cpf }}">
                                                                            <input type="hidden" name="crf" value="{{ $crf }}">
                                                                            <input type="hidden" name="nome" value="{{ $nome }}">
                                                                            <input type="hidden" name="sexo" value="{{ $sexo }}">
                                                                            <input type="hidden" name="endereco" value="{{ $endereco }}">
                                                                            <input type="hidden" name="cidade" value="{{ $cidade }}">
                                                                            <input type="hidden" name="cep" value="{{ $cep }}">
                                                                            <input type="hidden" name="email" value="{{ $email }}">
                                                                            <input type="hidden" name="seccional" value="{{ $seccional }}">
                                                                            <input type="hidden" name="idade" value="{{ $idade }}">
                                                                            <input type="hidden" name="nascimento" value="{{ $nascimento }}">
                                                                            <input type="hidden" name="nacionalidade" value="{{ $nacionalidade }}">
                                                                            <input type="hidden" name="celular" value="{{ $celular }}">
                                                                            <input type="hidden" name="telefone" value="{{ $telefone }}">
                                                                             {{ csrf_field() }}
                                                                            <input class="btn btn-block btn-admin" type="submit" value="Criar currículo"> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>@else Dados não coincidem. Entrar em contato com a central. @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>
