<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link async rel="stylesheet" href="{{ elixir('css/all.css') }}">
    <link rel="icon" href="{{ URL::asset('img/logos/favicon.ico') }}" type="image/x-icon">
    <script async src="{{ elixir('js/all.js') }}"></script>
    <title> Cadastro de usuário | Currículos </title>
</head>

<body>
    <div class="container">
        <!-- Dados da Empresa -->
        <div class="card login mx-auto">
            <div class="card-header text-center bg-paf-primary">
                <h6 class="text-uppercase">Digite seu CPF</h6> </div>
            <form action="{{ route('cadastro.buscasoap') }}" method="POST" id="myForm">
                <div class="row">
                    <div class="col-12 my-std">
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <!-- CPF -->
                                    <div class="col-12 col-md-6 col-lg-12"> {{ csrf_field() }}
                                        <div class="col-12 col-md-6 col-lg-8"> <spam class="form-control-label"> CPF:</spam>
                                            <input class= "form-control cpf" type="text" name="cpf"> </div>
                                        <div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-12 col-md-3"></div>
                                                                <div class="col-12 col-md-6">
                                                                    <input class="btn btn-block btn-admin" type="submit" value="Enviar"> </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>