<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link async rel="stylesheet" href="{{ elixir('css/all.css') }}">
    <link rel="icon" href="{{ URL::asset('img/logos/favicon.ico') }}" type="image/x-icon">
    <script async src="{{ elixir('js/all.js') }}"></script>
    <title> Login | Admin </title>
</head>

<body>
    <div class="container">
        <!-- Dados da Empresa -->
        <div class="card login mx-auto">
            <div class="card-header text-center bg-paf-primary">
                <h6 class="text-uppercase">Login</h6>
            </div>

            <div class="card-block">
                {{ Form::open(['route' => 'login','method'=>'POST']) }}
                <!-- Mensagens de erro -->
                @if ($message = Session::get('erro'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <p class="mb-0">
                        {{ $message }}
                    </p>
                </div>
                @endif
                <!-- /Mensagens de erro -->

                <div class="row">
                    <!-- Usuário -->
                    <div class="col-12">
                        <div class="form-group @if ( $errors -> has( 'TT001CC002' )) has-danger @endif">
                            {{ Form::label( 'TT001CC002', 'Usuário *', ['class' => 'form-control-label' ]) }}
                            {{ Form::text
                                (
                                    'TT001CC002',
                                    null,

                                    [
                                        'class' => 'form-control',
                                        'maxlength' => '50'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT001CC002'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT001CC002') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Usuário -->

                    <!-- Senha -->
                    <div class="col-12">
                        <div class="form-group @if ( $errors -> has( 'TT001CC003' )) has-danger @endif">
                            {{ Form::label( 'TT001CC003', 'Senha *', ['class' => 'form-control-label' ]) }}
                            {{ Form::password
                                (
                                    'TT001CC003',

                                    [
                                        'class' => 'form-control',
                                        'maxlength' => '50'
                                    ]
                                )
                            }}

                            <!-- Erro -->
                            @if ($errors->has('TT001CC003'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT001CC003') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Senha -->

                    <div class="col-12">
                        <label class="custom-control custom-checkbox">
                            <input name="remember" type="checkbox" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            Mantenha-me logado
                        </label>
                        <hr>
                    </div>

                    <div class="col-12">
                        {{ Form::button
                            (
                                '<i class="fa fa-arrow-circle-right fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</body>
</html>
