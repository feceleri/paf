<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link async rel="stylesheet" href="{{ elixir('css/all.css') }}">
    <link rel="icon" href="{{ URL::asset('img/logos/favicon.ico') }}" type="image/x-icon">
    <script async src="{{ elixir('js/all.js') }}"></script>
    <title> Cadastro de usuário | Currículos </title>
</head>

<body>
    <div class="container">
        <!-- Dados da Empresa -->
        <div class="card login mx-auto">
            <div class="card-header text-center bg-paf-primary">
                <h6 class="text-uppercase">Digite seu CRF com 5 digitos</h6> </div>
            <form action="{{ route('cadastro.validacrf') }}" method="POST" id="myForm">
                <div class="row">
                    <div class="col-12 my-std">
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <!-- CPF -->
                                    <div class="col-12 col-md-6 col-lg-12"> {{ csrf_field() }}
                                       
                                        <div class="col-12 col-md-6 col-lg-8"> <spam class="form-control-label"> Nome: {{ $nome }}<br>CPF: {{ $cpf }}<br></spam>
                                            <input class= "form-control " maxlength="5" type="text" name="crfdigitado"> </div>
                                             <input type="hidden"  name="cpf" value="{{ $cpf }}">
                                             <input type="hidden"  name="crf" value="{{ $crf }}">
                                             <input type="hidden"  name="nome" value="{{ $nome }}">
                                             <input type="hidden"  name="sexo" value="{{ $sexo }}">
                                             <input type="hidden"  name="endereco" value="{{ $endereco }}">
                                             <input type="hidden"  name="cidade" value="{{ $cidade }}">
                                             <input type="hidden"  name="cep" value="{{ $cep }}">
                                             <input type="hidden"  name="email" value="{{ $email }}">

                                             <input type="hidden"  name="seccional" value="{{ $seccional }}">
                                              <input type="hidden"  name="idade" value="{{ $idade }}">
                                               <input type="hidden"  name="nascimento" value="{{ $nascimento }}">
                                                <input type="hidden"  name="nacionalidade" value="{{ $nacionalidade }}">
                                                 <input type="hidden"  name="celular" value="{{ $celular }}">
                                                  <input type="hidden"  name="telefone" value="{{ $telefone }}">


                                                      <div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card">
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-12 col-md-3"></div>
                                                                <div class="col-12 col-md-6">

                                                                    <input class="btn btn-block btn-admin" type="submit" value="Validar"> </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
