<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ elixir('css/admin.css') }}">

    <link rel="icon" href="{{ URL::asset('img/logos/icon.png') }}" type="image/x-icon">
    <script async src="{{ elixir('js/admin.js') }}"></script>
    @yield('script')
    <title>@yield('titulo')</title>
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="side left_col menu_fixed">
                <div class="left_col">
                    <div class="navbar nav_title text-center mb-3">
                        <a href="{{ URL::to('/admin') }}">
                           {{ Html::image( URL::asset('img/logos/negativo-horizontal.png' ), "PAF-Programa de Assistência ao Farmacêutico", array( 'class' => 'header-logo img-fluid' )) }}
                            <i class="fa fa-home" aria-hidden="true"></i>
                        </a>
                    </div>

                    <!-- menu profile quick info -->
                    <div class="profile">

                    </div>

                    <!-- /menu profile quick info -->

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <!-- Geral -->
                        <div class="menu_section">
                            <h3>Geral</h3>
                            <ul class="nav flex-column side-menu">

                                <!-- Destaques -->
                                <li>
                                    <a>
                                        <i class="fa fa-star fa-lg" aria-hidden="true"></i> Destaques
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ URL::Route( 'admin.banners.index' ) }}">Banners</a></li>
                                    </ul>
                                </li>
                                <!-- /Destaques -->

                                <!-- Bolsa de Emprego -->
                                <li>
                                    <a>
                                        <i class="fa fa-briefcase fa-lg" aria-hidden="true"></i> PAF Empregos
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ URL::Route( 'admin.ramos.index' ) }}">Ramos de Atividade</a></li>
                                        <li><a href="{{ URL::Route( 'admin.empresas.index' ) }}">Empresas</a></li>
                                        <li><a href="{{ URL::Route( 'admin.cargos.index' ) }}">Cargos</a></li>
                                        <li><a href="{{ URL::Route( 'admin.vagas.index' ) }}">Vagas</a></li>
                                         <li><a href="{{ URL::Route( 'admin.estatisticas.index' ) }}">Estatísticas de Vagas</a></li>
                                        <li><a href="{{ URL::Route( 'admin.concursos.index' ) }}">Concursos</a></li>
                                    </ul>
                                </li>
                                <!-- /Bolsa de Emprego -->

                                 <!-- Currículos -->
                                <li>
                                    <a>
                                        <i class="fa fa-briefcase fa-lg" aria-hidden="true"></i> Currículos
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ URL::Route( 'admin.linguas.index' ) }}">Línguas</a></li>

                                    </ul>
                                </li>
                                <!-- /Currículos -->

                                <!-- PAF Descontos -->
                                <li>
                                    <a>
                                        <i class="fa fa-percent fa-lg" aria-hidden="true"></i> PAF Descontos
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ URL::Route( 'admin.categorias.index' ) }}">Categorias</a></li>
                                        <li><a href="{{ URL::Route( 'admin.parceiros.index' ) }}">Parceiros</a></li>
                                        <li><a href="{{ URL::Route( 'admin.descontos.index' ) }}">Descontos</a></li>
                                    </ul>
                                </li>
                                <!-- /PAF Descontos -->

                                <!-- PAF Regional -->
                                <li>
                                    <a>
                                        <i class="fa fa-location-arrow fa-lg" aria-hidden="true"></i> PAF Regional
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="#">Cidades</a></li>
                                        <li><a href="#">Seccionais</a></li>
                                    </ul>
                                </li>
                                <!-- /PAF Regional -->

                                <!-- Artigos -->
                                <li>
                                    <a>
                                        <i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i> Artigos
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ URL::Route( 'admin.temas.index' ) }}"> Temas </a></li>
                                        <li><a href="{{ URL::Route( 'admin.artigos.index' ) }}"> Artigos </a></li>
                                    </ul>
                                </li>
                                <!-- /Artigos -->
                                 <!-- Artigos -->
                                <li>
                                    <a>
                                        <i class="fa fa-address-card-o fa-lg" aria-hidden="true"></i> Peritos
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ URL::Route( 'admin.peritos.index' ) }}"> Peritos </a></li>
                                        </li>
                                    </ul>
                                </li>
                                <!-- /Artigos -->
                            </ul>
                        </div>
                        <!-- /Geral -->

                        <!-- Administrativo -->
                        <div class="menu_section">

                            <h3>Administrativo</h3>

                            <ul class="nav flex-column side-menu">
                                <!-- Gerenciar -->
                                <li>
                                    <a>
                                        <i class="fa fa-sliders fa-lg" aria-hidden="true"></i> Gerenciar
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ URL::Route( 'admin.usuarios.index' ) }}"> Usuários </a></li>
                                    </ul>
                                </li>
                                <!-- /Gerenciar -->
                            </ul>
                        </div>
                        <!-- Administrativo -->
                    </div>
                    <!-- /sidebar menu -->

                    <!-- Menu Footer -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Informações">
                            <i class="fa fa-info fa-2x" aria-hidden="true"></i>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Gráficos">
                            <i class="fa fa-area-chart fa-2x" aria-hidden="true"></i>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Configurações">
                            <i class="fa fa-cogs fa-2x" aria-hidden="true"></i>
                        </a>
                        {{ Form::open([ 'id' => 'logout-form', 'method' => 'Post','route' => 'logout' ]) }}
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" title="Logout">
                                <i class="fa fa-power-off fa-2x" aria-hidden="true"></i>
                            </a>
                        {{ Form::close() }}
                    </div>
                    <!-- /Menu Footer -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu p-1">
                    <nav>
                        <div class="row">
                            <div class="col-2">
                                <!-- toggle -->
                                <div class="nav toggle hidden-md-up">
                                    <div id="menu_toggle" class="hamburger hamburger-l-arrow">
                                        <span>toggle menu</span>
                                    </div>
                                </div>
                                <!-- /toggle -->
                            </div>

                            <div class="col-10">





                                <ul class="nav justify-content-end">
                                    <li>
                                        <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            {{ Auth::user() -> TT001CC001 }}
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="dropdown-item">
                                                <a href="#"> Profile</a>
                                            </li>
                                            <li class="dropdown-item">
                                                <a href="#">
                                                    <span>Settings</span>
                                                </a>
                                            </li>
                                            <li class="dropdown-item">
                                                <a href="#">Help</a>
                                            </li>
                                            <li class="dropdown-item">
                                                <a href="#">Log Out</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->

            <!-- Content -->
            <div class="right_col" role="main">
                @yield('conteudo')
            </div>
            <!-- /Content -->

            <!-- Footer -->
            <footer class="navbar footer-fixed bg-paf"></footer>
        </div>
    </div>
</body>

</html>
