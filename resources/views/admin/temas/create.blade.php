<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Cadastro de Temas | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Cadastro dos Temas</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de erro -->

<div class="card">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Tema dos Artigos</span></div>
        </div>

        {{ Form::open(['route' => 'admin.temas.store','method'=>'POST']) }}

        <div class="row">
            <!-- Cor -->
            <div class="col-2">
                <div class="form-group @if ( $errors -> has( 'TT004CC001' )) has-danger @endif">
                    {{ Form::label ( 'TT004CC001', 'Cor *', ['class' => 'form-control-label' ]) }}

                    {{ Form::color
                        (
                            'TT004CC001',
                            null,

                            [
                                'value' => '#202020',
                                'class' => 'form-control form-control-lg',
                                'maxlength' => '7'
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT004CC001'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT004CC001') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Cor -->

            <!-- Tema -->
            <div class="col-10">
                <div class="form-group @if ( $errors -> has( 'TT004CC002' )) has-danger @endif">
                    {{ Form::label( 'TT004CC002', 'Tema *', ['class' => 'form-control-label' ]) }}
                    {{ Form::text
                        (
                            'TT004CC002',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT004CC002'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT004CC002') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Tema -->
        </div>

        <div class="row">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
            </div>
        </div>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>

            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-1" href="{{ URL::Route( 'admin.temas.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->

            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
    {{ Form::close() }}
</div>

@stop
