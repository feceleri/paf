<!-- Temas -->
<div class="card my-std">
    <div class="card-block col-12 col-sm-6" >
        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Temas dos Artigos</span></div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th> Código </th>
                    <th> Cor </th>
                    <th> Tema </th>
                    <th> </th>
                </tr>
                @foreach ( $temas as $tema )
                <tr>
                    <td>{{ $tema -> TT004CC000 }}</td>
                    <td class="text-center">
                        <i class="fa fa-circle fa-3x" aria-hidden="true" style="color: {{ $tema -> TT004CC001 }};"></i>
                    </td>
                    <td>{{ $tema -> TT004CC002 }}</td>
                    <td>
                        <!-- Controle -->
                        <div class="row">
                            <!-- Editar -->
                            <div class="col-12 col-sm-6">
                                <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.temas.edit', $tema -> TT004CC000 )}}">
                                    <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                </a>
                            </div>
                            <!-- Editar -->

                            <!-- Deletar -->
                            <div class="col-12 col-sm-6">
                                {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.temas.destroy', $tema -> TT004CC000 ]]) }}
                                {{ Form::button
                                    (
                                        '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>',

                                        [
                                            'class' => 'btn btn-block btn-admin',
                                            'type' => 'submit'
                                        ]
                                    )
                                }}
                                {{ Form::close() }}
                            </div>
                            <!-- Deletar -->
                        </div>
                        <!-- /Controle -->
                    </td>

                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<!-- /Temas -->

<!-- Paginação -->
{{ $temas -> links( 'partials.pagination' ) }}
