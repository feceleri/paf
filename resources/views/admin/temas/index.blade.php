<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Temas | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Novo -->
        <div class="float-left">
            <a class="btn btn-admin" href="{{ URL::Route( 'admin.temas.create' ) }}">
                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </div>
<!-- /Novo -->
<!-- Título -->
<h1 class="text-center"> Temas </h1>



<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->

<!-- Temas -->
<section class="load-ajax">
    @include('admin::temas.load')
</section>
<!-- /Temas -->

@stop
