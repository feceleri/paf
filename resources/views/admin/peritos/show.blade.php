<!-- layout Master -->@extends('admin::app')
<!-- Título -->@section('titulo') Peritos | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Título -->
<h2 class="m-3">Peritos</h2>
<!-- /Título -->
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Mensagens de Alerta -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Mensagens de Alerta -->
<div class="row">
    <div class="col-6">
        <div class="card my-std">
            <div class="card-block text-left">

                <!-- Title Peritos -->
                <div class="title clearfix">
                    <div class="title-form"> {{ $perito -> TT030CC001 }} </div>
                </div>
                <!-- /Title Peritos -->
                <p class="card-text">
                    <table class="table table-bordered">
                        <tr>
                            <td> Cidade </td>
                            <td> {{ $perito -> cidade -> TT008CC003 }} - {{ $perito -> cidade -> TT008CC001 }} </td>
                        </tr>
                        <tr>
                            <td> CRF </td>
                            <td> {{ $perito -> TT030CC007 }} </td>
                        </tr>
                        <tr>
                            <td> CPF </td>
                            <td> {{ $perito -> TT030CC003 }} </td>
                        </tr>
                        <tr>
                            <td> RG </td>
                            <td> {{ $perito -> TT030CC004 }} </td>
                        </tr>
                        <tr>
                            <td> Nacionalidade </td>
                            <td> {{ $perito -> TT030CC005 }} </td>
                        </tr>
                        <tr>
                            <td> Estado Civil </td>
                            <td> {{ $perito -> TT030CC006 }} </td>
                        </tr>
                        <tr>
                            <td>Endereço </td>
                            <td> {{ $perito -> TT030CC008 }} </td>
                        </tr>
                        <tr>
                            <td> Bairro </td>
                            <td> {{ $perito -> TT030CC009 }} </td>
                        </tr>
                        <tr>
                            <td> CEP </td>
                            <td> {{ $perito -> TT030CC010 }} </td>
                        </tr>
                        <tr>
                            <td> E-mail </td>
                            <td> {{ $perito -> TT030CC011 }} </td>
                        </tr>
                        <tr>
                            <td> Telefone </td>
                            <td> {{ $perito -> TT030CC012 }} </td>
                        </tr>
                        <tr>
                            <td> Celular </td>
                            <td> {{ $perito -> TT030CC013 }} </td>
                        </tr>
                    </table>
                </p>

                <p class="card-text"> </p>
                <hr>
                <!-- Controle -->
                <div class="row">
                    <!-- Voltar -->

                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.peritos.index' ) }}">
                            <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>

                    <!-- /Voltar -->
                    <!-- Novo -->

                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.peritos.create' ) }}">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>

                    <!-- /Novo -->
                    <!-- Editar -->

                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.peritos.edit', $perito -> TT030CC000 )}}">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>

                    <!-- Editar -->
                    <!-- Deletar -->

                    <div class="col-12 col-sm-3">
                        {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.peritos.destroy', $perito -> TT030CC000 ]]) }}
                        {{ Form::button
                            (
                                '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                        {{ Form::close() }}
                    </div>

                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
    <div class="col-5">
        <div class="card my-std">
            <div class="card-block text-left">  <p class="card-text">
                   <br><br>
                    <table class="table table-bordered">
                        <tr>
                            <td> Especialização 1 </td>
                            <td> {{ $perito -> TT030CC015 }} </td>
                        </tr>
                        <tr>
                            <td> Instituição 1 </td>
                            <td> {{ $perito -> TT030CC016 }} </td>
                        </tr>
                        <tr>
                            <td> Carga horária </td>
                            <td> {{ $perito -> TT030CC017 }} </td>
                        </tr>
                        <tr>
                            <td> Ano de conclusão </td>
                            <td> {{ $perito -> TT030CC018 }} </td>
                        </tr>
                    </table>
                </p>
                <p class="card-text">
                    <table class="table table-bordered">
                        <tr>
                            <td> Especialização 2 </td>
                            <td> {{ $perito -> TT030CC019 }} </td>
                        </tr>
                        <tr>
                            <td> Instituição 2 </td>
                            <td> {{ $perito -> TT030CC020 }} </td>
                        </tr>
                        <tr>
                            <td> Carga horária </td>
                            <td> {{ $perito -> TT030CC021}} </td>
                        </tr>
                        <tr>
                            <td> Ano de conclusão </td>
                            <td> {{ $perito -> TT030CC022 }} </td>
                        </tr>
                    </table>
                </p>
                <p class="card-text">
                    <table class="table table-bordered">
                        <tr>
                            <td> Especialização 3 </td>
                            <td> {{ $perito -> TT030CC023 }} </td>
                        </tr>
                        <tr>
                            <td> Instituição 3 </td>
                            <td> {{ $perito -> TT030CC024 }} </td>
                        </tr>
                        <tr>
                            <td> Carga horária </td>
                            <td> {{ $perito -> TT030CC025 }} </td>
                        </tr>
                        <tr>
                            <td> Ano de conclusão </td>
                            <td> {{ $perito -> TT030CC026 }} </td>
                        </tr>
                    </table>
                </p>



                   <p class="card-text">
                    <table class="table table-bordered">
                        <tr>
                            <td> Descrição de atividades<br><br> {{ $perito -> TT030CC027 }} </td>
                        </tr>

                    </table>
                </p>
                     </div>
        </div>
    </div>@stop
