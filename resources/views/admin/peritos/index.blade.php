<!-- layout Master -->@extends('admin::app')
<!-- Título -->@section('titulo') Cadastro de Peritos | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Novo -->
<div class="float-left">
            <a class="btn btn-admin" href="{{ URL::Route( 'admin.peritos.create' ) }}">
                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </div>
<!-- /Novo -->
<!-- Título -->
<h1 class="text-center">Peritos</h1>
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="row">
            <div class="col-12 col-md-12"> {!! Form::open(['method'=>'GET','url'=>'admin/peritos','class'=>'filter-form navbar-left','role'=>'search']) !!}
                <div class="col-12 col-md-12">
                    <div class="row">

                        <div class="col-12 col-md-2">CRF
                            <input type="text" class="form-control" name="crfPerito" value="{{$crfPerito}}" placeholder="..."> </div>
                             <div class="col-12 col-md-4">Nome
                            <input type="text" class="form-control" name="nomePerito" value="{{$nomePerito}}" placeholder="..."> </div>
                        <div class="col-12 col-md-2"> Cidade
                            <select class="form-control" name="cidadePerito">
                                <option value="">Todas</option> @foreach( $cidades as $cidade )
                                <option value="{{ $cidade -> TT008CC000 }}" @if ($cidade -> TT008CC000 == $cidadePerito) selected @endif>{{ $cidade -> TT008CC003 }}  </option> @endforeach </select>
                        </div>
                        <div class="col-12 col-md-1">
                            <br> <span class="input-group-btn ">
                            <button class="btn btn-block btn-admin mb-1" type="submit">
                            <i class="fa fa-search fa-2x"></i>
                            </button>
                            </span> </div>
                        <div class="col-12 col-md-1">
                            <br>
                            <a href="/admin/peritos"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
                        </div>
                    </div> {!! Form::close() !!} </div>
            </div>
        </div>
    </div>
</div>
<!-- Buscas -->
<!-- Peritos -->
<section class="load-ajax"> @include('admin::peritos.load') </section>
<!-- /Peritos -->@stop
