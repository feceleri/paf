<!-- layout Master -->
@extends('admin::app')



<!-- Título -->
@section('titulo') Admin: Peritos @stop

<!-- Conteúdo -->
@section('conteudo')



<!-- Título -->
<h2 class="m-3">Cadastro de Peritos</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de erro -->

{{ Form::open(['route' => 'admin.peritos.store','method'=>'POST']) }}

<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <!-- Nome -->
                    <div class="col-12 col-lg-6">
                        <div class="form-group @if ( $errors -> has( 'TT030CC001' )) has-danger @endif"> {{ Form::label( 'TT030CC001', 'Nome *', ['class' => 'form-control-label' ]) }} {{ Form::text ( 'TT030CC001', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC001'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC001') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Nome -->
                    <!-- UF -->
                    <div class="col-4 col-sm-3 col-md-2 col-lg-1">
                        <div class="form-group"> {{ Form::label('uf', 'UF *', ['class' => 'form-control-label']) }} {{ Form::select ( 'uf', [''], null, [ 'id' => 'uf', 'class' => 'form-control', 'default' => old('uf', 'SP') ] ) }} </div>
                    </div>
                    <!-- /UF -->
                    <!-- Cidade -->
                    <div class="col-8  col-sm-9 col-md-7 col-lg-5">
                        <div class="form-group @if ( $errors -> has( 'TT030CC002' )) has-danger @endif"> {{ Form::label('TT030CC002', 'Cidade *', ['class' => 'form-control-label']) }} {{ Form::select ( 'TT030CC002', [''], null, [ 'id' => 'cidade', 'class' => 'form-control', 'default' => old('TT030CC002') ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC002'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC002') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Cidade -->
                    <!-- CPF -->
                    <div class="col-12 col-md-4 col-lg-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC003' )) has-danger @endif"> {{ Form::label('TT030CC003', 'CPF *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC003', null, [ 'maxlength' => '15', 'class' => 'form-control cpf', 'placeholder' => '000.000.000-00' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC003'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC003') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /CPF -->
                    <!-- RG -->
                    <div class="col-12 col-md-4 col-lg-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC004' )) has-danger @endif"> {{ Form::label('TT030CC004', 'RG *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC004', null, [ 'maxlength' => '11', 'class' => 'form-control rg', 'placeholder' => '000.000.000-0' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC004'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC004') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /RG -->
                    <!-- Nacionalidade -->
                    <div class="col-12 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT030CC005' )) has-danger @endif"> {{ Form::label( 'TT030CC005', 'Nacionalidade *', ['class' => 'form-control-label' ]) }} {{ Form::text ( 'TT030CC005', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC005'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC005') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Nacionalidade -->
                    <!-- Estado civil -->
                    <div class="col-12 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT030CC006' )) has-danger @endif"> {{ Form::label( 'TT030CC006', 'Estado civil *', ['class' => 'form-control-label' ]) }} {{ Form::select ( 'TT030CC006', [ 'Solteiro' => 'Solteiro', 'Casado' => 'Casado', 'Separado' => 'Separado', 'Divorciado' => 'Divorciado', 'Viúvo' => 'Viúvo', ], null, [ 'class' => 'form-control', 'placeholder' => 'Selecione ' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC006'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC006') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Estado civil -->
                    <!-- CRF -->
                    <div class="col-5 col-md-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC007' )) has-danger @endif"> {{ Form::label('TT030CC007', 'CRF *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC007', null, [ 'class' => 'form-control', 'maxlength' => '7' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC007'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC007') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /CRF -->
                    <!-- Endereço -->
                    <div class="col-12 col-md-6">
                        <div class="form-group @if ( $errors -> has( 'TT030CC008' )) has-danger @endif"> {{ Form::label('TT030CC008', 'Endereço completo *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC008', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC008'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC008') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Endereço -->
                    <!-- Bairro -->
                    <div class="col-12 col-md-4">
                        <div class="form-group @if ( $errors -> has( 'TT030CC009' )) has-danger @endif"> {{ Form::label('TT030CC009', 'Bairro *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC009', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC009'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC009') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Bairro -->
                    <!-- CEP -->
                    <div class="col-12 col-md-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC010' )) has-danger @endif"> {{ Form::label('TT030CC010', 'CEP *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC010', null, [ 'maxlength' => '9', 'class' => 'form-control cep', 'placeholder' => '00000-000' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC010'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC010') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /CEP -->
                    <!-- E-mail -->
                    <div class="col-12 col-md-6">
                        <div class="form-group @if ( $errors -> has( 'TT030CC011' )) has-danger @endif"> {{ Form::label('TT030CC011', 'E-mail *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC011', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC011'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC011') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /E-mail -->
                    <!-- Telefone -->
                    <div class="col-12 col-md-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC012' )) has-danger @endif"> {{ Form::label('TT030CC012', 'Telefone', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC012', null, [ 'maxlength' => '16', 'class' => 'form-control tel', 'placeholder' => '(00) 0000-0000' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC012'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC012') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Telefone -->
                    <!-- Celular -->
                    <div class="col-12 col-md-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC013' )) has-danger @endif"> {{ Form::label('TT030CC013', 'Celular *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC013', null, [ 'maxlength' => '16', 'class' => 'form-control tel', 'placeholder' => '(00) 00000-0000' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC013'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC013') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Celular -->
                </div>
                <div class="row"></div>
                <!-- Curso 1 -->
                  <div class="main-title-outer clearfix"><br> </div>
                <div class="row">
                    <!-- Curso -->
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT030CC015' )) has-danger @endif"> {{ Form::label('TT030CC015', 'Título especialização *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC015', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC015'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC015') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Curso -->
                    <!-- Instituição -->
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT030CC016' )) has-danger @endif"> {{ Form::label('TT030CC016', 'Instituição *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC016', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC016'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC016') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Instituição -->
                     <!-- CARGA -->
                    <div class="col-5 col-md-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC017' )) has-danger @endif"> {{ Form::label('TT030CC017', 'Carga *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC017', null, [ 'class' => 'form-control', 'maxlength' => '30' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC017'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC017') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /CARGA -->
                    <!-- ANO -->
                    <div class="col-12 col-md-4 col-lg-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC018' )) has-danger @endif"> {{ Form::label('TT030CC018', 'Ano de conclusão *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC018', null, [ 'maxlength' => '4', 'class' => 'form-control ', 'placeholder' => '0000' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC018'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC018') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /ANO -->
                </div>
                 <!-- /Curso 1 -->
                    <!-- Curso 2 -->
                  <div class="main-title-outer clearfix"><br> </div>
                <div class="row">
                    <!-- Curso -->
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT030CC019' )) has-danger @endif"> {{ Form::label('TT030CC019', 'Título especialização 2', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC019', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC019'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC019') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Curso -->
                    <!-- Instituição -->
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT030CC020' )) has-danger @endif"> {{ Form::label('TT030CC020', 'Instituição ', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC020', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC020'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC020') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Instituição -->
                     <!-- CARGA -->
                    <div class="col-5 col-md-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC021' )) has-danger @endif"> {{ Form::label('TT030CC021', 'Carga ', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC021', null, [ 'class' => 'form-control', 'maxlength' => '30' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC021'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC021') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /CARGA -->
                    <!-- ANO -->
                    <div class="col-12 col-md-4 col-lg-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC022' )) has-danger @endif"> {{ Form::label('TT030CC022', 'Ano de conclusão ', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC022', null, [ 'maxlength' => '4', 'class' => 'form-control ' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC022'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC022') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /ANO -->
                </div>
                 <!-- /Curso 2 -->
                      <!-- Curso 3 -->
                  <div class="main-title-outer clearfix"><br> </div>
                <div class="row">
                    <!-- Curso -->
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT030CC023' )) has-danger @endif"> {{ Form::label('TT030CC023', 'Título especialização 3', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC023', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC023'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC023') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Curso -->
                    <!-- Instituição -->
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="form-group @if ( $errors -> has( 'TT030CC024' )) has-danger @endif"> {{ Form::label('TT030CC024', 'Instituição', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC024', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC024'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC024') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /Instituição -->
                     <!-- CARGA -->
                    <div class="col-5 col-md-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC025' )) has-danger @endif"> {{ Form::label('TT030CC025', 'Carga', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC025', null, [ 'class' => 'form-control', 'maxlength' => '30' ] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC025'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC025') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /CARGA -->
                    <!-- ANO -->
                    <div class="col-12 col-md-4 col-lg-2">
                        <div class="form-group @if ( $errors -> has( 'TT030CC026' )) has-danger @endif"> {{ Form::label('TT030CC026', 'Ano de conclusão', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT030CC026', null, [ 'maxlength' => '4', 'class' => 'form-control '] ) }}
                            <!-- Erro -->@if ($errors->has('TT030CC026'))
                            <div class="form-control-feedback"> {{ $errors->first('TT030CC026') }} </div> @endif
                            <!-- /Erro -->
                        </div>
                    </div>
                    <!-- /ANO -->
                </div>
                 <!-- /Curso 3 -->
                  <!-- atividades -->
                <div class="row">
                    <div class="col-12">

                        <div class="form-group @if ( $errors -> has( 'TT030CC027' )) has-danger @endif">
                            {{ Form::label('TT030CC027', 'Descrição de Atividades ', ['class' => 'form-control-label']) }}
                            {{ Form::textArea
                                (
                                    'TT030CC027',
                                    null,

                                    [
                                        'rows' => '6',
                                        'class' => 'form-control'
                                    ]
                                )
                            }}
                            <!-- Erro -->
                            @if ($errors->has('TT030CC027'))
                            <div class="form-control-feedback">
                                {{ $errors->first('TT030CC027') }}
                            </div>
                            @endif
                            <!-- /Erro -->
                        </div>



                    </div>
                </div>
                <!-- /atividades -->
                <div class="row">
                    <div class="col-12">
                        <p class="card-text text-uppercase"><strong> (*) Campos obrigatórios</strong></p>
                    </div>
                </div>
            </div>
            <!-- Enviar Mensagem -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-block">
                            <script src="https://www.google.com/recaptcha/api.js" type="text/javascript"></script>
                            <script type="text/javascript">
                                function btnClick() {
                                    return true;
                                }


                            </script>

                            <div class="row">
                                <div class="col-12 col-md-2"></div>
                                <div class="col-12 col-md-8"> <br>{{ Form::button ( '
                                    <h5 class="m-0 text"><strong>Enviar Mensagem</strong></h5>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit', 'onclick' => 'btnClick()' ] ) }} </div>
                                <div class="col-12 col-md-2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Enviar Mensagem -->
        </div>
    </div>
</div>
</div>
{{ Form::close() }}

@stop
