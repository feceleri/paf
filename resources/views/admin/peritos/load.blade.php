<!-- Categorias -->
<div class="card my-std">
    <div class="card-block">
        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Peritos</span></div>
             <div class="title-form" style="float:right; width:10px">

            </div>
            <div class="title-form" style="float:right">
                   <a class="btn btn-block btn-admin " href="peritos/excel/csv"> <i class="fa fa-download fa-1x" aria-hidden="true"> Baixar CSV</i> </a>
            </div>
              <div class="title-form" style="float:right; width:10px">

            </div>
            <div class="title-form" style="float:right">
                   <a class="btn btn-block btn-admin " href="peritos/downloadExcel/xls"> <i class="fa fa-download fa-1x" aria-hidden="true"> Baixar XLS</i> </a>
            </div>

        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>

                    <th> CRF </th>
                    <th> Nome </th>
                    <th> Cidade/Estado </th>
                    <th> </th>
                </tr>
                <!-- peritos -->@foreach( $peritos->sortBy('TT030CC001') as $perito )
                <tr>

                    <td>{{ $perito -> TT030CC007 }}</td>
                    <td>{{ $perito -> TT030CC001 }}</td>
                    <td>{{ $perito -> cidade -> TT008CC003 }} - {{ $perito -> cidade -> TT008CC001 }}</td>
                    <td>
                        <!-- Controle -->
                        <div class="row">
                            <!-- enviar email -->
                            <div class="col-12 col-sm-2">
                                <a class="btn btn-block btn-admin mb-1" href="mailto:{{  $perito -> TT030CC011 }}"> <i class="fa fa-envelope fa-1x" aria-hidden="true"></i> </a>
                            </div>
                            <!-- enviar email -->
                            <!-- Visualizar -->
                            <div class="col-12 col-sm-2">
                                <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.peritos.show', $perito -> TT030CC000 )}}"> <i class="fa fa-expand fa-1x" aria-hidden="true"></i> </a>
                            </div>
                            <!-- Visualizar -->
                            <!-- Editar -->
                            <div class="col-12 col-sm-2">
                                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.peritos.edit', $perito -> TT030CC000 )}}"> <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i> </a>
                            </div>
                            <!-- Editar -->
                            <!-- Deletar -->
                            <div class="col-12 col-sm-2"> {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.peritos.destroy', $perito -> TT030CC000 ]]) }} {{ Form::button ( '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} {{ Form::close() }} </div>
                            <!-- Deletar -->
                        </div>
                        <!-- /Controle -->
                    </td>
                </tr> @endforeach </table>
        </div>
    </div>
</div>
<!-- /peritos -->
<!-- Paginação -->{{ $peritos -> links( 'partials.pagination' ) }}
