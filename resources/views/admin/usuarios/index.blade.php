<!-- layout Master -->@extends('admin::app')
<!-- Título -->@section('titulo') Cadastro de Usuários | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Novo -->
<div class="float-left">
    <a class="btn btn-admin" href="{{ URL::Route( 'admin.usuarios.create' ) }}"> <i class="fa fa-plus fa-2x" aria-hidden="true"></i> </a>
</div>
<!-- /Novo -->
<!-- Título -->
<h1 class="text-center">Usuários</h1>
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="row">
            <div class="col-12 col-md-12"> {!! Form::open(['method'=>'GET','url'=>'admin/usuarios','class'=>'filter-form navbar-left','role'=>'search']) !!}
                <div class="col-12 col-md-12">
                    <div class="row">
                        <div class="col-12 col-md-4">Nome do Usuário
                            <input type="text" class="form-control" name="nomeUsuario" value="{{$nomeUsuario}}" placeholder="..."> </div>
                          <div class="col-12 col-md-2">Login ou CRF do Usuário
                            <input type="text" class="form-control" name="loginUsuario" value="{{$loginUsuario}}" placeholder="..."> </div>
                            <div class="col-12 col-md-2">CPF ou CNPJ do Usuário
                            <input type="text" class="form-control" name="docUsuario" value="{{$docUsuario}}" placeholder="..."> </div>
                             <div class="col-12 col-md-2">E-Mail do Usuário
                            <input type="text" class="form-control" name="emailUsuario" value="{{$emailUsuario}}" placeholder="..."> </div>
                        <div class="col-12 col-md-2"> Tipo
                            <select class="form-control" name="tipoUsuario">
                                <option value="">Todos</option>
                                <option value="1" @if ( $tipoUsuario=="1" ) selected @endif>Admin</option>
                                <option value="2" @if ( $tipoUsuario=="2" ) selected @endif>Farmacêutico</option>
                                <option value="3" @if ( $tipoUsuario=="3" ) selected @endif>Empresa</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-2"> Status
                            <select class="form-control" name="statusUsuario">
                                <option value="">Todos</option>
                                <option value="1" @if ( $statusUsuario=="1" ) selected @endif>Ativo</option>
                                <option value="0" @if ( $statusUsuario=="0" ) selected @endif>Inativo</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-1">
                            <br> <span class="input-group-btn ">
                            <button class="btn btn-block btn-admin mb-1" type="submit">
                            <i class="fa fa-search fa-2x"></i>
                            </button>
                            </span> </div>
                        <div class="col-12 col-md-1">
                            <br>
                            <a href="/admin/usuarios"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
                        </div>
                    </div> {!! Form::close() !!} </div>
            </div>
        </div>
    </div>
</div>
<!-- Buscas -->
<!-- Peritos -->
<section class="load-ajax"> @include('admin::usuarios.load') </section>
<!-- /Peritos -->@stop
