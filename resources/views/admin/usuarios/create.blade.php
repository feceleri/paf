<!-- layout Master -->@extends('admin::app') @section('script')
<script asinc src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script> @stop
<!-- Título -->@section('titulo') Criação de Usuarios | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Título -->
<h2 class="m-3">Criação de Usuários</h2>
<!-- /Título -->


<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de erro -->





{{ Form::open(['route' => 'admin.usuarios.store','method'=>'POST']) }}
<div class="card my-std">
    <div class="card-block">
        <div class="row">
            <!-- Nome -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT001CC001' )) has-danger @endif"> {{ Form::label( 'TT001CC001', 'Nome *', ['class' => 'form-control-label' ]) }} {{ Form::text ( 'TT001CC001', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                    <!-- Erro -->@if ($errors->has('TT001CC001'))
                    <div class="form-control-feedback"> {{ $errors->first('TT001CC001') }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Nome -->
           
            <!-- CPF ou CNPJ -->
            <div class="col-12 col-md-4 col-lg-2">
                <div class="form-group @if ( $errors -> has( 'TT001CC006' )) has-danger @endif"> {{ Form::label('TT001CC006', 'CPF ou CNPJ *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT001CC006', null, [ 'maxlength' => '15', 'class' => 'form-control' ] ) }}
                    <!-- Erro -->@if ($errors->has('TT001CC006'))
                    <div class="form-control-feedback"> {{ $errors->first('TT001CC006') }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /CPF ou CNPJ -->
          
           
           
            <!-- CRF -->
            <div class="col-5 col-md-2">
                <div class="form-group @if ( $errors -> has( 'TT001CC002' )) has-danger @endif"> {{ Form::label('TT001CC002', 'CRF/Login *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT001CC002', null, [ 'class' => 'form-control', 'maxlength' => '7' ] ) }}
                    <!-- Erro -->@if ($errors->has('TT001CC002'))
                    <div class="form-control-feedback"> {{ $errors->first('TT001CC002') }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /CRF -->
           
          
          
            <!-- E-mail -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT001CC004' )) has-danger @endif"> {{ Form::label('TT001CC004', 'E-mail *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT001CC004', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                    <!-- Erro -->@if ($errors->has('TT001CC004'))
                    <div class="form-control-feedback"> {{ $errors->first('TT001CC004') }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /E-mail -->
            
            <!-- senha -->
            <div class="col-5 col-md-2">
                <div class="form-group @if ( $errors -> has( 'TT001CC002' )) has-danger @endif"> {{ Form::label('TT001CC003', 'Senha *', ['class' => 'form-control-label']) }} {{ Form::text ( 'TT001CC003', null, [ 'class' => 'form-control', 'maxlength' => '12' ] ) }}
                    <!-- Erro -->@if ($errors->has('TT001CC003'))
                    <div class="form-control-feedback"> {{ $errors->first('TT001CC003') }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /senha -->
            
            
           <!-- Tipo de usuario -->
            <div class="col-12 col-md-2">
                <div class="form-group @if ( $errors -> has( 'role_id' )) has-danger @endif">
                    {{ Form::label('role_id', 'Tipo de Vaga *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'role_id',

                            [
                                '1' => 'Administrador',
                                '2' => 'Farmacêutico',
                                '3' => 'Empresa'
                            ],

                            '2',

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('role_id'))
                    <div class="form-control-feedback">
                        {{ $errors->first('role_id') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Tipo de usuario -->
           
           
           
        </div>
       
  
        
        <div class="row">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong> (*) Campos obrigatórios</strong></p>
            </div>
        </div>
    </div>
    <!-- Controle -->
    <div class="row">
        <div class="col-12 col-sm-3"></div>
        <!-- Voltar -->
        <div class="col-12 col-sm-3">
            <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.usuarios.index' ) }}"> <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i> </a>
        </div>
        <!-- /Voltar -->
        <!-- Confirmar -->
        <div class="col-12 col-sm-3"> {{ Form::button ( '<i class="fa fa-check fa-2x" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} </div>
        <!-- /Confirmar -->
        <div class="col-12 col-sm-3"></div>
    </div>
    <!-- /Controle -->
</div>
</div> {{ Form::close() }}
 @stop
