<!-- Categorias -->
<div class="card my-std">
    <div class="card-block">
        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Usuários</span></div>
            <div class="title-form" style="float:right; width:10px"> </div>
            <div class="title-form" style="float:right">
                <a class="btn btn-block btn-admin " href="usuarios/downloadExcel/csv"> <i class="fa fa-download fa-1x" aria-hidden="true"> Baixar CSV</i> </a>
            </div>
            <div class="title-form" style="float:right; width:10px"> </div>
            <div class="title-form" style="float:right">
                <a class="btn btn-block btn-admin " href="usuarios/downloadExcel/xls"> <i class="fa fa-download fa-1x" aria-hidden="true"> Baixar XLS</i> </a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th> Nome </th>
                    <th> CRF </th>
                    <th> E-MAIL </th>
                    <th> CPF </th>
                    <th> Tipo </th>
                    <th> </th>
                     <th> Desativar usuário </th>
                </tr>
                <!-- peritos -->@foreach( $usuarios->sortBy('TT001CC001') as $usuario )
                
                <tr>
                    <td>{{ $usuario -> TT001CC001 }}</td>
                    <td>{{ $usuario -> TT001CC002 }}</td>
                    <td>{{ $usuario -> TT001CC004 }}</td>
                    <td>{{ $usuario -> TT001CC006 }}</td>
                    <td>@if ( $usuario -> role_id == 1) Administrativo @elseif ( $usuario -> role_id == 2) Farmacêutico @elseif ( $usuario -> role_id == 3)  Empresa @endif    </td>
                    <td>
                        <!-- Controle -->
                        <div class="row">
                            <!-- enviar email -->
                            <div class="col-12 col-sm-3">
                                <a class="btn btn-block btn-admin mb-1" href="mailto:{{  $usuario -> TT001CC004  }}"> <i class="fa fa-envelope fa-1x" aria-hidden="true"></i> </a>
                            </div>
                            <!-- enviar email -->
                            <!-- Visualizar -->
                            <div class="col-12 col-sm-3">
                                <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.usuarios.show', $usuario -> TT001CC000 )}}"> <i class="fa fa-expand fa-1x" aria-hidden="true"></i> </a>
                            </div>
                            <!-- Visualizar -->
                            <!-- Editar -->
                            <div class="col-12 col-sm-3">
                                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.usuarios.edit', $usuario -> TT001CC000 )}}"> <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i> </a>
                            </div>
                            <!-- Editar -->
                            <!-- Deletar -->
                            <div class="col-12 col-sm-3"> {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.usuarios.destroy', $usuario -> TT001CC000 ]]) }} {{ Form::button ( '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} {{ Form::close() }} </div>
                            <!-- Deletar -->

                        </div>
                        <!-- /Controle -->
                    </td>
                    <td>    <div class="row">  <!-- Ativar Vaga -->
                            <div class="col-12 col-sm-12"> {{ Form::open([ 'method' => 'Post','route' => [ 'admin.usuarios.active', $usuario -> TT001CC000 ]]) }}
                              <input type="hidden" class="form-control" name="paginacaoAtual" value="{{ $usuarios->currentPage() }}">
                                <input type="hidden" class="form-control" name="urlFiltroAtual" value="">

                                <label class="switch float-left"> {{ Form::checkbox('TT001CC005', $usuario -> TT001CC005, $usuario -> TT001CC005, ['onChange' => 'this.form.submit()']) }}
                                    <div class="slider slider-BE round"></div>
                                </label> {{ Form::close() }} </div>
                            <!-- Ativar Vaga -->



                        </div>
                            </td>


                </tr> @endforeach </table>
        </div>
    </div>
</div>
<!-- /peritos -->
<!-- Paginação -->{{ $usuarios -> links( 'partials.pagination' ) }}
