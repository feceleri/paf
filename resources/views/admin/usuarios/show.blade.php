<!-- layout Master -->@extends('admin::app')
<!-- Título -->@section('titulo') Usuarios | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Título -->
<h2 class="m-3">Usuarios</h2>
<!-- /Título -->
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Mensagens de Alerta -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Mensagens de Alerta -->
<div class="row">
    <div class="col-6">
        <div class="card my-std">
            <div class="card-block text-left">
                <!-- Title Usuarios -->
                <div class="title clearfix">
                    <div class="title-form"> {{ $usuario -> TT001CC001 }} </div>
                </div>
                <!-- /Title Usuarios -->
                <p class="card-text">
                    <table class="table table-bordered">
                        <tr>
                            <td> CRF </td>
                            <td> {{ $usuario -> TT001CC002 }} </td>
                        </tr>
                        <tr>
                            <td> CPF </td>
                            <td> {{ $usuario -> TT001CC006 }} </td>
                        </tr>
                        <tr>
                            <td> E-mail </td>
                            <td> {{ $usuario -> TT001CC004 }} </td>
                        </tr>
                        <tr>
                            <td> Tipo </td>
                            <td> @if ( $usuario -> role_id == 1) Administrativo @elseif ( $usuario -> role_id == 2) Farmacêutico @elseif ( $usuario -> role_id == 3)  Empresa @endif  </td>
                        </tr>
                        <tr> @if($tipoUsuario == 'farmaceutico')
                            <td> Currículo relacionado </td>
                            <td>  <a  href="{{ URL::Route('admin.curriculos.show',  $curriculo -> TT021CC000 ) }}">  Código: {{ $curriculo -> TT021CC000 }}  </a>

                             </td> @endif @if($tipoUsuario == 'empresa')
                            <td>Vagas relacionadas</td>  <td> @foreach($vagas->sortByDesc('TT014CC019') as $vaga )
                           {{ $vaga -> TT014CC005 }}
                                    <p class="card-text">
                                    @if ( $vaga -> TT014CC019 >= $vaga -> created_at ) Publicada em:<b>
                                @if ( $vaga -> TT014CC019 >=  Carbon\Carbon::now()->subDays(30) )
                                <span style="color:MediumSeaGreen  ">
                                @else
                                <span style="color:DarkGrey ">
                                @endif
                                {{ date_format( date_create( $vaga -> TT014CC019  ),'d/m/Y' ) }}<br>
                        </span></b> @endif

                            <a  href="{{ URL::Route('admin.vagas.show',  $vaga -> TT014CC000 ) }}">  Código: {{ $vaga -> TT014CC000 }} </a>

                            <br><br></p>
                             @endforeach</td>@endif </tr>
                    </table>
                </p>
                <p class="card-text"> </p>
                <hr>
                <!-- Controle -->
                <div class="row">
                    <!-- Voltar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.usuarios.index' ) }}"> <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i> </a>
                    </div>
                    <!-- /Voltar -->
                    <!-- Novo -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.usuarios.create' ) }}"> <i class="fa fa-plus fa-2x" aria-hidden="true"></i> </a>
                    </div>
                    <!-- /Novo -->
                    <!-- Editar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.usuarios.edit', $usuario -> TT001CC000 )}}"> <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i> </a>
                    </div>
                    <!-- Editar -->
                    <!-- Deletar -->
                    <div class="col-12 col-sm-3"> {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.usuarios.destroy', $usuario -> TT001CC000 ]]) }} {{ Form::button ( '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} {{ Form::close() }} </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
    <div class="col-5"> </div>@stop
