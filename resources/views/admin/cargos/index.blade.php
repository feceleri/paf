<!-- layout Master -->@extends('admin::app')
<!-- Título -->@section('titulo') Cargos | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Novo -->
<div class="float-left">
    <a class="btn btn-admin" href="{{ URL::Route( 'admin.cargos.create' ) }}"> <i class="fa fa-plus fa-2x" aria-hidden="true"></i> </a>
</div>
<!-- /Novo -->
<!-- Título -->
<h1 class="text-center">Cargos</h1>
<!-- Mensagens de sucesso -->@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Mensagens de Alerta -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Mensagens de Alerta -->
<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="row">
            <div class="col-12 col-md-12"> {!! Form::open(['method'=>'GET','url'=>'admin/cargos','class'=>'filter-form navbar-left','role'=>'search']) !!}
                <div class="col-12 col-md-12">
                    <div class="row">
                        <div class="col-12 col-md-4">Cargo
                            <input type="text" class="form-control" name="nomeCargo" value="{{$nomeCargo}}" placeholder="..."> </div>
                        <div class="col-12 col-md-2"> Novas Vagas
                            <select class="form-control" name="statusNVaga">
                                <option value="">Todos</option>
                                <option value="1" @if ( $statusNVaga=="1" ) selected @endif>Ativo</option>
                                <option value="0" @if ( $statusNVaga=="0" ) selected @endif>Inativo</option>

                            </select>
                        </div>
                          <div class="col-12 col-md-2"> Currículos
                            <select class="form-control" name="statusCurriculo">
                                <option value="">Todos</option>
                                <option value="1" @if ( $statusCurriculo=="1" ) selected @endif>Ativo</option>
                                <option value="0" @if ( $statusCurriculo=="0" ) selected @endif>Inativo</option>

                            </select>
                        </div>
                        <div class="col-12 col-md-1">
                            <br> <span class="input-group-btn ">
                            <button class="btn btn-block btn-admin mb-1" type="submit">
                            <i class="fa fa-search fa-2x"></i>
                            </button>
                            </span> </div>
                        <div class="col-12 col-md-1">
                            <br>
                            <a href="/admin/cargos"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
                        </div>
                    </div> {!! Form::close() !!} </div>
            </div>
        </div>
    </div>
</div>
<!-- Buscas -->
<!-- Cargos -->
<section class="load-ajax"> @include('admin::cargos.load') </section>
<!-- /Cargos -->@stop
