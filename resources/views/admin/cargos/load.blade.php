<!-- Cargos -->
<div class="card mb-3">
    <div class="card-block">
        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Cargos</span></div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th> Código </th>
                    <th> Cargo </th>
                    <th></th><th>Novas vagas </th><th>Currículos</th>
                </tr> @foreach ( $cargos as $cargo )
                <!--Aqui vai capturar a URL e enviar para o input urlFiltroAtual para que seja enviado ao controler e trazido como link da página-->
                <script type="text/javascript">
                    var filtro = window.location.href;
                    if (filtro.includes("tituloVaga=")) {
                        var urlFiltro = window.location.href;
                        urlFiltro = urlFiltro.replace("http://pafnovo.crfsp.org.br/admin/vagas?", "");
                        for (i = 0; i < document.getElementsByName("urlFiltroAtual").length; i++) {
                            document.getElementsByName("urlFiltroAtual")[i].value = '&' + urlFiltro;
                        }
                    }
                    else {
                        document.getElementsByName("urlFiltroAtual").value = "";
                    }
                </script>
                <!--FIM do script de capturar URL-->
                <tr>
                    <td>{{ $cargo -> TT003CC000 }}</td>
                    <td>{{ $cargo -> TT003CC001 }}</td>
                    <td>
                        <!-- Controle -->
                        <div class="row">
                            <!-- Editar -->
                            <div class="col-12 col-sm-6">
                                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.cargos.edit', $cargo -> TT003CC000 )}}"> <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i> </a>
                            </div>
                            <!-- Editar -->
                            <!-- Deletar -->
                            <div class="col-12 col-sm-6"> {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.cargos.destroy', $cargo -> TT003CC000 ]]) }} {{ Form::button ( '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} {{ Form::close() }} </div>
                            <!-- Deletar -->

                        </div>
                        <!-- /Controle -->
                    </td>
                    <td>    <div class="row">  <!-- Ativar Vaga -->
                            <div class="col-12 col-sm-12"> {{ Form::open([ 'method' => 'Post','route' => [ 'admin.cargos.active', $cargo -> TT003CC000 ]]) }}
                                <input type="hidden" class="form-control" name="paginacaoAtual" value="{{ $cargos->currentPage() }}">
                                <input type="hidden" class="form-control" name="urlFiltroAtual" value="">
                                <label class="switch float-left"> {{ Form::checkbox('TT003CC002', $cargo -> TT003CC002, $cargo -> TT003CC002, ['onChange' => 'this.form.submit()']) }}
                                    <div class="slider slider-BE round"></div>
                                </label> {{ Form::close() }} </div>
                            <!-- Ativar Vaga -->



                        </div>
                            </td>
                                <td>    <div class="row">  <!-- Ativar Vaga -->
                            <div class="col-12 col-sm-12"> {{ Form::open([ 'method' => 'Post','route' => [ 'admin.cargos.activecv', $cargo -> TT003CC000 ]]) }}
                                <input type="hidden" class="form-control" name="paginacaoAtual" value="{{ $cargos->currentPage() }}">
                                <input type="hidden" class="form-control" name="urlFiltroAtual" value="">
                                <label class="switch float-left"> {{ Form::checkbox('TT003CC003', $cargo -> TT003CC003, $cargo -> TT003CC003, ['onChange' => 'this.form.submit()']) }}
                                    <div class="slider slider-CB round"></div>
                                </label> {{ Form::close() }} </div>
                            <!-- Ativar Vaga -->



                        </div>
                            </td>
                </tr> @endforeach </table>
        </div>
    </div>
</div>
<!-- /Cargos -->
<!-- Paginação -->{{ $cargos -> links( 'partials.pagination' ) }}
