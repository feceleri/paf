<!-- Categorias -->
<div class="card my-std">
    <div class="card-block">
        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Categorias</span></div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th> Código </th>
                    <th> Categoria </th>
                    <th> </th>
                </tr>
                @foreach ( $categorias as $categoria )
                <tr>
                    <td>{{ $categoria -> TT005CC000 }}</td>
                    <td>{{ $categoria -> TT005CC001 }}</td>
                    <td>
                        <!-- Controle -->
                        <div class="row">
                            <!-- Editar -->
                            <div class="col-12 col-sm-6">
                                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.categorias.edit', $categoria -> TT005CC000 )}}">
                                    <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                </a>
                            </div>
                            <!-- Editar -->

                            <!-- Deletar -->
                            <div class="col-12 col-sm-6">
                                {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.categorias.destroy', $categoria -> TT005CC000 ]]) }}
                                {{ Form::button
                                    (
                                        '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>',

                                        [
                                            'class' => 'btn btn-block btn-admin',
                                            'type' => 'submit'
                                        ]
                                    )
                                }}
                                {{ Form::close() }}
                            </div>
                            <!-- Deletar -->
                        </div>
                        <!-- /Controle -->
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<!-- /Categorias -->

<!-- Paginação -->
{{ $categorias -> links( 'partials.pagination' ) }}
