<!-- layout Master -->@extends('admin::app')
<!-- Script -->@section('script') @stop
<!-- Título -->@section('titulo') Curriculos | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Novo -->
<div class="float-left">
    <a class="btn btn-admin" href="{{ URL::Route( 'admin.curriculos.create' ) }}"> <i class="fa fa-plus fa-2x" aria-hidden="true"></i> </a>
</div>
<!-- /Novo -->
<!-- Título -->
<h1 class="text-center">Curriculos</h1>
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="row">
            <div class="col-12 col-md-12"> {!! Form::open(['method'=>'GET','url'=>'admin/curriculos','class'=>'filter-form navbar-left','role'=>'search']) !!}




                <div class="col-12 col-md-12">
                    <div class="row">

                 <div class="col-12 col-md-3">Nome
                           <input type="text" class="form-control" name="nomeCurriculo" value="{{$nomeCurriculo}}" placeholder="...">


                        </div>
                        <div class="col-12 col-md-2"> Cidade
                            <select class="form-control" name="cidadeCV">
                                <option value="">Todas</option> @foreach( $cidades as $cidade )
                                <option value="{{ $cidade -> TT008CC000 }}" @if ($cidadeCV==$cidade->TT008CC000) selected @endif >
                                {{ $cidade -> TT008CC003 }}
                                </option> @endforeach </select>
                        </div>

                         <div class="col-12 col-md-2"> Nível de Formação
                            <select class="form-control" name="formacaoCV">
                                <option value="">Todas</option>
<!--                                <option value="TC" @if ( $formacaoCV=="TC" ) selected @endif>Técnico</option>-->
                                <option value="BA" @if ( $formacaoCV=="BA" ) selected @endif>Bacharel</option>
                                <option value="LI" @if ( $formacaoCV=="LI" ) selected @endif>Licenciatura</option>
<!--                                <option value="TN" @if ( $formacaoCV=="TN" ) selected @endif>Tecnólogo</option>-->
                                <option value="PL" @if ( $formacaoCV=="PL" ) selected @endif>Pós Graduação Latu Sensu</option>
                                <option value="ME" @if ( $formacaoCV=="ME" ) selected @endif>Mestrado</option>
                                <option value="DO" @if ( $formacaoCV=="DO" ) selected @endif>Doutorado</option>
                                <option value="PD" @if ( $formacaoCV=="PD" ) selected @endif>Pós-Doutorado</option>
                            </select>
                        </div>


                        <div class="col-12 col-md-2"> Idiomas
                            <select class="form-control" name="idiomaCV">
                                <option value="">Todos</option> @foreach( $linguas as $lingua )
                                <option value="{{ $lingua -> TT031CC000 }}" @if ( $idiomaCV ==  $lingua->TT031CC000 ) selected @endif >{{ $lingua -> TT031CC001 }} </option> @endforeach </select>
                        </div>


                        <div class="col-12 col-md-2"> Ramo
                            <select class="form-control" name="ramoCV">
                                <option value="">Todas</option> @foreach( $ramos as $ramo )
                                <option value="{{ $ramo -> TT002CC000 }}" @if ( $ramoCV== $ramo->TT002CC000  ) selected @endif > {{ $ramo -> TT002CC001 }} </option> @endforeach </select>
                        </div>

                        <div class="col-12 col-md-2"> Cargo
                            <select class="form-control" name="cargoCV">
                                <option value="">Todas</option> @foreach( $cargos as $cargo )
                                <option value="{{ $cargo -> TT003CC000 }}" @if ( $cargoCV== $cargo->TT003CC000  ) selected @endif > {{ $cargo -> TT003CC001 }} </option> @endforeach </select>
                        </div>


                       <div class="col-12 col-md-2"> Deficiência
                            <select class="form-control" name="deficienciaCV">
                                <option value="">-</option>
                                <option value="ANY" @if ( $deficienciaCV=="ANY" ) selected @endif>Qualquer</option>
                                <option value="AUD" @if ( $deficienciaCV=="AUD" ) selected @endif>Auditiva</option>
                                <option value="INT" @if ( $deficienciaCV=="INT" ) selected @endif>Intelectual</option>
                                <option value="MOT" @if ( $deficienciaCV=="MOT" ) selected @endif>Motora</option>
                                <option value="VIS" @if ( $deficienciaCV=="VIS" ) selected @endif>Visual</option>
                            </select>
                        </div>





                        <div class="col-12 col-md-1">
                            <br> <span class="input-group-btn ">
                            <button class="btn btn-block btn-admin mb-1" type="submit">
                            <i class="fa fa-search fa-2x"></i>
                            </button> </span> </div>
                        <div class="col-12 col-md-1">
                            <br>
                            <a href="/curriculos/empresas/pesquisa"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
                        </div>
                    </div>
                </div> {!! Form::close() !!} </div></div>
            </div>
        </div>

<!-- Buscas -->
<div class="row">
    <div class="col-12">
        <!-- Curriculos -->
        <section class="load-ajax">
            <input id="hiden" name="invisible" type="hidden"> @include('admin::curriculos.load') </section>
        <!-- /Curriculos -->
    </div>
</div> @stop
