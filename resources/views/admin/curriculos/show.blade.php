<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Currículo | Admin @stop

<!--$paginaAtual = $_SERVER['REQUEST_URI'];-->

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3"></h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de Alerta -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Mensagens de Alerta -->

<div class="row">
    <div class="col-6">
        <div class="card my-std">
            <div class="card-block text-left">


<!-- NOME CRF -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto"><h5> {{ $curriculo -> usuario -> TT001CC001 }}</h5></span></div>
                </div>
                <h6>  CRF: {{ $curriculo -> usuario -> TT001CC002 }} </h6> </div>
        </div>
    </div>
</div>
<!-- /NOME CRF -->
<!-- DADOS PESSOAIS -->
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto"> <h5>DADOS PESSOAIS</h5></span></div>
                </div>
                <p> <b>Idade:</b> {{ $curriculo -> TT021CC007 }}
                    <br><b>Sexo:</b> @if ( $curriculo -> TT021CC013 == 'M' ) Masculino @elseif ( $curriculo -> TT021CC013 == 'F' ) Feminino
                    <br> <b>Estado civil:</b> {{ $curriculo -> TT021CC008 }} @endif
                    <br> <b>Nacionalidade:</b> {{ $curriculo -> TT021CC009 }}
                    <br> <b>Endereço:</b> {{ $curriculo -> TT021CC004 }} - {{ $curriculo -> cidade -> TT008CC003 }}/{{ $curriculo -> cidade -> TT008CC001 }}
                    <br> <b>Telefone:</b> {{ $curriculo -> TT021CC005 }}
                    <br><b>E-mail:</b> {{ $curriculo -> usuario -> TT001CC004 }}
                    <br><b>Linkedin:</b> {{ $curriculo -> TT021CC006 }}</p>
            </div>
        </div>
    </div>
</div>
<!-- /DADOS PESSOAIS -->
<!-- INTERESSES PROFISSIONAIS -->@if ( $teminteresse > 0 )
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto">
                        @if ( $teminteresse > 1 ) INTERESSES PROFISSIONAIS @elseif ( $teminteresse > 0 )
                         INTERESSE PROFISSIONAL @endif </span></div>
                </div> @foreach($interesses as $interesse )
                <p class="text-justify"> {{ $interesse -> cargo -> TT003CC001 }} </p> @endforeach </div>
        </div>
    </div>
</div> @endif
<!-- /INTERESSES PROFISSIONAIS -->
<!-- HISTÓRICO PROFISSIONAL -->@if ( $temexperiencia > 0 )
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto">
                        @if ( $temexperiencia > 0 ) HISTÓRICO PROFISSIONAL @endif </span></div>
                </div> @foreach($experiencias->sortBy('TT027CC007')->sortByDesc('TT025CC006') as $experiencia )
                <p class="text-justify"> <b>Empresa:</b> {{ $experiencia -> TT025CC004 }}
                    <br> <b>Cargo: </b>{{ $experiencia -> cargo -> TT003CC001 }}
                    <br> <b>Ramo: </b>{{ $experiencia -> ramo -> TT002CC001 }}
                    <br> <b>Início:</b> {{ $experiencia -> TT025CC005 }}
                    <br> @if($experiencia -> TT025CC007 == true) <b>Cargo Atual </b>@else<b>Conclusão </b>{{ $experiencia -> TT025CC006 }} @endif
                    <br> @if ( $experiencia -> TT025CC008 ==! NULL ) <b>Descrição:</b> {{ $experiencia -> TT025CC008 }}
                    <br> @endif</p> @endforeach </div>
        </div>
    </div>
</div> @endif
<!-- /HISTÓRICO PROFISSIONAL -->
<!-- FORMAÇÃO ACADÊMICA -->@if ( $temformacao > 0 )
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto">
                        @if ( $temformacao > 1 ) FORMAÇÕES ACADÊMICAS @elseif ( $temformacao > 0 )
                         FORMAÇÃO ACADÊMICA @endif </span></div>
                </div> @foreach($formacoes->sortByDesc('TT023CC007') as $formacao )
                <p class="text-justify"> <strong> @if ( $formacao -> TT023CC003  == 'TC' ) Técnico:
                                                     @elseif ( $formacao -> TT023CC003  == 'BA' ) Bacharel:
                                                     @elseif ( $formacao -> TT023CC003  == 'LI' ) Licenciatura:
                                                     @elseif ( $formacao -> TT023CC003  == 'TN' ) Tecnólogo:
                                                     @elseif ( $formacao -> TT023CC003  == 'PL' ) Pós-graduação Latu Sensu:
                                                     @elseif ( $formacao -> TT023CC003  == 'ME' ) Mestrado:
                                                     @elseif ( $formacao -> TT023CC003  == 'DO' ) Doutorado:
                                                     @elseif ( $formacao -> TT023CC003  == 'PD' ) Pós-doutorado:
                                                     @endif {{ $formacao -> TT023CC002 }}</strong>
                    <br> <b>Instituição:</b> {{ $formacao -> TT023CC004 }}
                    <br> <b>País: </b>{{ $formacao -> TT023CC005 }}
                    <br> <b>Início:</b> {{ $formacao -> TT023CC006 }}
                    <br> <b>Conclusão </b>{{ $formacao -> TT023CC007 }}
                    <br> </p> @endforeach </div>
        </div>
    </div>
</div> @endif
<!-- /FORMAÇÃO ACADÊMICA -->
<!--  CURSOS E PARTICIPAÇÕES -->@if ( $temextracurso > 0 )
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto">
                       @if ( $temextracurso > 0 )
                                               CURSOS E PARTICIPAÇÕES @endif </span></div>
                </div> @foreach($extracursos->sortByDesc('TT032CC007') as $extracurso )
                <p class="text-justify"> <strong> @if ( $extracurso -> TT032CC003  == 'CU' ) Curso:
                                                     @elseif ( $extracurso -> TT032CC003  == 'CO' ) Congresso:
                                                     @elseif ( $extracurso -> TT032CC003  == 'PA' ) Palestra: @endif </strong> {{ $extracurso -> TT032CC002 }}
                    <br><b>Instituição:</b> {{ $extracurso -> TT032CC004 }}
                    <br><b>País: </b>{{ $extracurso -> TT032CC005 }}
                    <br> <b>Início:</b> {{ $extracurso -> TT032CC006 }}
                    <br> <b>Conclusão </b>{{ $extracurso -> TT032CC007 }} </p> @endforeach </div>
        </div>
    </div>
</div> @endif
<!-- / CURSOS E PARTICIPAÇÕES -->
<!-- IDIOMAS -->@if ( $temidioma > 0 )
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto">
                       @if ( $temidioma > 1 ) IDIOMAS @elseif ( $temidioma > 0 )
                        IDIOMA @endif </span></div>
                </div>
                <p class="text-justify"> @foreach($idiomas->sortByDesc('TT026CC002') as $idioma ) <strong> {{ $idioma -> lingua -> TT031CC001 }} -  @if ( $idioma -> TT026CC006  == 'BAS' ) Básico
                                                     @elseif ( $idioma -> TT026CC006  == 'INT' ) Intermediário
                                                     @elseif ( $idioma -> TT026CC006  == 'AVA' ) Avançado
                                                     @elseif ( $idioma -> TT026CC006  == 'FLU' ) Fluente
                                                      @endif </strong> | <b>Instituição </b>{{ $idioma -> TT026CC003 }}
                    <br> @endforeach </p>
            </div>
        </div>
    </div>
</div> @endif
<!-- / IDIOMAS -->
<!-- DEFICIÊNCIAS -->
 @if ( $temdeficiencia > 0 )
<div class="row">
    <div class="col-12 my-std">
        <div class="card">
            <div class="card-block">
                <div class="main-title-outer clearfix">
                    <div class="main-title"><span class="cor-texto">
                      @if ( $temdeficiencia > 1 )
                       DEFICIÊNCIAS @elseif ( $temdeficiencia > 0 )
                       DEFICIÊNCIA @endif </span></div>
                </div>
                <p class="text-justify"> @foreach($deficiencias->sortByDesc('TT024CC002') as $deficiencia ) <b>   @if ( $deficiencia -> TT024CC002 == 'MOT' ) Motora @elseif ( $deficiencia -> TT024CC002 == 'INT' ) Intelectual @elseif ( $deficiencia -> TT024CC002 == 'AUD' ) Autitiva @elseif ( $deficiencia -> TT024CC002 == 'VIS' ) Visual @endif </b> @if ( $deficiencia -> TT024CC003 ==! NULL ) | {{ $deficiencia -> TT024CC003}} @endif
                    <br> @endforeach </p>
            </div>
        </div>
    </div>
</div>
@endif
<!-- / DEFICIÊNCIAS -->

            </div>
        </div>
    </div>
</div>

@stop
