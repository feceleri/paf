
<!-- Curriculos -->

<!--<h1 class="card-title">{{ $curriculos->currentPage() }}  </h1>-->


<div class="row"> @foreach( $curriculos as $curriculo )

<!--FIM do script de capturar URL-->
 <div class="col-12 col-lg-4 my-std">
        <div class="card curriculo">
            <div class="card-block text-left">


                     <h5>{{ $curriculo -> usuario -> TT001CC001 }}</h5>
                        <!-- Cidade -->{{ $curriculo -> cidade -> TT008CC003 }}/{{ $curriculo -> cidade -> TT008CC001 }}
                        <br>
                        <!-- /Cidade -->
                        <!-- Experiências -->@foreach( $curriculo -> experiencias->sortBy('TT027CC007')->sortByDesc('TT025CC006') as $experiencia )
                        <!-- Pegar apenas a primeira ocorrência -->@if ($loop->first) @if($experiencia -> TT025CC007 == true) Cargo Atual: @else Anterior: @endif
                        <!-- Se cargo atual ou anterior -->{{ $experiencia -> cargo -> TT003CC001 }} | {{ $experiencia -> TT025CC004 }} @endif
                        <!-- /Pegar apenas a primeira ocorrência -->@endforeach

                <!-- Controle -->
                <div class="row">
                    <!-- Visualizar -->
                    <div class="col-12 col-sm-4">
                        <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.curriculos.show', $curriculo -> TT021CC000 )}}"> <i class="fa fa-expand fa-2x" aria-hidden="true"></i> </a>
                    </div>
                    <!-- Visualizar -->
                    <!-- Editar -->
                    <div class="col-12 col-sm-4">
                        <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.curriculos.edit', $curriculo -> TT021CC000 )}}"> <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i> </a>
                    </div>
                    <!-- Editar -->
                    <!-- Deletar -->
                    <div class="col-12 col-sm-4"> {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.curriculos.destroy', $curriculo -> TT021CC000 ]]) }} {{ Form::button ( '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} {{ Form::close() }} </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div> @endforeach </div>
<!-- /Curriculos -->
<!-- Paginação -->{{ $curriculos-> links( 'partials.pagination' ) }}
