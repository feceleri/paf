<!-- layout Master -->
@extends('admin::app')

@section('script') <script asinc src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script> @stop

<!-- Título -->
@section('titulo') Admin: Vagas @stop

<!-- Conteúdo -->
@section('conteudo')



<!-- Título -->
<h2 class="m-3">Cadastro de Vagas</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de erro -->

{{ Form::open(['route' => 'admin.vagas.store','method'=>'POST']) }}

<div class="card mb-3">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Dados da Vaga</span></div>
        </div>

        <div class="row">
            <!-- Tipo de Vaga -->
            <div class="col-12 col-md-3">
                <div class="form-group @if ( $errors -> has( 'TT014CC018' )) has-danger @endif">
                    {{ Form::label('TT014CC018', 'Tipo de Vaga *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT014CC018',

                            [
                                'EM' => 'Emprego',
                                'ES' => 'Estágio'
                            ],

                            'EM',

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC018'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC018') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Tipo de Vaga -->

            <!-- Empresa -->
            <div class="col-12 col-md-5">
                <div class="form-group @if ( $errors -> has( 'TT014CC004' )) has-danger @endif">
                    {{ Form::label('TT014CC004', 'Empresa *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT014CC004',
                            $empresas,
                            null,

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC004'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC004') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Empresa -->
             <!-- Ramo de Atividade -->
            <div class="col-12 col-md-9 col-lg-4">
                <div class="form-group @if ( $errors -> has( 'TT014CC001' )) has-danger @endif">
                    {{ Form::label('TT014CC001', 'Ramo de Atividade *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT014CC001',
                            $ramos,
                            null,

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT014CC001'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC001') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Ramo de Atividade -->
        </div>

        <div class="row">
    <!-- Cargo -->
    <div class="col-12 col-md-9 col-lg-4">
        <div class="form-group @if ( $errors -> has( 'TT014CC002' )) has-danger @endif"> {{ Form::label('TT014CC002', 'Cargo *', ['class' => 'form-control-label']) }} {{ Form::select ( 'TT014CC002', $cargos, null, [ 'class' => 'form-control', 'placeholder' => 'Selecione...' ] ) }}
            <!-- Erro -->@if ($errors->has('TT014CC002'))
            <div class="form-control-feedback"> {{ $errors->first('TT014CC002') }} </div> @endif
            <!-- /Erro -->
        </div>
    </div>
    <!-- /Cargo -->
            <!-- Remuneração -->
            <div class="col-12 col-md-3 col-lg-2">
                <div class="form-group @if ( $errors -> has( 'TT014CC014' )) has-danger @endif">
                    {{ Form::label('TT014CC014', 'Remuneração *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT014CC014',
                            null,

                            [
                                'maxlength' => '9',
                                'class' => 'form-control money',
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT014CC014'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC014') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Remuneração -->
 <!-- Tipo de Remuneração -->



                <div class="col-12 col-md-2" id="tipoRemuneracaoDiv">
                    <div class="form-group @if ( $errors -> has( 'TT014CC022' )) has-danger @endif">
                    {{ Form::label('TT014CC022', 'Tipo Remuneração *', ['class' => 'form-control-label']) }}
                    {{ Form::select (
                    'TT014CC022',
                    [ 'ME' => 'Mensal', 'HO' => 'Horista' ],
                     null,
                     [
                       'id' => 'tipo-remuneracao',
                       'class' => 'form-control' ]
                     ) }}
                    <!-- Erro -->@if ($errors->has('TT014CC022'))
                    <div class="form-control-feedback"> {{ $errors->first('TT014CC022') }} </div> @endif
                    <!-- /Erro -->
                </div>
                </div>

                    <!-- /Tipo de  Remuneração -->



            <!-- Vagas -->
            <div class="col-12 col-md-3 col-lg-2">
                <div class="form-group @if ( $errors -> has( 'TT014CC013' )) has-danger @endif">
                    {{ Form::label('TT014CC013', 'Vagas *', ['class' => 'form-control-label']) }}
                    {{ Form::number
                    (
                        'TT014CC013',
                        null,

                        [
                            'class' => 'form-control',
                        ]
                    )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC013'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC013') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Vagas -->
        </div>

        <div class="row">
            <!-- Horario de Trabalho -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT014CC015' )) has-danger @endif">
                    {{ Form::label('TT014CC015', 'Horário de Trabalho', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT014CC015',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC015'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC015') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Horario de Trabalho -->

            <!-- UF -->
            <div class="col-4 col-sm-3 col-md-2">
                <div class="form-group">
                    {{ Form::label('uf', 'UF *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'uf',
                            [''],
                            null,

                            [
                                'id' => 'uf',
                                'class' => 'form-control',
                                'default' => old('uf', 'SP')
                            ]
                        )
                    }}
                </div>
            </div>
            <!-- /UF -->

            <!-- Cidade -->
            <div class="col-8 col-sm-9 col-md-4">
                <div class="form-group @if ( $errors -> has( 'TT014CC003' )) has-danger @endif">
                    {{ Form::label('TT014CC003', 'Cidade *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT014CC003',
                            [''],
                            null,

                            [
                                'id' => 'cidade',
                                'class' => 'form-control',
                                'default' => old('TT014CC003')
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC003'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC003') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Cidade -->
        </div>

        <div class="row">
            <!-- Contato -->
            <div class="col-12 col-md-8 col-lg-4">
                <div class="form-group @if ( $errors -> has( 'TT014CC007' )) has-danger @endif">
                    {{ Form::label('TT014CC007', ' Nome do Contato *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT014CC007',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC007'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC007') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Contato -->

            <!-- Telefone -->
            <div class="col-12 col-md-4 col-lg-2">
                <div class="form-group @if ( $errors -> has( 'TT014CC008' )) has-danger @endif">
                    {{ Form::label('TT014CC008', 'Telefone *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT014CC008',
                            null,

                            [
                                'maxlength' => '16',
                                'class' => 'form-control tel',
                                'placeholder' => '(00) 0000-0000'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC008'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC008') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Telefone -->

            <!-- E-mail -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT014CC006' )) has-danger @endif">
                    {{ Form::label('TT014CC006', 'E-mail *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT014CC006',
                            null,
                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC006'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC006') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /E-mail -->
            
            
        </div>

        <div class="row">
            <!-- Descrição das Atividades -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT014CC009' )) has-danger @endif">
                    {{ Form::label('TT014CC009', 'Descrição das Atividades *', ['class' => 'form-control-label']) }}
                    {{ Form::textArea
                        (
                            'TT014CC009',
                            null,

                            [
                                'rows' => '6',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC009'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC009') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Descrição das Atividades -->

            <!-- Requisitos -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT014CC010' )) has-danger @endif">
                    {{ Form::label('TT014CC010', 'Requisitos *', ['class' => 'form-control-label']) }}
                    {{ Form::textArea
                        (
                            'TT014CC010',
                            null,

                            [
                                'rows' => '6',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC010'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC010') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Requisitos -->
        </div>

        <div class="row">
            <!-- Benefícios -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT014CC011' )) has-danger @endif">
                    {{ Form::label('TT014CC011', 'Benefícios', ['class' => 'form-control-label']) }}
                    {{ Form::textArea
                        (
                            'TT014CC011',
                            null,

                            [
                                'rows' => '6',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC011'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC011') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Benefícios -->

            <!-- Como de Candidatar -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT014CC012' )) has-danger @endif">
                    {{ Form::label('TT014CC012', 'Como se Candidatar *', ['class' => 'form-control-label', 'maxlength' => '255']) }}
                    {{ Form::textArea
                        (
                            'TT014CC012',
                            null,

                            [
                                'rows' => '6',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT014CC012'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC012') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Como de Candidatar -->
        </div>

        <div class="row mb-1">
           <!-- Mostrar Endereço? -->
            <div class="col-12">
                <label class="custom-control custom-checkbox">
                    <input name="TT014CC017" type="checkbox" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    <h4>Mostrar endereço nos anúncios</h4>
                </label>
            </div>
            <!-- /Mostrar Endereço? -->

            <div class="col-12">
                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
            </div>
        </div>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>

            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-1" href="{{ URL::Route( 'admin.vagas.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->

            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
</div>

{{ Form::close() }}
<script>
    CKEDITOR.replace( 'TT014CC009' );
    CKEDITOR.replace( 'TT014CC010' );
    CKEDITOR.replace( 'TT014CC011' );
    CKEDITOR.replace( 'TT014CC012' );
</script>
@stop
