<!-- layout Master -->@extends('admin::app')
<!-- Script -->@section('script') @stop
<!-- Título -->@section('titulo') Vagas | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Novo -->
<div class="float-left">
    <a class="btn btn-admin" href="{{ URL::Route( 'admin.vagas.create' ) }}"> <i class="fa fa-plus fa-2x" aria-hidden="true"></i> </a>
</div>
<!-- /Novo -->
<!-- Título -->
<h1 class="text-center">Vagas</h1>
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="row">
            <div class="col-12 col-md-12"> {!! Form::open(['method'=>'GET','url'=>'admin/vagas','class'=>'filter-form navbar-left','role'=>'search']) !!}
                <div class="col-12 col-md-12">
                    <div class="row">
                        <div class="col-12 col-md-2">Título da Vaga
                            <input type="text" class="form-control" name="tituloVaga" value="{{$tituloVaga}}" placeholder="...">
                        </div>
                        <div class="col-12 col-md-2"> Cidade
                            <select class="form-control" name="cidadeVaga">
                                <option value="">Todas</option> @foreach( $cidades as $cidade )
                                <option value="{{ $cidade -> TT008CC000 }}" @if ($cidade -> TT008CC000 == $cidadeVaga) selected @endif>{{ $cidade -> TT008CC003 }} ({{ $cidade -> qtde }}) </option> @endforeach </select>
                        </div>
<!--
                        <div class="col-12 col-md-3"> Razão Social
                            <select class="form-control" name="empresaVaga">
                                <option value="">Todas</option> @foreach( $empresas as $empresa )
                                <option value="{{ $empresa -> TT010CC000 }}" @if ($empresa -> TT010CC000 == $empresaVaga) selected @endif>{{ $empresa -> TT010CC004 }} ({{ $empresa -> qtde }}) </option> @endforeach </select>
                        </div>
-->
                        <div class="col-12 col-md-3"> Razão Social
                           <input type="text" class="form-control" name="empresaVaga" value="{{$empresaVaga}}" placeholder="...">


                        </div>
                         <div class="col-12 col-md-3"> CNPJ
                           <input type="text" class="form-control" name="cnpjVaga" value="{{$cnpjVaga}}" placeholder="...">


                        </div>
<!--
                        <div class="col-12 col-md-2"> CNPJ
                            <select class="form-control" name="cnpjVaga">
                                <option value="">Todas</option> @foreach( $empresas->sortBy('TT010CC003') as $empresa )
                                <option value="{{ $empresa -> TT010CC000 }}" @if ($empresa -> TT010CC000 == $empresaVaga) selected @endif >{{ $empresa -> TT010CC003 }} ({{ $empresa -> qtde }}) </option> @endforeach </select>
                        </div>
-->
                        <div class="col-12 col-md-2">ID da Vaga
                            <input type="text" class="form-control" name="idVaga" value="{{$idVaga}}" placeholder="...">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-2">E-mail
                            <input type="text" class="form-control" name="emailVaga" value="{{$emailVaga}}" placeholder="...">
                        </div>
                        <div class="col-12 col-md-2 ">Data de criação
                            <input type="date" class="form-control" name="dataVaga" value="{{$dataVaga}}" placeholder="...">
                        </div>
                         <div class="col-12 col-md-2 ">Data de publicação
                            <input type="date" class="form-control" name="publicacaoVaga" value="{{$publicacaoVaga}}" placeholder="...">
                        </div>
                        <div class="col-12 col-md-2"> Ativa/Inativa
                            <select class="form-control" name="ativaVaga">
                                <option value="">Todas</option>
                                <option value="1" @if ( $ativaVaga=="1" ) selected @endif>Ativa</option>
                                <option value="0">Inativa</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-2"> Status
                            <select class="form-control" name="statusVaga">
                                <option value="">Todas</option>
                                <option value="NO" @if ( $statusVaga=="NO" ) selected @endif>Nova!</option>
                                <option value="DE" @if ( $statusVaga=="DE" ) selected @endif>Deferida</option>
                                <option value="IN" @if ( $statusVaga=="IN" ) selected @endif>Indeferida</option>
                                <option value="AN" @if ( $statusVaga=="AN" ) selected @endif>Em análise</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-1">
                            <br> <span class="input-group-btn ">
        <button class="btn btn-block btn-admin mb-1" type="submit">
            <i class="fa fa-search fa-2x"></i>
        </button>
    </span> </div>
                        <div class="col-12 col-md-1">
                            <br>
                            <a href="/admin/vagas"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
                        </div>
                    </div> {!! Form::close() !!} </div>
            </div>
        </div>
    </div>
</div>
<!-- Buscas -->
<div class="row">
    <div class="col-12">
        <!-- Vagas -->
        <section class="load-ajax">
            <input id="hiden" name="invisible" type="hidden"> @include('admin::vagas.load') </section>
        <!-- /Vagas -->
    </div>
</div> @stop
