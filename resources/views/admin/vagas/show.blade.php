<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Prévia das Vagas | Admin @stop

<!--$paginaAtual = $_SERVER['REQUEST_URI'];-->

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Prévia</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de Alerta -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Mensagens de Alerta -->

<div class="row">
    <div class="col-6">
        <div class="card my-std">
            <div class="card-block text-left">

                <!-- Ativar Vaga -->
                {{ Form::open([ 'method' => 'Post','route' => [ 'admin.vagas.active', $vaga -> TT014CC000 ]]) }}
                <label class="switch float-right">
                    {{ Form::checkbox('TT014CC016', $vaga -> TT014CC016, $vaga -> TT014CC016, ['onChange' => 'this.form.submit()']) }}
                    <div class="slider slider-BE round"></div>
                </label>
                {{ Form::close() }}
                <!-- /Ativar Vaga -->

                <!-- Title Vagas -->
                <div class="title clearfix">
                    <div class="title-form">
                        <span class="cor-empregos"> Prévia da Vaga </span>
                    </div>
                    <div class="float-right">
                  <!-- Status da Vaga -->Status da Vaga:
                     @if ( $vaga -> TT014CC020 ===  'NO' )
                     <b><span style="color:OrangeRed   "> Nova Vaga! <i class="fa fa-flag"></i></span></b>
                       @elseif ( $vaga -> TT014CC020 ===  'DE' )
                     <b><span style="color:MediumSeaGreen   ">    Deferido <i class="fa fa-check-circle"></i></span></b>
                       @elseif ( $vaga -> TT014CC020 ===  'IN' )
                     <b><span style="color:DarkGrey   "> Indeferido <i class="fa fa-ban"></i></span></b>
                       @elseif ( $vaga -> TT014CC020 ===  'AN' )
                             <b><span style="color:#1e90ff">  Em análise  <i class="fa fa-cog"></i></span></b>
                       @endif  &nbsp;&nbsp;
                    </div>
                    </div>
                     <!-- /Status da Vaga -->
                <!-- /Title Vagas -->

                <h4 class="card-text text-uppercase color-paf-primary">
                    <strong>{{ $vaga -> cargo -> TT003CC001 }}</strong>
                </h4>
                <p class="card-text"><a href="{{ route( 'admin.empresas.show', $vaga -> empresa -> TT010CC000)}}">
                    {{ $vaga -> empresa -> TT010CC004  }} ( {{ $vaga -> ramo -> TT002CC001 }} )
                    <br>CNPJ: {{ $vaga -> empresa -> TT010CC003  }} /
                    Telefone: {{ $vaga -> empresa -> TT010CC009  }}</a>
                </p>
                <p class="card-text">
                    <strong>vagas: {{ $vaga -> TT014CC013 }}</strong> |
                    {{ $vaga -> cidade -> TT008CC003 }} - {{ $vaga -> cidade -> TT008CC001 }} |
                    {{ formatDate($vaga -> TT014CC019) }}
                </p>

                <p>
                    R$ {{ formatMoney( $vaga -> TT014CC014 ) }}
                           @if ( $vaga -> TT014CC022 ===  'HO' )/ hora
                           @elseif ( $vaga -> TT014CC022 ===  'ME' )
                           @endif   | {{ $vaga -> TT014CC015 }}
                </p>

                <!-- Mostrar endereço? -->
                @if ($vaga -> TT014CC017)
                <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Endereço</strong>
                    </h6>
                    {{ $vaga -> empresa -> TT010CC007 }} - {{ $vaga -> empresa -> TT010CC008 }} - CEP: {{ formatCep($vaga -> empresa -> TT010CC006) }}
                </p>
                @endif

                <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Descrição das Atividades</strong>
                    </h6>
                    {!! $vaga -> TT014CC009 !!}
                </p>
                <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Requisitos</strong>
                    </h6>
                    {!! $vaga -> TT014CC010 !!}
                </p>
                <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Benefícios</strong>
                    </h6>
                    {!! $vaga -> TT014CC011 !!}
                </p>
                <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Como se candidatar</strong>
                    </h6>
                    {!!$vaga -> TT014CC012 !!}
                </p>
                  <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Código da Vaga:</strong>
                    </h6>
                    {!!$vaga -> TT014CC000 !!}
                </p>



                <hr>

                <!-- Controle -->
                <div class="row">
                    <!-- Voltar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.vagas.index' ) }}">
                            <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- /Voltar -->

                    <!-- Novo -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.vagas.create' ) }}">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- /Novo -->

                    <!-- Editar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.vagas.edit', $vaga -> TT014CC000 )}}">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Editar -->

                    <!-- Deletar -->
                    <div class="col-12 col-sm-3">
                        {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.vagas.destroy', $vaga -> TT014CC000 ]]) }}
                        {{ Form::button
                            (
                                '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                        {{ Form::close() }}
                    </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
</div>

@stop
