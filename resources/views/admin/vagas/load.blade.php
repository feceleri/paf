
<!-- Vagas -->

<!--<h1 class="card-title">{{ $vagas->currentPage() }}  </h1>-->


<div class="row"> @foreach( $vagas as $vaga )
<!--Aqui vai capturar a URL e enviar para o input urlFiltroAtual para que seja enviado ao controler e trazido como link da página-->
<script type="text/javascript">
       var filtro = window.location.href;

       if (filtro.includes("tituloVaga="))
       {
           var urlFiltro = window.location.href;
           urlFiltro = urlFiltro.replace("http://pafnovo.crfsp.org.br/admin/vagas?","");
           for(i=0;i<document.getElementsByName("urlFiltroAtual").length;i++){

               document.getElementsByName("urlFiltroAtual")[i].value = '&'+urlFiltro;
           }
       }
       else{
           document.getElementsByName("urlFiltroAtual").value = "";
       }
</script>
<!--FIM do script de capturar URL-->
 <div class="col-12 col-lg-4 my-std">
        <div class="card vaga">
            <div class="card-block text-left">

                <!-- Ativar Vaga -->{{ Form::open([ 'method' => 'Post','route' => [ 'admin.vagas.active', $vaga -> TT014CC000 ]]) }}
                 <input type="hidden" class="form-control" name="paginacaoAtual" value="{{ $vagas->currentPage() }}" >
                 <input type="hidden" class="form-control" name="urlFiltroAtual" value="" >
                <label class="switch float-right"> {{ Form::checkbox('TT014CC016', $vaga -> TT014CC016, $vaga -> TT014CC016, ['onChange' => 'this.form.submit()']) }}
                    <div class="slider slider-BE round"></div>
                </label> {{ Form::close() }}
                <!-- Ativar Vaga -->
                 <div class="float-right text-uppercase ">

                  <!-- Status da Vaga -->
                    <a  href="{{ route( 'admin.vagas.edit', $vaga -> TT014CC000 )}}">
                     @if ( $vaga -> TT014CC020 ===  'NO' )
                     <b><span style="color:OrangeRed   "> Nova Vaga! <i class="fa fa-flag"></i></span></b>
                       @elseif ( $vaga -> TT014CC020 ===  'DE' )
                     <b><span style="color:MediumSeaGreen   ">    Deferido <i class="fa fa-check-circle"></i></span></b>
                       @elseif ( $vaga -> TT014CC020 ===  'IN' )
                     <b><span style="color:DarkGrey   "> Indeferido <i class="fa fa-ban"></i></span></b>
                       @elseif ( $vaga -> TT014CC020 ===  'AN' )
                             <b><span style="color:#1e90ff">  Em análise  <i class="fa fa-cog"></i></span></b>
                       @endif  &nbsp;&nbsp;
                       </a>
                 <!-- Status da Vaga -->
            </div>

                <h5 class="card-title">{{ $vaga -> TT014CC005 }}</h5>

                <h6 class="card-title">{{ $vaga -> empresa -> TT010CC004 }} | {{ $vaga -> empresa -> TT010CC005 }}</h6>
                <p class="card-text"> Salário: R$ {{ $vaga -> TT014CC014 }}  @if ( $vaga -> TT014CC022 ===  'HO' )/ hora @elseif ( $vaga -> TT014CC022 ===  'ME' )  @endif  <!--definindo se por hora ou mês -->| Vagas: {{ $vaga -> TT014CC013 }}  | <b><span style="color:MediumSeaGreen  ">{{ $vaga -> cidade -> TT008CC003 }} -
                    {{ $vaga -> cidade -> TT008CC001 }}</span></b></p>
                <p class="card-text"> Criada em: {{ date_format( date_create( $vaga -> created_at ),'d/m/Y' ) }} | @if ( $vaga -> TT014CC019 >= $vaga -> created_at ) Publicada em:<b>
                                @if ( $vaga -> TT014CC019 >=  Carbon\Carbon::now()->subDays(30) )
                                <span style="color:MediumSeaGreen  ">
                                @else
                                <span style="color:DarkGrey ">
                                @endif

                    {{ date_format( date_create( $vaga -> TT014CC019 ),'d/m/Y' ) }}</span></b>  @endif | Código: {{ $vaga -> TT014CC000 }}

              </p>
<!--                <p class="card-text"><small>{!! limitText( $vaga -> TT014CC009, 100 ) !!}</small></p>-->
                <hr>
                <!-- Controle -->
                <div class="row">
                    <!-- Visualizar -->
                    <div class="col-12 col-sm-4">
                        <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.vagas.show', $vaga -> TT014CC000 )}}"> <i class="fa fa-expand fa-2x" aria-hidden="true"></i> </a>
                    </div>
                    <!-- Visualizar -->
                    <!-- Editar -->
                    <div class="col-12 col-sm-4">
                        <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.vagas.edit', $vaga -> TT014CC000 )}}"> <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i> </a>
                    </div>
                    <!-- Editar -->
                    <!-- Deletar -->
                    <div class="col-12 col-sm-4"> {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.vagas.destroy', $vaga -> TT014CC000 ]]) }} {{ Form::button ( '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} {{ Form::close() }} </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div> @endforeach </div>
<!-- /Vagas -->
<!-- Paginação -->{{ $vagas-> links( 'partials.pagination' ) }}
