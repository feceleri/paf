<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Banners | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<div class="row">


</div>

<!-- Título -->

<!-- Novo -->
<div class="float-left">
            <a class="btn btn-admin" href="{{ URL::Route( 'admin.banners.create' ) }}">
                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </div><!-- /Novo -->

<h1 class="text-center">Banners</h1>



<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->



<!-- Buscas -->
<div class="card mb-3 col-md-4">
    <div class="card-block">


 <div class="col-12 col-md-12">

{!! Form::open(['method'=>'GET','url'=>'admin/banners','class'=>'filter-form navbar-left','role'=>'search'])  !!}


<div class="row">


     <div class="col-12 col-md-6"> Parceiro
       <select class="form-control" name="parceiroBanner">
               <option value="">Todos</option>
           @foreach( $parceiros as $parceiro )
                <option value="{{ $parceiro -> TT011CC000 }}" @if ($parceiro -> TT011CC000 == $parceiroBanner) selected @endif>{{ $parceiro -> TT011CC001 }}  </option>
           @endforeach
       </select>
    </div>


   <div class="col-12 col-md-3"><br>
   <span class="input-group-btn ">
        <button class="btn btn-block btn-admin mb-1" type="submit">
            <i class="fa fa-search fa-2x"></i>
        </button>
    </span>
    </div>
    <div class="col-12 col-md-3"><br>
        <a href="/admin/banners"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
    </div>
</div>
{!! Form::close() !!}


            </div>
        </div>

</div>

  <!-- Buscas -->



<!-- Banners -->
<section class="load-ajax">
    @include('admin::banners.load')
</section>
<!-- /Banners -->

@stop
