<!-- layout Master -->@extends('admin::app')
<!-- Título -->@section('titulo') Edição de Banners | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Título -->
<h2 class="m-3">Edição de Banners</h2>
<!-- /Título -->
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Mensagens de Erro -->@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div> @endif
<!-- /Mensagens de Erro -->
<div class="card my-std">
    <div class="card-block">
        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto"> Banner </span></div>
        </div> {{ Form::model( $banner, [ 'method' => 'PATCH','route' => [ 'admin.banners.update', $banner -> TT013CC000 ], 'files' => true ]) }}
        <div class="row">
            <!-- Parceiros -->
            <div class="col-12 col-md-4">
                <div class="form-group @if ( $errors -> has( 'TT013CC001' )) has-danger @endif"> {{ Form::label( 'TT013CC001', 'Parceiro', ['class' => 'form-control-label' ]) }} {{ Form::select ( 'TT013CC001', $parceiros, null, [ 'class' => 'form-control', 'placeholder' => 'Selecione...' ] ) }}
                    <!-- Erro -->@if ($errors->has('TT013CC001'))
                    <div class="form-control-feedback"> {{ $errors->first('TT013CC001') }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Parceiros -->
            <!-- Título -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT013CC002' )) has-danger @endif"> {{ Form::label( 'TT013CC002', 'Título *', ['class' => 'form-control-label' ]) }} {{ Form::text ( 'TT013CC002', null, [ 'class' => 'form-control', 'maxlength' => '150' ] ) }}
                    <!-- Erro -->@if ($errors->has('TT013CC002'))
                    <div class="form-control-feedback"> {{ $errors->first('TT013CC002') }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Título -->
            <!-- Fim da publicação -->
            <div class="col-12 col-lg-2">
                <div class="form-group @if ( $errors -> has( 'TT013CC008' )) has-danger @endif"> {{ Form::label('TT013CC008', 'Fim da Publicação *', ['class' => 'form-control-label']) }}
                  {{ Form::date (

                    'TT013CC008',
                     \Carbon\Carbon::now()->addDays(30),

                     [ 'max' => '2100-12-31', 'class' => 'form-control', ] ) }}
                    <!-- Erro -->@if ($errors->has('TT013CC008'))
                    <div class="form-control-feedback"> {{ $errors->first('TT013CC008') }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- Fim da publicação -->
        </div>
        <div class="row">
            <!-- Site -->
            <div class="col-12 col-md-7">
                <div class="form-group @if ( $errors -> has( 'TT013CC003' )) has-danger @endif"> {{ Form::label( 'TT013CC003', 'Site *', ['class' => 'form-control-label' ]) }}
                    <!-- Input -->{{ Form::text ( 'TT013CC003', null, [ 'class' => 'form-control', 'maxlength' => '150', 'placeholder' => 'https://www.exemplo.com.br' ] ) }}
                    <!-- /Input -->
                    <!-- Erro -->@if ($errors -> has( 'TT013CC003' ))
                    <div class="form-control-feedback"> {{ $errors->first('TT013CC003') }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Site -->
            <!-- Imagem -->
            <div class="col-12 col-md-5">
                <div class="form-group @if ( $errors -> has( 'TT013CC005' )) has-danger @endif"> {{ Form::label('TT013CC005', 'Imagem * (736px x 420 px)', ['class' => 'form-control-label']) }}
                    <div class="input-group">
                        <label class="input-group-btn mb-0"> <span class="btn btn-secondary">
                                Upload <input name="TT013CC005" type="file" style="display: none;">
                            </span> </label> {{ Form::text ('TT013CC005', null, ['readonly','class' => 'form-control'] ) }}
                        <!--  <input type="text" class="form-control" readonly>-->
                    </div>
                    <!-- Erro -->@if ( $errors -> has( 'TT013CC005' ))
                    <div class="form-control-feedback"> {{ $errors->first( 'TT013CC005' ) }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Imagem -->
        </div>
        <div class="row">
            <!-- Descritivo -->
            <div class="col-7">
                <div class="form-group @if ( $errors -> has( 'TT013CC006' )) has-danger @endif"> {{ Form::label('TT013CC006', 'Descritivo', ['class' => 'form-control-label']) }} {{ Form::textArea ( 'TT013CC006', null, [ 'rows' => '5', 'maxlength' => '250', 'class' => 'form-control', 'placeholder' => 'Máximo 250 caracteres' ] ) }}
                    <!-- Erro -->@if ($errors->has('TT013CC006'))
                    <div class="form-control-feedback"> {{ $errors->first('TT013CC006') }} </div> @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Descritivo -->
            <div class="col-2">
                <br>
                <a href="#" onclick="" data-toggle="modal" data-target="#modal_show">
                    <figure class="figure mb-0 img-hrz-center"> {{ Html::image( $banner -> TT013CC005, $banner -> TT013CC002, array( 'class' => 'figure-img card-img-top img-fluid' )) }} </figure>
                </a>
            </div>
        </div>
        <div class="row mb-1">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
            </div>
        </div>
        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-1"></div>
            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.banners.index' ) }}"> <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i> </a>
            </div>
            <!-- /Voltar -->
            <!-- Confirmar -->
            <div class="col-12 col-sm-3"> {{ Form::button ( '<i class="fa fa-check fa-2x" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} </div>{{ Form::close() }}
            <!-- /Confirmar -->
            <div class="col-12 col-sm-3"></div>
            <div class="col-12 col-sm-2"> {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.banners.destroy', $banner -> TT013CC000 ]]) }} {{ Form::button ( '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }}</div>
        </div>
        <!-- /Controle -->
    </div>
    <!-- Deletar -->

    <div class="col-12 col-sm-2"> </div>
    <!-- Deletar -->
</div>
<!--Modal-->
<div class="modal fade" id="modal_show">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Banner</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <div class="card my-std">
                    <div class="card-block">
                        <figure class="figure mb-0 img-hrz-center"> {{ Html::image( $banner -> TT013CC005, $banner -> TT013CC002, array( 'class' => 'figure-img card-img-top img-fluid' )) }} </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Modal-->@stop
