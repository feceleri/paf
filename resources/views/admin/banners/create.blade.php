<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Cadastro de Banners | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Cadastro dos Banners</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de erro -->

<div class="card my-std">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto"> Banner </span></div>
        </div>

        {{ Form::open(['route' => 'admin.banners.store', 'method' => 'POST', 'files' => true ]) }}

        <div class="row">
            <!-- Parceiros -->
            <div class="col-12 col-md-4">
                <div class="form-group @if ( $errors -> has( 'TT013CC001' )) has-danger @endif">
                    {{ Form::label('TT013CC001', 'Parceiro', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT013CC001',
                            $parceiros,
                            null,

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT013CC001'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT013CC001') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Parceiros -->

            <!-- Título -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT013CC002' )) has-danger @endif">
                    {{ Form::label( 'TT013CC002', 'Título *', ['class' => 'form-control-label' ]) }}
                    {{ Form::text
                        (
                            'TT013CC002',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT013CC002'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT013CC002') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Título -->


            <!-- Fim da publicação -->
            <div class="col-12 col-lg-2">
                <div class="form-group @if ( $errors -> has( 'TT013CC008' )) has-danger @endif">
                    {{ Form::label('TT013CC008', 'Fim da Publicação *', ['class' => 'form-control-label']) }}
                    {{ Form::date
                        (
                            'TT013CC008',
                            \Carbon\Carbon::now()->addDays(30),

                            [
                                'max' => '2100-12-31',
                                'class' => 'form-control',
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT013CC008'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT013CC008') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- Fim da publicação -->
        </div>

        <div class="row">
            <!-- Site -->
            <div class="col-12 col-md-7">
                <div class="form-group @if ( $errors -> has( 'TT013CC003' )) has-danger @endif">
                    {{ Form::label( 'TT013CC003', 'Site *', ['class' => 'form-control-label' ]) }}
                    <!-- Input -->
                    {{ Form::text
                        (
                            'TT013CC003',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150',
                                'placeholder' => 'https://www.exemplo.com.br'
                            ]
                        )
                    }}
                    <!-- /Input -->

                    <!-- Erro -->
                    @if ($errors -> has( 'TT013CC003' ))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT013CC003') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Site -->

            <!-- Imagem -->
            <div class="col-12 col-md-5">
                <div class="form-group @if ( $errors -> has( 'TT013CC005' )) has-danger @endif">
                    {{ Form::label('TT013CC005', 'Imagem * (736px x 420 px)', ['class' => 'form-control-label']) }}
                    <div class="input-group">
                        <label class="input-group-btn mb-0">
                            <span class="btn btn-secondary">
                                Upload <input name="TT013CC005" type="file" style="display: none;">
                            </span>
                        </label>
                        <input type="text" class="form-control" readonly>
                    </div>

                    <!-- Erro -->
                    @if ( $errors -> has( 'TT013CC005' ))
                    <div class="form-control-feedback">
                        {{ $errors->first( 'TT013CC005' ) }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Imagem -->
        </div>

        <div class="row">
            <!-- Descritivo -->
            <div class="col-12">
                <div class="form-group @if ( $errors -> has( 'TT013CC006' )) has-danger @endif">
                    {{ Form::label('TT013CC006', 'Descritivo', ['class' => 'form-control-label']) }}
                    {{ Form::textArea
                        (
                            'TT013CC006',
                            null,

                            [
                                'rows' => '5',
                                'maxlength' => '250',
                                'class' => 'form-control',
                                'placeholder' => 'Máximo 250 caracteres'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT013CC006'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT013CC006') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Descritivo -->
        </div>

        <div class="row mb-1">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
            </div>
        </div>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>

            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-1" href="{{ URL::Route( 'admin.banners.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->

            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
    {{ Form::close() }}
</div>

@stop
