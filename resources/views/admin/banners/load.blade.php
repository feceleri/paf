<!-- Banners -->
<div class="row">
@foreach( $banners as $banner )
    <div class="col-3 my-std">
        <div class="card">
            <div class="card-block">

                <!-- Imagem -->
                <figure class="figure mb-0 img-hrz-center">
                    {{ Html::image( $banner -> TT013CC005, $banner -> TT013CC002, array( 'class' => 'figure-img card-img-top img-fluid' )) }}
                </figure>
                <!-- /Imagem -->


                <hr>


                <!-- Controle -->
                <div class="row">
                       <!-- Dados -->
                <div class="col-12 col-sm-6">
                    <h6 class="card-title text-center">
                        {{ $banner -> TT013CC002 }}
                    </h6>
                    <p class="card-text mb-0">{{ $banner -> TT013CC006 }}</p>
                     <p style="font-size:10px; text-align:center;" class="card-text mb-0">
                     <!-- Aqui é uma condição para mostrar se o banner está valido ou não baseado na datade fim da publicação. Carbon -->
                     @if ( $banner -> TT013CC008 >  Carbon\Carbon::now() )
                    <span style="color: deepskyblue;"> Ativo até: <strong>{{ formatDate( $banner -> TT013CC008 ) }}</strong></span>
                     @else
                         <span style="color: orange;">  Banner desativado</span>
                     @endif


                     </p>

                </div>
                <!-- /Dados -->

                    <!-- Editar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.banners.edit', $banner -> TT013CC000 )}}">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Editar -->

                    <!-- Deletar -->
                    <div class="col-12 col-sm-3">
                    {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.banners.destroy', $banner -> TT013CC000 ]]) }}
                    {{ Form::button
                        (
                            '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                            [
                                'class' => 'btn btn-block btn-admin',
                                'type' => 'submit'
                            ]
                        )
                    }}


                      {{ Form::close() }}
                    </div>
                    <!-- Deletar -->


                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
@endforeach
</div>
<!-- /Banners -->

<!-- Paginação -->
{{ $banners -> links( 'partials.pagination' ) }}
