<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Edição de Parceiros | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Edição de Parceiros</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->

<!-- Mensagens de Erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de Erro -->

<div class="card my-std">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto"> Parceiro </span></div>
        </div>

        {{ Form::model( $parceiro, [ 'method' => 'PATCH','route' => [ 'admin.parceiros.update', $parceiro -> TT011CC000], 'files' => true ]) }}

        <div class="row">
            <!-- Nome -->
            <div class="col-12 col-sm-6">
                <div class="form-group @if ( $errors -> has( 'TT011CC001' )) has-danger @endif">
                    {{ Form::label( 'TT011CC001', 'Nome *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT011CC001',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT011CC001'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT011CC001') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Nome -->

            <!-- Tipo -->
            <div class="col-12 col-sm-6">
                <div class="form-group @if ( $errors -> has( 'TT011CC002' )) has-danger @endif">
                    {{ Form::label('TT011CC002', 'Tipo de Parceria *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT011CC002',

                            [
                                'BE' => 'PAF Empregos',
                                'CB' => 'PAF Descontos'
                            ],

                            null,

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT011CC002'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT011CC002') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Tipo -->
        </div>

        <div class="row">

            <!-- Site -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT011CC004' )) has-danger @endif">
                    {{ Form::label( 'TT011CC004', 'Site', ['class' => 'form-control-label' ]) }}
                    <!-- Input -->
                    {{ Form::text
                        (
                            'TT011CC004',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150',
                                'placeholder' => 'https://www.exemplo.com.br'
                            ]
                        )
                    }}
                    <!-- /Input -->

                    <!-- Erro -->
                    @if ($errors -> has( 'TT011CC004' ))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT011CC004') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Site -->
         <div class="col-12 col-lg-1"><br>
                        <figure class="figure mb-1">
                            {{ Html::image( $parceiro -> TT011CC003, $parceiro -> TT011CC001, array( 'class' => 'img-fluid' )) }}
                        </figure>
                    </div>
            <!-- Imagem -->
            <div class="col-12 col-md-4">
                <div class="form-group @if ( $errors -> has( 'TT011CC003' )) has-danger @endif">
                    {{ Form::label('TT011CC003', 'Imagem * (400px x 150px)', ['class' => 'form-control-label']) }}
                    <div class="input-group">
                        <label class="input-group-btn mb-0">
                            <span class="btn btn-secondary">
                                Upload <input name="TT011CC003" type="file" style="display: none;">
                            </span>
                        </label>
                         {{ Form::text
                        ('TT011CC003', null,  ['readonly','class' => 'form-control']
                        )
                    }}

                    </div>


                    <!-- Erro -->
                    @if ( $errors -> has( 'TT011CC003' ))
                    <div class="form-control-feedback">
                        {{ $errors->first( 'TT011CC003' ) }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Imagem -->
        </div>

        <div class="row mb-1">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
            </div>
        </div>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>

            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-1" href="{{ URL::Route( 'admin.parceiros.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->

            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
    {{ Form::close() }}
</div>

@stop
