<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Parceiros | Admin @stop

<!-- Conteúdo -->
@section('conteudo')
<!-- Novo -->
 <div class="float-left">
            <a class="btn btn-admin" href="{{ URL::Route( 'admin.parceiros.create' ) }}">
                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </div>
<!-- Novo -->
<!-- Título -->
<h1 class="text-center">Parceiros</h1>




<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->

<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">
      <div class="row">
       <div class="col-12 col-md-12">
{!! Form::open(['method'=>'GET','url'=>'admin/parceiros','class'=>'filter-form navbar-left','role'=>'search'])  !!}
<div class="col-12 col-md-12">
<div class="row">
     <div class="col-12 col-md-2">Parceiro  <input type="text" class="form-control" name="parceiroSelected" value="{{$parceiroSelected}}" placeholder="..."></div>
        <div class="col-12 col-md-2"> Modalidade
                        <select class="form-control" name="modalidadeParceiro">
                            <option value="">Todas</option>
                            <option value="BE" @if ( $modalidadeParceiro=="BE" ) selected @endif>Empregos</option>
                            <option value="CB" @if ( $modalidadeParceiro=="CB" ) selected @endif>Descontos</option>
                        </select>
         </div>
           <div class="col-12 col-md-2"> Status
                        <select class="form-control" name="ativoParceiro">
                            <option value="">Todos</option>
                            <option value="1" @if ( $ativoParceiro=="1" ) selected @endif>Ativo</option>
                            <option value="0" @if ( $ativoParceiro=="0" ) selected @endif>Inativo</option>
                        </select>
                    </div>
        <div class="col-12 col-md-3 ">Mês de criação
                        <input type="month" class="form-control" name="dataParceiro" value="{{$dataParceiro}}" placeholder="..."> </div>
   <div class="col-12 col-md-1"><br>
   <span class="input-group-btn ">
        <button class="btn btn-block btn-admin mb-1" type="submit">
            <i class="fa fa-search fa-2x"></i>
        </button>
    </span>
    </div>
    <div class="col-12 col-md-1"><br>
        <a href="/admin/parceiros"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
    </div>
</div>
{!! Form::close() !!}
  </div>

            </div>
        </div>
    </div>
</div>

  <!-- Buscas -->
<div>*Ativar ou desativar o Parceiro, apenas o tira da esteira da Home. </div>

<!-- Parceiros -->
<section class="load-ajax">
    @include('admin::parceiros.load')
</section>
<!-- /Parceiros -->


@stop
