<!-- Parceiros -->

<div class="row">
@foreach( $parceiros as $parceiro )
   <!--Aqui vai capturar a URL e enviar para o input urlFiltroAtual para que seja enviado ao controler e trazido como link da página-->
<script type="text/javascript">
       var filtro = window.location.href;

       if (filtro.includes("parceiroSelected="))
       {
           var urlFiltro = window.location.href;
           urlFiltro = urlFiltro.replace("http://pafnovo.crfsp.org.br/admin/parceiros?","");
           for(i=0;i<document.getElementsByName("urlFiltroAtual").length;i++){

               document.getElementsByName("urlFiltroAtual")[i].value = '&'+urlFiltro;
           }
       }
       else{
           document.getElementsByName("urlFiltroAtual").value = "";
       }
</script>
<!--FIM do script de capturar URL-->
    <div class="col-12 col-lg-4">
        <div class="card parceiro my-std">
            <div class="card-block text-center text-lg-left">
                <div class="row cor-texto">
                    <div class="col-12 col-lg-4">

                        <figure class="figure mb-1">
                            {{ Html::image( $parceiro -> TT011CC003, $parceiro -> TT011CC001, array( 'class' => 'img-fluid' )) }}
                        </figure>
                    </div>
                         <!-- Ativar Parceiro -->
                        <div class="col-12 col-lg-8"> {{ Form::open([ 'method' => 'Post','route' => [ 'admin.parceiro.active', $parceiro-> TT011CC000 ]]) }}
                         <input type="hidden" class="form-control" name="paginacaoAtual" value="{{ $parceiros->currentPage() }}" >
                         <input type="hidden" class="form-control" name="urlFiltroAtual" value="" >
                        <label class="switch float-right">
                         {{ Form::checkbox('TT011CC005', $parceiro -> TT011CC005, $parceiro -> TT011CC005, ['onChange' => 'this.form.submit()']) }}
                            <div class="slider slider-BE round"></div>
                        </label>
                        {{ Form::close() }}
                           <!-- /Ativar Parceiro  -->
                        <h5 class="media-heading">
                            {{ $parceiro -> TT011CC001 }}
                        </h5>

                        <!-- Tipo -->
                        <p>
                           <strong class="text-uppercase">
                                @if  ( $parceiro -> TT011CC002 == 'CB' )
                                    Clube de Descontos
                                @else
                                    Bolsa de Emprego
                                @endif
                            </strong>
                        </p>
                        <!-- /Tipo -->
                    </div>
                </div>

                <hr>

                <!-- Controle -->
                <div class="row">
                    <div class="col-12 col-sm-3"></div>

                    <!-- Editar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.parceiros.edit', $parceiro -> TT011CC000 )}}">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Editar -->

                    <!-- Deletar -->
                    <div class="col-12 col-sm-3">
                        {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.parceiros.destroy', $parceiro -> TT011CC000 ]]) }}
                        {{ Form::button
                            (
                                '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                        {{ Form::close() }}
                    </div>
                    <!-- Deletar -->

                    <div class="col-12 col-sm-3"></div>
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
@endforeach
</div>
<!-- /Parceiros -->

<!-- Paginação -->
{{ $parceiros -> links( 'partials.pagination' ) }}
