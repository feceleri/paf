<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Prévia dos Descontos | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Prévia</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->

<div class="row">
    <div class="col-12">
        <div class="card my-std">
            <div class="card-block">

                <!-- Ativar Desconto -->
                {{ Form::open([ 'method' => 'Post','route' => [ 'admin.descontos.active', $desconto -> TT012CC000 ]]) }}
                <label class="switch float-right">
                    {{ Form::checkbox('TT012CC006', $desconto -> TT012CC006, $desconto -> TT012CC006, ['onChange' => 'this.form.submit()']) }}
                    <div class="slider slider-CB round"></div>
                </label>
                {{ Form::close() }}
                <!-- /Ativar Desconto -->

                <!-- Title Descontos -->
                <div class="title clearfix">
                    <div class="title-form">
                        <span class="cor-descontos"> Prévia do Desconto </span>
                    </div>
                </div>
                <!-- /Title Descontos -->

                <h2 class="card-title text-center">{{ $desconto -> TT012CC003 }}</h2>

                <figure class="figure desconto-img">
                    {{ Html::image( $desconto -> TT012CC004, $desconto -> TT012CC003, array('class' => 'figure-img img-fluid')) }}
                </figure>

                {!! $desconto -> TT012CC005 !!}

                <hr>

                <!-- Controle -->
                <div class="row">
                    <!-- Voltar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.descontos.index' ) }}">
                            <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- /Voltar -->

                    <!-- Novo -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.descontos.create' ) }}">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- /Novo -->

                    <!-- Editar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.descontos.edit', $desconto -> TT012CC000 )}}">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Editar -->

                    <!-- Deletar -->
                    <div class="col-12 col-sm-3">
                        {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.descontos.destroy', $desconto -> TT012CC000 ]]) }}
                        {{ Form::button
                            (
                                '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                        {{ Form::close() }}
                    </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
</div>

@stop
