<!-- layout Master -->
@extends('admin::app')

<!-- Script -->
@section('script') <script asinc src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script> @stop

<!-- Título -->
@section('titulo') Cadastro de Descontos | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Cadastro dos Descontos</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de erro -->

<div class="card my-std">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto"> Desconto </span></div>
        </div>

        {{ Form::open(['route' => 'admin.descontos.store', 'method' => 'POST', 'files' => true ]) }}

        <div class="row">
            <!-- Título -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT012CC003' )) has-danger @endif">
                    {{ Form::label( 'TT012CC003', 'Título *', ['class' => 'form-control-label' ]) }}

                    {{ Form::text
                        (
                            'TT012CC003',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT012CC003'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT012CC003') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Título -->

            <!-- Parceiros -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT012CC001' )) has-danger @endif">
                    {{ Form::label('TT012CC001', 'Parceiro *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT012CC001',
                            $parceiros,
                            null,

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT012CC001'))
                    <div class="form-control-feedback">
                        {{ $errors -> first('TT012CC001') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Parceiros -->
        </div>

        <div class="row">
            <!-- Categorias -->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="form-group @if ( $errors -> has( 'TT012CC002' )) has-danger @endif">
                    {{ Form::label( 'TT012CC002', 'Categoria *', ['class' => 'form-control-label'] ) }}
                    {{ Form::select
                        (
                            'TT012CC002',
                            $categorias,
                            null,

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has( 'TT012CC002' ))
                    <div class="form-control-feedback">
                        {{ $errors -> first( 'TT012CC002' ) }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Categorias -->

            <!-- Modalidade -->
            <div class="col-12 col-md-6 col-lg-3">
                <div class="form-group @if ( $errors -> has( 'TT012CC007' )) has-danger @endif">
                    {{ Form::label( 'TT012CC007', 'Modalidade *', ['class' => 'form-control-label'] ) }}
                    {{ Form::select
                        (
                            'TT012CC007',

                            [
                                'GG' => 'Geral',
                                'PF' => 'PAF Descontos',
                                'PJ' => 'PAF Empresa'

                            ],

                            null,

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT012CC007'))
                    <div class="form-control-feedback">
                        {{ $errors -> first('TT012CC007') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Modalidade -->

            <!-- Imagem -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT012CC004' )) has-danger @endif">
                    {{ Form::label('TT012CC004', 'Imagem * (380px x 280px)', ['class' => 'form-control-label']) }}
                    <div class="input-group">
                        <label class="input-group-btn mb-0">
                            <span class="btn btn-secondary">
                                Upload <input name="TT012CC004" type="file" style="display: none;">
                            </span>
                        </label>
                        <input type="text" class="form-control" readonly>
                    </div>

                    <!-- Erro -->
                    @if ( $errors -> has( 'TT012CC004' ))
                    <div class="form-control-feedback">
                        {{ $errors -> first( 'TT012CC004' ) }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Imagem -->
        </div>

        <div class="row">
            <!-- Estados -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'estados' )) has-danger @endif">
                    {{ Form::label('estados[]', 'Estados abrangentes', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'estados[]',

                            $estados,

                            null,

                            [
                                'size' => '5',
                                'name'=>'estados[]',
                                'multiple'=>'multiple',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('estados'))
                    <div class="form-control-feedback">
                        {{ $errors->first('estados') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Estados -->

            <!-- Cidades -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'cidades' )) has-danger @endif">
                    {{ Form::label('cidades[]', 'Cidades abrangentes', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'cidades[]',

                            $cidades,

                            null,

                            [
                                'size' => '5',
                                'name'=>'cidades[]',
                                'multiple'=>'multiple',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('cidades'))
                    <div class="form-control-feedback">
                        {{ $errors->first('cidades') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Cidades -->

            <!-- Abrangência nacional -->
            <div class="col-12">
                <label class="custom-control custom-checkbox">
                    <input name="TT012CC008" type="checkbox" class="custom-control-input">
                    <span class="custom-control-indicator"></span>
                    Abrangência nacional
                </label>
            </div>
            <!-- /Abrangência nacional -->
        </div>

        <div class="row">
            <!-- Descritivo -->
            <div class="col-12">
                <div class="form-group @if ( $errors -> has( 'TT012CC005' )) has-danger @endif">
                    {{ Form::label('TT012CC005', 'Descritivo *', ['class' => 'form-control-label']) }}
                    {{ Form::textArea
                        (
                            'TT012CC005',
                            null,

                            [
                                'id' => 'TT012CC005',
                                'rows' => '20',
                                'class' => 'form-control',
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT012CC005'))
                    <div class="form-control-feedback">
                        {{ $errors -> first('TT012CC005') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Descritivo -->
        </div>

        <div class="row mb-1">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
            </div>
        </div>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>
            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-1" href="{{ URL::Route( 'admin.descontos.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->
            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
    {{ Form::close() }}
</div>

<script>
    CKEDITOR.replace( 'TT012CC005' );
</script>

@stop
