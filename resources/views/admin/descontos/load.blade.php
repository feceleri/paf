<!-- Descontos -->
<div class="row">
@foreach( $descontos as $desconto )
<!--Aqui vai capturar a URL e enviar para o input urlFiltroAtual para que seja enviado ao controler e trazido como link da página-->
<script type="text/javascript">
       var filtro = window.location.href;

       if (filtro.includes("dataDesconto="))
       {
           var urlFiltro = window.location.href;
           urlFiltro = urlFiltro.replace("http://pafnovo.crfsp.org.br/admin/descontos?","");
           for(i=0;i<document.getElementsByName("urlFiltroAtual").length;i++){

               document.getElementsByName("urlFiltroAtual")[i].value = '&'+urlFiltro;
           }
       }
       else{
           document.getElementsByName("urlFiltroAtual").value = "";
       }
</script>
<!--FIM do script de capturar URL-->
    <div class="col-12 col-md-3 my-std">
        <div class="card desconto">
            <!-- Ativar Desconto -->
            <div class="m-2">
                {{ Form::open([ 'method' => 'Post','route' => [ 'admin.descontos.active', $desconto -> TT012CC000 ]]) }}
                 <input type="hidden" class="form-control" name="paginacaoAtual" value="{{ $descontos->currentPage() }}" >
                 <input type="hidden" class="form-control" name="urlFiltroAtual" value="" >
                <label class="switch float-right">
                    {{ Form::checkbox('TT012CC006', $desconto -> TT012CC006, $desconto -> TT012CC006, ['onChange' => 'this.form.submit()']) }}
                    <div class="slider slider-CB round"></div>
                </label>
                {{ Form::close() }}
            </div>
            <!-- /Ativar Desconto -->

            <div class="card-block">
                <figure class="figure mb-0 img-hrz-center">
                    {{ Html::image( $desconto -> TT012CC004, $desconto -> TT012CC003, array( 'class' => 'figure-img card-img-top img-fluid desconto' )) }}
                </figure>
                <div class="card-block cor-texto py-0">
                    <h6 class="card-title text-center">
                        {{ $desconto -> parceiro -> TT011CC001 }}
                    </h6>
                    <p class="card-text mb-0">{{ $desconto -> TT012CC003 }}</p>
                </div>

                <hr>

                <!-- Controle -->
                <div class="row">
                    <!-- Visualizar -->
                    <div class="col-12 col-sm-4">
                        <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.descontos.show', $desconto -> TT012CC000 )}}">
                            <i class="fa fa-expand fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Visualizar -->

                    <!-- Editar -->
                    <div class="col-12 col-sm-4">
                        <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.descontos.edit', $desconto -> TT012CC000 )}}">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Editar -->

                    <!-- Deletar -->
                    <div class="col-12 col-sm-4">
                        {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.descontos.destroy', $desconto -> TT012CC000 ]]) }}
                        {{ Form::button
                            (
                                '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                        {{ Form::close() }}
                    </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
@endforeach
</div>
<!-- /Descontos -->

<!-- Paginação -->
{{ $descontos -> links( 'partials.pagination' ) }}
