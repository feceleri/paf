<!-- layout Master -->@extends('admin::app')
<!-- Título -->@section('titulo') Descontos | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Novo -->
<div class="float-left">
    <a class="btn btn-admin" href="{{ URL::Route( 'admin.descontos.create' ) }}"> <i class="fa fa-plus fa-2x" aria-hidden="true"></i> </a>
</div>
<!-- Novo -->
<!-- Título -->
<h1 class="text-center"> Descontos </h1>
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->
<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="row">
            <!-- Tipo de Vaga -->
            <div class="col-12 col-md-12"> {!! Form::open(['method'=>'GET','url'=>'admin/descontos','class'=>'filter-form navbar-left','role'=>'search']) !!}
                <div class="row">
                    <div class="col-12 col-md-3 ">Mês de criação
                        <input type="month" class="form-control" name="dataDesconto" value="{{$dataDesconto}}" placeholder="..."> </div>
                    <div class="col-12 col-md-4">Título
                        <input type="text" class="form-control" name="tituloDesconto" value="{{$tituloDesconto}}" placeholder="..."> </div>


                     <div class="col-12 col-md-2"> Status
                        <select class="form-control" name="ativoDesconto">
                            <option value="">Todos</option>
                            <option value="1" @if ( $ativoDesconto=="1" ) selected @endif>Ativo</option>
                            <option value="0" @if ( $ativoDesconto=="0" ) selected @endif>Inativo</option>
                        </select>
                    </div>

                </div>
                <div class="row">
                     <div class="col-12 col-md-3"> Parceiro
                        <select class="form-control" name="parceiroDesconto">
                            <option value="">Todos</option> @foreach( $parceiros as $parceiro )
                            <option value="{{ $parceiro -> TT011CC000 }}" @if ($parceiro -> TT011CC000 == $parceiroDesconto) selected @endif>{{ $parceiro -> TT011CC001 }}</option> @endforeach </select>
                    </div>
                    <div class="col-12 col-md-2"> Categoria
                        <select class="form-control" name="categoriaDesconto">
                            <option value="">Todas</option> @foreach( $categorias as $categoria )
                            <option value="{{ $categoria -> TT005CC000 }}" @if ($categoria -> TT005CC000 == $categoriaDesconto) selected @endif>{{ $categoria -> TT005CC001 }}</option> @endforeach </select>
                    </div>
<!--
                    <div class="col-12 col-md-2"> Cidade
                        <select class="form-control" name="cidadeDesconto">
                            <option value="">Todas</option> @foreach( $cidades as $cidade )
                            <option value="{{ $cidade -> TT008CC000 }}" @if ($cidade -> TT008CC000 == $cidadeDesconto) selected @endif >{{ $cidade -> TT008CC003 }}</option> @endforeach </select>
                    </div>
                    <div class="col-12 col-md-2"> Estado
                        <select class="form-control" name="estadoDesconto">
                            <option value="">Todos</option> @foreach( $estados as $estado )
                            <option value="{{ $estado -> TT007CC000 }}" @if ($estado -> TT007CC000 == $estadoDesconto) selected @endif>{{ $estado -> TT007CC001 }}</option> @endforeach </select>
                    </div>
-->
                    <div class="col-12 col-md-2"> Modalidade
                        <select class="form-control" name="modalidadeDesconto">
                            <option value="">Todas</option>
                            <option value="GG" @if ( $modalidadeDesconto=="GG" ) selected @endif>Geral</option>
                            <option value="PF" @if ( $modalidadeDesconto=="PF" ) selected @endif>PAF Descontos</option>
                            <option value="PJ" @if ( $modalidadeDesconto=="PJ" ) selected @endif>PAF Empresa</option>
                        </select>
                    </div>

                    <div class="col-12 col-md-1">
                        <br> <span class="input-group-btn ">
        <button class="btn btn-block btn-admin mb-1" type="submit">
            <i class="fa fa-search fa-2x"></i>
        </button>
    </span> </div>
                    <div class="col-12 col-md-1">
                        <br>
                        <a href="/admin/descontos"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
                    </div>
                </div> {!! Form::close() !!} </div>
        </div>
    </div>
</div>
<!-- Buscas -->
<!-- Descontos -->
<section class="load-ajax"> @include('admin::descontos.load') </section>
<!-- /Descontos -->@stop
