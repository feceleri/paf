<!-- Linguas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Línguas</span></div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th> Código </th>
                    <th> Língua </th>
                    <th></th>
                </tr>
                @foreach ( $linguas as $lingua )
                <tr>
                    <td>{{ $lingua -> TT031CC000 }}</td>
                    <td>{{ $lingua -> TT031CC001 }}</td>
                    <td>
                        <!-- Controle -->
                        <div class="row">
                            <!-- Editar -->
                            <div class="col-12 col-sm-6">
                                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.linguas.edit', $lingua -> TT031CC000 )}}">
                                    <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                </a>
                            </div>
                            <!-- Editar -->

                            <!-- Deletar -->

                            <div class="col-12 col-sm-6">
                                {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.linguas.destroy', $lingua -> TT031CC000 ]]) }}
                                {{ Form::button
                                    (
                                        '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>',

                                        [
                                            'class' => 'btn btn-block btn-admin',
                                            'type' => 'submit'
                                        ]
                                    )
                                }}
                                {{ Form::close() }}
                            </div>
                            <!-- Deletar -->
                        </div>
                        <!-- /Controle -->
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<!-- /Linguas -->

<!-- Paginação -->
{{ $linguas -> links( 'partials.pagination' ) }}
