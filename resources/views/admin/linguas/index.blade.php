<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Línguas | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Novo -->
        <div class="float-left">
            <a class="btn btn-admin" href="{{ URL::Route( 'admin.linguas.create' ) }}">
                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </div>
<!-- /Novo -->
<!-- Título -->
<h1 class="text-center">Linguas</h1>



<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de Alerta -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Mensagens de Alerta -->
<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="row">
            <div class="col-12 col-md-12"> {!! Form::open(['method'=>'GET','url'=>'admin/linguas','class'=>'filter-form navbar-left','role'=>'search']) !!}
                <div class="col-12 col-md-12">
                    <div class="row">

                        <div class="col-12 col-md-4">Lingua
                            <input type="text" class="form-control" name="nomeLingua" value="{{$nomeLingua}}" placeholder="..."> </div>

                        <div class="col-12 col-md-1">
                            <br> <span class="input-group-btn ">
                            <button class="btn btn-block btn-admin mb-1" type="submit">
                            <i class="fa fa-search fa-2x"></i>
                            </button>
                            </span> </div>
                        <div class="col-12 col-md-1">
                            <br>
                            <a href="/admin/linguas"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
                        </div>
                    </div> {!! Form::close() !!} </div>
            </div>
        </div>
    </div>
</div>
<!-- Buscas -->
<!-- Linguas -->
<section class="load-ajax">
    @include('admin::linguas.load')
</section>
<!-- /Linguas -->

@stop
