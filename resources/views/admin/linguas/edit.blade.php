<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Edição de Línguas | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Edição de Línguas</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->

<!-- Mensagens de Erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de Erro -->

{{ Form::model( $lingua, [ 'method' => 'PATCH', 'route' => [ 'admin.linguas.update', $lingua -> TT031CC000 ]]) }}

<div class="card">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Dados do Língua</span></div>
        </div>

        <div class="row">
            <!-- Lingua -->
            <div class="col-12">
                <div class="form-group @if ( $errors -> has( 'TT031CC001' )) has-danger @endif">
                    {{ Form::label( 'TT031CC001', 'Língua *', ['class' => 'form-control-label' ]) }}
                    {{ Form::text
                        (
                            'TT031CC001',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT031CC001'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT031CC001') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Lingua -->
        </div>

        <div class="row mb-1">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
            </div>
        </div>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>

            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.linguas.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->

            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
</div>

{{ Form::close() }}

@stop
