<!-- layout Master -->@extends('admin::app')
<!-- Script -->@section('script')



  @stop
<!-- Título -->@section('titulo') Vagas | Admin @stop
<!-- Conteúdo -->@section('conteudo')

<!-- Título -->
<h1 class="text-center">Estatísticas de Vagas</h1>
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Alertas -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Alertas -->

<div class="row">
    <div class="col-12">

    <div id="chart-div"></div>
        @donutchart('IMDB', 'chart-div')
    </div>
</div> @stop
