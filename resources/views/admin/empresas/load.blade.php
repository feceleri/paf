

<!-- Empresas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Cargos</span></div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th> Código </th>
                    <th> Razão Social  </th>
                    <th> Nome Fantasia </th>
                    <th> CNPJ </th>
                    <th></th>
                </tr>
                @foreach ( $empresas as $empresa )
                <tr>
                     <td>{{ $empresa -> TT010CC000 }}<br></td>
                    <td>  <a href="{{ route( 'admin.empresas.show',  $empresa -> TT010CC000)}}"> <h6 class="card-title">{{ $empresa -> TT010CC004 }}</h6></a></td>

                       <td>{{ $empresa -> TT010CC005 }}<br></td>
                       <td> {{ $empresa -> TT010CC003 }}<br></td>

                    <td>
                        <!-- Controle -->
                        <div class="row">
                            <!-- Editar -->
                            <div class="col-12 col-sm-6">
                               <a class="btn btn-block btn-admin mb-1" href="{{ route( 'admin.empresas.edit', $empresa -> TT010CC000 )}}">
                                    <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                </a>
                            </div>
                            <!-- Editar -->

                            <!-- Deletar -->

                            <div class="col-12 col-sm-6">



                                  {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.empresas.destroy', $empresa -> TT010CC000 ]]) }}
                                {{ Form::button
                                (
                                '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                            }}
                            {{ Form::close() }}
                            </div>
                            <!-- Deletar -->
                        </div>
                        <!-- /Controle -->
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<!-- /Empresas -->


<!-- Paginação -->
{{ $empresas -> links( 'partials.pagination' ) }}
