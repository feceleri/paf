<!-- layout Master -->
@extends('admin::app')

@section('script') <script asinc src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script> @stop

<!-- Título -->
@section('titulo') Cadastro de Empresa | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Cadastro de Empresas</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de erro -->

{{ Form::open(['route' => 'admin.empresas.store', 'method' => 'POST']) }}

<!-- Dados da Empresa -->
<div class="card">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto"> Empresa </span></div>
        </div>

        <div class="row">
            <!-- Razão Social -->
            <div class="col-12 col-md-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT010CC004' )) has-danger @endif">
                    {{ Form::label( 'TT010CC004', 'Razão Social *', ['class' => 'form-control-label' ]) }}
                    {{ Form::text
                        (
                            'TT010CC004',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT010CC004'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC004') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Razão Social -->

            <!-- Nome Fantasia -->
            <div class="col-12 col-md-8 col-lg-4">
                <div class="form-group @if ( $errors -> has( 'TT010CC005' )) has-danger @endif">
                    {{ Form::label( 'TT010CC005', 'Nome Fantasia *', ['class' => 'form-control-label' ]) }}
                    {{ Form::text
                        (
                            'TT010CC005',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT010CC005'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC005') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Nome Fantasia -->

            <!-- CNPJ -->
            <div class="col-12 col-md-4 col-lg-2">
                <div class="form-group @if ( $errors -> has( 'TT010CC003' )) has-danger @endif">
                    {{ Form::label('TT010CC003', 'CNPJ *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT010CC003',
                            null,

                            [
                                'maxlength' => '18',
                                'class' => 'form-control cnpj',
                                'placeholder' => '00.000.000/0000-00'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT010CC003'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC003') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /CNPJ -->
        </div>

        <div class="row">
            <!-- Ramo de Atividade -->
            <div class="col-12 col-md-9 col-lg-4">
                <div class="form-group @if ( $errors -> has( 'TT010CC001' )) has-danger @endif">
                    {{ Form::label('TT010CC001', 'Ramo de Atividade *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT010CC001',

                            $ramos,

                            null,

                            [
                                'class' => 'form-control',
                                'placeholder'=>'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT010CC001'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC001') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Ramo de Atividade -->

            <!-- Telefone -->
            <div class="col-12 col-md-3 col-lg-2">
                <div class="form-group @if ( $errors -> has( 'TT010CC009' )) has-danger @endif">
                    {{ Form::label('TT010CC009', 'Telefone *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT010CC009',
                            null,

                            [
                                'maxlength' => '15',
                                'class' => 'form-control tel',
                                'placeholder' => '(00) 0000-0000'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT010CC009'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC009') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Telefone -->

            <!-- E-mail -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT010CC010' )) has-danger @endif">
                    {{ Form::label('TT010CC010', 'E-mail *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT010CC010',
                            null,

                            [
                                'class' => 'form-control has-danger',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT010CC010'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC010') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /E-mail -->
        </div>

        <div class="row">

            <!-- Endereço -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT010CC007' )) has-danger @endif">
                    {{ Form::label('TT010CC007', 'Endereço *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT010CC007',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT010CC007'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC007') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Endereço -->

            <!-- Bairro -->
            <div class="col-12 col-md-9 col-lg-4">
                <div class="form-group @if ( $errors -> has( 'TT010CC008' )) has-danger @endif">
                    {{ Form::label('TT010CC008', 'Bairro *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT010CC008',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT010CC008'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC008') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Bairro -->

            <!-- CEP -->
            <div class="col-12 col-md-3 col-lg-2">
                <div class="form-group @if ( $errors -> has( 'TT010CC006' )) has-danger @endif">
                    {{ Form::label('TT010CC006', 'CEP *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT010CC006',
                            null,

                            [
                                'maxlength' => '9',
                                'class' => 'form-control cep',
                                'placeholder' => '00000-000'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT010CC006'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC006') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /CEP -->
        </div>

        <div class="row">
            <!-- UF -->
            <div class="col-4 col-sm-3 col-md-2">
                <div class="form-group">
                    {{ Form::label('uf', 'UF *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'uf',
                            [''],
                            null,

                            [
                                'id' => 'uf',
                                'class' => 'form-control',
                                'default' => old('uf', 'SP')
                            ]
                        )
                    }}
                </div>
            </div>
            <!-- /UF -->

            <!-- Cidade -->
            <div class="col-8  col-sm-9 col-md-10 col-lg-4">
                <div class="form-group @if ( $errors -> has( 'TT010CC002' )) has-danger @endif">
                    {{ Form::label('TT010CC002', 'Cidade *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT010CC002',
                            [''],
                            null,

                            [
                                'id' => 'cidade',
                                'class' => 'form-control',
                                'default' => old('TT010CC002')
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT010CC002'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC002') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Cidade -->

            <!-- Site -->
            <div class="col-12 col-md-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT010CC011' )) has-danger @endif">
                    {{ Form::label('TT010CC011', 'Site', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT010CC011',
                            null,

                            [
                                'class' => 'form-control',
                                'placeholder' => 'https://www.exemplo.com.br',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT010CC011'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC011') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Site -->
        </div>

        <div class="row">
            <!-- Observações -->
            <div class="col-12">
                <div class="form-group @if ( $errors -> has( 'TT010CC012' )) has-danger @endif">
                    {{ Form::label('TT010CC012', 'Observações', ['class' => 'form-control-label', 'maxlength' => '255']) }}
                    {{ Form::textArea
                        (
                            'TT010CC012',
                            null,

                            [
                                'rows' => '5',
                                'maxlength' => '250',
                                'class' => 'form-control',
                                'placeholder' => 'Máximo 250 caracteres'
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT010CC012'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT010CC012') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Observações -->
        </div>

        <div class="row">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong>(*) Campos obrigatórios</strong></p>
            </div>
        </div>

        <hr>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>

            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-xs-1" href="{{ URL::Route( 'admin.empresas.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->

            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
</div>
<!-- /Dados da Empresa -->

{{ Form::close() }}
<script>
   // CKEDITOR.replace( 'TT010CC012' );
</script>
@stop
