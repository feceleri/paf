<!-- layout Master -->@extends('admin::app')
<!-- Título -->@section('titulo') Empresas | Admin @stop
<!-- Conteúdo -->@section('conteudo')
<!-- Novo -->
<div class="float-left">
    <a class="btn btn-admin" href="{{ URL::Route( 'admin.empresas.create' ) }}"> <i class="fa fa-plus fa-2x" aria-hidden="true"></i> </a>
</div>
<!-- /Novo -->
<!-- Título -->
<h1 class="text-center">Empresas</h1>
<!-- /Título -->
<!-- Mensagens de sucesso -->@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0"> {{ $message }} </p>
</div> @endif
<!-- /Mensagens de sucesso -->
<!-- Mensagens de Alerta -->@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    <p class="mb-0">{{ $message }}</p>
</div> @endif
<!-- /Mensagens de Alerta -->
<!-- Buscas -->
<div class="card mb-3 col-md-12">
    <div class="card-block">
        <div class="col-12 col-md-12"> {!! Form::open(['method'=>'GET','url'=>'admin/empresas','class'=>'filter-form navbar-left','role'=>'search']) !!}
            <div class="row">


                   <div class="col-12 col-md-3"> Razão Social
                    <select class="form-control" name="razaoEmpresa">
                        <option value="">Todas</option> @foreach( $buscaEmpresas as $buscaEmpresa )
                        <option value="{{ $buscaEmpresa -> TT010CC000 }}" @if ($buscaEmpresa -> TT010CC000 == $razaoEmpresa) selected @endif>{{ $buscaEmpresa -> TT010CC004 }}  </option> @endforeach </select>
                </div>

                   <div class="col-12 col-md-3"> CNPJ
                    <select class="form-control" name="cnpjEmpresa">
                        <option value="">Todas</option> @foreach( $buscaEmpresas->sortBy('TT010CC003') as $buscaEmpresa )
                        <option value="{{ $buscaEmpresa -> TT010CC000 }}" @if ($buscaEmpresa -> TT010CC000 == $cnpjEmpresa) selected @endif>{{ $buscaEmpresa -> TT010CC003 }}  </option> @endforeach </select>
                </div>

                <div class="col-12 col-md-3 ">Mês de criação
                    <input type="month" class="form-control" name="dataEmpresa" value="{{$dataEmpresa}}" placeholder="..."> </div>
                <div class="col-12 col-md-1">
                    <br> <span class="input-group-btn ">
        <button class="btn btn-block btn-admin mb-1" type="submit">
            <i class="fa fa-search fa-2x"></i>
        </button>
    </span> </div>
                <div class="col-12 col-md-1">
                    <br>
                    <a href="/admin/empresas"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
                </div>
            </div> {!! Form::close() !!} </div>
    </div>
</div>
<!-- Buscas -->
<!-- Empresas -->
<section class="load-ajax"> @include('admin::empresas.load') </section>
<!-- /Empresas -->@stop
