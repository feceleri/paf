<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Prévia dos Empresas | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Prévia</h2>
<!-- /Título -->


<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->

<div class="row">
    <div class="col-12">
        <div class="card my-std">
            <div class="card-block">



                <!-- Title empresas -->
                <div class="title clearfix">
                    <div class="title-form">
                        <span class="cor-empresas"> {{ $empresa -> TT010CC004 }} </span>
                    </div>
                </div>
                <!-- /Title empresas -->




                 <p >
                        <strong> Nome Fantasia: </strong>{{ $empresa -> TT010CC005 }}<br>
                        <strong> CNPJ: </strong>{{ $empresa -> TT010CC003 }}<br>
                        <strong> Ramo de Atividade: </strong>{{ $empresa -> ramo -> TT002CC001 }}<br>
                        <strong> Telefone: </strong>{{ $empresa -> TT010CC009 }}<br>
                          <strong> E-mail: </strong>{{ $empresa -> TT010CC010 }}<br>
                          <strong> Site: </strong>{{ $empresa -> TT010CC011 }}<br>
                          <strong> Endereço: </strong>
                            {{ $empresa -> TT010CC007 }} -
                            {{ $empresa -> TT010CC008 }} -
                            {{ $empresa -> TT010CC006 }} -
                            {{ $empresa -> cidade -> TT008CC003 }} - {{ $empresa -> cidade -> TT008CC001 }}<br>
                            Obs.: {{ $empresa -> TT010CC012 }}
                          </p>


                <hr>

                <!-- Controle -->
                <div class="row">
                    <!-- Voltar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::previous() }}">
                            <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- /Voltar -->

                    <!-- Novo -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.empresas.create' ) }}">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- /Novo -->

                    <!-- Editar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.empresas.edit', $empresa -> TT010CC000 )}}">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Editar -->

                    <!-- Deletar -->
                    <div class="col-12 col-sm-3">
                        {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.empresas.destroy', $empresa -> TT010CC000 ]]) }}
                        {{ Form::button
                            (
                                '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                        {{ Form::close() }}
                    </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
</div>

@stop
