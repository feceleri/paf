<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Cadastro de Bloqueios | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Cadastro de Bloqueios</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de erro -->

{{ Form::open(['route' => 'admin.bloqueios.store','method'=>'POST']) }}

<div class="card">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Bloqueios</span></div>
        </div>

        <div class="row">
            <!-- Bloqueio -->
            <div class="col-12">
                <div class="form-group @if ( $errors -> has( 'TT034CC001' )) has-danger @endif">
                    {{ Form::label( 'TT034CC003', 'Texto do aviso *', ['class' => 'form-control-label' ]) }}
                    {{ Form::text
                        (
                            'TT034CC003',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '500'
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT034CC003'))
                    <div class="form-control-feedback">
                        {{ $errors->first( 'TT034CC003' ) }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Bloqueio -->
        </div>

        <div class="row mb-1">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
            </div>
        </div>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>

            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-xs-1" href="{{ URL::Route( 'admin.bloqueios.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->

            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
</div>

{{ Form::close() }}

@stop
