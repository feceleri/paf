<!-- Bloqueios -->
<div class="card mb-3">
    <div class="card-block">
        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Bloqueios</span></div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th> Código </th>
                    <th> Tipo </th>
                <th> Texto aviso </th>
                     <th> Início </th>
                      <th> Final </th>
                    <th></th>
                </tr>
                @foreach ( $bloqueios as $bloqueio )
                <tr>
                    <td>{{ $bloqueio -> TT034CC000 }}</td>
                    <td>{{ $bloqueio -> TT034CC001 }}</td>
                    <td>{{ $bloqueio -> TT034CC002 }}</td>

                    <td>{{ date_format( date_create( $bloqueio -> TT034CC003 ),'d/m/Y - h:m' ) }}</td>
                    <td>{{ date_format( date_create( $bloqueio -> TT034CC004 ),'d/m/Y - h:m' ) }}</td>
                    <td>
                        <!-- Controle -->
                        <div class="row">
                            <!-- Editar -->
                            <div class="col-12 col-sm-6">
                                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.bloqueios.edit', $bloqueio -> idbloqueio )}}">
                                    <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                </a>
                            </div>
                            <!-- Editar -->

                            <!-- Deletar -->

                            <div class="col-12 col-sm-6">
                                {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.bloqueios.destroy', $bloqueio -> idbloqueio ]]) }}
                                {{ Form::button
                                    (
                                        '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>',

                                        [
                                            'class' => 'btn btn-block btn-admin',
                                            'type' => 'submit'
                                        ]
                                    )
                                }}
                                {{ Form::close() }}
                            </div>
                            <!-- Deletar -->
                        </div>
                        <!-- /Controle -->
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<!-- /Bloqueios -->

<!-- Paginação -->
{{ $bloqueios -> links( 'partials.pagination' ) }}
