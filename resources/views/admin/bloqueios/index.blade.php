<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Bloqueios Temporários | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Novo -->
        <div class="float-left">
            <a class="btn btn-admin" href="{{ URL::Route( 'admin.bloqueios.create' ) }}">
                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </div>
<!-- /Novo -->
<!-- Título -->
<h1 class="text-center">Bloqueios</h1>



<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de Alerta -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Mensagens de Alerta -->
<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">
        <div class="row">

        </div>
    </div>
</div>
<!-- Buscas -->
<!-- Bloqueios -->
<section class="load-ajax">
    @include('admin::bloqueios.load')
</section>
<!-- /Bloqueios -->

@stop
