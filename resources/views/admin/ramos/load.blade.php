<!-- Ramos -->
<div class="card mb-3">
    <div class="card-block">
        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto"> Ramos </span></div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th class="align-middle text-center"> Código </th>
                    <th class="align-middle text-center"> Ramo </th>
                    <th> </th>
                </tr>
                @foreach ( $ramos as $ramo )
                <tr>
                    <td class="align-middle text-center">{{ $ramo -> TT002CC000 }}</td>
                    <td class="align-middle text-center">{{ $ramo -> TT002CC001 }}</td>
                    <td class="align-middle text-center">
                        <!-- Controle -->
                        <div class="row">
                            <!-- Editar -->
                            <div class="col-12 col-sm-6">
                                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.ramos.edit', $ramo -> TT002CC000 )}}">
                                    <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                </a>
                            </div>
                            <!-- Editar -->

                            <!-- Deletar -->
                            <div class="col-12 col-sm-6">
                                {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.ramos.destroy', $ramo -> TT002CC000 ]]) }}
                                {{ Form::button
                                    (
                                        '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>',

                                        [
                                            'class' => 'btn btn-block btn-admin',
                                            'type' => 'submit'
                                        ]
                                    )
                                }}
                                {{ Form::close() }}
                            </div>
                            <!-- Deletar -->
                        </div>
                        <!-- /Controle -->
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<!-- /Ramos -->

<!-- Paginação -->
{{ $ramos -> links( 'partials.pagination' ) }}
