<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Prévia dos Concursos | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Prévia</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de Alerta -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Mensagens de Alerta -->

<div class="row">
    <div class="col-6">
        <div class="card my-std">
            <div class="card-block text-left">

                <!-- Ativar Concurso -->
                {{ Form::open([ 'method' => 'Post','route' => [ 'admin.concurso.active', $concurso -> TT015CC000 ]]) }}
                <label class="switch float-right">
                    {{ Form::checkbox('TT015CC010', $concurso -> TT015CC010, $concurso -> TT015CC010, ['onChange' => 'this.form.submit()']) }}
                    <div class="slider slider-BE round"></div>
                </label>
                {{ Form::close() }}
                <!-- /Ativar Concurso -->

                <!-- Title Concursos -->
                <div class="title clearfix">
                    <div class="title-form">
                        <span class="cor-empregos"> Prévia do Concurso </span>
                    </div>
                </div>
                <!-- /Title Concursos -->

                <h6 class="card-title text-uppercase">{{ $concurso -> TT015CC001 }}</h6>

                <p>
                    <h6 class="card-text">
                        <strong class="text-uppercase">Instituição</strong>
                    </h6>
                    {{ $concurso -> TT015CC003 }}  | {{ $concurso ->  TT015CC013 }} -  {{ $concurso ->  TT015CC012 }}
                </p>
                <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Organizadora</strong>
                    </h6>
                    {{ $concurso -> TT015CC004 }}
                </p>
                <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Site</strong>
                    </h6>
                    <a href="{{ $concurso -> TT015CC002 }}">
                        {{ $concurso -> TT015CC002 }}
                    </a>
                </p>
                <p>
                    Até {{ FormatDate($concurso -> TT015CC009) }}
                </p>
                <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Descritivo</strong>
                    </h6>
                    {!! $concurso -> TT015CC005 !!}
                </p>
                <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Requisitos</strong>
                    </h6>
                    {!! $concurso -> TT015CC006 !!}
                </p>
                <p>
                    <h6 class="card-text mb-0">
                        <strong class="text-uppercase">Informações Adicionais</strong>
                    </h6>
                    {!! $concurso -> TT015CC007 !!}
                </p>

                <hr>

                <!-- Controle -->
                <div class="row">
                    <!-- Voltar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.concursos.index' ) }}">
                            <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- /Voltar -->

                    <!-- Novo -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.concursos.create' ) }}">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- /Novo -->

                    <!-- Editar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.concursos.edit', $concurso -> TT015CC000 )}}">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Editar -->

                    <!-- Deletar -->
                    <div class="col-12 col-sm-3">
                        {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.concursos.destroy', $concurso -> TT015CC000 ]]) }}
                        {{ Form::button
                            (
                                '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                        {{ Form::close() }}
                    </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
</div>

@stop
