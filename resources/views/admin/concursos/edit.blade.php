<!-- layout Master -->
@extends('admin::app')

@section('script') <script asinc src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script> @stop

<!-- Título -->
@section('titulo') Edição de Concursos | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Edição de Concursos</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->

<!-- Mensagens de Erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de Erro -->

<div class="card my-std">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto">Dados do Concurso</span></div>
        </div>

        {{ Form::model( $concurso, [ 'method' => 'PATCH', 'route' => [ 'admin.concursos.update', $concurso -> TT015CC000 ]]) }}

        <div class="row">
            <!-- Título -->
            <div class="col-12 col-lg-4">
                <div class="form-group @if ( $errors -> has( 'TT015CC001' )) has-danger @endif">
                    {{ Form::label('TT015CC001', 'Título *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT015CC001',
                            null,

                            [
                                'maxlength' => '150',
                                'class' => 'form-control',
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT015CC001'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT015CC001') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Título -->

            <!-- Instituição -->
            <div class="col-12 col-lg-4">
                <div class="form-group @if ( $errors -> has( 'TT015CC003' )) has-danger @endif">
                    {{ Form::label('TT015CC003', 'Instituição *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT015CC003',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT015CC003'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT015CC003') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Instituição -->

                  <!-- UF -->
                    <div class="col-4 col-sm-3 col-md-2 col-lg-1">
                        <div class="form-group">

                           {{ Form::label('TT015CC012', 'UF *', [ 'class' => 'form-control-label' ])}}
                            {{ Form::select
                               (
                                   'TT015CC012',

                                    [

                                       "	AC	" => "	AC	" ,
                                        "	AL	" => "	AL	" ,
                                       "	AC	" => "	AC	" ,
                                       "	AP	" => "	AP	" ,
                                       "	AC	" => "	AC	" ,
                                       "	AM	" => "	AM	" ,
                                       "	AC	" => "	AC	" ,
                                       "	BA	" => "	BA	" ,
                                       "	AC	" => "	AC	" ,
                                       "	CE	" => "	CE	" ,
                                       "	AC	" => "	AC	" ,
                                       "	DF	" => "	DF	" ,
                                       "	AC	" => "	AC	" ,
                                       "	ES	" => "	ES	" ,
                                       "	AC	" => "	AC	" ,
                                       "	GO	" => "	GO	" ,
                                       "	AC	" => "	AC	" ,
                                       "	MA	" => "	MA	" ,
                                       "	AC	" => "	AC	" ,
                                       "	MT	" => "	MT	" ,
                                       "	AC	" => "	AC	" ,
                                       "	MS	" => "	MS	" ,
                                       "	AC	" => "	AC	" ,
                                       "	MG	" => "	MG	" ,
                                       "	AC	" => "	AC	" ,
                                       "	PA	" => "	PA	" ,
                                       "	AC	" => "	AC	" ,
                                       "	PB	" => "	PB	" ,
                                       "	AC	" => "	AC	" ,
                                       "	PR	" => "	PR	" ,
                                       "	AC	" => "	AC	" ,
                                       "	PE	" => "	PE	" ,
                                       "	AC	" => "	AC	" ,
                                       "	PI	" => "	PI	" ,
                                       "	AC	" => "	AC	" ,
                                       "	RJ	" => "	RJ	" ,
                                       "	AC	" => "	AC	" ,
                                       "	RN	" => "	RN	" ,
                                       "	AC	" => "	AC	" ,
                                       "	RS	" => "	RS	" ,
                                       "	AC	" => "	AC	" ,
                                       "	RO	" => "	RO	" ,
                                       "	AC	" => "	AC	" ,
                                       "	RR	" => "	RR	" ,
                                       "	AC	" => "	AC	" ,
                                       "	SC	" => "	SC	" ,
                                       "	AC	" => "	AC	" ,
                                       "	SP	" => "	SP	" ,
                                       "	AC	" => "	AC	" ,
                                       "	SE	" => "	SE	" ,
                                       "	AC	" => "	AC	" ,
                                       "	TO	" => "	TO	" ,

                                    ],
                                    null,

                                    [
                                        'class' => 'form-control',

                                        'default' => old('TT015CC012', 'SP')
                                    ]
                                )
                            }}



                              </div>

                               <!-- Erro -->@if ($errors->has('TT015CC012'))
                            <div class="form-control-feedback"> {{ $errors->first('TT015CC012') }} </div> @endif
                            <!-- /Erro -->
                    </div>
                    <!-- /UF -->


            <!-- Instituição -->
            <div class="col-12 col-lg-3">
                <div class="form-group @if ( $errors -> has( 'TT015CC003' )) has-danger @endif">
                    {{ Form::label('TT015CC003', 'Cidade da instituição *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT015CC013',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT015CC003'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT015CC003') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Instituição -->

        </div>

        <div class="row">
            <!-- Organizadora -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT015CC004' )) has-danger @endif">
                    {{ Form::label('TT015CC004', 'Organizadora', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT015CC004',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT015CC004'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT015CC004') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Organizadora -->

            <!-- Site -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT015CC002' )) has-danger @endif">
                    {{ Form::label('TT015CC002', 'Site *', ['class' => 'form-control-label']) }}
                    {{ Form::text
                        (
                            'TT015CC002',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT015CC002'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT015CC002') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Site -->
        </div>

        <div class="row">
            <!-- Estados -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'estados' )) has-danger @endif">
                    {{ Form::label('estados[]', 'Estados abrangentes', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'estados[]',

                            $estados,

                            $selectedEstados,

                            [
                                'size' => '5',
                                'name'=>'estados[]',
                                'multiple'=>'multiple',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('estados'))
                    <div class="form-control-feedback">
                        {{ $errors->first('estados') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Estados -->

            <!-- Cidades -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'cidades' )) has-danger @endif">
                    {{ Form::label('cidades[]', 'Cidades abrangentes', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'cidades[]',

                            $cidades,

                            $selectedCidades,

                            [
                                'size' => '5',
                                'name' => 'cidades[]',
                                'multiple' => 'multiple',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('cidades'))
                    <div class="form-control-feedback">
                        {{ $errors->first('cidades') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Cidades -->

            <!-- Abrangência nacional -->
            <div class="col-12">
                <label class="custom-control custom-checkbox">
                    <input name="TT015CC011" type="checkbox" class="custom-control-input" "@if ($concurso -> TT015CC011) checked @endif">
                    <span class="custom-control-indicator"></span>
                    Abrangência nacional
                </label>
            </div>
            <!-- /Abrangência nacional -->
        </div>

        <div class="row">
            <!-- Inicio das Inscrições -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT015CC008' )) has-danger @endif">
                    {{ Form::label('TT015CC008', 'Inicio das Inscrições *', ['class' => 'form-control-label']) }}
                    {{ Form::date
                        (
                            'TT015CC008',
                            null,

                            [
                                'max' => '2100-12-31',
                                'class' => 'form-control',
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT015CC008'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT015CC008') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- Inicio das Inscrições -->

            <!-- Fim das Inscrições -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT015CC009' )) has-danger @endif">
                    {{ Form::label('TT015CC009', 'Fim das Inscrições *', ['class' => 'form-control-label']) }}
                    {{ Form::date
                        (
                            'TT015CC009',
                            null,

                            [
                                'max' => '2100-12-31',
                                'class' => 'form-control',
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT015CC009'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT015CC009') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- Fim das Inscrições -->
        </div>

        <div class="row">
            <!-- Descrição -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT015CC005' )) has-danger @endif">
                    {{ Form::label('TT015CC005', 'Descrição *', ['class' => 'form-control-label']) }}
                    {{ Form::textArea
                        (
                            'TT015CC005',
                            null,

                            [
                                'rows' => '6',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT015CC005'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT015CC005') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Descrição -->

            <!-- Requisitos -->
            <div class="col-12 col-lg-6">
                <div class="form-group @if ( $errors -> has( 'TT015CC006' )) has-danger @endif">
                    {{ Form::label('TT015CC006', 'Requisitos *', ['class' => 'form-control-label']) }}
                    {{ Form::textArea
                        (
                            'TT015CC006',
                            null,

                            [
                                'rows' => '6',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT015CC006'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT015CC006') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Requisitos -->
        </div>

        <div class="row">
            <!-- Informações Adicionais -->
            <div class="col-12">
                <div class="form-group @if ( $errors -> has( 'TT015CC007' )) has-danger @endif">
                    {{ Form::label('TT015CC007', 'Informações Adicionais', ['class' => 'form-control-label', 'maxlength' => '255']) }}
                    {{ Form::textArea
                        (
                            'TT015CC007',
                            null,

                            [
                                'rows' => '6',
                                'class' => 'form-control'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT015CC007'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT015CC007') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- Informações Adicionais -->
        </div>

        <div class="row mb-1">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
            </div>
        </div>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>
            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.concursos.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->
            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
    {{ Form::close() }}
</div>
<script>
    CKEDITOR.replace( 'TT015CC005' );
    CKEDITOR.replace( 'TT015CC006' );
    CKEDITOR.replace( 'TT015CC007' );
</script>
@stop
