<!--<h1 class="card-title">{{ $concursos->currentPage() }}  </h1>-->
<!-- Concursos -->
<div class="row mt-2">
@if ( $message = Session::get( 'success' ))

@endif



@foreach( $concursos->sortByDesc('TT015CC009') as $concurso )
<!--Aqui vai capturar a URL e enviar para o input urlFiltroAtual para que seja enviado ao controler e trazido como link da página-->
<script type="text/javascript">
       var filtro = window.location.href;

       if (filtro.includes("instituicaoConcurso="))
       {
           var urlFiltro = window.location.href;
           urlFiltro = urlFiltro.replace("http://pafnovo.crfsp.org.br/admin/concursos?","");
           for(i=0;i<document.getElementsByName("urlFiltroAtual").length;i++){

               document.getElementsByName("urlFiltroAtual")[i].value = '&'+urlFiltro;
           }
       }
       else{
           document.getElementsByName("urlFiltroAtual").value = "";
       }
</script>
<!--FIM do script de capturar URL-->
    <div class="col-12 col-lg-4">
        <div class="card concurso my-std">
            <div class="card-block text-left">


                <!-- /Ativar Concurso -->

                         {{ Form::open([ 'method' => 'Post','route' => [ 'admin.concurso.active', $concurso -> TT015CC000 ]]) }}
                          <input type="hidden" class="form-control" name="paginacaoAtual" value="{{ $concursos->currentPage() }}" >
                         <input type="hidden" class="form-control" name="urlFiltroAtual" value="" >
                        <label class="switch float-right">
                         {{ Form::checkbox('TT015CC010', $concurso -> TT015CC010, $concurso -> TT015CC010, ['onChange' => 'this.form.submit()']) }}
                            <div class="slider slider-BE round"></div>
                        </label>
                        {{ Form::close() }}
                        <!-- Ativar Concurso -->
                <h5 class="card-title">{{ $concurso -> TT015CC001 }}</h5>
                <p class="card-text"> <b>{{ $concurso -> TT015CC003 }} | {{ $concurso ->  TT015CC013 }} -  {{ $concurso ->  TT015CC012 }} </b> </p>
                <p class="card-text"> Inscrições de {{ formatDate($concurso -> TT015CC008) }} a {{ formatDate($concurso -> TT015CC009) }}</p>

                <p class="card-text">{!! limitText( $concurso -> TT015CC005, 100 ) !!}</p>
                <p class="card-text"> Data de criação: {{ formatDate($concurso -> created_at) }} </p>
<!--
                <p class="card-text">
                    {{ formatDate( $concurso -> created_at ) }}
                </p>
-->

                <hr>

                <!-- Controle -->
                <div class="row">
                    <!-- Visualizar -->
                    <div class="col-12 col-sm-4">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.concursos.show', $concurso -> TT015CC000 )}}">
                            <i class="fa fa-expand fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Visualizar -->

                    <!-- Editar -->
                    <div class="col-12 col-sm-4">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.concursos.edit', $concurso -> TT015CC000 )}}">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Editar -->

                    <!-- Deletar -->
                    <div class="col-12 col-sm-4">
                        {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.concursos.destroy', $concurso -> TT015CC000 ]]) }}
                        {{ Form::button
                            (
                                '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                        {{ Form::close() }}
                    </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
@endforeach
</div>
<!-- /Concursos -->

<!-- Paginação -->
{{ $concursos -> links( 'partials.pagination' ) }}
