<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Prévia dos Artigos | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Prévia</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->

<div class="row">
    <div class="col-12">
        <div class="card my-std">
            <div class="card-block">

                <!-- Ativar Artigo -->
                {{ Form::open([ 'method' => 'Post','route' => [ 'admin.artigos.active', $artigo -> TT009CC000 ]]) }}
                <label class="switch float-right">
                    {{ Form::checkbox('TT009CC005', $artigo -> TT009CC005, $artigo -> TT009CC005, ['onChange' => 'this.form.submit()']) }}
                    <div class="slider slider-artigo round"></div>
                </label>
                {{ Form::close() }}
                <!-- /Ativar Artigo -->

                <!-- Title Artigos -->
                <div class="title clearfix">
                    <div class="title-form">
                        <span class="cor-artigos"> Prévia do Artigo </span>
                    </div>
                </div>
                <!-- /Title Artigos -->

                <h1 class="text-center">{{ $artigo -> TT009CC002 }}</h1>

                <figure class="figure artigo-img text-center">
                    {{ Html::image( $artigo -> TT009CC003, $artigo -> TT009CC002, array('class' => 'figure-img img-fluid img-thumbnail')) }}
                </figure>

                {!! $artigo -> TT009CC004 !!}

                <hr>

                <!-- Controle -->
                <div class="row">
                    <!-- Voltar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::previous() }}">
                            <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- /Voltar -->

                    <!-- Novo -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.artigos.create' ) }}">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- /Novo -->

                    <!-- Editar -->
                    <div class="col-12 col-sm-3">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.artigos.edit', $artigo -> TT009CC000 )}}">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                    <!-- Editar -->

                    <!-- Deletar -->
                    <div class="col-12 col-sm-3">
                        {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.artigos.destroy', $artigo -> TT009CC000 ]]) }}
                        {{ Form::button
                            (
                                '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>',

                                [
                                    'class' => 'btn btn-block btn-admin',
                                    'type' => 'submit'
                                ]
                            )
                        }}
                        {{ Form::close() }}
                    </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div>
</div>

@stop
