<!-- layout Master -->
@extends('admin::app')

<!-- Script -->
@section('script') <script asinc src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script> @stop

<!-- Título -->
@section('titulo') Edição de Artigos | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Título -->
<h2 class="m-3">Edição de Artigos</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->

<!-- Mensagens de Erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de Erro -->

<div class="card my-std">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto"> Artigo </span></div>
        </div>

        {{ Form::model( $artigo, [ 'method' => 'PATCH','route' => [ 'admin.artigos.update', $artigo -> TT009CC000], 'files' => true ]) }}

        <div class="row">
            <!-- Temas -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT009CC001' )) has-danger @endif">
                    {{ Form::label('TT009CC001', 'Tema *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT009CC001',
                            $temas,
                            null,

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT009CC001'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT009CC001') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
                 <!-- /Temas -->
                <!-- Título -->
                 <div class="form-group @if ( $errors -> has( 'TT009CC002' )) has-danger @endif">
                    {{ Form::label( 'TT009CC002', 'Título *', ['class' => 'form-control-label' ]) }}

                    {{ Form::text
                        (
                            'TT009CC002',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT009CC002'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT009CC002') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
                <!-- Título -->
                   <!-- Imagem -->

                <div class="form-group @if ( $errors -> has( 'TT009CC003' )) has-danger @endif">
                    {{ Form::label('TT009CC003', 'Imagem * (380px x 280 px)', ['class' => 'form-control-label']) }}
                    <div class="input-group">
                        <label class="input-group-btn mb-0">
                            <span class="btn btn-secondary">
                                Upload <input name="TT009CC003" type="file" style="display: none;">
                            </span>
                        </label>
                         {{ Form::text
                        ('TT009CC003', null,  ['readonly','class' => 'form-control']
                        )
                    }}
                    </div>

                    <!-- Erro -->
                    @if ( $errors -> has( 'TT009CC003' ))
                    <div class="form-control-feedback">
                        {{ $errors->first( 'TT009CC003' ) }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>

            <!-- /Imagem -->
            </div>

             <!-- /imagem jpg -->

            <div class="col-12 col-md-3">
                <figure class="figure mb-1"> {{ Html::image( $artigo -> TT009CC003, $artigo -> TT009CC002, array( 'class' => 'img-fluid' )) }} </figure>
            </div>
            <!-- /imagem jpg -->


        </div>

        <div class="row">
            <!-- Descritivo -->
            <div class="col-12">
                <div class="form-group @if ( $errors -> has( 'TT009CC004' )) has-danger @endif">
                    {{ Form::label('TT009CC004', 'Descritivo *', ['class' => 'form-control-label']) }}
                    {{ Form::textArea
                        (
                            'TT009CC004',
                            null,

                            [
                                'id' => 'TT009CC004',
                                'rows' => '20',
                                'class' => 'form-control',
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT009CC004'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC011') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Descritivo -->
        </div>

        <div class="row mb-1">
            <div class="col-12">
                <p class="card-text text-uppercase"><strong>( * ) Campos obrigatórios</strong></p>
            </div>
        </div>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>

            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ URL::Route( 'admin.artigos.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->

            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
    {{ Form::close() }}
</div>

<script>
    CKEDITOR.replace( 'TT009CC004' );
</script>

@stop
