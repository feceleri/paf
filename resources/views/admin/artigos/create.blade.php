<!-- layout Master -->
@extends('admin::app')

<!-- Script -->
@section('script') <script asinc src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script> @stop

<!-- Título -->
@section('titulo') Cadastro de Artigos | Admin @stop

<!-- Conteúdo -->
@section('conteudo')


<!-- Título -->
<h2 class="m-3">Cadastro dos Artigos</h2>
<!-- /Título -->

<!-- Mensagens de sucesso -->
@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Mensagens de erro -->
@if ( count( $errors ) > 0 )
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0"> Confira os campos em destaque </p>
</div>
@endif
<!-- /Mensagens de erro -->

<div class="card my-std">
    <div class="card-block">

        <div class="title clearfix">
            <div class="title-form"><span class="cor-texto"> Artigo </span></div>
        </div>

        {{ Form::open(['route' => 'admin.artigos.store', 'method' => 'POST', 'files' => true ]) }}

        <div class="row">
            <!-- Temas -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT009CC001' )) has-danger @endif">
                    {{ Form::label('TT009CC001', 'Tema *', ['class' => 'form-control-label']) }}
                    {{ Form::select
                        (
                            'TT009CC001',
                            $temas,
                            null,

                            [
                                'class' => 'form-control',
                                'placeholder' => 'Selecione...'
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT009CC001'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT009CC001') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Temas -->

            <!-- Título -->
            <div class="col-12 col-md-6">
                <div class="form-group @if ( $errors -> has( 'TT009CC002' )) has-danger @endif">
                    {{ Form::label( 'TT009CC002', 'Título *', ['class' => 'form-control-label' ]) }}

                    {{ Form::text
                        (
                            'TT009CC002',
                            null,

                            [
                                'class' => 'form-control',
                                'maxlength' => '150'
                            ]
                        )
                    }}

                    <!-- Erro -->
                    @if ($errors->has('TT009CC002'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT009CC002') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Título -->

            <!-- Imagem -->
            <div class="col-12">
                <div class="form-group @if ( $errors -> has( 'TT009CC003' )) has-danger @endif">
                    {{ Form::label('TT009CC003', 'Imagem * (380px x 280 px)', ['class' => 'form-control-label']) }}
                    <div class="input-group">
                        <label class="input-group-btn mb-0">
                            <span class="btn btn-secondary">
                                Upload <input name="TT009CC003" type="file" style="display: none;">
                            </span>
                        </label>
                        <input type="text" class="form-control" readonly>
                    </div>

                    <!-- Erro -->
                    @if ( $errors -> has( 'TT009CC003' ))
                    <div class="form-control-feedback">
                        {{ $errors->first( 'TT009CC003' ) }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Imagem -->
        </div>

        <div class="row">
            <!-- Descritivo -->
            <div class="col-12">
                <div class="form-group @if ( $errors -> has( 'TT009CC004' )) has-danger @endif">
                    {{ Form::label('TT009CC004', 'Descritivo *', ['class' => 'form-control-label']) }}
                    {{ Form::textArea
                        (
                            'TT009CC004',
                            null,

                            [
                                'id' => 'TT009CC004',
                                'rows' => '20',
                                'class' => 'form-control',
                            ]
                        )
                    }}
                    <!-- Erro -->
                    @if ($errors->has('TT009CC004'))
                    <div class="form-control-feedback">
                        {{ $errors->first('TT014CC011') }}
                    </div>
                    @endif
                    <!-- /Erro -->
                </div>
            </div>
            <!-- /Descritivo -->
        </div>

        <!-- Controle -->
        <div class="row">
            <div class="col-12 col-sm-3"></div>

            <!-- Voltar -->
            <div class="col-12 col-sm-3">
                <a class="btn btn-block btn-admin mb-1" href="{{ URL::Route( 'admin.artigos.index' ) }}">
                    <i class="fa fa-arrow-left fa-2x" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /Voltar -->

            <!-- Confirmar -->
            <div class="col-12 col-sm-3">
                {{ Form::button
                   (
                        '<i class="fa fa-check fa-2x" aria-hidden="true"></i>',

                        [
                            'class' => 'btn btn-block btn-admin',
                            'type' => 'submit'
                        ]
                    )
                }}
            </div>
            <!-- /Confirmar -->

            <div class="col-12 col-sm-3"></div>
        </div>
        <!-- /Controle -->
    </div>
    {{ Form::close() }}
</div>

<script>
    CKEDITOR.replace( 'TT009CC004' );
</script>

@stop
