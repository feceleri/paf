<!-- Artigos -->
<div class="row"> @foreach( $artigos as $artigo )
   <!--Aqui vai capturar a URL e enviar para o input urlFiltroAtual para que seja enviado ao controler e trazido como link da página-->
<script type="text/javascript">
       var filtro = window.location.href;

       if (filtro.includes("temaArtigo="))
       {
           var urlFiltro = window.location.href;
           urlFiltro = urlFiltro.replace("http://pafnovo.crfsp.org.br/admin/artigos?","");
           for(i=0;i<document.getElementsByName("urlFiltroAtual").length;i++){

               document.getElementsByName("urlFiltroAtual")[i].value = '&'+urlFiltro;
           }
       }
       else{
           document.getElementsByName("urlFiltroAtual").value = "";
       }
</script>
<!--FIM do script de capturar URL-->
    <div class="col-4">
        <div class="card my-std">
            <div class="card-block text-left">


                <div class="row">

                    <div class="col-12 col-md-4">
                      <!-- Ativar Artigo -->
                       {{ Form::open([ 'method' => 'Post','route' => [ 'admin.artigos.active', $artigo -> TT009CC000 ]]) }}
                        <input type="hidden" class="form-control" name="paginacaoAtual" value="{{ $artigos->currentPage() }}" >
                         <input type="hidden" class="form-control" name="urlFiltroAtual" value="" >
                        <label class="switch float-left"> {{ Form::checkbox('TT009CC005', $artigo -> TT009CC005, $artigo -> TT009CC005, ['onChange' => 'this.form.submit()']) }}
                            <div class="slider slider-artigo round"></div>
                        </label> {{ Form::close() }}
                        <!-- Ativar Artigo -->
                        <br>
                        <figure class="figure mb-1"> {{ Html::image( $artigo -> TT009CC003, $artigo -> TT009CC002, array( 'class' => 'img-fluid' )) }} </figure>
                    </div>
                    <div class="col-12 col-md-8">
                        <h5 class="media-heading text-md-left">
                            {{ $artigo -> TT009CC002 }}
                        </h5>
<!--                         {!! limitText( $artigo -> TT009CC004, 120 ) !!} -->
                         </div>
                </div>
                <hr>
                <!-- Controle -->
                <div class="row">
                    <!-- Visualizar -->
                    <div class="col-12 col-sm-4">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.artigos.show', $artigo -> TT009CC000 )}}"> <i class="fa fa-expand fa-2x" aria-hidden="true"></i> </a>
                    </div>
                    <!-- Visualizar -->
                    <!-- Editar -->
                    <div class="col-12 col-sm-4">
                        <a class="btn btn-block btn-admin mb-2 mb-md-0" href="{{ route( 'admin.artigos.edit', $artigo -> TT009CC000 )}}"> <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i> </a>
                    </div>
                    <!-- Editar -->
                    <!-- Deletar -->
                    <div class="col-12 col-sm-4"> {{ Form::open([ 'method' => 'Delete','route' => [ 'admin.artigos.destroy', $artigo -> TT009CC000 ]]) }} {{ Form::button ( '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>', [ 'class' => 'btn btn-block btn-admin', 'type' => 'submit' ] ) }} {{ Form::close() }} </div>
                    <!-- Deletar -->
                </div>
                <!-- /Controle -->
            </div>
        </div>
    </div> @endforeach </div>
<!-- /Artigos -->
<!-- Paginação -->{{ $artigos -> links( 'partials.pagination' ) }}
