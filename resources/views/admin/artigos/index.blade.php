<!-- layout Master -->
@extends('admin::app')

<!-- Título -->
@section('titulo') Artigos | Admin @stop

<!-- Conteúdo -->
@section('conteudo')

<!-- Novo -->
        <div class="float-left">
            <a class="btn btn-admin" href="{{ URL::Route( 'admin.artigos.create' ) }}">
                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
            </a>
        </div>
 <!-- /Novo -->

<!-- Título -->
<h1 class="text-center">Artigos</h1>



<!-- Mensagens de sucesso -->
@if ( $message = Session::get( 'success' ))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">
        {{ $message }}
    </p>
</div>
@endif
<!-- /Mensagens de sucesso -->

<!-- Alertas -->
@if ( $message = Session::get( 'alert' ))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <p class="mb-0">{{ $message }}</p>
</div>
@endif
<!-- /Alertas -->



<!-- Buscas -->
<div class="card mb-3">
    <div class="card-block">

              <div class="row">
            <!-- Tipo de Vaga -->
            <div class="col-12 col-md-12">




{!! Form::open(['method'=>'GET','url'=>'admin/artigos','class'=>'filter-form navbar-left','role'=>'search'])  !!}

<div class="col-12 col-md-12">
<div class="row">
      <div class="col-12 col-md-2"> Tema
       <select class="form-control" name="temaArtigo">
               <option value="">Todos</option>
           @foreach( $temas as $tema )
                <option value="{{ $tema -> TT004CC000 }}"  @if ($tema -> TT004CC000 == $temaArtigo) selected @endif>{{ $tema -> TT004CC002 }}  </option>
           @endforeach
       </select>
    </div>
    <div class="col-12 col-md-4">Título <input type="text" class="form-control" name="tituloArtigo" value="{{$tituloArtigo}}" placeholder="..."></div>
    <div class="col-12 col-md-2 ">Data de criação<input type="date" class="form-control" name="dataArtigo" value="{{$dataArtigo}}" placeholder="..."></div>


   <div class="col-12 col-md-1"><br>
   <span class="input-group-btn ">
        <button class="btn btn-block btn-admin mb-1" type="submit">
            <i class="fa fa-search fa-2x"></i>
        </button>
    </span>
    </div>
    <div class="col-12 col-md-1"><br>
        <a href="/admin/artigos"> <i class="btn btn-block btn-admin mb-1 fa fa-eraser fa-2x"></i></a>
    </div>
</div>
{!! Form::close() !!}
  </div>

            </div>
        </div>
    </div>
</div>

  <!-- Buscas -->


<!-- Artigos -->
<section class="load-ajax">
    @include('admin::artigos.load')
</section>
<!-- /Artigos -->

@stop
